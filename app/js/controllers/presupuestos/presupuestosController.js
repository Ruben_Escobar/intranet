/****************************************************************************/
/*							Controlador de Presupuestos						*/
/****************************************************************************/
App.controller('PresupuestosCntrl', PresupuestosCntrl);
function PresupuestosCntrl($scope, $rootScope, $timeout, $modal, uiGridConstants, ngDialog, focus, printer, dataService){
	
	$scope.buffer = true;
	$scope.progressValue = 0;
	$scope.progressUpload = null;
	$scope.mostrar = false;
	$scope.editar = false;
	$scope.cliente = undefined;
	$scope.editando = false;
	$scope.copiando = false;
	$scope.aceptado = null;
	$scope.uploading = false;
	$scope.errorPresupuestos = null;
	$scope.errorFormPresupuestos = null;
	$scope.nuevoCliente = false;
	  
	var registros = [];
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PresupuestosCntrl ------------------------------');
		
	$scope.columnas = [ 
		{name: 'editar', width: 70, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.copiarPresupuesto(row.entity)" title="Copiar" class="btn btn-sm btn-success"><em class="fa fa-copy"></em></button><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Editar" class="btn btn-sm btn-info ml-xs"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false},
		    
		{name: 'referencia', field: 'referencia', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 145},
		{name: 'financiado', field: 'financiado', displayName: '¿Financiado?', enableSorting: false, minWidth: 105, enableHiding: false, enablePinning:false, headerCellClass : "hidden-xs", cellClass: "hidden-xs"},
		{name: 'fecha', field: 'fecha', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 90},
		{name: 'base', field: 'base', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 100, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.base}} €</div>', headerCellClass : "hidden-xs", cellClass: "hidden-xs"},
		{name: 'total', field: 'total', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 100, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.total}} €</div>', headerCellClass : "hidden-xs", cellClass: "hidden-xs"}
	];
	
	$scope.columnas2 = [
		{name: 'eliminar', width: 35, cellTemplate:'<div class="buttons" data-ng-if="grid.appScope.presupuesto.borrador""><button type="button" data-ng-click="grid.appScope.quitarProducto(row.entity)" title="Edit" class="btn btn-sm btn-danger"><em class="fa fa-trash"></em></button></div>', pinnedRight: true, enableHiding: false, enableColumnResizing: false, displayName: ''},
		{name: 'nombre', field: 'nombre', enableHiding: false, minWidth: 90},		
		{name: 'descripcion', field: 'descripcion', displayName: 'Descripción', enableHiding: false, minWidth: 150, headerCellClass : "hidden-xs", cellClass: "cellToolTip hidden-xs", cellTemplate: '<div class="celdaToolTip ui-grid-cell-contents" data-ng-class="{'+'deshabilitar'+':!grid.appScope.presupuesto.borrador}">'
			+'<input type="text" name="descripcion" placeholder="Descripción del producto" data-ng-model="grid.appScope.descripcion" required="" class="floatLeft inputTabla" data-ng-show="row.entity.visibleD"/>'		
			+'<div class="floatLeft ml-xs mr-xs">'
				+'<span data-ng-click="grid.appScope.guardarDescProd(row.entity)" class="fa fa-check mr-sm" data-ng-show="row.entity.visibleD"></span>'
				+'<span data-ng-click="grid.appScope.cancelarDescProd(row.entity)" class="fa fa-close mr-sm" data-ng-show="row.entity.visibleD"></span>'
			+'</div>'
			+'<p tooltip={{row.entity.descripcion}} tooltip-placement="top">'		
				+'<span data-ng-class="{'+'deshabilitar'+': grid.appScope.editando}" data-ng-click="grid.appScope.editarDesc(row.entity)" data-ng-hide="row.entity.visibleD">{{row.entity.descripcion}}</span>'
				+'</p>'
		+'</div>'},		
		{name: 'cantidad', field: 'cantidad', enableHiding: false, minWidth: 90, cellTemplate: '<div class="ui-grid-cell-contents" data-ng-class="{'+'deshabilitar'+':!grid.appScope.presupuesto.borrador}">'
			+'<input type="number" min="1" name="cantidad" placeholder="Cantidad del producto" data-ng-model="grid.appScope.cantidad" required="" class="floatLeft inputTablaC" data-ng-show="row.entity.visibleC"/>'
				
			+'<div class="floatLeft ml-xs mr-xs">'
				+'<span data-ng-click="grid.appScope.guardarCantProd(row.entity)" class="fa fa-check mr-sm" data-ng-show="row.entity.visibleC"></span>'
				+'<span data-ng-click="grid.appScope.cancelarCantProd(row.entity)" class="fa fa-close mr-sm" data-ng-show="row.entity.visibleC"></span>'
			+'</div>'
				
			+'<span data-ng-class="{'+'deshabilitar'+': grid.appScope.editando}" data-ng-click="grid.appScope.editarCantidad(row.entity)" data-ng-hide="row.entity.visibleC">{{row.entity.cantidad}}</span>'
		+'</div>'},
		
		{name: 'pvp', field: 'pvp', displayName: 'Precio', enableHiding: false, minWidth: 150, cellTemplate:'<div class="ui-grid-cell-contents" data-ng-class="{'+'deshabilitar'+':!grid.appScope.presupuesto.borrador}">'
			+'<input type="number" min="1" name="pvp" placeholder="Pvp del producto" data-ng-model="grid.appScope.pvp" required="" class="floatLeft inputTabla" data-ng-show="row.entity.visible"/>'
				
			+'<div class="floatLeft ml-xs mr-xs">'
				+'<span data-ng-click="grid.appScope.guardarPrecioProd(row.entity)" class="fa fa-check mr-sm" data-ng-show="row.entity.visible"></span>'
				+'<span data-ng-click="grid.appScope.cancelarPrecioProd(row.entity)" class="fa fa-close mr-sm" data-ng-show="row.entity.visible"></span>'
			+'</div>'
				
			+'<span data-ng-class="{'+'deshabilitar'+': grid.appScope.editando}" data-ng-click="grid.appScope.editarPrecio(row.entity)" data-ng-hide="row.entity.visible">{{row.entity.pvp}} €</span>'
		+'</div>'},
	
		{name: 'total', field: 'total', enableHiding: false, minWidth: 80, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.total}} €</div>'}
	];
	
	$scope.columnas3 = [	  
		{name: 'referencia', field: 'referencia', minWidth: 140},
		{name: 'nombre', field: 'nombre', enableHiding: false, minWidth: 150},
		{name: 'descripcion', field: 'descripcion', displayName: 'Descripción', minWidth: 100},		
		{name: 'coste', field: 'coste', enableHiding: false, minWidth: 60, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.coste}} €</div>'}
	];
	
	//Inicializamos la tabla.
	$scope.gridPresupuestos = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};	
	$scope.gridProductosPresupuesto = {
		paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: false,
		enableSorting: false,
		enableHiding: false,
		enablePinning:false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas2,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};	
	$scope.gridProductos = {
		paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas3,
		onRegisterApi: function(gridApi) {
		   $scope.gridApiProductos = gridApi;
	    }
	};

	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridPresupuestos.paginationPageSize*36)+100) > (($scope.gridPresupuestos.data.length*36)+100) ){
			return ($scope.gridPresupuestos.data.length*36)+100;
		}else{
			return ($scope.gridPresupuestos.paginationPageSize*36)+100;
		}
	}
	$scope.comprobarTamano2 = function(){
		if( (($scope.gridProductosPresupuesto.paginationPageSize*36)+65) > (($scope.gridProductosPresupuesto.data.length*36)+65) ){
			return ($scope.gridProductosPresupuesto.data.length*36)+65;
		}else{
			return ($scope.gridProductosPresupuesto.paginationPageSize*36)+65;
		}
	}
	$scope.comprobarTamano3 = function(){
		if( (($scope.gridProductos.paginationPageSize*36)+120) > (($scope.gridProductos.data.length*36)+120) ){
			return ($scope.gridProductos.data.length*36)+120;
		}else{
			return ($scope.gridProductos.paginationPageSize*36)+120;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (presupuestos PresupuestosCntrl): ', data.registros.length);
					$scope.gridPresupuestos.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue =  data.progressValue;
					console.log('Cargando (presupuestos PresupuestosCntrl): '+$scope.progressValue+'%.');
					$scope.gridPresupuestos.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
			});
	}
	recuperarRegistros('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
	
	var traerProductos = function(){
		dataService
			.getAll('catalogo?filter=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				console.log('Total de productos (PresupuestosCntrl): ', data.registros.length);
				$scope.gridProductos.data = angular.copy(data.registros);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
			});
	}
	traerProductos();
	
	//Función que trae los valores de configuración.
	var traerConfig = function(){
		dataService
			.getAll('configuracion?filter=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				for( var i=0; i<data.registros.length; i++){
					if(data.registros[i].parametro == 'impuesto'){
						$scope.impuesto = parseFloat(data.registros[i].valor);
					}
					if(data.registros[i].parametro == 'condiciones'){
						$scope.condiciones = data.registros[i].valor;
					}
				}
				console.log('Impuesto (%): ', $scope.impuesto);
				console.log('Términos y condiciones: '+$scope.condiciones);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
			});
	}
	traerConfig();
	
	//Función que trae los clientes.
	var traerClientes = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.buffer2 = true;
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					$scope.clientes = angular.copy(data.registros);
					console.log('Clientes recibidos: ', data.registros.length);
					$scope.buffer2 = false;
					$scope.progressValue2 = 0;
				}else{
					$scope.progressValue2 = data.progressValue;
					console.log('Cargando (traerClientes PresupuestosCntrl): '+$scope.progressValue+'%.');
					$scope.clientes = angular.copy(data.registros);
					traerClientes(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros)
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
			});
	}
	traerClientes('cliente?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los productos del presupuesto solicitado.
	var traerProdPre = function(id){
		dataService
			.getFiltro('productos_presupuesto?filter=id%3E', 0,'%26%26id_presupuesto%3D'+id, 0, 0, 0, [])
			.then(function(data){
				$scope.gridProductosPresupuesto.data = angular.copy(data.registros);
				for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
					$scope.gridProductosPresupuesto.data[i].visible = false;
					$scope.gridProductosPresupuesto.data[i].visibleC = false;
					$scope.gridProductosPresupuesto.data[i].visibleD = false;
				}
				console.log('Productos del presupuesto '+id+' recibidos: ', data.registros.length);
				$scope.calcularTotal();
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
			});
	}
	
	//Función que modifica un producto en concreto.
	var modificarProducto = function(producto){
		dataService
			.putElements('productos_presupuesto', producto)
			.then(function(data){
				console.log('Producto del presupuesto '+$scope.presupuesto.referencia+' modificado: ', data.id);
				traerProdPre($scope.presupuesto.id);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
			});
	}
	
	//Función que inicializa el formulario.
	$scope.mostrarForm = function(){
		traerClientes('cliente?filter=id%3E', 0, 0, 0, 0, []);
		var hoy = new Date();
		$scope.presupuesto = {};
		$scope.presupuesto.referencia = 'PR'+crearReferencia();
		$scope.presupuesto.condiciones = angular.copy($scope.condiciones);
		$scope.presupuesto.fecha = hoy.getDate()+'-'+(hoy.getMonth()+1)+'-'+hoy.getFullYear();
		$scope.presupuesto.borrador = 1;
		$scope.presupuesto.aceptado = 0;
		//Hacemos la llamada SQL para crear el presupuesto.
		dataService
			.postElements('presupuesto', $scope.presupuesto)
			.then(function(data){
				console.log('Presupuesto insertado (PresupuestosCntrl): ', data.id);
				$scope.presupuesto.id = data.id;
				$scope.buffer = true;
				recuperarRegistros('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
			});
		$scope.titulo = 'NUEVO PRESUPUESTO';
		$scope.boton = 'Añadir';
		$scope.clienteSel = 'Seleccione un Cliente';
		$scope.mostrar = true;
		focus('mainFocus');
	}
	
	//Función que inicializa el formulario en forma de edición.
	$scope.editar = function(presupuesto){
		$scope.gridProductosPresupuesto.data = [];
		$scope.presupuesto = {};
		$scope.presupuesto = angular.copy(presupuesto);
		$scope.mostrar = true;
		$scope.errorFinanciero = null;
		$scope.clienteSel = 'Seleccione un Cliente';
		for(var i=0; i<$scope.clientes.length; i++){
			if($scope.clientes[i].id == $scope.presupuesto.id_cliente){
				$scope.cliente = $scope.clientes[i];
			}
		}
		traerProdPre($scope.presupuesto.id);
		$scope.titulo = 'PRESUPUESTO';
		$scope.boton = 'Modificar';
		focus('mainFocus');
	}
	
	//Función que inicia el formulario en forma de copia de otro formulario.
	$scope.copiarPresupuesto = function(presupuesto){
		$scope.copiando = true;
		$scope.gridProductosPresupuesto.data = [];
		var hoy = new Date();
		$scope.presupuesto = {};
		$scope.presupuesto.referencia = 'PR'+crearReferencia();
		$scope.presupuesto.condiciones = presupuesto.condiciones;
		$scope.presupuesto.fecha = hoy.getDate()+'-'+(hoy.getMonth()+1)+'-'+hoy.getFullYear();
		$scope.presupuesto.borrador = true;
		//Hacemos la llamada SQL para crear el presupuesto.
		dataService
			.postElements('presupuesto', $scope.presupuesto)
			.then(function(data){
				console.log('Presupuesto insertado (PresupuestosCntrl): ', data.id);
				$scope.presupuesto.id = data.id;
				$scope.buffer = true;
				recuperarRegistros('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
				var productosAux = [];
				dataService
					.getFiltro('productos_presupuesto?filter=id%3E', 0, '%26%26id_presupuesto%3D'+presupuesto.id, 0, 0, 0, [])
					.then(function(data){
						productosAux = angular.copy(data.registros);
						for(var i=0; i<productosAux.length; i++){
							productosAux[i].id_presupuesto = $scope.presupuesto.id;
						}
						console.log('Productos para la copia recibidos: ', data.registros.length);
						if(productosAux.length != 0){
							dataService
								.postElements('productos_presupuesto', productosAux)
								.then(function(data){
									dataService
										.getFiltro('productos_presupuesto?filter=id%3E', 0, '%26%26id_presupuesto%3D'+presupuesto.id, 0, 0, 0, [])
										.then(function(data){
											$scope.gridProductosPresupuesto.data = angular.copy(data.registros);
											for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
												$scope.gridProductosPresupuesto.data[i].visible = false;
												$scope.gridProductosPresupuesto.data[i].visibleC = false;
												$scope.gridProductosPresupuesto.data[i].visibleD = false;
												$scope.gridProductosPresupuesto.data[i].id = $scope.presupuesto.id;
											}
											$scope.calcularTotal();
											$scope.copiando = false;
											$scope.titulo = 'NUEVO PRESUPUESTO';
											$scope.boton = 'Añadir';
											$scope.clienteSel = 'Seleccione un Cliente';
											$scope.mostrar = true;
											focus('mainFocus');
										})										
										.catch(function(error){
											if(error.status==401){$rootScope.relogin();}
											$scope.errorPresupuestos = parseError(error.status, error.data);
											$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
										});
								})
								.catch(function(error){
									if(error.status==401){$rootScope.relogin();}
									$scope.errorPresupuestos = parseError(error.status, error.data);
									$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
								});
						}else{
							$scope.copiando = false;
							$scope.titulo = 'NUEVO PRESUPUESTO';
							$scope.boton = 'Añadir';
							$scope.clienteSel = 'Seleccione un Cliente';
							$scope.mostrar = true;
							focus('mainFocus');
						}
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorPresupuestos = parseError(error.status, error.data);
						$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
					});
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
			});
	}
	
	//Función que trae los valores de las posibles financiaciones.
	$scope.valoresFinanciacion = function(){		
		if($scope.presupuesto.base != undefined && $scope.presupuesto.base != ''){
			$scope.valoresFinancieros = [];
			dataService
				.getFiltro('financiacion?filter=id%3E', 0, '%26%26capital%3E%3D'+$scope.presupuesto.base+'&order%3Dcapital', 0, 0, 0, [])
				.then(function(data){
					if(data.registros.length == 0){
						$scope.errorFinanciero = 'No se puede realizar el cálculo de la financiación para el capital solicitado. Consulte con un superior.';
						$scope.presupuesto.cuota = 0;
					}else{
						$scope.presupuesto.cuota = 1;
						$scope.errorFinanciero = null;
						//Creamos la variable techo para quedarnos solo con las tuplas del capital superior más próximo.
						var techo = data.registros[0].capital;
						for(var i=0; i<data.registros.length; i++){
							if(data.registros[i].capital == techo){
								$scope.valoresFinancieros.push(data.registros[i]);
							}
						}
						//Creamos el nombre que mostraremos en el select.
						for(var i=0; i<$scope.valoresFinancieros.length; i++){
							$scope.valoresFinancieros[i].nombre = $scope.valoresFinancieros[i].nombre+' - '+$scope.valoresFinancieros[i].plazo;
						}
						//Comprobamos si el presupuesto tiene asignado algún plazo.
						for(var i=0; i<$scope.valoresFinancieros.length; i++){
							if(($scope.valoresFinancieros[i].plazo == $scope.presupuesto.plazo) && ($scope.valoresFinancieros[i].coeficiente == $scope.presupuesto.coeficiente)){
								$scope.valores = $scope.valoresFinancieros[i];
							}
						}
						console.log('Valores financieros recuperados correctamente. categoría: '+$scope.valoresFinancieros[0].capital);
					}
					if($scope.errorFinanciero == null){
						$scope.calcularFinanciacion($scope.presupuesto);
					}
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormPresupuestos = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
				});
		}else{
			$scope.presupuesto.financiado = false;
			$scope.errorFormPresupuestos = 'El presupuesto no contiene elementos.';
			$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
		}
	}
	
	//Función que calcula la financiación según el plazo seleccionado.
	$scope.calcularFinanciacion = function(valor){
		$scope.presupuesto.plazo = valor.plazo;
		$scope.presupuesto.coeficiente = valor.coeficiente;
		$scope.presupuesto.cuota = $scope.presupuesto.base * $scope.presupuesto.coeficiente;
		$scope.presupuesto.cuota = Math.round(parseFloat($scope.presupuesto.cuota) *100)/100;
		dataService
			.putElements('presupuesto', $scope.presupuesto)
			.then(function(data){
				console.log('Presupuesto modificado (valores financiación): ', data.id);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
			});
	}
	
	//Función que resetea el formulario.
	$scope.resetFormPresupuestos = function(){
		$scope.presupuesto = {};
		$scope.mostrar = false;
		$scope.cliente = undefined;
		$scope.editando = false;
		$scope.errorFinanciero = null;
		$scope.gridProductosPresupuesto.data = [];
	}
	
	//Función que da la oportunidad de cambiar el cliente del presupuesto.
	$scope.quitarCliente = function(){
		$scope.cliente = undefined;
		$scope.presupuesto.id_cliente = undefined;
		$scope.clienteSel = 'Seleccione un Cliente';
	}
	
	//Función que muestra el listado de productos para añadir al presupuesto.
	$scope.addProducto = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogProductos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function () {
			var data = $scope.gridApiProductos.selection.getSelectedRows()
			if(data.length > 0){
				var aux = {};
				var productos = [];
				for(var i=0; i<data.length; i++){
					data[i].id = undefined;
					data[i].cantidad = 1;
					data[i].total = data[i].pvp;
					data[i].id_presupuesto = $scope.presupuesto.id;
					aux = angular.copy(data[i]);
					productos.push(aux);
					aux = {};
				}
				dataService
					.postElements('productos_presupuesto', productos)
					.then(function(data){
						console.log('Productos del presupuesto '+$scope.presupuesto.referencia+' insertados: ', data.record.length);
						traerProdPre($scope.presupuesto.id);
						$scope.calcularFinanciacion($scope.presupuesto);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormPresupuestos = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
					});
			}
	    }, function (reason) {
	    	console.log('Modal promise rejected. Reason: ', reason);
	    });
	}
	
	//Función que quita un producto del presupuesto.
	$scope.quitarProducto = function(producto){
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			if($scope.gridProductosPresupuesto.data[i].id == producto.id){
				dataService
					.deleteElements('productos_presupuesto', '?filter=id%3D'+producto.id)
					.then(function(data){
						console.log('Producto del presupuesto '+$scope.presupuesto.referencia+' eliminado: ', data.id);
						traerProdPre($scope.presupuesto.id);
						$scope.calcularFinanciacion($scope.presupuesto);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormPresupuestos = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
					});
				break;
			}
		}
	}
	
	/*------------------------- Sección de edición de los productos del presupuesto -------------------------*/
	
	//Función que calcula el total del presupuesto.
	$scope.calcularTotal = function(){
		//Calculamos la base del presupuesto.
		$scope.presupuesto.base = 0;
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			$scope.presupuesto.base = $scope.presupuesto.base + $scope.gridProductosPresupuesto.data[i].total;
			$scope.presupuesto.base = Math.round(parseFloat($scope.presupuesto.base) *100)/100;
		}
		if($scope.presupuesto.descuento){
			$scope.presupuesto.subTotal = angular.copy($scope.presupuesto.base);
			$scope.presupuesto.base = $scope.presupuesto.base - $scope.presupuesto.precioDescuento;
		}
		//Calculamos el valor del total del presupuestosegún el impuesto de la tabla de configuraciones.
		$scope.presupuesto.total = $scope.presupuesto.base * (1+($scope.impuesto / 100) );
		//Redondeamos a 2 cifras
		$scope.presupuesto.total = Math.round(parseFloat($scope.presupuesto.total) *100)/100;
		//Calculamos el iva.
		$scope.iva = ($scope.presupuesto.base*$scope.impuesto)/100;
		$scope.iva = Math.round(parseFloat($scope.iva)*100)/100;
		
		if($scope.presupuesto.financiado){
			$scope.valoresFinanciacion();
		}
	}
	
	//Función que muestra la parte de edición del precio del producto.
	$scope.editarPrecio = function(producto){
		$scope.editando = true;
		$scope.pvp = producto.pvp;
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			if($scope.gridProductosPresupuesto.data[i].id == producto.id){
				$scope.gridProductosPresupuesto.data[i].visible = true;		
				break;
			}
		}
	}
	
	//Función que guarda los datos del producto a editar.
	$scope.guardarPrecioProd = function(producto){
		if($scope.pvp > 0){
			$scope.editando = false;
			producto.pvp = $scope.pvp;
			producto.total = producto.cantidad * producto.pvp;
			producto.total = Math.round(parseFloat(producto.total) *100)/100;
			modificarProducto(producto);
			$scope.pvp = undefined;
		}
	}
	
	//Función que cancela la edición del precio del producto.
	$scope.cancelarPrecioProd = function(producto){
		$scope.editando = false;
		$scope.pvp = undefined;
		
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			if($scope.gridProductosPresupuesto.data[i].id == producto.id){
				$scope.gridProductosPresupuesto.data[i].visible = false;
				break;
			}
		}
	}
	
	//Función que muestra la parte de edición de la cantidad del producto.
	$scope.editarCantidad = function(producto){
		$scope.editando = true;
		$scope.cantidad = producto.cantidad;
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			if($scope.gridProductosPresupuesto.data[i].id == producto.id){
				$scope.gridProductosPresupuesto.data[i].visibleC = true;		
				break;
			}
		}
	}
	
	//Función que guarda los datos del producto a editar.
	$scope.guardarCantProd = function(producto){
		if($scope.cantidad > 0){
			$scope.editando = false;
			producto.cantidad = $scope.cantidad;
			producto.total = producto.cantidad * producto.pvp;
			producto.total = Math.round(parseFloat(producto.total) *100)/100;
			modificarProducto(producto);
			$scope.cantidad = undefined;
		}
	}
	
	//Función que cancela la edición de la cantidad del producto.
	$scope.cancelarCantProd = function(producto){
		$scope.editando = false;
		$scope.cantidad = undefined;
		
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			if($scope.gridProductosPresupuesto.data[i].id == producto.id){
				$scope.gridProductosPresupuesto.data[i].visibleC = false;
				break;
			}
		}
	}
	
	//Función que muestra la parte de edición de la descripción del producto.
	$scope.editarDesc = function(producto){
		$scope.editando = true;
		$scope.descripcion = producto.descripcion;
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			if($scope.gridProductosPresupuesto.data[i].id == producto.id){
				$scope.gridProductosPresupuesto.data[i].visibleD = true;		
				break;
			}
		}
	}
	
	//Función que guarda los datos del producto a editar.
	$scope.guardarDescProd = function(producto){
		if($scope.descripcion != undefined){
			$scope.editando = false;
			producto.descripcion = $scope.descripcion;
			$scope.descripcion = undefined;
			modificarProducto(producto);
		}
	}
	
	//Función que cancela la edición de la cantidad del producto.
	$scope.cancelarDescProd = function(producto){
		$scope.editando = false;
		$scope.descripcion = undefined;
		
		for(var i=0; i<$scope.gridProductosPresupuesto.data.length; i++){
			if($scope.gridProductosPresupuesto.data[i].id == producto.id){
				$scope.gridProductosPresupuesto.data[i].visibleD = false;
				break;
			}
		}
	}
	
	/*--------------------------------------------------------------------------------------------------------*/
	
	//Función que ejecuta el mensaje modal.
	var ModalInstanceCtrl = function($scope, $modalInstance) {
		$scope.ok = function () {
			$modalInstance.close('closed');
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	};
	ModalInstanceCtrl.$inject = ["$scope", "$modalInstance"];
	
	//Función que pasa un presupuesto de borrador a presupuesto confirmado.
	$scope.confirmarPresupuesto = function(){

	    var modalInstance = $modal.open({
	    	templateUrl: '/app/views/partials/ngDialogConfirmPre.html',
			controller: ModalInstanceCtrl,
			size: 'sm'
	    });
	
	    var state = $('#modal-state');
	    modalInstance.result.then(function () {
	    	state.text('Modal dismissed with OK status');

	    	//Comprobamos que se ha añadido algún producto al presupuesto, y por lo tanto, existe un preceio final.
			if($scope.presupuesto.total != undefined){
				//Comprobamos que se ha seleccionado un cliente.
				if($scope.cliente != undefined){
					//Comprobamos si se ha seleccionado descuento, y en tal caso, comprobamos que se ha introducido la cantidad.
					if(($scope.presupuesto.descuento && ($scope.presupuesto.precioDescuento != undefined)) || (!$scope.presupuesto.descuento)){
						//Si el presupuesto no se ha financiado, reseteamos todos los campos que implica para que se modifiquen en la tabla.
						if(!$scope.presupuesto.financiado){
							$scope.presupuesto.plazo = null;
							$scope.presupuesto.coeficiente = null;
							$scope.presupuesto.cuota = null;
						}
						$scope.presupuesto.borrador = false;
						dataService
							.putElements('presupuesto', $scope.presupuesto)
							.then(function(data){
								console.log('Presupuesto modificado: ', data.id);
								//$scope.resetFormPresupuestos();
								$scope.buffer = true;
								recuperarRegistros('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormPresupuestos = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
							});
					}else{
						$scope.errorFormPresupuestos = 'Ha seleccionado descuento para el presupuesto pero no ha especificado la cantidad.';
						$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
					}
				}else{
					$scope.errorFormPresupuestos = 'Seleccione el cliente del presupuesto.';
					$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
				}
			}else{
				$scope.errorFormPresupuestos = 'Añada productos al presupuesto.';
				$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
			}
					
	    },function () {
	    	state.text('Modal dismissed with Cancel status');
	    });
	}
	
	//Función que abre el formulario de la aceptación del presupuesto.
	$scope.abrirAceptarPresupuesto = function(presupuesto){		
		$scope.tituloAceptacion = 'Generar Aceptación del presupuesto '+presupuesto.referencia;
		$scope.mostrarFormAceptacion = true;
		$rootScope.$broadcast('aceptarPresupuesto', presupuesto);
		$scope.resetFormPresupuestos();
	}
			
	//Función para insertar o modificar presupuestos.
	$scope.add = function(){
		//Comprobamos que se ha añadido algún producto al presupuesto, y por lo tanto, existe un preceio final.
		if($scope.presupuesto.total != undefined){
			//Comprobamos que se ha seleccionado un cliente.
			if($scope.cliente != undefined){
				//Comprobamos si se ha seleccionado descuento, y en tal caso, comprobamos que se ha introducido la cantidad.
				if(($scope.presupuesto.descuento && ($scope.presupuesto.precioDescuento != undefined)) || (!$scope.presupuesto.descuento)){
					//Si el presupuesto no se ha financiado, reseteamos todos los campos que implica para que se modifiquen en la tabla.
					if(!$scope.presupuesto.financiado){
						$scope.presupuesto.plazo = null;
						$scope.presupuesto.coeficiente = null;
						$scope.presupuesto.cuota = null;
					}
					if(!$scope.presupuesto.descuento){
						$scope.presupuesto.subTotal = $scope.presupuesto.base;
					}
					dataService
						.putElements('presupuesto', $scope.presupuesto)
						.then(function(data){
							console.log('data modificado: ', data.id);
							$scope.resetFormPresupuestos();
							$scope.buffer = true;
							recuperarRegistros('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.errorFormPresupuestos = parseError(error.status, error.data);
							$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
						});
				}else{
					$scope.errorFormPresupuestos = 'Ha seleccionado descuento para el presupuesto pero no ha especificado la cantidad.';
					$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
				}
			}else{
				$scope.errorFormPresupuestos = 'Seleccione el cliente del presupuesto.';
				$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
			}
		}else{
			$scope.errorFormPresupuestos = 'Añada productos al presupuesto.';
			$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
		}
	}
	
	//Función que elimina un presupuestos
	$scope.delPresupuesto = function(presupuesto){
		dataService
			.deleteElements('presupuesto', '?filter=id%3D'+presupuesto.id)
			.then(function(data){
				console.log('Presupuesto eliminado: ', data.id);
				$scope.resetFormPresupuestos();
				$scope.buffer = true;
				recuperarRegistros('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormPresupuestos = null;}, 15000);
			});
	}
	
	//Función para imprimir la vista del presupuesto
	$scope.imprimir = function(presupuesto, impuesto, cliente, gridProductosPresupuesto, iva){		
		//Comprobamos que todas las cantidades numéricas van con 2 decimales, para que todo cuadre dentro de la tabla.
		var productos = angular.copy(gridProductosPresupuesto.data);
		for(var i=0; i<productos.length; i++){
			productos[i].pvp = parseFloat(""+productos[i].pvp).toFixed(2);
			productos[i].total = parseFloat(""+productos[i].total).toFixed(2);
		}
		presupuesto.base = parseFloat(""+presupuesto.base).toFixed(2);
		presupuesto.total = parseFloat(""+presupuesto.total).toFixed(2);
		presupuesto.cuota = parseFloat(""+presupuesto.cuota).toFixed(2);
		iva = parseFloat(""+iva).toFixed(2);
		
		//Llamamos a la directiva que hemos creado.
		//El primer parámetro es la url al template que tenemos creado.
		//El segundo parámetro es un objeto que contiene los elementos que sustituiremos dentro del template.
		var obj = {
			presupuesto,
			impuesto,
			cliente,
			productos,
			iva
		};
		
		$rootScope.presupuesto = angular.copy(presupuesto);
		
		printer.print('app/views/partials/impresionPresupuesto.html', obj);
	}
		
	//Función para cerrar los paneles de error.
	$scope.clearErrorPresupuestos = function(){
		$scope.errorPresupuestos = null;
	}
	$scope.clearErrorFormPresupuestos = function(){
		$scope.errorFormPresupuestos = null;
	}
	
	//Función que escucha a los hijos para cerrar el apartado del formulario de la aceptación.
	$scope.$on('cerrarAceptacion', function(e, data){
		$scope.mostrarFormAceptacion = data;
		$rootScope.$broadcast('cerrarFormularioAceptacion', false);
	});
		
	//Función que informa de que la aceptación del presupuesto ha sido realizada con éxito.
	$scope.$on('aceptacionCorrecta', function(e, datas){
		//Marcamos el campo de aceptado.
		datas.aceptado = true;
		//Realizamos la modificación del presupuesto en su tabla.
		dataService
			.putElements('presupuesto', datas)
			.then(function(data){
				$scope.buffer = true;
				recuperarRegistros('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
				$scope.aceptado = 'Aceptación del presupuesto '+datas.referencia+' realizada correctamente.';
				$timeout(function(){$scope.aceptado = null;}, 5000);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPresupuestos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPresupuestos = null;}, 15000);
			});
	});
	
	//Función que recoge la llamada del hijo de que está subiendo archivos.
	$scope.$on('uploading', function(e, data){
		if(data.progressUpload == 100){
			$scope.progressUpload = data.progressUpload;
			$timeout(function(){$scope.uploading = data.uploading;}, 1000);
		}else{
			$scope.uploading = data.uploading;
			$scope.progressUpload = data.progressUpload;	
		}
	});
	
	//Función que abre el formulario para añadir un nuevo cliente.
	$scope.newCliente = function(){
		$scope.clienteNuevo = {};
		$scope.nuevoCliente = true;
	}
	
	
	//Función que resetea el formulario de añadir un nuevo cliente.
	$scope.resetFormClientePresupuesto = function(){
		$scope.clienteNuevo = {};
		$scope.nuevoCliente = false;
	}
	
	/*---------------------------------------- Controlador del Select ----------------------------------------*/
	//Función que sustituye la población actual por la seleccionada de la tabla.
	$scope.cambiarCliente = function(cliente){
		$scope.clienteSel = cliente.nombre;
		$scope.presupuesto.id_cliente = cliente.id;
		$scope.cliente = angular.copy(cliente);
		$scope.desplegarUl = !$scope.desplegarUl;
	}
	
	//Desplegamos el Select.
	$scope.desplegar = function(){
		if($scope.desplegarUl){
			$scope.desplegarUl = !$scope.desplegarUl;
			$scope.filtro='';
			focus('mainFocus');
		}else{
			$scope.desplegarUl = !$scope.desplegarUl;
			focus('focusMe');
		}
	}
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masClientes = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogClientes.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (cliente) {
			if(cliente.id == null || cliente.id == undefined){
				dataService
					.postElements('cliente', cliente)
					.then(function(data){
						cliente.id = data.id;
						$scope.clienteSel = cliente.nombre;
						$scope.presupuesto.id_cliente = cliente.id;
						$scope.cliente = angular.copy(cliente);
						$scope.nuevoCliente = false;
						$scope.desplegarUl = !$scope.desplegarUl;
						console.log('Cliente añadido correctamente (masClientes PresupuestosCntrl)');
						traerClientes('cliente?filter=id%3E', 0, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){
							$rootScope.relogin();
						}else{
							$scope.errorFormCliente = parseError(error.status, error.data);
							$timeout(function(){$scope.errorFormCliente = null;}, 15000);
						}
					});
			}else{
				$scope.clienteSel = cliente.nombre;
				$scope.presupuesto.id_cliente = cliente.id;
				$scope.cliente = angular.copy(cliente);
				$scope.desplegarUl = !$scope.desplegarUl;
			}
	    }, function (reason) {
		    $scope.desplegarUl = !$scope.desplegarUl;
	    });
	}
}