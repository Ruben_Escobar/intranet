/****************************************************************************/
/*		Controlador de los contactos de la aceptación de Presupuestos		*/
/****************************************************************************/
App.controller('PresupuestosAceptacionContactosCntrl', PresupuestosAceptacionContactosCntrl);
function PresupuestosAceptacionContactosCntrl($scope, $rootScope, $timeout, uiGridConstants, ngDialog, focus, dataService){
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PresupuestosAceptacionContactosCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.notas}} tooltip-placement="top">{{row.entity.notas}}</div>';
	
	var columnas = [
	        {name: 'nombre', field: 'nombre', minWidth: 90, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'apellidos', field: 'apellidos', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'tipo', field: 'tipo', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'telefono', field: 'telefono', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Teléfono', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'movil', field: 'movil', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Móvil', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'correo', field: 'correo', displayName: 'e-mail', minWidth: 150, enableHiding: false, enablePinning:false, cellClass: 'hidden-xs', headerCellClass : "hidden-xs"},
	        {name: 'notas', field: 'notas', minWidth: 85, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs hidden-sm', headerCellClass : 'hidden-xs hidden-sm'},
	        {name: 'editar', width: 70, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.showFormCon(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button><button data-ng-click="grid.appScope.delCon(row.entity)" title="Edit" class="btn btn-sm btn-danger"><em class="fa fa-trash"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ];
	var columnas2 = [
	        {name: 'nombre', field: 'nombre', minWidth: 90, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'apellidos', field: 'apellidos', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'tipo', field: 'tipo', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'telefono', field: 'telefono', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Teléfono', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'movil', field: 'movil', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Móvil', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'correo', field: 'correo', displayName: 'e-mail', minWidth: 150, enableHiding: false, enablePinning:false, cellClass: 'hidden-xs', headerCellClass : "hidden-xs"},
	        {name: 'notas', field: 'notas', minWidth: 85, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs hidden-sm', headerCellClass : 'hidden-xs hidden-sm'},
	        {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.showFormCon(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ];
	
	//Inicializamos las tablas.
	$scope.gridConAc = {
		paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
	    enableRowSelection: true,
	    selectionRowHeaderWidth: 38,
		enableColumnMenus: false,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
	    columnDefs: columnas,
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi2 = gridApi;
	    }
	};
	$scope.gridConAcTotal = {
		paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
	    enableRowSelection: true,
	    selectionRowHeaderWidth: 38,
		enableColumnMenus: false,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
	    columnDefs: columnas2,
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi = gridApi;
	    }
	};
	
	//Funciones que cambian el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridConAc.paginationPageSize*36)+100) > (($scope.gridConAc.data.length*36)+100) ){
			return ($scope.gridConAc.data.length*36)+100;
		}else{
			return ($scope.gridConAc.paginationPageSize*36)+100;
		}
	}
	$scope.comprobarTamano2 = function(){
		if( (($scope.gridConAcTotal.paginationPageSize*36)+100) > (($scope.gridConAcTotal.data.length*36)+100) ){
			return ($scope.gridConAcTotal.data.length*36)+100;
		}else{
			return ($scope.gridConAcTotal.paginationPageSize*36)+100;
		}
	}
	
	//Función que trae todos los contactos de la BBDD.
	var traerContactos = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (traerContactos SistemasConCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
						data.registros[i].apellidos = removeAccents(data.registros[i].apellidos);
					}
					$scope.gridConAcTotal.data = angular.copy(data.registros);
					$scope.buffer2 = false;
					$scope.progressValue2 = 0;
				}else{
					$scope.progressValue2 = data.progressValue;
					console.log('Cargando (traerContactos SistemasConCntrl): '+$scope.progressValue2+'%.');
					$scope.gridConAcTotal.data = angular.copy(data.registros);
					traerContactos(data.tabla, registros[(registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorSisCon = parseError(error.status, error.data);
				$timeout(function(){$scope.errorSisCon = null;}, 15000);
			});
	}
	
	//Función que comprueba si un contacto está dentro del array y lo modifica con el valor facilitado.
	var comprobarContactos = function(contacto){
		for(var i=0; i<$scope.gridConAc.data.length; i++){
			if($scope.gridConAc.data[i].id == contacto.id){
				$scope.gridConAc.data[i] = contacto;
				break;
			}
		}
	}
	
	//Función que escucha al padre e inicializa la pestaña del renting.
	$scope.$on('aceptarPresupuesto', function(e, data){
    	//Inicializamos las variables que cogen datos del presupuesto para el formulario y mostramos el mismo.
    	$scope.gridConAc.data = [];
		$scope.titulo = 'Contactos';
		$scope.buffer2 = true;
		traerContactos('contacto?filter=id%3E',0, 0, 0, 0, []);
	});
				
	//Función que abre la tabla de contactos totales.
	$scope.abrirTabla = function(){
		$scope.contactosTotales = true;
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogTablaContactos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (nuevo) {
			//Si es un contacto que viene desde el formulario del diálogo, comprobamos si es edición o inserción y hacemos lo pertinente.
			//Además lo añadimos al array de usuarios de la aceptación.
			if(nuevo){
				dataService
					.postElements('contacto', $scope.contacto)
					.then(function(data){
						console.log('Contacto añadido:', data.id);
						$scope.buffer2 = true;
						traerContactos('contacto?filter=id%3E',0, 0, 0, 0, []);
						$scope.gridConAc.data.push($scope.contacto);
						$scope.resetFormCon();
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormCon = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormCon = null;}, 15000);
					});
			//En caso de que no sea nuevo, recogemos la variable que contiene los contactos seleccionados y procedemos.
			}else{
				var contactosAux = $scope.gridApi.selection.getSelectedRows();
				var contactosPush = $scope.gridApi.selection.getSelectedRows();
				if($scope.gridConAc.data.length == 0){
					$scope.gridConAc.data = contactosAux;
				}else{
					//Recoremos el array de contactos de la aceptación para comprobar si alguno se repite.
					//Primero recorremos el array de contactos seleccionados.
					for(var i=0; i<contactosAux.length; i++){
						//Después el array de contactos que ya hay añadidos en la aceptación.
						for(var j=0; j<$scope.gridConAc.data.length; j++){
							//En caso de que alguno coincida.
							if(contactosAux[i].id == $scope.gridConAc.data[j].id){
								//Recorremos el array auxiliar y eliminamos el contacto coincidente del mismo.
								for(var l=0; l<contactosPush.length; l++){
									if(contactosAux[i].id == contactosPush[l].id){
										contactosPush.splice(l, 1);
										break;
									}
								}
								break;
							}
						}
					}
					//Por último, recorremos el array auxiliar añadiendo los contactos que no sean coincidentes al array original de contactos de la aceptación.
					for(var k=0; k<contactosPush.length; k++){
						$scope.gridConAc.data.push(contactosPush[k]);
					}
				}
				$scope.gridApi.selection.clearSelectedRows();
				$scope.contactosTotales = false;
			}
	    }, function (reason) {
		    //Código para el cierre del diálogo.		    
		});
	}
	
	//Función que abre el formulario de edición del contacto.
	$scope.showFormCon = function(contacto){
		$scope.contacto = angular.copy(contacto);
		$scope.mostrarFormCon = true;
		$scope.titulo = 'Formulario Editar';
		$scope.botonForm = 'Modificar';
	}
	
	//Función que edita el contacto.
	$scope.editarContacto = function(){
		comprobarContactos(angular.copy($scope.contacto));
		dataService
			.putElements('contacto', $scope.contacto)
			.then(function(data){
				console.log('Contacto añadido:', data.id);
				$scope.buffer2 = true;
				traerContactos('contacto?filter=id%3E',0, 0, 0, 0, []);
				$scope.resetFormCon();
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormCon = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormCon = null;}, 15000);
			});
	}
	
	//Función que abre el formulario de añadir nuevo contacto.
	$scope.newContacto = function(){
		$scope.contacto = {};
		$scope.mostrarFormCon = true;
		$scope.titulo = 'Formulario Añadir';
		$scope.botonForm = 'Añadir';
	}
	
	//Función que cierra el formulario.
	$scope.resetFormCon = function(){
		$scope.mostrarFormCon = false;
		$scope.contacto = {};
	}
	
	//Función que elimina un contacto de la lista de añadir contactos.
	$scope.delCon = function(contacto){
		//Recorremos el array de contactos guardados.
		for(var i=0; i<$scope.gridConAc.data.length; i++){
			//Comprobamos cual es el contacto que coincide con el que se nos facilita en la función
			if($scope.gridConAc.data[i].id == contacto.id){
				//Eliminamos el contacto del array de contactos.
				$scope.gridConAc.data.splice(i, 1);
				break;
			}
		}
	}
	
	//Función para mandar los datos introducitos al hermano.
	$scope.aceptar = function(){
		if($scope.gridConAc.data.length == 0){
			$scope.errorConAc = 'No ha añadido ningún contacto a la lista de contactos de la aceptación.';
			$timeout(function(){$scope.errorConAc = null;}, 15000);
		}else{
			$rootScope.$broadcast('datosContactos', $scope.gridConAc.data);
			$scope.enviado = 'Datos enviados correctamente al formulario principal.';
			$timeout(function(){$scope.enviado = null;}, 5000);
		}
	}
	
	//Función para cerrar el formulario de la aceptación del presupuesto.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarAceptacion', false);
	}
}