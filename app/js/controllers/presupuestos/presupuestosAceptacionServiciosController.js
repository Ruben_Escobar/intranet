/****************************************************************************/
/*		Controlador de los servicios de la aceptación de Presupuestos		*/
/****************************************************************************/
App.controller('PresupuestosAceptacionServiciosCntrl', PresupuestosAceptacionServiciosCntrl);
function PresupuestosAceptacionServiciosCntrl($scope, $rootScope, $timeout, ngDialog, focus){
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PresupuestosAceptacionServiciosCntrl ------------------------------');
	
	//Función que escucha al padre e inicializa la pestaña del renting.
	$scope.$on('aceptarPresupuesto', function(e, data){
    	//Inicializamos las variables que cogen datos del presupuesto para el formulario y mostramos el mismo.
		$scope.aceptacionServicios = {};
		$scope.titulo = 'Servicios';
		focus('mainFocus');
	});
	
	//Función para mandar los datos introducitos al hermano.
	$scope.aceptar = function(){
		$rootScope.$broadcast('datosServicios', $scope.aceptacionServicios);
		$scope.enviado = 'Datos enviados correctamente al formulario principal.';
		$timeout(function(){$scope.enviado = null;}, 5000);
	}
	
	//Función para cerrar el formulario de la aceptación del presupuesto.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarAceptacion', false);
	}
}