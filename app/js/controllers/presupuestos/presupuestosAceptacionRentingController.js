/****************************************************************************/
/*			Controlador del renting de la aceptación de Presupuestos		*/
/****************************************************************************/
App.controller('PresupuestosAceptacionRentingCntrl', PresupuestosAceptacionRentingCntrl);
function PresupuestosAceptacionRentingCntrl($scope, $rootScope, $timeout, ngDialog, focus){
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PresupuestosAceptacionRentingCntrl ------------------------------');
	
	//Función que escucha al padre e inicializa la pestaña del renting.
	$scope.$on('aceptarPresupuesto', function(e, data){
    	//Inicializamos las variables que cogen datos del presupuesto para el formulario y mostramos el mismo.
		$scope.aceptacionRenting = {};
		$scope.aceptacionRenting.plazoRenting = data.plazo;
		$scope.aceptacionRenting.cuotaRenting = data.cuota;
		$scope.aceptacionRenting.observacionesRenting = data.observacionesRenting;
		$scope.titulo = 'Renting';
		focus('mainFocus');
	});
	
	//Función para mandar los datos introducitos al hermano.
	$scope.aceptar = function(){
		$rootScope.$broadcast('datosRenting', $scope.aceptacionRenting);
		$scope.enviado = 'Datos enviados correctamente al formulario principal.';
		$timeout(function(){$scope.enviado = null;}, 5000);
	}
	
	//Función para cerrar el formulario de la aceptación del presupuesto.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarAceptacion', false);
	}
}