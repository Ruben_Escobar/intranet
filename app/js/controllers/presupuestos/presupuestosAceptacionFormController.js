/****************************************************************************/
/*			Controlador del formulario de la Aceptación de Presupuestos		*/
/****************************************************************************/
App.controller('PresupuestosAceptacionFormCntrl', PresupuestosAceptacionFormCntrl);
function PresupuestosAceptacionFormCntrl($scope, $rootScope, $timeout, uiGridConstants, ngDialog, focus, dataService, uploadService){
	
	$scope.files = [];
	$scope.filesUploaded = [];
	$scope.errorFormAceptacion = null;
	
	var contactosFinales = [];
	var datosContactos = {};	
	var servicios = false;
	var contactos = false;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PresupuestosAceptacionFormCntrl ------------------------------');
	
	//Función que acepta el presupuesto. Pasa el presupuesto a aceptación.
	$scope.aceptarPresupuesto = function(){
		$scope.aceptacion.id = undefined;
		//Comprobamos que se han añadido los valores de los servicios y los contactos.
		if(servicios){
			if(contactos){
				//Una vez verificados los datos de las demás pestañas, verificamos que se han añadido ficheros.
				if(($scope.files.length != 0) && ($scope.progressUpload == 100)){
					//Por último, comprobamos que se ha seleccionado una forma de pago.
					if($scope.aceptacion.formaPagoInst == 'RENTING' || $scope.aceptacion.formaPagoInst == 'CONTADO'){
						//Una vez comprobado que todo está correctamente completado, insertamos la aceptación en su tabla correspondiente.						
						//Insertamos la aceptación (aceptacion)
						dataService
							.postElements('aceptacion', $scope.aceptacion)
							.then(function(data){
								$scope.aceptacion.id = data.id;
								console.log('Aceptación insertada correctamante: ', $scope.aceptacion.id);
								//Creamos los contactos correspondientes para la inserción en la tabla de unión (contactos_aceptacion).
								contactosFinales = [];
								for(var i=0; i<datosContactos.length; i++){
									contactosFinales.push({
										"id_contacto": datosContactos[i].id,
										"id_aceptacion": $scope.aceptacion.id										
									});
								}
								console.log('Contactos: ', contactosFinales);
								//Insertamos la relación de contactos en su tabla correspondiente (contacto_aceptacion).
								dataService
									.postElements('contacto_aceptacion', contactosFinales)
									.then(function(data){
										console.log('Contactos añadidos correctamente: ', data.record.length);
										var contactosElim = data.record;
										$scope.filesUploaded = [];
										//Creamos la estructura para hacer la inserción en la tabla ficheros_aceptacion.
										for(var i=0; i<$scope.files.length; i++){
											$scope.filesUploaded.push({
												"id_aceptacion": $scope.aceptacion.id,
												"nombre": $scope.files[i].file.name,
												"descripcion": $scope.files[i].descripcion
											});
										}
										//Insertamos la relación de ficheros en su tabla correspondiente (ficheros_aceptacion).
										dataService
											.postElements('ficheros_aceptacion', $scope.filesUploaded)
											.then(function(data){
												console.log('Ficheros añadidos correctamente: ', data.record.length);
												$rootScope.$broadcast('aceptacionCorrecta', $scope.presupuesto);
												$scope.cerrar();
											})
											.catch(function(error){
												var ar = '';
												for(var i=0; i<contactosElim.length; i++){
													if(i==0){
														ar = ar+contactosElim[i].id;
													}else{
														ar = ar+'%7C%7Cid%3D'+contactosElim[i].id;
													}													
												}
												dataService
													.deleteElements('contacto_aceptacion', '?filter=id%3D'+ar)
													.then(function(data){
														dataService.deleteElements('aceptacion', '?filter=id%3D'+$scope.aceptacion.id);
													});
												if(error.status==401){$rootScope.relogin();}
												$scope.errorFormAceptacion = parseError(error.status, error.data);
												$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
											});
									})
									.catch(function(error){
										dataService.deleteElements('aceptacion', '?filter=id%3D'+$scope.aceptacion.id);
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormAceptacion = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
									});
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormAceptacion = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
							});
					}else{
						$scope.errorFormAceptacion = 'No ha seleccionado la forma de pago.';
						$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
					}
				}else{
					$scope.errorFormAceptacion = 'No ha adjuntado ningún fichero a la aceptación, o está en progreso de subida.';
					$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
				}
			}else{
				$scope.errorFormAceptacion = 'No se han añadido contactos a la aceptación, o no pulsó botón aceptar.';
				$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
			}
		}else{
			$scope.errorFormAceptacion = 'No se han rellenado los campos de servicios correctamente, o no pulsó el botón aceptar.';
			$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
		}
	}
	
	//Función para cerrar el formulario de la aceptación del presupuesto.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarAceptacion', false);
	}
	
	//Función que escucha al padre e inicializa la pestaña del formulario.
	$scope.$on('aceptarPresupuesto', function(e, data){
		$scope.presupuesto = data;
    	//Inicializamos las variables que cogen datos del presupuesto para el formulario y mostramos el mismo.
		$scope.aceptacion = {};
		$scope.aceptacion.referencia = 'A'+crearReferencia();
		$scope.aceptacion.id_presupuesto = data.id;
		$scope.aceptacion.precioFinal = data.total;
		if(data.descuento){
			$scope.aceptacion.precioDescuento = data.precioDescuento;
		}
		$scope.aceptacion.descripcion = data.descripcion;
		$scope.aceptacion.condiciones = data.condiciones;
		$scope.aceptacion.observaciones = data.observaciones;
		
		$scope.aceptacion.plazoRenting = data.plazo;
		$scope.aceptacion.cuotaRenting = data.cuota;
		$scope.aceptacion.observacionesRenting = data.observacionesRenting;
		
		$scope.titulo = 'General';
		$scope.mostrarFormAceptacion = true;
		focus('mainFocus');
		$scope.files = [];
	});
	
	//Función que escucha al padre y resetea el formulario.
	$scope.$on('cerrarFormularioAceptacion', function(e, data){
		$scope.mostrarFormAceptacion = false;
		$scope.aceptacion = {};
		$scope.formAceptacion.$setPristine();
		$scope.files = [];
		$scope.errorFormAceptacion = null;
		$scope.presupuesto = {};
		$scope.progressUpload = null;
	});
	
	//Funciones que reciben datos de los hermanos.
	$scope.$on('datosRenting', function(e, data){
		console.log('datosRenting recibidos: ', data);
		$scope.aceptacion.plazoRenting = data.plazoRenting;
		$scope.aceptacion.cuotaRenting = data.cuotaRenting;
		$scope.aceptacion.observacionesRenting = data.observacionesRenting;
	});
	$scope.$on('datosServicios', function(e, data){
		console.log('datosServicios recibidos: ', data);
		$scope.aceptacion.servicios = data.servicios;
		$scope.aceptacion.costeServicio = data.costeServicio;
		$scope.aceptacion.formaPagoSer = data.formaPagoSer;
		$scope.aceptacion.tipoPagoSer = data.tipoPagoSer;
		$scope.aceptacion.observacionesSer = data.observacionesSer;
		servicios = true;
	});
	$scope.$on('datosContactos', function(e, data){
		console.log('datosContactos recibidos: ', data);
		datosContactos = angular.copy(data);
		contactos = true;
	});
	
	//Función que cierra el panel de error del formulario.
	$scope.clearErrorFormAceptacion = function(){
		$scope.errorFormAceptacion = null;
	}
	
	/*--------------------------------------------- Upload Files ---------------------------------------------*/
    
    $scope.uploadFiles = function (files) {
        $rootScope.$broadcast('uploading', {"uploading": true, "progressUpload": 0});
        for(var i=0; i<files.length; i++){
	        $scope.files.push({
		        "file": files[i],
				"descripcion": ''
			});
        }
        var contador = 0;
        for(var i=0; i<files.length; i++){
	        //Primero hacemos la inserción del archivo en 
	        uploadService
	            .subirArchivos($scope.aceptacion.referencia+'/'+files[i].name+'?app_name=intranet', files[i])
	            .then(function(data){
		            console.log('Archivo subido:', data.file[0].path);
		            contador = contador + 1;
	                $scope.progressUpload = ((((contador)*100)/files.length).toFixed());
	                
	                if($scope.progressUpload == 100){
		                $rootScope.$broadcast('uploading', {"uploading": false, "progressUpload": $scope.progressUpload});
	                }else{
		                $rootScope.$broadcast('uploading', {"uploading": true, "progressUpload": $scope.progressUpload});
	                }
	            })	
	            .catch(function(error){
	                if(error.status==401){$rootScope.relogin();}
					$scope.errorMsg = parseError(error.status, error.data);
					$timeout(function(){$scope.errorMsg = null;}, 15000);
	                $scope.progressUpload = null;
	            });
        }
    }
    
    $scope.eliminarArchivo = function(archivo){
	    for(var i=0; i<$scope.files.length; i++){
		    if($scope.files[i] == archivo){
			    $scope.files.splice(i, 1);
		    }
	    }
	    if($scope.files.length == 0){
		    $scope.progressUpload = null;
	    }
	    uploadService
	        .eliminarArchivos($scope.aceptacion.referencia+'/'+archivo.file.name+'?app_name=intranet')
	        .then(function(data){
	            console.log('Archivo eliminado:', data.file[0].path);
	        })	
	        .catch(function(data, status){
	        	if(error.status==401){$rootScope.relogin();}
				$scope.errorMsg = parseError(error.status, error.data);
				$timeout(function(){$scope.errorMsg = null;}, 15000);
	        });
    }
}