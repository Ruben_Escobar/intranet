/****************************************************************************/
/*					Controlador de la vista de Servicios					*/
/****************************************************************************/
App.controller('ServiciosCntrl', ServiciosCntrl);
function ServiciosCntrl($scope, $rootScope, $filter, $timeout, ngDialog, uiGridConstants, focus, dataService) {
	$scope.buffer = true;
	$scope.mostrar = false;
	$scope.progressValue = 0;
	$scope.errorFormSer = null;
	
	var registros = [];
	var data = [];
	var pasarela = 0;
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ServiciosCntrl ------------------------------');
	
	//Inicializamos la tabla.
	$scope.gridOptionsComplex = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableRowSelection: true,
		selectionRowHeaderWidth: 38,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
		    {name: 'nombre', field: 'nombre', minWidth: 100, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning: false},
		    {name: 'descripcion', field: 'descripcion', displayName: 'Descripción', minWidth: 100, enableHiding: false, enablePinning: false},
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridOptionsComplex.paginationPageSize*36)+100) >(($scope.gridOptionsComplex.data.length*36)+100) ){
			return ($scope.gridOptionsComplex.data.length*36)+100;
		}else{
			return ($scope.gridOptionsComplex.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (servicios ServiciosCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
					}
					$scope.gridOptionsComplex.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (servicios ServiciosCntrl): '+$scope.progressValue+'%.');
					$scope.gridOptionsComplex.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	recuperarRegistros('servicio?filter=id%3E', 0, 0, 0, 0, []);

	//Eliminar servicio por id
	$scope.delSer = function(id){
		dataService
			.deleteElements('servicio', '?filter=id%3D'+id)
			.then(function(data){
				console.log('Servicio eliminado: ', data);
				$scope.resetFormSer();
			    recuperarRegistros('servicio?filter=id%3E', 0, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormSer = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormSer = null;}, 15000);
			});
	}
	
	//Función que elimina uno o varios servicios.
	$scope.remove = function(){
		data = $scope.gridApi.selection.getSelectedRows();
		//Comprobamos si hay o no objetos seleccionados.
	    var url = '';
	    for(var i=0; i<data.length; i++){
		    if(i == 0){
			    url = url+data[i].id;
			}else{
				url = url + '%7C%7Cid%3D'+data[i].id;
			}
	    }
	    if(data.length!=0){
	        dataService
	        	.deleteElements('servicio', '?filter=id%3D'+url)
	        	.then(function(data){
		        	console.log('Servicios eliminados.');
		        	recuperarRegistros('servicio?filter=id%3E', 0, 0, 0, 0, []);
		        })
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			ngDialog.open({
	            template: '<p class="rojo centrado">No has seleccionado ninún elemento para eliminar</p>',
	            plain: true
	        });
		}
	}
	
	/*---------------------------------------- FORMULARIO ----------------------------------------*/	
	//Abrir el formulario vacío.
	$scope.mostrarForm = function(){
		$scope.boton = 'Añadir Servicio';
		$scope.mostrar=true;
		$scope.titulo = 'Formulario Añadir';
	}	
	
	//Abrimos el formulario con los datos de la población a editar.
	$scope.editar = function(servicio){
		registros=[];
		$scope.ser = angular.copy(servicio);
		$scope.boton = 'Modificar Sistema';
		$scope.mostrar=true;
		$scope.titulo = 'Formulario Editar';
	}
	
	//Resetear formulario
	$scope.resetFormSer = function(){
		registros=[];
		$scope.formSer.$setPristine();
		$scope.ser = undefined;
		$scope.mostrar=false;
	}
	
	//Función para añadir el registro
	$scope.add = function(){
		//Comprobamos si es una edición o una inserción y procedemos según corresponda.
		if($scope.ser.id==undefined){		
			//Inserción.
			dataService
				.postElements('servicio', $scope.ser)
				.then(function(data){
					console.log('Servicio insertado: ', data);
					$scope.resetFormSer();
					$scope.buffer = true;
					recuperarRegistros('servicio?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			//Edición.
			dataService
				.putElements('servicio', $scope.ser)
				.then(function(data){
					console.log('Servicio modificado:', data.id);
					$scope.resetFormSer();
					$scope.buffer = true;
					recuperarRegistros('servicio?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}
	}
	
	//Funciones para cerrar los paneles de error.
	$scope.clearError = function(){
		$scope.currentError = null;
		$scope.buffer = false;
	}
	$scope.clearErrorFormSer = function(){
		$scope.errorFormSer = null;
		$scope.buffer = false;
	}
}