/****************************************************************************/
/*					Controlador del registro de usuarios					*/
/****************************************************************************/
App.controller('RegisterFormController', RegisterFormController);
function RegisterFormController($scope, $state, userService) {
	//Reseteamos los valores del formulario.
	$scope.account = {};
    
    //Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ RegisterFormController ------------------------------');
	
	$scope.register = function() {
    	$scope.authMsg = '';
	    if($scope.registerForm.$valid) {
	    	userService
		        .registerUser('register?login=true', {"email": $scope.account.email, "new_password": $scope.account.password})
		        .then(function(data) {
	            	$state.go('app.dashboard');
		        })
		        .catch(function(error){
					$scope.authMsg = 'Server Request Error';
				});
	    }else{
	    	//Si se pulsa el botón de registro sin haber rellenado los campos, mostramos los mensajes de error.
			$scope.registerForm.account_email.$dirty = true;
			$scope.registerForm.account_password.$dirty = true;
			$scope.registerForm.account_agreed.$dirty = true;
	    }
	}
}