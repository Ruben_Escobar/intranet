/****************************************************************************/
/*			Controlador de la vista de las Incidencias de cada sistema		*/
/****************************************************************************/
App.controller('SistemasInciCntrl', SistemasInciCntrl);
function SistemasInciCntrl($scope, $rootScope, $timeout, dataService){
	
	var fasesAux = [];
	
	$scope.errorFormInciSis = null;
	$scope.errorInciSis = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasInciCntrl ------------------------------');
	
	
	//Función que retorna todas las incidencias del sistema.
	var traerInciSis = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.incidenciasFinal = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				console.log('Incidencias del sistema cargadas (SistemasInciCntrl)');
				var ids = [];
				for(var i=0; i<data.registros.length; i++){
					ids.push(data.registros[i].id);
				}
				$scope.incidenciasFinal = angular.copy(data.registros);
				traerTareas(ids);
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorInciSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInciSis = null;}, 15000);
			});
	}
	
	//Función que retorna todas las fases de las incidencias del sistema. Tabla fase_ins
	var traerTareas = function(ids){
		$scope.tareasInst = [];
		var srt = '';
		for(var i=0; i<ids.length; i++){
			if(i==0){
				srt = srt+'%26%26id_incidencia%3D'+ids[i];
			}else{
				srt = srt+'%7C%7Cid_incidencia%3D'+ids[i];
			}
		}	
		dataService
			.getFiltro('incid_task?filter=id%3E', 0, srt+'&order=timestamp%20ASC', 0, 0, 0, [])
			.then(function(data){
				console.log('Total de tareas de las Incidencias (SistemasInciCntrl): ', data.registros.length);
				$scope.tareasInst = angular.copy(data.registros);
				intercalarTareas();
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorInciSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInciSis = null;}, 15000);
			});
	}
	
	//Intercalar fases de cada instalación.
	var intercalarTareas = function(){
		for(var i=0; i<$scope.incidenciasFinal.length; i++){
			$scope.incidenciasFinal[i].tareas = [];
			for(var j=0; j<$scope.tareasInst.length; j++){
				if($scope.incidenciasFinal[i].id == $scope.tareasInst[j].id_incidencia){
					$scope.incidenciasFinal[i].tareas.push($scope.tareasInst[j]);
				}
			}
		}
	}
	
	//Función que trae los datos del contrato del sistema de la incidencia.
	var traerContrato = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.contrato = null;
		$scope.llavesContrato = undefined;
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.registros.length>0){
					$scope.contrato = angular.copy(data.registros[0]);
					$scope.contrato.servicios='';
					//Hacemos la petición de los servicios de dicho contrato.
					dataService
						.getFiltro('servicios?filter=id_servicio%3E', 0, '%26%26id_contrato%3D'+$scope.contrato.id, 0, 0, 0, [])
						.then(function(data){
							var servicios = angular.copy(data.registros);
							var hoy = new Date();
							$scope.contrato.caduca = new Date($scope.contrato.caduca);
							
							//Recorremos la lista de servicios quedándonos con los nombres
							for(var i=0; i<servicios.length; i++){
								if($scope.contrato.servicios == ''){
									$scope.contrato.servicios = servicios[i].nombre;
								}else{
									$scope.contrato.servicios = $scope.contrato.servicios +', '+servicios[i].nombre;
								}
							}
							//Comprobamos si está caducado o no el contrato.
							if(hoy.getTime() >= $scope.contrato.caduca.getTime()){
								$scope.contrato.caducado = true;
							}else{
								$scope.contrato.caducado = false;
							}
							console.log('Contrato del sistema: ', $scope.contrato.ref);
							
							//Realizamos la petición de los contactos del contrato (Según el sistema).
							dataService
								.getFiltro('contactos?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.contrato.id_sistema, 0, 0, 0, [])
								.then(function(data){
									var contactosRecibidos = angular.copy(data.registros);
									console.log('Contactos: ', contactosRecibidos.length);
									$scope.contactos = [];
									if(contactosRecibidos.length>0){
										if(contactosRecibidos.length>1){
											for(var i=0; i<contactosRecibidos.length; i++){
												$scope.contactos.push({"cuerpo": contactosRecibidos[i].nombre+', '+contactosRecibidos[i].apellidos+' - '
													+contactosRecibidos[i].telefono+' - '+contactosRecibidos[i].movil+' - '+contactosRecibidos[i].correo,
													"notas": contactosRecibidos[i].notas});
											}
										}else{
											$scope.contactos.push({"cuerpo": contactosRecibidos[0].nombre+', '+contactosRecibidos[0].apellidos+' - '
													+contactosRecibidos[0].telefono+' - '+contactosRecibidos[0].movil+' - '+contactosRecibidos[0].correo,
													"notas": contactosRecibidos[0].notas});
										}
									}else{
										$scope.contactos = undefined;
									}
									//Una vez que tenemos todos los datos del contrato, mostramos el cuadro dentro del formulario de incidencias.
									$scope.mostrarContrato = true;
								})
								.catch(function(error){
									if(error.status==401){$rootScope.relogin();}
									$scope.currentError = parseError(error.status, error.data);
									$timeout(function(){$scope.currentError = null;}, 15000);
								});
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.currentError = parseError(error.status, error.data);
							$timeout(function(){$scope.currentError = null;}, 15000);
						});
				}else{
					$scope.mostrarContrato = true;
				}
			})
			.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
			});
		
		//Hacemos la llamada para traer las llaves del sistema.
		dataService
			.getFiltro('llaves?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, [])
			.then(function(data){
				if(data.registros.length != 0){
					$scope.llavesContrato = angular.copy(data.registros);
					console.log('Llaves del sistema (IncidenciasFormCntrl): ', $scope.llavesContrato.length);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que trae los tipos de incidencia de la tabla de configuraciones.
	var traerTipoIncidencia = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.tiposIncidencia = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				var tipos = data.registros[0].valor.split(",");
				var obj = {};
				for(var i=0; i<tipos.length; i++){
					obj = {};
					obj.nombre = tipos[i];
					obj.id = (i+1);
					$scope.tiposIncidencia.push(obj);
				}
				if($scope.inciSis.tipo != ''){
					for(var i=0; i<$scope.tiposIncidencia.length; i++){
						if($scope.tiposIncidencia[i].nombre == $scope.inciSis.tipo){
							$scope.inciSis.tipo = $scope.tiposIncidencia[i].id;
							break;
						}
					}
				}
				console.log('Traigo los tipos de incidencias (SistemasInciCntrl): ', $scope.tiposIncidencia);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}

	//Función que comprueba si una incidencia está fuera de tiempo.
	$scope.comprobarInciSis = function(incidencia){
		if(incidencia.status=='CERRADA'){
			return 2;
		}else{
			var hoy = new Date();
			//Parseo la fecha de la caducidad para operar con ella como una fecha.
			var fecha = new Date(incidencia.cad.replace(/-/g, "/"));
			if(hoy.getTime() > fecha.getTime() && incidencia.fin == null){
				return 1;
			}else{
				return 0;
			}
		}
	}
	
	//Función que manda orden para cambiar de vista y manda los datos para dicha vista.
	$scope.irInci = function(incidencia){		
		var data = {
			"incidencia" : incidencia,
			"sistema" : $scope.sis
		};
		$rootScope.$broadcast('llamadaIncidencias', data);
		$rootScope.$broadcast('cerrarIncidencias', false);
	}
	
	//Función que abre el formulario para añadir una nueva instalación.
	$scope.newInciSis = function(){
		$scope.nuevaInci = true;
		$scope.inciSis = {};
		$scope.inciSis.referencia = "IC"+crearReferencia();
		$scope.inciSis.id_sistema = $scope.sis.id;
		$scope.inciSis.sistema = $scope.sis.nombre;
		$scope.titulo = 'Formulario Añadir';
		traerTipoIncidencia('configuracion?filter=id%3E', 0, "%26%26parametro='tipoIncidencia'", 0, 0, 0, []);
		traerContrato('contrato?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
	}
	
	//Función que añade la instalación nueva al sistema.
	$scope.add = function(){
		for(var i=0; i<$scope.tiposIncidencia.length; i++){
			if($scope.tiposIncidencia[i].id == $scope.inciSis.tipo){
				$scope.inciSis.tipo = $scope.tiposIncidencia[i].nombre;
				break;
			}
		}
		$scope.inciSis.status = putStatus($scope.status);
		var hoy = new Date();
					
		$scope.inciSis.inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
								+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
		//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
		var tiempo = hoy.getTime();
		var mil = parseInt(2*24*60*60*1000);
		var fin = new Date();
		fin.setTime(tiempo+mil);
		$scope.inciSis.cad = ""+fin.getFullYear()+'-'+twoDigits((fin.getMonth()+1))+'-'+twoDigits(fin.getDate())+''
							+twoDigits(fin.getHours())+':'+twoDigits(fin.getMinutes())+":00";
		$scope.inciSis.referencia = "IC"+crearReferencia();
				
		dataService
			.postElements('incidencia', $scope.inciSis)
			.then(function(data){
				console.log('Incidencia insertada: ', data);
				$scope.resetFormInciSis();
				traerInciSis('incidencias?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id+'&order=cad%20DESC', 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormInci = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormInci = null;}, 15000);
			});		
	}
	
	//Función que traduce los números del select a texto de los tipos.
	var putStatus = function(status){
		switch(parseInt(status)){
			case 1:
				return 'ABIERTA';
				break;
				
			case 2:
				return 'PENDIENTE';
				break;
				
			case 3:
				return 'CERRADA';
				break;
		}
	}
	
	//Función que comprueba el tipo que se pone y modifica el campo registro policial.
	$scope.registro = function(tipo){
		if(tipo == 1 || tipo == 2){
			$scope.inciSis.registro_policia = true;
		}else{
			$scope.inciSis.registro_policia = false;
		}
	}
	
	//Función que resetea los valores del formulario.
	$scope.resetFormInciSis = function(){
		$scope.errorFormInciSis = null;
		$scope.inciSis = {};
		$scope.nuevaInci = false;
		$scope.formInciSis.$setPristine();
	}
	
	//Función qu emanda orden de cerrar todo.
	$scope.cerrar = function(){
		$scope.resetFormInciSis();
		$scope.errorInciSis = null;
		$rootScope.$broadcast('cerrarIncidencias', false);
	}
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('mostrar', function(e,data){
		$scope.mostrar = data;
		$scope.nuevo = data;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('editar', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.sistemas = $scope.gridOptionsComplex.data;
		$scope.sistemaSel = $scope.sis.nombre;
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerInciSis('incidencias?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id+'&order=cad%20DESC', 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('recall', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.sistemas = $scope.gridOptionsComplex.data;
		$scope.sistemaSel = $scope.sis.nombre;
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerInciSis('incidencias?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id+'&order=cad%20DESC', 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del hermano para cerrar la parte de edición.
	$scope.$on('cerrarTodo', function(e,data){
		$scope.resetFormInciSis();
		$scope.errorInciSis = null;
	});
	
	//Funciones para cerrar los mensajes de error.
	$scope.clearErrorFormInciSis = function(){
		$scope.errorFormInciSis = null;
	}
	$scope.clearErrorInciSis = function(){
		$scope.errorInciSis = null;
	}
}