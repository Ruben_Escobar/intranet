/****************************************************************************/
/*			Controlador de la vista de las Llaves de cada sistema			*/
/****************************************************************************/
App.controller('SistemasLlavesCntrl', SistemasLlavesCntrl);
function SistemasLlavesCntrl($scope, $rootScope, $timeout, focus, uiGridConstants, dataService){
	
	$scope.errorFormLlaveSis = null;
	$scope.errorLlaveSis = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasLlavesCntrl ------------------------------');
	
	
	//Función que retorna todas las incidencias del sistema.
	var traerLlavesSis = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.lavesFinal = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.llavesFinal = angular.copy(data.registros);
				console.log('Laves del sistema cargadas (SistemasInciCntrl)');
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorInciSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInciSis = null;}, 15000);
			});
	}
	
	//Función que abre el formulario para añadir una nueva llave.
	$scope.newLlaveSis = function(){
		$scope.llaveSis = {};
		//Creamos la referencia ejemplo de la llave, la cual está formada por la letra 'S' más la id del sistema y la letra L más el número de llave que es.
		$scope.llaveSis.referencia = "S"+$scope.sis.id+"L"+crearReferencia();
		$scope.llaveSis.id_sistema = $scope.sis.id;
		$scope.llaveSis.sistema = $scope.sis.nombre;
		$scope.nuevaLlave = true;
		$scope.boton = 'Añadir';
		$scope.titulo = 'Formulario Añadir';
	}
	
	//Funciónq ue abre el formulario pero en forma de edición.
	$scope.editLlave = function(llave){
		$scope.llaveSis = angular.copy(llave);
		$scope.llaveSis.sistema = $scope.sis.nombre;
		$scope.nuevaLlave = true;
		$scope.boton = 'Modificar';
		$scope.titulo = 'Formulario Editar';
	}
	
	//Función que añade la instalación nueva al sistema.
	$scope.add = function(){
		//Comprobamos si es una edición o una inserción.
		if($scope.llaveSis.id){
			dataService
				.putElements('llaves', $scope.llaveSis)
				.then(function(data){
					console.log('Llave insertada: ', data);
					$scope.resetFormLlaveSis();
					traerLlavesSis('llaves?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormLlaveSis = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormLlaveSis = null;}, 15000);
				});
		}else{
			dataService
				.postElements('llaves', $scope.llaveSis)
				.then(function(data){
					console.log('Llave insertada: ', data);
					$scope.resetFormLlaveSis();
					traerLlavesSis('llaves?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormLlaveSis = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormLlaveSis = null;}, 15000);
				});
		}		
	}
	
	//Función que eliminar una llave del sistema.
	$scope.delLlave = function(){
		dataService
			.deleteElements('llaves', '?filter=id%3D'+$scope.llaveSis.id)
			.then(function(data){
				console.log('Llave eliminada: ', data);
				$scope.resetFormLlaveSis();
				traerLlavesSis('llaves?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormLlaveSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormLlaveSis = null;}, 15000);
			});
	}
	
	//Función que resetea los valores del formulario.
	$scope.resetFormLlaveSis = function(){
		$scope.errorFormLlaveSis = null;
		$scope.llaveSis = {};
		$scope.nuevaLlave = false;
		$scope.formLlaveSis.$setPristine();
	}
	
	//Función qu emanda orden de cerrar todo.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarLlaves', false);
	}
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('mostrar', function(e,data){
		$scope.mostrar = data;
		$scope.nuevo = data;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('editar', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerLlavesSis('llaves?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('recall', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerLlavesSis('llaves?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre para cerrar la parte de edición.
	$scope.$on('cerrarTodo', function(e,data){
		$scope.resetFormLlaveSis();
		$scope.errorLlaveSis = null;
	});
	
	//Funciones para cerrar los mensajes de error.
	$scope.clearErrorFormLlaveSis = function(){
		$scope.errorFormLlaveSis = null;
	}
	$scope.clearErrorLlaveSis = function(){
		$scope.errorLlaveSis = null;
	}
	
}