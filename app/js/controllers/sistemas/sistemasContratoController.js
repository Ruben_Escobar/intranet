/****************************************************************************/
/*			Controlador de la vista de los Contratos de cada sistema		*/
/****************************************************************************/
App.controller('SistemasContratoCntrl', SistemasContratoCntrl);
function SistemasContratoCntrl($scope, $rootScope, $timeout, ngDialog, dataService){
	
	$scope.editando = false;
	$scope.progressValue = 0;
	$scope.errorFormConSis = null;
	$scope.errorFormSistemas = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasContratoCntrl ------------------------------');
	
	//Función que trae los datos del contrato/s y los servicios asociados al sistema
	var traerContratos = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.contratosFinal = [];
		//Hacemos la petición del contrato/s del sistema.
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.registros.length>0){
					$scope.contratosFinal = angular.copy(data.registros);
					
					for(var i=0; i<$scope.contratosFinal.length; i++){								
						var hoy = new Date();
						var fechaCaduca = new Date($scope.contratosFinal[i].caduca);
						//Comprobamos si está caducado o no el contrato.
						if(hoy.getTime() >= fechaCaduca.getTime()){
							$scope.contratosFinal[i].caducado = true;
						}else{
							$scope.contratosFinal[i].caducado = false;
						}
					}
					console.log('Contratos del sistema cargados.');
				}else{
					$scope.contratosFinal = undefined;
				}
			})			
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorContratoSistemas = parseError(error.status, error.data);
				$timeout(function(){$scope.errorContratoSistemas = null;}, 15000);
			});
	}
	
	//Función que retorna todos los servicios (Duplicados, por que es la vista que combina tablas).
	var traerSer = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.serviciosContratos = [];
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.serviciosContratos = angular.copy(data.registros);
				console.log('Listado de servicios (SistemasContratoCntrl)');
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorContratoSistemas = parseError(error.status, error.data);
				$timeout(function(){$scope.errorContratoSistemas = null;}, 15000);
			});
	}
	
	//Función que retorna todos los servicios de la lista original.
	var traerSerTotal = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.serviciosTotal = [];
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.serviciosTotal = angular.copy(data.registros);
				console.log('Listado de serviciosTotal (SistemasContratoCntrl)');
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorContratoSistemas = parseError(error.status, error.data);
				$timeout(function(){$scope.errorContratoSistemas = null;}, 15000);
			});
	}
	
	//Función que retorna los servicios del contrato.
	var traerSerCont = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.serviciosContrato = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.serviciosContrato = angular.copy(data.registros);
				console.log('Traigo servicios del contrato (SistemasContratoCntrl)');
				comprobarServicios();
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorContratoSistemas = parseError(error.status, error.data);
				$timeout(function(){$scope.errorContratoSistemas = null;}, 15000);
			});
	}
	
	//Función que trae los presupuestos.
	var traerPresupuestos = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.buffer = true;
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total presupuestos (SistemasContratoCntrl): ', data.registros.length);
					$scope.presupuestos = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 100;
				}else{
					$scope.progressValue = data.progressValue;
					traerClientes(data.tabla, data.registros[data.registros.length-1].id, data.acumulados, data.progressValue, data.total, data.registros)
				}
			})
			.catch(function(Error){
				
			});
	}
	
	//Comprobamos los servicios contratados y los ponemos activos en el listado.
	var comprobarServicios = function(){
		for(var i=0; i<$scope.serviciosTotal.length; i++){
			$scope.serviciosTotal[i].asignado = false;
		}
		//Recorremos el array que contiene todos los servicios que existen
		//y seleccionamos los que tiene asignados el contrato en este momento.
		for(var i=0; i<$scope.serviciosTotal.length; i++){
			for(var j=0; j<$scope.serviciosContrato.length; j++){
				if($scope.serviciosTotal[i].id == $scope.serviciosContrato[j].id_servicio){
					$scope.serviciosTotal[i].asignado = true;
				}
			}
		}
	}
	
	//Función que abre el formulario de edición del contrato.
	$scope.editarContSis = function(contrato){
		$scope.editando = true;
		$scope.contratoSistema = angular.copy(contrato);
		$scope.contratoSistema.caduca = new Date($scope.contratoSistema.caduca);
		$scope.contratoSistema.inicio = new Date($scope.contratoSistema.inicio);
		$scope.boton = 'Modificar Contrato';
		$scope.titulo = 'Formulario Editar';
		traerSerCont('servicios?filter=id_servicio%3E', 0, '%26%26id_contrato%3D'+contrato.id, 0, 0, 0, []);
	}
	
	//Nuevo contrato del sistema
	$scope.newConSis = function(){
		$scope.contratoSistema = {};
		$scope.contratoSistema.ref = "C"+crearReferencia();
		$scope.contratoSistema.inicio = new Date();
		$scope.contratoSistema.id_sistema = $scope.sis.id;
		$scope.editando = true;
		$scope.sistemaSel = $scope.sis.nombre;
		$scope.boton = 'Añadir Contrato';
		$scope.titulo = 'Formulario Añadir';
		traerSerTotal('servicio?filter=id%3E', 0, 0, 0, 0, []);
		focus('mainFocusForm');
	}
	
	//Función que resetea el formulario y lo cierra.
	$scope.resetformConSis = function(){
		$scope.editando = false;
		$scope.contratoSistema = {};
		$scope.formConSis.$setPristine();
		$scope.desplegarUl = false;
		traerSer('servicios?filter=id_servicio%3E', 0, 0, 0, 0, []);
	}
	
	//Función para añadir el registro
	$scope.add = function(){
		var hayServicios = false;
		//Primero comprobamos que hay seleccionado algún servicio.
		for(var i=0; i<$scope.serviciosTotal.length; i++){
			if($scope.serviciosTotal[i].asignado == true){
				hayServicios = true;
				break;
			}
		}
		//Si no hay seleccionados, mostramos un error.
		if(!hayServicios){
			$scope.errorformConSis = 'Seleccione algún servicio para el contrato.';
			$timeout(function(){$scope.errorformConSis = null;}, 15000);
		}else{
			$scope.contratoSistema.caduca = parseFecha($scope.contratoSistema.caduca);
			$scope.contratoSistema.inicio = parseFecha($scope.contratoSistema.inicio);
			//Comprobamos si es una edición o una inserción y procedemos según corresponda.
			if($scope.contratoSistema.id==undefined){		
				//Inserción.
				dataService
					.postElements('contrato', $scope.contratoSistema)
					.then(function(data){
						console.log('Contrato insertado: ', data);
						eliminarServicios(data.id);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormContrato = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormContrato = null;}, 15000);
					});
			}else{
				//Edición.
				dataService
					.putElements('contrato', $scope.contratoSistema)
					.then(function(data){
						console.log('Contrato modificado:', data);
						eliminarServicios(data.id);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormContrato = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormContrato = null;}, 15000);
					});
			}
		}
	}
	
	//Función que elimina las inserciones de la tabla servicio_contrato de un contrato en concreto (id).
	var eliminarServicios = function(id){
		dataService
			.deleteElements('servicio_contrato', '?filter=id_contrato='+id)
			.then(function(data){
				console.log('Servicios de contrato '+id+' eliminados.');
				addServicios(id);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormContrato = null;}, 15000);
			});
	}
	
	//Función que añade los servicios de la lista de seleccionados a la tabla servicio_contrato para crear el enlace.
	var addServicios = function(id){
		var serviciosAdd = [];
		//El for empieza en 1 para saltarse la primera posición, que es la que utilizamos para traer el id del contrato.
		for(var i=0; i<$scope.serviciosTotal.length; i++){
			if($scope.serviciosTotal[i].asignado != undefined && $scope.serviciosTotal[i].asignado == true){
				serviciosAdd.push({
					'id_servicio': $scope.serviciosTotal[i].id,
					'id_contrato': id
				});
			}
		}
		dataService
			.postElements('servicio_contrato', serviciosAdd)
			.then(function(data){
				console.log('Servicio de contrato insertado: ', data);
				traerContratos('contratos?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
				$scope.resetformConSis();
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormContrato = null;}, 15000);
			});
	}
	
	//Abrimos un diálogo con el listado de presupuestos para seleccionar el correspondiente
	$scope.abrirPresupuestos = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogPresupuestos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (presupuesto) {
			$scope.contratoSistema.presupuesto = presupuesto.referencia;
			$scope.filtro2 = '';
	    }, function (reason) {
	    	console.log('Modal promise rejected. Reason: ', reason);
	    });
	}
	
	//Función que cierra las vistas del formulario.
	$scope.cerrar = function(){
		$scope.resetformConSis();
		$scope.nuevo = false;
		$scope.mostrar = false;
		$rootScope.$broadcast('cerrarContratos', false);
	}
	
	//Función que elimina el contrato del Sistema.
	$scope.delContratoSistema = function(id){
		dataService
			.deleteElements('servicio_contrato', '?filter=id_contrato%3D'+id)
	    	.then(function(data){
		    	console.log('Servicios del contrato '+id+' eliminados.');
		    	dataService
		    		.deleteElements('contrato', '?filter=id%3D'+id)
		    		.then(function(data){
			    		console.log('Contrato '+id+' eliminado.');
			    		$scope.resetformConSis();
		        	})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormContrato = parseError(serror.tatus, error.data);
						$timeout(function(){$scope.errorFormContrato = null;}, 15000);
					});
			})
			.catch(function(data, status){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormContrato = null;}, 15000);
			});
	}
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('mostrar', function(e,data){
		$scope.mostrar = data;
		$scope.nuevo = data;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('editar', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.sistemas = $scope.gridOptionsComplex.data;
		$scope.sistemaSel = $scope.sis.nombre;
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerContratos('contratos?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
		traerSer('servicios?filter=id_servicio%3E', 0, 0, 0, 0, []);
		traerSerTotal('servicio?filter=id%3E', 0, 0, 0, 0, []);
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('recall', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.sistemas = $scope.gridOptionsComplex.data;
		$scope.sistemaSel = $scope.sis.nombre;
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerContratos('contratos?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
		traerSer('servicios?filter=id_servicio%3E', 0, 0, 0, 0, []);
		traerSerTotal('servicio?filter=id%3E', 0, 0, 0, 0, []);
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del hermano y manda la orden de cerrar al siguiente hermano.
	$scope.$on('cerrarTodo', function(e, data){
		$scope.resetformConSis();
		$scope.nuevo = false;
		$scope.mostrar = false;
	});
	
	//Funciones para cerrar los mensajes de error.
	$scope.clearErrorFormConSis = function(){
		$scope.errorFormConSis = null;
	}
	$scope.clearErrorFormSistemas = function(){
		$scope.errorFormSistemas = null;
	}
}