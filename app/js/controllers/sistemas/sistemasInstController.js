/****************************************************************************/
/*		Controlador de la vista de las Instalaciones de cada sistema		*/
/****************************************************************************/
App.controller('SistemasInstCntrl', SistemasInstCntrl);
function SistemasInstCntrl($scope, $rootScope, $timeout, ngDialog, dataService){
	
	var fasesAux = [];
	var fasesOut = [];
	
	$scope.errorFormInstSis = null;
	$scope.errorInstSis = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasInstCntrl ------------------------------');
	
	
	//Función que retorna todas las instalaciones del sistema.
	var traerInstSis = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.instalacionesFinal = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.instalacionesFinal = angular.copy(data.registros);
				console.log('Instalaciones del sistema cargadas (SistemasInstCntrl)');
				var ids = [];
				for(var i=0; i<$scope.instalacionesFinal.length; i++){
					ids.push($scope.instalacionesFinal[i].id);
					$scope.instalacionesFinal[i].colapsar = true;
				}
				console.log($scope.instalacionesFinal);
				fasesFuera($scope.instalacionesFinal);
				traerFases(ids);
				traerPendientes(ids);
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorInstSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInstSis = null;}, 15000);
			});
	}
	
	//Función que comprueba las fases fuera de fecha.
	var fasesFuera = function(instalaciones){
		var hoy = new Date();
		hoy = "'"+hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()+" "+hoy.getHours()+"%3A"+hoy.getMinutes()+"%3A00'";
		dataService
			.getFiltro('fase_ins?filter=id%3E', 0, '%26%26limite%3C'+hoy+'%26%26fin%20IS%20NULL', 0, 0, 0, [])
			.then(function(data){
				fasesOut = data.registros;
				console.log('Total de registros (fasesOut InstalacionesCntrl): ', fasesOut.length);
				for(var i=0; i < instalaciones.length; i++){
					for(var j=0; j<fasesOut.length; j++){
						if(instalaciones[i].id == fasesOut[j].id_instalacion){
							instalaciones[i].out_time = 1;
							break;
						}else{
							instalaciones[i].out_time = 0;
						}
					}
				}
				$scope.instalacionesFinal = angular.copy(instalaciones);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFasesIns = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFasesIns = null;}, 15000);
			});
	}
	
	//Intercalar fases de cada instalación.
	var intercalarFases = function(){
		for(var i=0; i<$scope.instalacionesFinal.length; i++){
			for(var j=0; j<$scope.fasesInst.length; j++){
				if($scope.instalacionesFinal[i].id == $scope.fasesInst[j].id_instalacion && $scope.fasesInst[j].inicio!=undefined && $scope.fasesInst[j].fin==undefined){
					$scope.instalacionesFinal[i].faseActual = $scope.fasesInst[j].nombre;
					var aux = i;
					dataService
						.getFiltro('fase_ins_task?filter=id%3E', 0, '%26%26id_fase_ins%3D'+$scope.fasesInst[j].id, 0, 0, 0, [])
						.then(function(data){
							$scope.instalacionesFinal[aux].tareas = data.registros;
							console.log('Tareas recuperadas: ', data.registros);
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.currentError = parseError(error.status, error.data);
							$timeout(function(){$scope.currentError = null;}, 15000);
						});
					break;
				}
			}
		}
	}
	
	//Función que retorna todas las fases de las instalaciones del sistema. Tabla fase_ins
	var traerFases = function(ids){
		$scope.fasesInst = [];
		var srt = '';
		for(var i=0; i<ids.length; i++){
			if(i==0){
				srt = srt+'%26%26id_instalacion%3D'+ids[i];
			}else{
				srt = srt+'%7C%7Cid_instalacion%3D'+ids[i];
			}
		}	
		dataService
			.getFiltro('fase_ins?filter=id%3E', 0, srt, 0, 0, 0, [])
			.then(function(data){
				console.log('Total de fases de las Instalaciones (SistemasInstCntrl): ', data.registros.length);
				$scope.fasesInst = angular.copy(data.registros);
				intercalarFases();
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que retorna todas las pendientes de las instalaciones del sistema.
	var traerPendientes = function(ids){
		$scope.fasesInst = [];
		var srt = '';
		for(var i=0; i<ids.length; i++){
			if(i==0){
				srt = srt+'%26%26id_instalacion%3D'+ids[i];
			}else{
				srt = srt+'%7C%7Cid_instalacion%3D'+ids[i];
			}
		}	
		dataService
			.getFiltro('pendientes?filter=id%3E', 0, srt, 0, 0, 0, [])
			.then(function(data){
				console.log('Total de pendientes de las Instalaciones (SistemasInstCntrl): ', data.registros.length);
				$scope.pendInst = angular.copy(data.registros);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Traemos las fases Master para añadirlas a la tabla fase_ins como fases por defecto de la instalación.
	var traerFasesMaster = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				fasesAux = angular.copy(data.registros);
				console.log('Fases Master recuperadas (SistemasInstCntrl): ', fasesAux.length);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormIns = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormIns = null;}, 15000);
			});
	}
	traerFasesMaster('fase?filter=id%3E', 0, 0, 0, 0, []);
	
	var traerFasesOut = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		//Llamo a las fases que estén fuera de tiempo de la instalación que se nos facilita.
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				console.log('Fases fuera de tiempo (SistemasInstCntrl): ', data.registros.length);
				$scope.fasesOut = angular.copy(data.registros);
			})
			.catch(function(error){
				if(status==401){$rootScope.relogin();}
				$scope.errorInstSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInstSis = null;}, 15000);
			});
	}
	var hoy = new Date();
	hoy = "'"+hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()+" "+hoy.getHours()+"%3A"+hoy.getMinutes()+"%3A00'";
	traerFasesOut('fase_ins?filter=id%3E', 0, '%26%26limite%3C'+hoy+'%26%26fin%20IS%20NULL', 0, 0, 0, []);
	
	//Función que trae los presupuestos.
	var traerPresupuestos = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.buffer = true;
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total presupuestos (SistemasContratoCntrl): ', data.registros.length);
					$scope.presupuestos = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 100;
				}else{
					$scope.progressValue = data.progressValue;
					traerClientes(data.tabla, data.registros[data.registros.length-1].id, data.acumulados, data.progressValue, data.total, data.registros)
				}
			})
			.catch(function(Error){
				
			});
	}
		
	//Función que manda orden para cambiar de vista y manda los datos para dicha vista.
	$scope.irInst = function(instalacion){		
		var data = {
			"instalacion" : instalacion,
			"sistema" : $scope.sis
		};
		$rootScope.$broadcast('llamadaInstalaciones', data);
	}
	
	//Función que abre el formulario para añadir una nueva instalación.
	$scope.newInstSis = function(){
		$scope.nuevaInst = true;
		$scope.instSis = {};
		$scope.instSis.referencia = "I"+crearReferencia();
		$scope.instSis.id_sistema = $scope.sis.id;
		$scope.instSis.sistema = $scope.sis.nombre;
		$scope.titulo = 'Formulario Añadir';
	}
	
	//Función que comprueba si una instalación está
	$scope.comprobarInstSis = function(instalacion){
		if($scope.fasesOut){
			for(var i=0; i<$scope.fasesOut.length; i++){
				if($scope.fasesOut[i].id_instalacion == instalacion.id){
					return 1;
				}
			}
		}
		if($scope.pendInst){
			for(var j=0; j<$scope.pendInst.length; j++){
				if($scope.pendInst[j].id_instalacion == instalacion.id){
					return 1;
				}
			}
		}
		if(instalacion.fin == undefined || instalacion.fin == null){
			return 0;
		}else{
			return 2;
		}
	}
	
	//Función que añade la instalación nueva al sistema.
	$scope.add = function(){
		var hoy = new Date();
		$scope.instSis.inicio = parseFechaHora(hoy);
		dataService
			.postElements('instalacion', $scope.instSis)
			.then(function(data){
				console.log('Instalación del sistema insertada (SistemasInstCntrl): ', data.id);
				var id = data.id;
				var fasesSel = [];
				var hoy = new Date();
				//Recoremos el array de fases master cargadas anteriormente.
				for(var i=0; i<fasesAux.length; i++){
					//Creamos los objetos de la tabla fase_ins mediante los de la tabla fase (fases master).
					fasesSel[i] = {
						"id_instalacion": id,
						"nombre": fasesAux[i].nombre,
						"descripcion": fasesAux[i].descripcion,
						"caducidad": fasesAux[i].caducidad
					};
				}				
				//Insertamos las fases por defecto de la instalación.
				dataService
					.postElements('fase_ins', fasesSel)
					.then(function(data){
						console.log('Fases Base añadidas a la instalación '+id+' (SistemasInstCntrl).');
						//Una vez insertadas, las recuperamos para modificarlas el campo orden.
						dataService
							.getFiltro('fase_ins?filter=id%3E', 0, '%26%26id_instalacion='+id, 0, 0, 0, [])
							.then(function(data){
								console.log('Recuperadas las fases de la instalación '+id+' (SistemasInstCntrl).');
								fasesSel = [];
								fasesSel = angular.copy(data.registros);
								//Procesamos los datos de la consulta get para poner los campos orden en su valor edacuado.
								//Iniciamos la primera fase poniendole como fecha de inicio la fecha actual
								//y como limite la fecha inicial mas la caducidad.
								for(var i=0; i<fasesSel.length; i++){
									if(i==0){
										fasesSel[i].inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
															+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
										//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
										var tiempo = hoy.getTime();
										var mil = parseInt(fasesSel[i].caducidad*24*60*60*1000);
										var fin = new Date();
										fin.setTime(tiempo+mil);
										fasesSel[i].limite = ""+fin.getFullYear()+'-'+twoDigits(fin.getMonth()+1)+'-'+twoDigits(fin.getDate())+' '
															+twoDigits(fin.getHours())+':'+twoDigits(fin.getMinutes())+":00";
										fasesSel[i].orden = 0;
									}else{
										fasesSel[i].orden = fasesSel[(i-1)].id;
									}
								}
								//Insertamos el total de las fases.
								dataService
									.putElements('fase_ins', fasesSel)
									.then(function(data){
										console.log('Fases de la instalación '+id+' modificadas (SistemasInstCntrl).');
										//Una vez terminadas todas las inserciones de manera correcta, reseteamos la lista de instalaciones y el formulario.
										traerInstSis('instalaciones?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.instSis.id_sistema, 0, 0, 0, []);
										$scope.resetFormInstSis();
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormInstSis = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormInstSis = null;}, 15000);
									});
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormInstSis = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormInstSis = null;}, 15000);
							});
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormInstSis = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormInstSis = null;}, 15000);
					});
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormInstSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormInstSis = null;}, 15000);
			});
	}
	
	//Abrimos un diálogo con el listado de presupuestos para seleccionar el correspondiente
	$scope.abrirPresupuestos = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogPresupuestos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (presupuesto) {
			$scope.instSis.presupuesto = presupuesto.referencia;
			$scope.filtro2 = '';
	    }, function (reason) {
	    	console.log('Modal promise rejected. Reason: ', reason);
	    });
	}
	
	//Función que resetea los valores del formulario.
	$scope.resetFormInstSis = function(){
		$scope.errorFormInstSis = null;
		$scope.instSis = {};
		$scope.nuevaInst = false;
		$scope.formInstSis.$setPristine();
	}
	
	//Función qu emanda orden de cerrar todo.
	$scope.cerrar = function(){
		$scope.resetFormInstSis();
		$scope.errorInstSis = null;
		$rootScope.$broadcast('cerrarInstalaciones', false);
	}
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('mostrar', function(e,data){
		$scope.mostrar = data;
		$scope.nuevo = data;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('editar', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.sistemas = $scope.gridOptionsComplex.data;
		$scope.sistemaSel = $scope.sis.nombre;
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerInstSis('instalaciones?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('recall', function(e,data){
		$scope.sis = angular.copy(data);
		$scope.sistemas = $scope.gridOptionsComplex.data;
		$scope.sistemaSel = $scope.sis.nombre;
		$scope.mostrar = true;
		$scope.nuevo = false;
		traerInstSis('instalaciones?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('cerrarTodo', function(e,data){
		$scope.resetFormInstSis();
		$scope.errorInstSis = null;
	});
	
	//Funciones para cerrar los mensajes de error.
	$scope.clearErrorFormInstSis = function(){
		$scope.errorFormInstSis = null;
	}
	$scope.clearErrorInstSis = function(){
		$scope.errorInstSis = null;
	}
}