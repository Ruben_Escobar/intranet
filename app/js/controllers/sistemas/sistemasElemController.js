/****************************************************************************/
/*			Controlador de la vista de los Elementos de cada sistema		*/
/****************************************************************************/
App.controller('SistemasElemCntrl', SistemasElemCntrl);
function SistemasElemCntrl($scope, $rootScope, $timeout, ngDialog, focus, uiGridConstants, dataService, printer, emailSender){
	
	$scope.campoAdd = false;
	$scope.elementosConfig = [];
	$scope.camposI = [];
	$scope.camposD = [];
	$scope.urlsI = [];
	$scope.urlsD = [];
	$scope.cambios = 0;
	$scope.errorElemSis = null;
	$scope.errorCampo = null;
	
	var elementoMover = {};
	var elementosListado = [];
	var copiaElemento = [];
	var indice = 1;
		
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.notas}} tooltip-placement="top">{{row.entity.notas}}</div>';
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasElemCntrl ------------------------------');
	
	//Función que trae los elementos del sistema.
	var traerElementos = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.elementos = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				//Indicamos que el arbol de elementos lo creará la función crearArbol.
				elementosListado = angular.copy(data.registros);
				$scope.elementos = [{'id': 0, 'title':'Configuración', 'nodes': crearArbol(null, data.registros)}];
				//Dejamos un segundo para que se realicen las operaciones de creación del árbol.
				//Una vez pasado ese tiempo, inicializamos la variable que se encarga de contar los cambios realizados en el mismo.
				$timeout(function(){$scope.cambios = 0;$scope.buffer3 = false;}, 1000);
				//Nos encargaremos de observar si el elemento 'elementos' cambia, y en tal caso incrementaremos la variable.
				//Añadimos un retardo en el watch para que de tiempo a las funciones a actuar sobre el elemento y borrar el rastro
				//en caso de que no sea necesario guardar cambios de posición en el árbol.
				$scope.$watch('elementos', function(newVal, oldVal){$timeout(function(){$scope.cambios++;}, 50);}, true);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorElemSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorElemSis = null;}, 15000);
			});
	}
	
	//Función que crea la lista de plantillas.
	var traerPlantillas = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.plantillas = data.registros;
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorElemSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorElemSis = null;}, 15000);
			});
	}
	traerPlantillas('elemento?filter=id%3E', 0, '%26%26plantilla%3D1', 0, 0, 0, []);
	
	//Función recursiva que recorre el array de todos los elementos y llama a los hijos.	
	var crearArbol = function(idPadre, arrayTotal){
		var children = [];
		var objAux = [];
		for(var i=0; i<arrayTotal.length; i++){
			//JSON.JSON.stringify(); para pasar a texto JSON.
			arrayTotal[i].camposAux = JSON.parse(arrayTotal[i].campos);
			arrayTotal[i].urlsAux = JSON.parse(arrayTotal[i].urls);
				
			//Dividimos los campos a mostrar en el formulario en dos columnas.
			for(var j=0; j<arrayTotal[i].camposAux.length; j++){
			    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
			    if(j>1){
				    arrayTotal[i].camposAux[j].clase = true;
				}else{
				    arrayTotal[i].camposAux[j].clase = false;
			    }
			}
			for(var j=0; j<arrayTotal[i].urlsAux.length; j++){
			    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
			    if(j>1){
				    arrayTotal[i].urlsAux[j].clase = true;
				}else{
				    arrayTotal[i].urlsAux[j].clase = false;
			    }
			}
			
			//En la primera llamada a la función le indicamos como idPadre null, de esta manera colocamos las raices de los árboles.
			if(idPadre == null){
				if(arrayTotal[i].id_elemento == idPadre){
					objAux.splice(objAux.length, 0,
						{
							id:		arrayTotal[i].id,
							title:	arrayTotal[i].nombre,
							data:	{
								id: arrayTotal[i].id,
								id_elemento: arrayTotal[i].id_elemento,
								id_sistema: arrayTotal[i].id_sistema,
								nombre: arrayTotal[i].nombre,
								fichaTexto: arrayTotal[i].fichaTexto,
								fichaURL: arrayTotal[i].fichaURL,
								plantilla: arrayTotal[i].plantilla,
								campos: arrayTotal[i].campos,
								camposAux: arrayTotal[i].camposAux,
								urls: arrayTotal[i].urls,
								urlsAux: arrayTotal[i].urlsAux
							},
							nodes:	crearArbol(arrayTotal[i].id, arrayTotal)
						}
					);
				}
			//En las sucesivas llamadas, les pasamos el id del elemento padre, por lo que buscaran las ramas del árbol hasta llegar a las hojas.
			}else{
				if(arrayTotal[i].id_elemento == idPadre){
					objAux = {
						id:		arrayTotal[i].id,
						title:	arrayTotal[i].nombre,
						data:	{
							id: arrayTotal[i].id,
							id_elemento: arrayTotal[i].id_elemento,
							id_sistema: arrayTotal[i].id_sistema,
							nombre: arrayTotal[i].nombre,
							fichaTexto: arrayTotal[i].fichaTexto,
							fichaURL: arrayTotal[i].fichaURL,
							plantilla: arrayTotal[i].plantilla,
							campos: arrayTotal[i].campos,
							camposAux: arrayTotal[i].camposAux,
							urls: arrayTotal[i].urls,
							urlsAux: arrayTotal[i].urlsAux
						},
						nodes:	crearArbol(arrayTotal[i].id, arrayTotal)
					};
					children.splice(children.length, 0, objAux);
				}
			}
		}
		//Si alguna de las dos arrays de control tienen datos las devolvemos, si no, devolvemos un array vacío.
		if(objAux.length>0){
			return objAux;
		}else if(children.length>0){
			return children;
		}else{
			return [];
		}
	}
	
	/* ------------------------------ Operaciones con los elementos ------------------------------*/
    
	//Función que muestra la información del elemento seleccionado en el momento.
	$scope.informacion = function(data){
		$scope.elemento = data.$nodeScope.$modelValue.data;
	    $scope.elemEdit = false;
	    $scope.addElem = false;
	    $scope.elementoEdit = {};
	    $scope.camposI = [];
	    $scope.camposD = [];
	    $scope.camposI2 = [];
	    $scope.camposD2 = [];
	    
	    $scope.urlEdit = false;
	    $scope.url = {};
	    $scope.urlsI = [];
	    $scope.urlsD = [];
	    $scope.urlsI2 = [];
	    $scope.urlsD2 = [];

		var par = true;
			
		//Dividimos los campos a mostrar en el formulario en dos columnas.
		for(var i=0; i<$scope.elemento.camposAux.length; i++){
		    if(par){
			    $scope.camposI.push($scope.elemento.camposAux[i]);
			    par = false;
		    }else{
			    $scope.camposD.push($scope.elemento.camposAux[i]);
			    par =true;
		    }
		}
		//Realizamos una copia de seguridad de los campos para en caso de que se pulse el botón cancelar tener los datos originales.
		$scope.camposI2 = angular.copy($scope.camposI);
		$scope.camposD2 = angular.copy($scope.camposD);
		
		par = true;
		//Dividimos los campos a mostrar en el formulario en dos columnas.
		for(var i=0; i<$scope.elemento.urlsAux.length; i++){
		    if(par){
			    $scope.urlsI.push($scope.elemento.urlsAux[i]);
			    par = false;
		    }else{
			    $scope.urlsD.push($scope.elemento.urlsAux[i]);
			    par = true;
		    }
		}
		//Realizamos una copia de seguridad de los campos para en caso de que se pulse el botón cancelar tener los datos originales.
		$scope.urlsI2 = angular.copy($scope.urlsI);
		$scope.urlsD2 = angular.copy($scope.urlsD);
		
				
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogFormElementos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (data) {
			if(data == 'editar'){
				$scope.editarElemento();
			}else if(data == 'nuevo'){
				$scope.addElemento();
			}
		}, function (reason) {
		    if(reason == 'eliminar'){
			    $scope.eliminarElem();
		    }else{
			    $scope.camposI = angular.copy($scope.camposI2);
				$scope.camposD = angular.copy($scope.camposD2);
				$scope.urlsI = angular.copy($scope.urlsI2);
				$scope.urlsD = angular.copy($scope.urlsD2);
		    	//Si se pulsa la cruz del ngDialog reseteamos los valores del formulario.
		    	$scope.resetFormElem();
			}
		});
	}
	
	//Función que duplica el elemento del árbol.
	$scope.duplicar = function(scope){
		//Creamos la copia del elemento y le quitamos el id para hacer la inserción en la BBDD.
		var elemDuplicar = angular.copy(scope.$nodeScope.$modelValue.data);
		elemDuplicar.id = undefined;
		elemDuplicar.camposAux = JSON.parse(elemDuplicar.campos);
		elemDuplicar.urlsAux = JSON.parse(elemDuplicar.urls);
		
		//Dividimos los campos a mostrar en el formulario en dos columnas.
		for(var j=0; j<elemDuplicar.camposAux.length; j++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(j>1){
			    elemDuplicar.camposAux[j].clase = true;
			}else{
			    elemDuplicar.camposAux[j].clase = false;
		    }
		}
		for(var k=0; k<elemDuplicar.urlsAux.length; k++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(k>1){
			    elemDuplicar.urlsAux[k].clase = true;
			}else{
			    elemDuplicar.urlsAux[k].clase = false;
		    }
		}
		
		//Recuperamos cual es el padre del elemento.
		$scope.raiz = scope.$nodeScope.$parent.$parent.$parent.$modelValue;
		
		//Realizamos la inserción en la BBDD.		
		dataService
			.postElements('elemento', elemDuplicar)
			.then(function(data){
				elemDuplicar.id = data.id;
				$scope.raiz.nodes.push({
		        	id: elemDuplicar.id,
					title: elemDuplicar.nombre,
					data: elemDuplicar,
					nodes: []
		        });
		        elementosListado.push(elemDuplicar);
		        $scope.cambios--;
		        console.log('Inserción del elemento correcta.');
		        $scope.resetFormElem();
				traerPlantillas('elemento?filter=id%3E', 0, '%26%26plantilla%3D1', 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorElemSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorElemSis = null;}, 15000);
			});
	}
	
	//Función que abre la sección con la tabla de configuración general.
	$scope.abrirConf = function(scope){
		$scope.elementosConfig = [];
		var elementoConfig = scope.$nodeScope.$modelValue;		
		$scope.elementosConfig = traerConfiguraciones(indice, elementoConfig, []);
		//$timeout(function(){
			ngDialog.openConfirm({
				template: '/app/views/partials/ngDialogConfigElem.html',
				className: 'ngdialog-theme-default',
				scope: $scope
			}).then(function (data) {
				//No habrá botón de aceptar, sólo de cerrar.
			}, function (reason) {
			    //Al cerrar el diálogo no haremos nada, puesto que el array de configuraciones se reseteal cada vez que se presiona el botón.
			});
		//}, 500);		
	}
	
	//Función recursiva que trae las configuraciones de todos los elementos.
	var traerConfiguraciones = function(indice, elementoConfig, configuraciones){
		//Comprobamos si es el nodo Configuración, es decir, si hay que mostrar todo el árbol de configuraciones.
		if(elementoConfig.id == 0){
			//Comprobamos si el nodo tiene hijos
			//Si tiene hijos insertamos los datos del nodo en el array y hacemos la llamada recursiva enviando como árbol la lista de hijos.
			if(elementoConfig.nodes.length > 0){
				for(var j=0; j<elementoConfig.nodes.length; j++){
					traerConfiguraciones(indice+j, elementoConfig.nodes[j], configuraciones);
				}
			//En caso de que no tenga hijos, insertamos el objeto en el array sin más.
			}else{
				configuraciones = [];
			}
		}else{
			//Comprobamos si el nodo tiene hijos
			//Si tiene hijos insertamos los datos del nodo en el array y hacemos la llamada recursiva enviando como árbol la lista de hijos.
			if(elementoConfig.nodes.length > 0){
				configuraciones.push({"nombre": indice+'.- '+elementoConfig.data.nombre, "config": elementoConfig.data.camposAux});
				for(var j=0; j<elementoConfig.nodes.length; j++){
					traerConfiguraciones(indice+'.'+(j+1), elementoConfig.nodes[j], configuraciones);
				}
			//En caso de que no tenga hijos, insertamos el objeto en el array sin más.
			}else{
				configuraciones.push({"nombre": indice+' - '+elementoConfig.data.nombre, "config": elementoConfig.data.camposAux});
			}
		}
		return configuraciones;
	}
	
	//Función que muestra el formulario y lo inicializa.
	$scope.abrirForm = function(data){
		//console.log('DATA - Padre: ', data.$nodeScope.$parent.$parent.$parent.$modelValue);
		//console.log('DATA - Mismo: ', data.$nodeScope.$modelValue);
		//console.log('DATA - Hijos: ', data.$nodeScope.$modelValue.nodes);
		
		var obj = data.$nodeScope.$modelValue;
		
		//Reiniciamos los valores del formulario por si se ha cerrado indevidamente.
		$scope.resetFormElem();
		$scope.elemEdit = true;
		$scope.addElem = true;
		$scope.elementoEdit = {};
		$scope.elemento = {};
		//Comprobamos si es un padre con datos (Un elemento creado por nosotros), o un padre estandar de la inicialización del árbol (Elementos).
		//En caso de ser 'Elementos' devolvemos el padre a null para indicar en la inserción que tendrá el puntero a otro elemento a null.
		//Por otro lado, indicaremos que la raiz es el objeto inicial 'Elementos'. En caso de ser un padre creado por nosotros, la raiz será este mismo.
		if(obj.id == 0){
			$scope.padre = null;
		}else{
			$scope.padre = obj.id;
		}
		$scope.raiz = obj;
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogFormElementos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (data) {
			if(data == 'editar'){
				$scope.editarElemento();
			}else if(data == 'nuevo'){
				$scope.addElemento();
			}
		}, function (reason) {
		    if(reason == 'eliminar'){
			    $scope.eliminarElem();
		    }else{
			    $scope.camposI = angular.copy($scope.camposI2);
				$scope.camposD = angular.copy($scope.camposD2);
		    	//Si se pulsa la cruz del ngDialog reseteamos los valores del formulario.
		    	$scope.resetFormElem();
			}
		});
	}
	
	//Función que añade un elemento nuevo en el árbol.
	$scope.addElemento = function() {
		if($scope.padre != null){
			$scope.elementoEdit.id_elemento = $scope.padre;
		}else{
			$scope.elementoEdit.id_elemento = null;
		}
		$scope.elementoEdit.id_sistema = $scope.sis.id;
		var arrayAux = [];
		//Mientras que las arrays tengan contenido, vamos intercalando los campos.
		//Esto lo hacemos de la misma manera que creamos las dos arrais para repartir los campos en el formulario.
		while($scope.camposI.length != 0){
			if($scope.camposI.length != 0){
				arrayAux.push({"key": $scope.camposI[0].key, "valor": $scope.camposI[0].valor});
				$scope.camposI.splice(0, 1);
			}
			if($scope.camposD.length != 0){
				arrayAux.push({"key": $scope.camposD[0].key, "valor": $scope.camposD[0].valor});
				$scope.camposD.splice(0, 1);
			}
		}
		$scope.elementoEdit.camposAux = angular.copy(arrayAux);
		
		var arrayAux2 = [];
		while($scope.urlsI.length != 0){
			if($scope.urlsI.length != 0){
				arrayAux2.push({"key": $scope.urlsI[0].key, "texto": $scope.urlsI[0].texto, "url": $scope.urlsI[0].url});
				$scope.urlsI.splice(0, 1);
			}
			if($scope.urlsD.length != 0){
				arrayAux2.push({"key": $scope.urlsD[0].key, "texto": $scope.urlsD[0].texto, "url": $scope.urlsD[0].url});
				$scope.urlsD.splice(0, 1);
			}
		}
		$scope.elementoEdit.urlsAux = angular.copy(arrayAux2);
		
		//Dividimos los campos a mostrar en el formulario en dos columnas.
		for(var j=0; j<$scope.elementoEdit.camposAux.length; j++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(j>1){
			    $scope.elementoEdit.camposAux[j].clase = true;
			}else{
			    $scope.elementoEdit.camposAux[j].clase = false;
		    }
		}
		for(var k=0; k<$scope.elementoEdit.urlsAux.length; k++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(k>1){
			    $scope.elementoEdit.urlsAux[k].clase = true;
			}else{
			    $scope.elementoEdit.urlsAux[k].clase = false;
		    }
		}
		
		$scope.elementoEdit.campos = JSON.stringify(arrayAux);
		$scope.elementoEdit.urls = JSON.stringify(arrayAux2);
		if($scope.elementoEdit.plantilla == undefined){$scope.elementoEdit.plantilla = false;}
		dataService
			.postElements('elemento', $scope.elementoEdit)
			.then(function(data){
				$scope.elementoEdit.id = data.id;
				$scope.raiz.nodes.push({
		        	id: data.id,
					title: $scope.elementoEdit.nombre,
					data: $scope.elementoEdit,
					nodes: []
		        });
		        elementosListado.push($scope.elementoEdit);
		        $scope.cambios--;
		        console.log('Inserción del elemento '+data.id+' correcta.');
		        $scope.resetFormElem();
				traerPlantillas('elemento?filter=id%3E', 0, '%26%26plantilla%3D1', 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorElemSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorElemSis = null;}, 15000);
			});
	}
	
	//Función que recoge los datos del formulario y hace la llamada put a la BBDD.
	$scope.editarElemento = function(){
		var arrayAux = [];
		//Mientras que las arrays tengan contenido, vamos intercalando los campos.
		//Esto lo hacemos de la misma manera que creamos las dos arrais para repartir los campos en el formulario.
		while($scope.camposI.length != 0){
			if($scope.camposI.length != 0){
				arrayAux.push({"key": $scope.camposI[0].key, "valor": $scope.camposI[0].valor});
				$scope.camposI.splice(0, 1);
			}
			if($scope.camposD.length != 0){
				arrayAux.push({"key": $scope.camposD[0].key, "valor": $scope.camposD[0].valor});
				$scope.camposD.splice(0, 1);
			}
		}
		$scope.elementoEdit.camposAux = angular.copy(arrayAux);
		
		var arrayAux2 = [];
		while($scope.urlsI.length != 0){
			if($scope.urlsI.length != 0){
				arrayAux2.push({"key": $scope.urlsI[0].key, "texto": $scope.urlsI[0].texto, "url": $scope.urlsI[0].url});
				$scope.urlsI.splice(0, 1);
			}
			if($scope.urlsD.length != 0){
				arrayAux2.push({"key": $scope.urlsD[0].key, "texto": $scope.urlsD[0].texto, "url": $scope.urlsD[0].url});
				$scope.urlsD.splice(0, 1);
			}
		}
		$scope.elementoEdit.urlsAux = angular.copy(arrayAux2);
		
		//Dividimos los campos a mostrar en el formulario en dos columnas.
		for(var j=0; j<$scope.elementoEdit.camposAux.length; j++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(j>1){
			    $scope.elementoEdit.camposAux[j].clase = true;
			}else{
			    $scope.elementoEdit.camposAux[j].clase = false;
		    }
		}
		for(var k=0; k<$scope.elementoEdit.urlsAux.length; k++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(k>1){
			    $scope.elementoEdit.urlsAux[k].clase = true;
			}else{
			    $scope.elementoEdit.urlsAux[k].clase = false;
		    }
		}
		
		$scope.elementoEdit.campos = JSON.stringify(arrayAux);
		$scope.elementoEdit.urls = JSON.stringify(arrayAux2);
		if($scope.elementoEdit.plantilla == undefined){$scope.elementoEdit.plantilla = false;}
		var elementoAux = angular.copy($scope.elementoEdit);
		//Sobreescribimos los elementos que se pueden modificar del elemento.
		dataService
			.putElements('elemento', $scope.elementoEdit)
			.then(function(data){
				console.log('Inserción del elemento correcta.');
				elementosListado.push($scope.elementoEdit);
				$scope.resetFormElem();
				editarElementoArbol($scope.elementos, elementoAux);
				traerPlantillas('elemento?filter=id%3E', 0, '%26%26plantilla%3D1', 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){
					$rootScope.relogin();
				}else{
					$scope.errorElemSis = parseError(error.status, error.data);
					$timeout(function(){$scope.errorElemSis = null;}, 15000);
				}
			});		
	}
	
	//Función recursiva que modifica los valores del elemento del árbol.
	var editarElementoArbol = function(elementos, elemento){
		for(var i=0; i<elementos.length; i++){
			if(elementos[i].id == elemento.id){
				elementos[i].title = elemento.nombre;
				elementos[i].data = elemento;
				$scope.cambios--;
				break;
			}else{
				if(elementos[i].nodes.length > 0){
					editarElementoArbol(elementos[i].nodes, elemento);
				}
			}
		}
	}
	
	//Función que elimina un elemento del árbol.
	$scope.eliminarElem = function(scope){
		//Eliminamos el elemento de la tabla.
		dataService
			.deleteElements('elemento', '?filter=id%3D'+scope.$nodeScope.$modelValue.id)
			.then(function(data){
				console.log('Elemento '+data.record[0].id+' del sistema eliminado correctamente.');
				//Llamamos a la función auxiliar que borra el elemento del árbol.
				scope.remove();
				$scope.cambios--;
				$scope.resetFormElem();
				//$scope.buffer3 = true;
				//traerElementos('elemento?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
				traerPlantillas('elemento?filter=id%3E', 0, '%26%26plantilla%3D1', 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				if(error.status==500){
					$scope.errorElemSis = 'Debe eliminar primero los elementos dependientes antes de eliminar el padre.';
					$timeout(function(){$scope.errorElemSis = null;}, 15000);
				}else{
					$scope.errorElemSis = parseError(error.status, error.data);
					$timeout(function(){$scope.errorElemSis = null;}, 15000);
				}
			});
	}
	
	//Función que resetea el formulario de elementos.
	$scope.resetFormElem = function(){
		$scope.elementoEdit = null;
		$scope.elemento = null;
		$scope.padre = null;
		$scope.addElem = false;
		$scope.elemEdit = false;
		$scope.camposI = [];
		$scope.camposD = [];
	}
	
	//Función que guarda los cambios realizados en el orden de los elementos.
	$scope.guardarCambios = function(){
		dataService
			.putElements('elemento', deshacerArbol(null, $scope.elementos[0].nodes, []))
			.then(function(data){
				console.log('Elementos guardados con exito (guardarCambios SistemasElemCntrl): ', data.record.length);
				$scope.cambios = 0;
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorElemSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorElemSis = null;}, 10000);
			});
	}
	
	//Función recursiva que cambia los id's de los padres, y nos devuelve un array de los objetos.
	var deshacerArbol = function(idPadre, arbol, arbolDeshecho){
		for(var i=0; i<arbol.length; i++){
			arbol[i].data.id_elemento = idPadre;
			//Comprobamos si el nodo tiene hijos
			//Si tiene hijos insertamos los datos del nodo en el array y hacemos la llamada recursiva enviando como árbol la lista de hijos.
			if(arbol[i].nodes.length > 0){
				arbolDeshecho.push(arbol[i].data);
				deshacerArbol(arbol[i].id, arbol[i].nodes, arbolDeshecho);
			//En caso de que no tenga hijos, insertamos el objeto en el array sin más.
			}else{
				arbolDeshecho.push(arbol[i].data);
			}
		}
		//Por último devolvemos la variable arbolDeshecho, que contendrá todos los datos de los nodos.
		return arbolDeshecho;
	}
	
	
	/* ----------------------------- Operaciones sobre los elementos -----------------------------*/
	
	//activa la edición del elemento.
	$scope.setEditable = function(){
		$scope.elemEdit = true;
		focus('focusMe');
		$scope.elementoEdit = angular.copy($scope.elemento);
	}
	
	//Desactiva la edición del elemento.
	$scope.setNonEditable = function(){
		//Restauramos los valores de los campos, dado que se ha pulsado cancelar y se descartan los cambios realizados.
		$scope.elementoEdit = {};
		$scope.camposI = angular.copy($scope.camposI2);
		$scope.camposD = angular.copy($scope.camposD2);
		$scope.elemEdit = false;
	}
	
	//Función que selecciona una plantilla y cambia los campos que contiene el elemento.
	$scope.cambiarPlantilla = function(plantilla){
		for(var i=0; i<elementosListado.length; i++){
			if(elementosListado[i].id == plantilla){
				var camposAux = JSON.parse(elementosListado[i].campos);
				var urlsAux = JSON.parse(elementosListado[i].urls);
				break;
			}
		}
		
		//Una vez tenemos los campos que componen la plantilla, sustituimos los campos del elemento que estamos editando.
		$scope.camposI = [];
		$scope.camposD = [];
		var par = true;
			
		//Dividimos los campos a mostrar en el formulario en dos columnas.
		for(var i=0; i<camposAux.length; i++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(i>1){
			    camposAux[i].clase = true;
			}else{
			    camposAux[i].clase = false;
		    }
		    if(par){
			    $scope.camposI.push(camposAux[i]);
			    par = false;
		    }else{
			    $scope.camposD.push(camposAux[i]);
			    par =true;
		    }
		}
		
		$scope.urlsI = [];
		$scope.urlsD = [];
		
		//Dividimos los campos a mostrar en el formulario en dos columnas.
		for(var i=0; i<urlsAux.length; i++){
		    //Variable auxiliar para la vista (le damos true para que el campo se separe del anterior).
		    if(i>1){
			    urlsAux[i].clase = true;
			}else{
			    urlsAux[i].clase = false;
		    }
		    if(par){
			    $scope.urlsI.push(urlsAux[i]);
			    par = false;
		    }else{
			    $scope.urlsD.push(urlsAux[i]);
			    par =true;
		    }
		}
	}
	
	
	/* ----------------------- Operaciones sobre los campos de los elementos -----------------------*/
	
	//Funciones que habilitan los campos para añadir elementos.
	$scope.newCampo = function(){
		$scope.campoAdd = true;
		$scope.campoNuevo = {};
		$scope.errorCampo = null;
	}
	$scope.newURL = function(){
		$scope.urlAdd = true;
		$scope.urlNueva = {};
		$scope.errorCampo = null;
	}
	
	//Función que añade el campo a la lista correspondiente.
	$scope.addCampo = function(){
		$scope.errorCampo = null;
		//Comprobamos que al menos el nombre del campo esté relleno.
		if($scope.campoNuevo.key != undefined && $scope.campoNuevo.key != ''){
			//Si el campo de valor no está relleno, lo inicializamos a vacío.
			if($scope.campoNuevo.valor == undefined){
				$scope.campoNuevo.valor = '';
			}
			//Comprobamos cual de las arrays de campos es más pequeña o si son iguales e insertamos el campo donde corresponda.
			if($scope.camposI.length > $scope.camposD.length){
				if($scope.camposD.length > 0){$scope.campoNuevo.clase = true;}
				$scope.camposD.push($scope.campoNuevo);
			}else{
				if($scope.camposI.length > 0){$scope.campoNuevo.clase = true;}
				$scope.camposI.push($scope.campoNuevo);
			}
			$scope.campoAdd = false;
		}else{
			$scope.errorCampo = 'Al menos el nombre debe estár relleno para crear el nuevo campo.';
			$timeout(function(){$scope.errorCampo = null;}, 10000);
		}
	}
	
	//Función que añade la nueva URL a la lista correspondiente.
	$scope.addURL = function(){
		$scope.errorCampo = null;
		//Comprobamos que al menos el nombre del campo esté relleno.
		if($scope.urlNueva.key != undefined && $scope.urlNueva.key != '' && $scope.urlNueva.texto != undefined && $scope.urlNueva.texto != ''){
			//Si el campo de valor no está relleno, lo inicializamos a vacío.
			if($scope.urlNueva.url == undefined){
				$scope.urlNueva.url = '';
			}
			//Comprobamos cual de las arrays de campos es más pequeña o si son iguales e insertamos el campo donde corresponda.
			if($scope.urlsI.length > $scope.urlsD.length){
				if($scope.urlsD.length > 0){$scope.urlNueva.clase = true;}
				$scope.urlsD.push($scope.urlNueva);
			}else{
				if($scope.urlsI.length > 0){$scope.urlNueva.clase = true;}
				$scope.urlsI.push($scope.urlNueva);
			}
			$scope.urlAdd = false;
		}else{
			$scope.errorCampo = 'Al menos el nombre y el texto a mostrar deben estár relleno para crear el nuevo campo de URL.';
			$timeout(function(){$scope.errorCampo = null;}, 10000);
		}
	}
	
	//Funciones que ocultan los campos de creación de nuevos campos.
	$scope.cancelCampo = function(){
		$scope.campoAdd = false;
		$scope.errorCampo = null;
	}
	$scope.cancelURL = function(){
		$scope.urlAdd = false;
		$scope.errorCampo = null;
	}
	
	//Funciones que eliminan un campo.
	$scope.eliminarCampo = function(campos, campo){		
		if(campos == 'I'){
			for(var i=0; i<$scope.camposI.length; i++){
				if($scope.camposI[i].key == campo.key){
					$scope.camposI.splice(i, 1);
					break;
				}
			}
		}else if(campos == 'D'){
			for(var i=0; i<$scope.camposD.length; i++){
				if($scope.camposD[i].key == campo.key){
					$scope.camposD.splice(i, 1);
					break;
				}
			}
		}
	}
	$scope.eliminarURL = function(campos, url){		
		if(campos == 'I'){
			for(var i=0; i<$scope.urlsI.length; i++){
				if($scope.urlsI[i].key == url.key){
					$scope.urlsI.splice(i, 1);
					break;
				}
			}
		}else if(campos == 'D'){
			for(var i=0; i<$scope.urlsD.length; i++){
				if($scope.urlsD[i].key == url.key){
					$scope.urlsD.splice(i, 1);
					break;
				}
			}
		}
	}
	
	
	/* ------------------- Operaciones sobre la configuración de los elementos --------------------*/
	
	//Función que cierra la ventana de configuraciones de los elementos.
	$scope.cerrarConfTotal = function(){
		$scope.mostrarConfig = false;
	}
	
	//Función que imprime la configuración.
	$scope.imprimirConfig = function(){
		printer.printFromScope('app/views/partials/impresionConfiguraciones.html', $scope);
	}
	
	//Función que abre el diálogo para introducir los datos de envío del e-mail.
	$scope.emailConfig = function(){		
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogEmail.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (datosEmail) {
			$scope.cuerpo = angular.copy(datosEmail.cuerpo);
			console.log('E-mail enviado a '+datosEmail.email);
			emailSender.send({"name": "Altatec","email": "noreply@altatec-seguridad.com"}, {"name": datosEmail.name,"email": datosEmail.email}, datosEmail.asunto, 'app/views/partials/emailConfiguraciones.html', $scope);
		}, function (reason) {
		    $scope.datosEmail = {};
		});
	}
	
	/* ----------------------------- Operaciones generales de la vista ----------------------------*/	
	
	//Función que manda señal de cerrar el formulario.
	$scope.cerrar = function(){
		$scope.sis = {};
		$scope.elementos = [];
		$scope.nuevo = false;
		$scope.mostrar = false;
		$rootScope.$broadcast('cerrarElementos', false);
	}
	
	//Función que cierra el panel de error.
	$scope.clearErrorElemSis = function(){
		$scope.errorElemSis = null;
	}
	$scope.clearErrorCampo = function(){
		$scope.errorCampo = null;
	}
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('mostrar', function(e,data){
		$scope.mostrar = data;
		$scope.nuevo = data;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('editar', function(e,data){
		$scope.buffer3 = true;
		$scope.sis = angular.copy(data);
		$scope.mostrar = true;
		$scope.nuevo = false;
		$scope.elemento = null;
		traerElementos('elemento?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('recall', function(e,data){
		$scope.buffer3 = true;
		$scope.sis = angular.copy(data);
		$scope.mostrar = true;
		$scope.nuevo = false;
		$scope.elemento = null;
		traerElementos('elemento?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del hermano y manda la orden de cerrar al siguiente hermano.
	$scope.$on('cerrarTodo', function(e, data){
		$scope.sis = {};
		$scope.elementos = [];
		$scope.nuevo = false;
		$scope.mostrar = false;
	});
}