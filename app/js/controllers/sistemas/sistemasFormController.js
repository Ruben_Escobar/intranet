/****************************************************************************/
/*				Controlador de la vista del formularios Sistemas			*/
/****************************************************************************/
App.controller('SistemasFormCntrl', SistemasFormCntrl);
function SistemasFormCntrl($scope, $rootScope, $timeout, ngDialog, focus, uiGridConstants, dataService, SerSistemas){
	
	$scope.desplegarUl = false;
	$scope.sis = {};
	$scope.errorFormSistemas = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasFormCntrl ------------------------------');
    
	//Función que trae los tipos de cliente de la tabla de configuraciones.
	var traerTiposCliente = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.tiposCliente = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				var tipos = data.registros[0].valor.split(",");
				var obj = {};
				for(var i=0; i<tipos.length; i++){
					obj = {};
					obj.nombre = tipos[i];
					obj.id = (i+1);
					$scope.tiposCliente.push(obj);
				}
				console.log('Traigo los tipos de cliente (SistemasFormCntrl): ', $scope.tiposCliente);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	traerTiposCliente('configuracion?filter=id%3E', 0, "%26%26parametro='tipoCliente'", 0, 0, 0, []);
	
	//Función para añadir el registro
	$scope.add = function(){
		for(var i=0; i<$scope.tiposCliente.length; i++){
			if($scope.tiposCliente[i].id == $scope.sis.tipoCliente){
				$scope.sis.tipoCliente = $scope.tiposCliente[i].nombre;
				break;
			}
		}
		//Comprobamos si es una edición o una inserción y procedemos según corresponda.
		if($scope.sis.id == undefined){
			//Inserción.
			dataService
				.postElements('sistema', $scope.sis)
				.then(function(data){
					console.log('Sistema insertado: ', data);
					$scope.resetFormSis(false);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormSistemas = parseError(error.status, error.data);					
					$timeout(function(){$scope.errorFormSistemas = null;}, 15000);
				});
		}else{
			//Edición.
			dataService
				.putElements('sistema', $scope.sis)
				.then(function(data){
					console.log('Sistema modificado:', data.id);
					SerSistemas.data.modificado = false;
					$scope.formSis.$dirty = false;
					$scope.formSis.$setPristine();
					$rootScope.$broadcast('recargar', true);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormSistemas = parseError(error.status, error.data);					
					$timeout(function(){$scope.errorFormSistemas = null;}, 15000);
				});
		}
	}
	
	//Función que elimina el sistema seleccionado.
	$scope.delSis = function(id){
		dataService
			.deleteElements('sistema', '?filter=id='+id)
			.then(function(data){
				console.log('Sistema eliminado: ', data.record[0].id);
				$scope.resetFormSis(false);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormSistemas = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormSistemas = null;}, 15000);
			});
	}
	
	//Función que cambia el estado del servicio al actuar sobre algún campo del formualrio.
	$scope.cambiarModificado = function(){
		SerSistemas.data.modificado = true;
	}
	
	//Función que resetea la propiedad dirty del formulario.
	$scope.quitarDirty = function(){
		$scope.formSis.$dirty = false;
	}
	
	//Función que cierra el panel de erro.
	$scope.clearErrorFormSistemas = function(){
		$scope.errorFormSistemas = null;
	}
	
	//Resetear formulario
	$scope.resetFormSis = function(reset){
		$rootScope.$broadcast('cerrarFormulario', reset);
		$scope.formSis.$setPristine();
		$scope.sis = undefined;
		$scope.mostrar = false;
		$scope.desplegarUl = false;
		SerSistemas.data.modificado = false;
		$scope.formSis.$dirty = false;
	}
	
	//Función que testea los cambios del formulario y avisa en caso de haber cambios antes de cerrar.
	$scope.cerrar = function(){
		if($scope.formSis.$dirty){
			if(confirm('Hay cambios en el formulario, si cierra los perderá')){
				$scope.resetFormSis(true);
			}
		}else{
			$scope.resetFormSis(true);
		}
	}
	
	/*---------------------------------------- Controlador del Select ----------------------------------------*/
	//Función que sustituye la población actual por la seleccionada de la tabla.
	$scope.cambiarPoblacion = function(poblacion){
		$scope.ciudadSel = poblacion.poblacion;
		$scope.sis.id_poblacion = poblacion.idpoblacion;
		$scope.sis.poblacion = poblacion.poblacion;
		$scope.sis.cp = poblacion.postal;
		$scope.desplegarUl = !$scope.desplegarUl;
		SerSistemas.data.modificado = true;
		$scope.formSis.$dirty = true;
	}
	
	//Desplegamos el Select.
	$scope.desplegar = function(){
		if($scope.desplegarUl){
			$scope.desplegarUl = !$scope.desplegarUl;
			$scope.filtro='';
		}else{
			$scope.desplegarUl = !$scope.desplegarUl;
			focus('focusMe');
		}
	}
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masPoblaciones = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogPoblaciones.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (poblacion) {
			$scope.ciudadSel = poblacion.poblacion;
			$scope.sis.id_poblacion = poblacion.idpoblacion;
			$scope.sis.poblacion = poblacion.poblacion;
			$scope.sis.cp = poblacion.postal;
			$scope.desplegarUl = !$scope.desplegarUl;
	    }, function (reason) {
	    	console.log('Modal promise rejected. Reason: ', reason);
	    });
	}
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('mostrar', function(e,data){
		$scope.sis = {};
		$scope.sis.referencia = "S"+crearReferencia();
		$scope.buffer2 = data;
		$scope.poblaciones = $rootScope.poblaciones;
		for(var i=0; i<$scope.poblaciones.length; i++){
			$scope.poblaciones[i].provincia = undefined;
			$scope.poblaciones[i].provinciaseo = undefined;
		}
		$scope.ciudadSel = 'Seleccione una Población';
		$scope.boton = 'Añadir Sistema';
		$scope.titulo = 'Formulario Añadir';
		$scope.mostrar = data;
		$scope.nuevo = data;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('editar', function(e,data){
		$scope.buffer2 = true;
		$scope.poblaciones = $rootScope.poblaciones;
		$scope.ciudadSel = data.poblacion;
		$scope.sis = angular.copy(data);
		$scope.boton = 'Modificar Sistema';
		$scope.titulo = 'Formulario';
		if($scope.sis.referencia == null){
			$scope.sis.referencia = "S"+crearReferencia();
		}
		for(var i=0; i<$scope.tiposCliente.length; i++){
			if($scope.tiposCliente[i].nombre == $scope.sis.tipoCliente){
				$scope.sis.tipoCliente = $scope.tiposCliente[i].id;
				break;
			}
		}
		$scope.mostrar = true;
		$scope.nuevo = false;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('recall', function(e,data){
		$scope.buffer2 = true;
		$scope.poblaciones = $rootScope.poblaciones;
		$scope.ciudadSel = data.poblacion;
		$scope.sis = angular.copy(data);
		$scope.boton = 'Modificar Sistema';
		$scope.titulo = 'Formulario';
		if($scope.sis.referencia == null){
			$scope.sis.referencia = "S"+crearReferencia();
		}
		for(var i=0; i<$scope.tiposCliente.length; i++){
			if($scope.tiposCliente[i].nombre == $scope.sis.tipoCliente){
				$scope.sis.tipoCliente = $scope.tiposCliente[i].id;
				break;
			}
		}
		$scope.mostrar = true;
		$scope.nuevo = false;
	});
		
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('cerrar', function(e,data){
		$scope.formSis.$setPristine();
		$scope.sis = undefined;
		$scope.mostrar = false;
		$rootScope.$broadcast('cerrarTodo', false);
	});
}