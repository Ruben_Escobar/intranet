/****************************************************************************/
/*						Controlador de la vista de Sistemas					*/
/****************************************************************************/
App.controller('SistemasCntrl', SistemasCntrl);
function SistemasCntrl($scope, $rootScope, $timeout, $state, ngDialog, uiGridConstants, SerInst, SerInci, dataService, SerSistemas) {
	$scope.buffer = true;
	$scope.mostrar = false;
	$scope.progressValue = 0;
	$scope.filtrado = false;
	$scope.currentError = null;
	SerSistemas.data.modificado = false;
	
	var vm = this;
	var registros = [];
	var registrosPob = [];
	var data = [];
	var pasarela = 0;
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	//Inicializamos la tabla.
	$scope.gridOptionsComplex = {
	    paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
	    enableRowSelection: true,
		enableColumnMenus: false,
	    selectionRowHeaderWidth: 38,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
		    {name: 'id', field: 'id', width: 45, enableHiding: false, enablePinning:false},
	        {name: 'nombre', field: 'nombre', minWidth: 130, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'direccion', field: 'direccion', cellClass: "hidden-xs", headerCellClass : "hidden-xs", displayName: 'Dirección', minWidth: 90, enableHiding: false, enablePinning:false},
	        {name: 'poblacion', field: 'poblacion', displayName: 'Población', cellClass: "hidden-xs", headerCellClass : "hidden-xs", minWidth: 85, enableHiding: false, enablePinning:false},
	        {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi = gridApi;
	    }
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridOptionsComplex.paginationPageSize*36)+100) > (($scope.gridOptionsComplex.data.length*36)+100) ){
			return ($scope.gridOptionsComplex.data.length*36)+100;
		}else{
			return ($scope.gridOptionsComplex.paginationPageSize*36)+100;
		}
	}


	//Función que recupera todos las Incidencias de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){	
		$scope.buffer = true;
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				//Comprobamos si se ha terminado la carga o no. En caso negativo, volvemos a llamar a la función.
				if(data.progressValue == 100){
					console.log('Total de registros (SistemasCntrl): ', data.registros.length);
					registros = angular.copy(data.registros);
					for(var i=0; i<registros.length; i++){
						//Filtramos acentos antes de nada.
						registros[i].nombre = removeAccents(registros[i].nombre);
						if(registros[i].poblacion != null){
							registros[i].poblacion = removeAccents(registros[i].poblacion);
						}
						$scope.progressValue = ((i/registros.length)*100).toFixed();
					}
					$scope.gridOptionsComplex.data = angular.copy(registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Carga (SistemasCntrl): '+$scope.progressValue+'%.');
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 5000);
			});
	}
    recuperarRegistros('sistemas?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae solo los registros que faltan por revisar.
	$scope.togleRevisados = function(){
		//Comprobamos si está activo el filtro, en caso de no estarlo, lo llamamos.
		if(!$scope.filtrado){
			dataService
				.getFiltro('sistemas?filter=id%3E', 0, '%26%26referencia%20IS%20NULL', 0, 0, 0, [])
				.then(function(data){
					console.log('Total de registros sin revisar (SistemasCntrl): '+data.registros.length);
					$scope.gridOptionsComplex.data = angular.copy(data.registros);
					$scope.filtrado=!$scope.filtrado;
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 5000);
				});
		}else{
			$scope.filtrado=!$scope.filtrado;
			recuperarRegistros('sistemas?filter=id%3E', 0, 0, 0, 0, []);
		}
	}
	
	//Función que elimina uno o varios sistemas.
	$scope.remove = function(){
		data = $scope.gridApi.selection.getSelectedRows();
		var urlsAux = '?filter=id=';
		//Comprobamos si hay o no objetos seleccionados.
	    if(data.length!=0){
	        //Recorro los elementos del array de seleccionados y los mando eliminar.
			for(var i=0; i<data.length; i++){
	            if(i == 0){
		        	urlsAux = urlsAux+data[i].id;
	            }else{
		            urlsAux = urlsAux+'%7C%7Cid='+data[i].id;
	            }
			}			
			dataService
				.deleteElements('sistema', urlsAux)
				.then(function(data){
					for(var i=0; i<data.record.length; i++){
						console.log('Sistema '+data.record[i].id+' eliminado.');
					}
					registros = [];
					$scope.buffer = true;
					recuperarRegistros('sistemas?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 5000);
				});
		}else{
			ngDialog.open({
	            template: '<p class="rojo centrado">No has seleccionado ninún elemento para eliminar</p>',
	            plain: true
	        });
		}
	}
		
	//Abrir el formulario vacío.
	$scope.mostrarForm = function(){
		$scope.mostrar = true;
		$rootScope.$broadcast('mostrar', true);
	}	
	
	//Abrimos el formulario con los datos de la población a editar.
	$scope.editar = function(sistema){
		$scope.mostrar = true;
		$rootScope.$broadcast('editar', sistema);
	}
	
	//Función para cerrar el panel de error.
	$scope.clearError = function(){
		$scope.currentError = null;
	}
	
	//Función que comprueba si el formulario ha sido modificado antes de cambiar de pestaña.
	$scope.pararSiModificado = function (event) {
		//Comprobamos si el servicio está a true, en cuyo caso se habrá modificado algún campo del formulario.
		if (SerSistemas.data.modificado){
			var answer = confirm('Hay cambios sin guardar en el formulario. ¿Desea continuar?');
		    !answer && event.preventDefault();
		}
	};
	
	//Funciones que escucha a los hijos y cierra el formulario.
	$scope.$on('cerrarFormulario', function(e, data){
		if(!data){
			$scope.buffer = true;
			recuperarRegistros('sistemas?filter=id%3E', 0, 0, 0, 0, []);
		}
		$rootScope.$broadcast('cerrarTodo', false);
		$scope.mostrar = false;
	});
	$scope.$on('recargar', function(e, data){
		$scope.buffer = data;
		recuperarRegistros('sistemas?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que recoge las llamadas de precarga general.
	$scope.$on('cargando', function(e, data){
		$scope.cargando = data;
	});
	
	//Función que escucha a los hijos y llama a la vista de la instalación.
	$scope.$on('llamadaInstalaciones', function(e, data){
		SerInst.data.sistema = angular.copy(data.sistema);
		SerInst.data.instalacion = angular.copy(data.instalacion);
		$state.go('app.instalaciones');
	});
	
	//Función que escucha al hijo y llama a la vista de incidencias.
	$scope.$on('llamadaIncidencias', function(e, data){
		SerInci.data.sistema = angular.copy(data.sistema);
		SerInci.data.incidencia = angular.copy(data.incidencia);
		$state.go('app.incidencias');
	})
	
	//Comprobamos si el servicio SerInst tiene datos, en cuyo caso procedemos a mostrar el sistema solicitado.
    if(SerInst.data.sistema != undefined){
	    console.log('Comunicación devuelta desde Instalaciones para Sistemas');
	    $scope.mostrar = true;
		$timeout(function(){$rootScope.$broadcast('recall', SerInst.data.sistema);}, 5);
		$timeout(function(){SerInst.data.sistema = undefined; SerInst.data.instalacion = undefined;}, 100);
    }
    
    //Comprobamos si el servicio SerInci tiene datos, en cuyo caso procedemos a mostrar el sistema solicitado.
    if(SerInci.data.sistema != undefined){
	    console.log('Comunicación devuelta desde Incidencias para Sistemas');
	    $scope.mostrar = true;
		$timeout(function(){$rootScope.$broadcast('recall', SerInci.data.sistema);}, 5);
		$timeout(function(){SerInci.data.sistema = undefined; SerInci.data.incidencia = undefined;}, 100);
    }
}