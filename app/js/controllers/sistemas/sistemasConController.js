/****************************************************************************/
/*			Controlador de la vista de los Contactos de cada sistema		*/
/****************************************************************************/
App.controller('SistemasConCntrl', SistemasConCntrl);
function SistemasConCntrl($scope, $rootScope, $timeout, ngDialog, focus, uiGridConstants, dataService){
	
	var contactosSistema = [];
	var registros = [];
	var contactosSistema = [];
	var acumulados = 0;
	var total = 0;
	$scope.errorConSis =null;
	$scope.errorFormCon =null;
	$scope.progressValue2 = 0;
		
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.notas}} tooltip-placement="top">{{row.entity.notas}}</div>';
	
	//Inicializamos las tabla.
	$scope.gridConAcTotal = {
	    paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
	    enableRowSelection: true,
		enableColumnMenus: false,
	    selectionRowHeaderWidth: 38,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
	        {name: 'nombre', field: 'nombre', minWidth: 90, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'apellidos', field: 'apellidos', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'tipo', field: 'tipo', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'telefono', field: 'telefono', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Teléfono', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'movil', field: 'movil', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Móvil', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'correo', field: 'correo', displayName: 'e-mail', minWidth: 150, enableHiding: false, enablePinning:false, cellClass: 'hidden-xs', headerCellClass : "hidden-xs"},
	        {name: 'notas', field: 'notas', minWidth: 85, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs hidden-sm', headerCellClass : 'hidden-xs hidden-sm'},
	        {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.showFormCon(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi = gridApi;
	    }
	}
	$scope.gridConSis = {
	    paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
		enableColumnMenus: false,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
	        {name: 'nombre', field: 'nombre', minWidth: 90, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'apellidos', field: 'apellidos', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'tipo', field: 'tipo', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'telefono', field: 'telefono', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Teléfono', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'movil', field: 'movil', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Móvil', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'correo', field: 'correo', displayName: 'e-mail', minWidth: 150, enableHiding: false, enablePinning:false, cellClass: 'hidden-xs', headerCellClass : "hidden-xs"},
	        {name: 'notas', field: 'notas', minWidth: 85, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs hidden-sm', headerCellClass : 'hidden-xs hidden-sm'},
	        {name: 'editar', width: 70, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.showFormConPropio(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button><button data-ng-click="grid.appScope.delCon(row.entity)" title="Edit" class="btn btn-sm btn-danger"><em class="fa fa-trash"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi2 = gridApi;
	    }
	}
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ SistemasConCntrl ------------------------------');
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano2 = function(){
		if( (($scope.gridConAcTotal.paginationPageSize*36)+110) > (($scope.gridConAcTotal.data.length*36)+110) ){
			return ($scope.gridConAcTotal.data.length*36)+110;
		}else{
			return ($scope.gridConAcTotal.paginationPageSize*36)+110;
		}
	}
	$scope.comprobarTamano = function(){
		if( (($scope.gridConSis.paginationPageSize*36)+100) > (($scope.gridConSis.data.length*36)+100) ){
			return ($scope.gridConSis.data.length*36)+100;
		}else{
			return ($scope.gridConSis.paginationPageSize*36)+100;
		}
	}

	//Función que trae los contactos del sistema a editar.
	var traerContactosSistema = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				contactosSistema = contactosSistema.concat(data.registros);
				console.log('Total de registros (traerContactosSistema SistemasConCntrl): ', contactosSistema.length);
				for(var i=0; i<contactosSistema.length; i++){
					//Filtramos acentos antes de nada.
					contactosSistema[i].nombre = removeAccents(contactosSistema[i].nombre);
					contactosSistema[i].apellidos = removeAccents(contactosSistema[i].apellidos);
				}
				$scope.gridConSis.data = contactosSistema;
				$scope.buffer4 = false;
				contactosSistema = [];
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorSisCon = parseError(error.status, error.data);
				$timeout(function(){$scope.errorSisCon = null;}, 15000);
			});
	}
	
	//Función que trae todos los contactos.
	var traerContactos = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					registros = angular.copy(data.registros);
					console.log('Total de registros (traerContactos SistemasConCntrl): ', registros.length);
					for(var i=0; i<registros.length; i++){
						//Filtramos acentos antes de nada.
						registros[i].nombre = removeAccents(registros[i].nombre);
						registros[i].apellidos = removeAccents(registros[i].apellidos);
					}
					$scope.gridConAcTotal.data = registros;
					$scope.buffer2 = false;
					registros = [];
					$scope.progressValue2 = 0;
				}else{
					$scope.progressValue2 = data.progressValue;
					console.log('Cargando (traerContactos SistemasConCntrl): '+$scope.progressValue2+'%.');
					traerContactos(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorSisCon = parseError(error.status, error.data);
				$timeout(function(){$scope.errorSisCon = null;}, 15000);
			});
	}
	
	//Función que abre la tabla de contactos totales.
	$scope.abrirTabla = function(){
		$scope.contactosTotales = true;
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogTablaContactos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (nuevo) {
			//Si es un contacto que viene desde el formulario del diálogo, comprobamos si es edición o inserción y hacemos lo pertinente.
			//Además lo añadimos al array de usuarios de la aceptación.
			if(nuevo){
				if($scope.contacto.tipo == 'TECNICO' || $scope.contacto.tipo == 'ADMINISTRATIVO'){
					dataService
						.postElements('contacto', $scope.contacto)
						.then(function(data){
							console.log('Contacto añadido:', data.id);
							registros = [];
							$scope.buffer2 = true;
							traerContactos('contacto?filter=id%3E', 0, 0, 0, 0, []);
							
							dataService
								.postElements('contacto_sistema', {"id_contacto": data.id, "id_sistema": $scope.sis.id})
								.then(function(data){
									console.log('Contacto enlazado al sistema '+$scope.sis.nombre+'.');
									$scope.buffer4 = true;
									contactosSistema = [];
									traerContactosSistema('contactos?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
								})
								.catch(function(error){
									if(error.status==401){$rootScope.relogin();}
									$scope.errorFormCon = parseError(error.status, error.data);
									$timeout(function(){$scope.errorFormCon = null;}, 15000);
								});							
							$scope.resetFormCon();
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.errorFormCon = parseError(error.status, error.data);
							$timeout(function(){$scope.errorFormCon = null;}, 15000);
						});
				}else{
					$scope.errorFormCon = 'Seleccione tipo de contacto.';
					$timeout(function(){$scope.errorFormCon = null;}, 15000);
				}
			//En caso de que no sea nuevo, recogemos la variable que contiene los contactos seleccionados y procedemos.
			}else{
				contactosSistema = $scope.gridApi.selection.getSelectedRows();
				if(contactosSistema.length>0){
					var contactosInsertar = [];
					//Primero creamos el array completo de elementos a insertar.
					for(var k=0; k<contactosSistema.length; k++){
						contactosInsertar.splice(contactosInsertar.length, 0, {"id_contacto": contactosSistema[k].id, "id_sistema": $scope.sis.id});
					}
					//Recorremos el array que contiene los contactos que el usuario ha seleccionado para añadir.
					for(var i=0; i<contactosSistema.length; i++){
						//Recorremos el array con los contactos que tiene asignados ahora mismo el sistema.
						for(var j=0; j<$scope.gridConSis.data.length; j++){
							//Comprobamos si el contacto está ya asociado al sistema, si es así forzamos la salida del bucle y
							//eliminamos el registro del array auxiliar (Que será el que más adelante usaremos para hacer la inserción).
							if($scope.gridConSis.data[j].id == contactosSistema[i].id){
								//Una vez que sabemos que coinciden los IDs, es decir, que ya está asociado, lo busamos en el array auxiliar y lo quitamos.
								for(var l=0; l<contactosInsertar.length; l++){
									if(contactosInsertar[l].id_contacto == $scope.gridConSis.data[j].id){
										contactosInsertar.splice(l, 1);
									}
								}
								break;
							}
						}
					}
					if(contactosInsertar.length>0){
						dataService
							.postElements('contacto_sistema', contactosInsertar)
							.then(function(data){
								console.log('Contactos enlazados al sistema '+$scope.sis.nombre+'.');
								$scope.contactosTotales = false;
								$scope.buffer4 = true;
								contactosSistema = [];
								contactosInsertar = [];
								$scope.gridApi.selection.clearSelectedRows();
								traerContactosSistema('contactos?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorConSis = parseError(error.status, error.data);
								$timeout(function(){$scope.errorConSis = null;}, 15000);
							});
					}else{
						$scope.contactosTotales = false;
						contactosSistema = [];
						contactosInsertar = [];
						$scope.gridApi.selection.clearSelectedRows();
					}
				}else{
					ngDialog.open({
					    template: '<p class="rojo centrado">No has seleccionado ninún contacto para enlazar.</p>',
					    plain: true
					});
				}
			}
	    }, function (reason) {
		    //Código para el cierre del diálogo.		    
		});
	}
			
	//Función que edita el contacto.
	$scope.editarContacto = function(){
		if($scope.contacto.tipo == 'TECNICO' || $scope.contacto.tipo == 'ADMINISTRATIVO'){
			dataService
				.putElements('contacto', $scope.contacto)
				.then(function(data){
					console.log('Contacto modificado:', data.id);
					registros = [];
					$scope.buffer2 = true;
					traerContactos('contacto?filter=id%3E', 0, 0, 0, 0, []);
					$scope.resetFormConPropio();
					$scope.resetFormCon();
					contactosSistema = [];
					$scope.buffer4 = true;
					traerContactosSistema('contactos?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormCon = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormCon = null;}, 15000);
				});
		}else{
			$scope.errorFormCon = 'Seleccione tipo de contacto.';
			$timeout(function(){$scope.errorFormCon = null;}, 15000);
		}
	}
		
	//Función que abre el formulario vacío.
	$scope.newContacto = function(){
		$scope.contacto = {};
		$scope.mostrarFormCon = true;
		$scope.titulo = 'Formulario Añadir';
		$scope.botonForm = 'Añadir';
	}
	
	//Función que abre el formulario de edición del contacto.
	$scope.showFormCon = function(contacto){
		$scope.contacto = angular.copy(contacto);
		$scope.mostrarFormCon = true;
		$scope.titulo = 'Formulario Editar';
		$scope.botonForm = 'Modificar';
	}
	$scope.showFormConPropio = function(contacto){
		$scope.contacto = angular.copy(contacto);
		$scope.mostrarFormConPropio = true;
		$scope.titulo = 'Formulario Editar';
		$scope.botonForm = 'Modificar';
	}
	
	//Función que resetea el formulario de contactos.
	$scope.resetFormCon = function(){
		$scope.formCon.$setPristine();
		$scope.contacto = {};
		$scope.mostrarFormCon = false;
	}
	
	//Función que resetea el formulario de contactos.
	$scope.resetFormConPropio = function(){
		$scope.formCon.$setPristine();
		$scope.contacto = {};
		$scope.mostrarFormConPropio = false;
	}
		
	//Función que elimina el enlace entre el contacto y el sistema.
	$scope.delCon = function(contacto){
		dataService
			.deleteElements('contacto_sistema', '?filter=id_contacto%3D'+contacto.id+'%26%26id_sistema%3D'+$scope.sis.id)
			.then(function(data){
				console.log('Contacto eliminado correctamente.');
				$scope.buffer4 = true;
				contactosSistema = [];
				traerContactosSistema('contactos?filter=id%3E', 0, '%26%26id_sistema='+$scope.sis.id, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorConSis = parseError(error.status, error.data);
				$timeout(function(){$scope.errorConSis = null;}, 15000);
			});
	}
	
	//Función que manda señal de cerrar el formulario.
	$scope.cerrar = function(){
		$scope.sis = {};
		$scope.nuevo = false;
		$scope.mostrarFormContactos = false;
		$scope.contactosTotales = false;
		$scope.mostrar = false;
		$rootScope.$broadcast('cerrarContratos', false);
	}
	
	//Funciones que cierran los paneles de error.
	$scope.clearErrorConSis = function(){
		$scope.errorConSis =null;
	}
	$scope.clearErrorformCon = function(){
		$scope.errorFormCon =null;
	}
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('mostrar', function(e,data){
		$scope.mostrar = data;
		$scope.nuevo = data;
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('editar', function(e,data){
		$scope.buffer2 = true;
		$scope.buffer4 = true;
		$scope.sis = angular.copy(data);
		$scope.mostrar = true;
		$scope.nuevo = false;
		$scope.mostrarFormCon = false;
		$scope.contactosTotales = false;
		traerContactosSistema('contactos?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
		traerContactos('contacto?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y prepara el formulario.
	$scope.$on('recall', function(e,data){
		$scope.buffer2 = true;
		$scope.buffer4 = true;
		$scope.sis = angular.copy(data);
		$scope.mostrar = true;
		$scope.nuevo = false;
		$scope.mostrarFormCon = false;
		$scope.contactosTotales = false;
		traerContactosSistema('contactos?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.sis.id, 0, 0, 0, []);
		traerContactos('contacto?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre y cierra.
	$scope.$on('cerrarTodo', function(e, data){
		$scope.sis = {};
		$scope.nuevo = false;
		$scope.mostrarFormContactos = false;
		$scope.contactosTotales = false;
		$scope.mostrar = false;
	});
}