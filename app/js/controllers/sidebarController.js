/****************************************************************************/
/*							Controlador del menú							*/
/****************************************************************************/

App.controller('SidebarController', ['$rootScope', '$scope', '$state', '$http', '$timeout', 'Utils',
  function($rootScope, $scope, $state, $http, $timeout, Utils){

    var collapseList = [];

    // demo: when switch from collapse to hover, close all items
    /*$rootScope.$watch('app.layout.asideHover', function(oldVal, newVal){
      if ( newVal === false && oldVal === true) {
        closeAllBut(-1);
      }
    });
	*/
    
    // Check item and children active state
    var isActive = function(item) {
      if(!item) return;
      if(!item.sref || item.sref == '#') {
        var foundActive = false;
        angular.forEach(item.submenu, function(value, key) {
          if(isActive(value)) foundActive = true;
        });
        return foundActive;
      }
      else
        return $state.is(item.sref) || $state.includes(item.sref);
    };

    // Load menu from json file
    // ----------------------------------- 
    
    $scope.getMenuItemPropClasses = function(item) {
      return (item.heading ? 'nav-heading' : '') +
             (isActive(item) ? ' active' : '') ;
    };

    $scope.loadSidebarMenu = function() {
		//Gestionamos el menú según el rol que tenga el usuario.
		if($rootScope.user.is_sys_admin){
			var menuJson = 'server/sidebar-menu.json',
	        	menuURL  = menuJson + '?v=' + (new Date().getTime()); // jumps cache
	        console.log('Rol del usuario: Administrador');
		}else{
			switch($rootScope.user.role){
				case 'accesoBDD':
					var menuJson = 'server/sidebar-menu.json',
						menuURL  = menuJson + '?v=' + (new Date().getTime()); // jumps cache
					console.log('Rol del usuario: ', $rootScope.user.role);
					break;
				
				case 'Tecnico':
					var menuJson = 'server/sidebar-menu-tecnico.json',
						menuURL  = menuJson + '?v=' + (new Date().getTime()); // jumps cache
					console.log('Rol del usuario: ', $rootScope.user.role);
					break;
					
				case 'Incidencias':
					var menuJson = 'server/sidebar-menu-tecnico.json',
						menuURL  = menuJson + '?v=' + (new Date().getTime()); // jumps cache
					console.log('Rol del usuario: ', $rootScope.user.role);
					break;
			}
		}
        
		$http.get(menuURL)
        	.success(function(items) {
				$scope.menuItems = organizarMenu('',items);
			})
			.error(function(data, status, headers, config) {
				alert('Failure loading menu');
			});
    };
    $scope.loadSidebarMenu();
    
    //Función que inicializa el menú. Las variables activo e index.
    var organizarMenu = function(cad, items){
		for(var i=0; i<items.length; i++){
			if(cad == ''){
				items[i].activo = false;
				items[i].pIndex = (i+1);
			}else{
				items[i].activo = false;
				items[i].pIndex = cad+'-'+i;
			}			
			if(items[i].submenu){
				collapseList[items[i].pIndex] = true;
				items[i].submenu = organizarMenu(items[i].pIndex, items[i].submenu);
			}
		}
		return items;
    }
    
    // Handle sidebar collapse items
    // ----------------------------------- 
	
    $scope.addCollapse = function($index, item) {
    	collapseList[$index] = $rootScope.app.layout.asideHover ? true : !isActive(item);
    };
	
    $scope.isCollapse = function($index) {
      return (collapseList[$index]);
    };
	
    $scope.toggleCollapse = function($index, isParentItem) {
		//Identificamos si es alguno de los menus de configuración de Técnico o Comercial
		if($index == '6-2'){	//El index del menú correspondiente con la configuración del apartado Técnico.
			//Hacemos llamada recursiba para abrir la sección de configuración correspondiente a Técnico.
			collapseList[8] = false;
			collapseList['8-0'] = false;
			
		}else if($index == '7-3'){	//El index del menú correspondiente con la configuración del apartado Comercial.
			//Hacemos llamada recursiba para abrir la sección de configuración correspondiente a Comercial.
			collapseList[8] = false;
			collapseList['8-1'] = false;
			
		}else{
			// collapsed sidebar doesn't toggle drodopwn
			if( Utils.isSidebarCollapsed() /*|| $rootScope.app.layout.asideHover*/ ) return true;
	
			// make sure the item index exists
			if( angular.isDefined( collapseList[$index] ) ) {
	        	if (!$scope.lastEventFromChild) {
					collapseList[$index] = !collapseList[$index];
					//closeAllBut($index);
				}
			}else if (isParentItem) {
				collapseList[$index] = !collapseList[$index];
			}
		}

		$scope.lastEventFromChild = isChild($index);
		
		return true;
    };
	
	/*
    function closeAllBut(index) {
      index += '';
      for(var i in collapseList) {
        if(index < 0 || index.indexOf(i) < 0)
          collapseList[i] = true;
      }
    }
	*/
    function isChild($index) {
      return (typeof $index === 'string') && !($index.indexOf('-') < 0);
    }
    
}]);