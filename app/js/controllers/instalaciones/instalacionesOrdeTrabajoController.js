/****************************************************************************/
/*	Controlador de la vista de las Órdenes de Trabajo de cada Instalación	*/
/****************************************************************************/
App.controller('InstalacionesOrdenTrabajoCntrl', InstalacionesOrdenTrabajoCntrl);
function InstalacionesOrdenTrabajoCntrl($scope, $rootScope, $timeout, $modal, focus, uiGridConstants, dataService, ngDialog){
	
	$scope.errorFormMaterialesInst = null;
	$scope.errorMaterialesInst = null;
	$scope.mostrarFormMaterialesInst = false;
	
	$scope.errorFormRecursosInst = null;
	$scope.errorRecursosInst = null;
	$scope.mostrarFormRecursosInst = false;
	
	$scope.costeHoraHombre = 1;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ InstalacionesOrdenTrabajoCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var tooltipMateriales = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	var tooltipRecursos = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	//Variables para el header.
	var headerTemplateGears = '<div role="columnheader" ng-class="{ \'sortable\': sortable }" '+
		'ui-grid-one-bind-aria-labelledby-grid="col.uid + \'-header-text \' + col.uid + \'-sortdir-text\'" '+
		'aria-sort="{{col.sort.direction == asc ? \'ascending\' : ( col.sort.direction == desc ? \'descending\' : '+
		'(!col.sort.direction ? \'none\' : \'other\'))}}"><div role="button" tabindex="0" class="ui-grid-cell-contents '+
		'ui-grid-header-cell-primary-focus" col-index="renderIndex" title="TOOLTIP"> <span class="ui-grid-header-cell-label" '+
		'ui-grid-one-bind-id-grid="col.uid + \'-header-text\'"><i class="fa fa-gears"></i></span><span '+
		'ui-grid-one-bind-id-grid="col.uid + \'-sortdir-text\'" ui-grid-visible="col.sort.direction" aria-label="{{getSortDirectionAriaLabel()}}">'+
		'<i ng-class="{ \'ui-grid-icon-up-dir\': col.sort.direction == asc, \'ui-grid-icon-down-dir\': '+
		'col.sort.direction == desc, \'ui-grid-icon-blank\': !col.sort.direction }" title="{{col.sort.priority ? '+
		'i18n.headerCell.priority + \' \' + col.sort.priority : null}}" aria-hidden="true"></i><sub class="ui-grid-sort-priority-number">'+
		'{{col.sort.priority}}</sub></span></div><div role="button" tabindex="0" ui-grid-one-bind-id-grid="col.uid + \'-menu-button\'" '+
		'class="ui-grid-column-menu-button" ng-if="grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false" '+
		'ng-click="toggleMenu($event)" ng-class="{\'ui-grid-column-menu-button-last-col\': isLastCol}" '+
		'ui-grid-one-bind-aria-label="i18n.headerCell.aria.columnMenuButtonLabel" aria-haspopup="true"><i class="ui-grid-icon-angle-down" '+
		'aria-hidden="true">&nbsp;</i></div><div ui-grid-filter></div></div>';
	var headerTemplateTruck = '<div role="columnheader" ng-class="{ \'sortable\': sortable }" '+
		'ui-grid-one-bind-aria-labelledby-grid="col.uid + \'-header-text \' + col.uid + \'-sortdir-text\'" '+
		'aria-sort="{{col.sort.direction == asc ? \'ascending\' : ( col.sort.direction == desc ? \'descending\' : '+
		'(!col.sort.direction ? \'none\' : \'other\'))}}"><div role="button" tabindex="0" class="ui-grid-cell-contents '+
		'ui-grid-header-cell-primary-focus" col-index="renderIndex" title="TOOLTIP"> <span class="ui-grid-header-cell-label" '+
		'ui-grid-one-bind-id-grid="col.uid + \'-header-text\'"><i class="fa fa-truck"></i></span><span '+
		'ui-grid-one-bind-id-grid="col.uid + \'-sortdir-text\'" ui-grid-visible="col.sort.direction" aria-label="{{getSortDirectionAriaLabel()}}">'+
		'<i ng-class="{ \'ui-grid-icon-up-dir\': col.sort.direction == asc, \'ui-grid-icon-down-dir\': '+
		'col.sort.direction == desc, \'ui-grid-icon-blank\': !col.sort.direction }" title="{{col.sort.priority ? '+
		'i18n.headerCell.priority + \' \' + col.sort.priority : null}}" aria-hidden="true"></i><sub class="ui-grid-sort-priority-number">'+
		'{{col.sort.priority}}</sub></span></div><div role="button" tabindex="0" ui-grid-one-bind-id-grid="col.uid + \'-menu-button\'" '+
		'class="ui-grid-column-menu-button" ng-if="grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false" '+
		'ng-click="toggleMenu($event)" ng-class="{\'ui-grid-column-menu-button-last-col\': isLastCol}" '+
		'ui-grid-one-bind-aria-label="i18n.headerCell.aria.columnMenuButtonLabel" aria-haspopup="true"><i class="ui-grid-icon-angle-down" '+
		'aria-hidden="true">&nbsp;</i></div><div ui-grid-filter></div></div>';
	var headerTemplateHome = '<div role="columnheader" ng-class="{ \'sortable\': sortable }" '+
		'ui-grid-one-bind-aria-labelledby-grid="col.uid + \'-header-text \' + col.uid + \'-sortdir-text\'" '+
		'aria-sort="{{col.sort.direction == asc ? \'ascending\' : ( col.sort.direction == desc ? \'descending\' : '+
		'(!col.sort.direction ? \'none\' : \'other\'))}}"><div role="button" tabindex="0" class="ui-grid-cell-contents '+
		'ui-grid-header-cell-primary-focus" col-index="renderIndex" title="TOOLTIP"> <span class="ui-grid-header-cell-label" '+
		'ui-grid-one-bind-id-grid="col.uid + \'-header-text\'"><i class="fa fa-home"></i></span><span '+
		'ui-grid-one-bind-id-grid="col.uid + \'-sortdir-text\'" ui-grid-visible="col.sort.direction" aria-label="{{getSortDirectionAriaLabel()}}">'+
		'<i ng-class="{ \'ui-grid-icon-up-dir\': col.sort.direction == asc, \'ui-grid-icon-down-dir\': '+
		'col.sort.direction == desc, \'ui-grid-icon-blank\': !col.sort.direction }" title="{{col.sort.priority ? '+
		'i18n.headerCell.priority + \' \' + col.sort.priority : null}}" aria-hidden="true"></i><sub class="ui-grid-sort-priority-number">'+
		'{{col.sort.priority}}</sub></span></div><div role="button" tabindex="0" ui-grid-one-bind-id-grid="col.uid + \'-menu-button\'" '+
		'class="ui-grid-column-menu-button" ng-if="grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false" '+
		'ng-click="toggleMenu($event)" ng-class="{\'ui-grid-column-menu-button-last-col\': isLastCol}" '+
		'ui-grid-one-bind-aria-label="i18n.headerCell.aria.columnMenuButtonLabel" aria-haspopup="true"><i class="ui-grid-icon-angle-down" '+
		'aria-hidden="true">&nbsp;</i></div><div ui-grid-filter></div></div>';
	var headerTemplateClose = '<div role="columnheader" ng-class="{ \'sortable\': sortable }" '+
		'ui-grid-one-bind-aria-labelledby-grid="col.uid + \'-header-text \' + col.uid + \'-sortdir-text\'" '+
		'aria-sort="{{col.sort.direction == asc ? \'ascending\' : ( col.sort.direction == desc ? \'descending\' : '+
		'(!col.sort.direction ? \'none\' : \'other\'))}}"><div role="button" tabindex="0" class="ui-grid-cell-contents '+
		'ui-grid-header-cell-primary-focus" col-index="renderIndex" title="TOOLTIP"> <span class="ui-grid-header-cell-label" '+
		'ui-grid-one-bind-id-grid="col.uid + \'-header-text\'"><i class="fa fa-close"></i></span><span '+
		'ui-grid-one-bind-id-grid="col.uid + \'-sortdir-text\'" ui-grid-visible="col.sort.direction" aria-label="{{getSortDirectionAriaLabel()}}">'+
		'<i ng-class="{ \'ui-grid-icon-up-dir\': col.sort.direction == asc, \'ui-grid-icon-down-dir\': '+
		'col.sort.direction == desc, \'ui-grid-icon-blank\': !col.sort.direction }" title="{{col.sort.priority ? '+
		'i18n.headerCell.priority + \' \' + col.sort.priority : null}}" aria-hidden="true"></i><sub class="ui-grid-sort-priority-number">'+
		'{{col.sort.priority}}</sub></span></div><div role="button" tabindex="0" ui-grid-one-bind-id-grid="col.uid + \'-menu-button\'" '+
		'class="ui-grid-column-menu-button" ng-if="grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false" '+
		'ng-click="toggleMenu($event)" ng-class="{\'ui-grid-column-menu-button-last-col\': isLastCol}" '+
		'ui-grid-one-bind-aria-label="i18n.headerCell.aria.columnMenuButtonLabel" aria-haspopup="true"><i class="ui-grid-icon-angle-down" '+
		'aria-hidden="true">&nbsp;</i></div><div ui-grid-filter></div></div>';
	var headerTemplateInbox = '<div role="columnheader" ng-class="{ \'sortable\': sortable }" '+
		'ui-grid-one-bind-aria-labelledby-grid="col.uid + \'-header-text \' + col.uid + \'-sortdir-text\'" '+
		'aria-sort="{{col.sort.direction == asc ? \'ascending\' : ( col.sort.direction == desc ? \'descending\' : '+
		'(!col.sort.direction ? \'none\' : \'other\'))}}"><div role="button" tabindex="0" class="ui-grid-cell-contents '+
		'ui-grid-header-cell-primary-focus" col-index="renderIndex" title="TOOLTIP"> <span class="ui-grid-header-cell-label" '+
		'ui-grid-one-bind-id-grid="col.uid + \'-header-text\'"><i class="fa fa-inbox"></i></span><span '+
		'ui-grid-one-bind-id-grid="col.uid + \'-sortdir-text\'" ui-grid-visible="col.sort.direction" aria-label="{{getSortDirectionAriaLabel()}}">'+
		'<i ng-class="{ \'ui-grid-icon-up-dir\': col.sort.direction == asc, \'ui-grid-icon-down-dir\': '+
		'col.sort.direction == desc, \'ui-grid-icon-blank\': !col.sort.direction }" title="{{col.sort.priority ? '+
		'i18n.headerCell.priority + \' \' + col.sort.priority : null}}" aria-hidden="true"></i><sub class="ui-grid-sort-priority-number">'+
		'{{col.sort.priority}}</sub></span></div><div role="button" tabindex="0" ui-grid-one-bind-id-grid="col.uid + \'-menu-button\'" '+
		'class="ui-grid-column-menu-button" ng-if="grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false" '+
		'ng-click="toggleMenu($event)" ng-class="{\'ui-grid-column-menu-button-last-col\': isLastCol}" '+
		'ui-grid-one-bind-aria-label="i18n.headerCell.aria.columnMenuButtonLabel" aria-haspopup="true"><i class="ui-grid-icon-angle-down" '+
		'aria-hidden="true">&nbsp;</i></div><div ui-grid-filter></div></div>';
	
	$scope.columnasMateriales = [	  
		//Columnas fijas en la tabla 		    
		{name: 'editar', width: 70, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editarMaterial(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button><button type="button" data-ng-click="grid.appScope.delMaterial(row.entity)" title="Delete" class="btn btn-sm btn-danger"><em class="fa fa-trash"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false},
		    
		{name: 'estado', field: 'estado', width: 70, cellTemplate: '<div class="centrado"><i class="fa fa-circle" data-ng-class="'+"{'text-warning': row.entity.estado=='parcial', 'text-danger': row.entity.estado=='pendiente', 'text-success': row.entity.estado=='completo'}"+'"></i><span data-ng-show="false">{{row.entity.estado}}</span></div>', enableHiding: false, pinnedLeft: true, enablePinning: false, enableColumnResizing: false},
		    			
		//Columnas variables
		{name: 'nombre', field: 'nombre', enableHiding: false, enablePinning:false, minWidth: 90},
		
		{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 95, enableHiding: false, enablePinning:false, cellTemplate: tooltipMateriales, cellClass: 'cellToolTip hidden-xs'},
		
		{name: 'configurando', field: 'configurando', cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning:false, width: 55, minWidth: 55, headerCellTemplate: headerTemplateGears},
		
		{name: 'salidas', field: 'salidas', cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning:false, width: 55, minWidth: 55, headerCellTemplate: headerTemplateTruck},
		
		{name: 'entregado', field: 'entregado', cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning:false, width: 55, minWidth: 55, headerCellTemplate: headerTemplateHome},
		
		{name: 'devuelto', field: 'devuelto', cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning:false, width: 55, minWidth: 55, headerCellTemplate: headerTemplateClose},
		
		{name: 'solicitado', field: 'solicitado', cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning:false, width: 55, minWidth: 55, headerCellTemplate: headerTemplateInbox},
		
		{name: 'total', field: 'total', enableHiding: false, enableSorting: false, enablePinning:false, width: 80, minWidth: 80, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.total}} €</div>', headerCellClass : "hidden-xs", cellClass: "hidden-xs"}
	];
	
	$scope.columnasRecursos = [	  
		//Columnas fijas en la tabla 		    
		{name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editarRecurso(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false},
		    			
		//Columnas variables
		{name: 'nombre', field: 'nombre', enableHiding: false, enablePinning:false, minWidth: 90},
		
		{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 95, enableHiding: false, enablePinning:false, cellTemplate: tooltipRecursos, cellClass: 'cellToolTip hidden-xs'},
		
		{name: 'coste', field: 'coste', enableHiding: false, enableSorting: false, enablePinning:false, width: 80, minWidth: 80, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.coste}} €</div>', headerCellClass : "hidden-xs", cellClass: "hidden-xs"},
		
		{name: 'unidades', field: 'unidades', enableHiding: false, enableSorting: false, enablePinning:false, width: 80, minWidth: 80, headerCellClass : "hidden-xs", cellClass: "hidden-xs"}
	];
						
	//Inicializamos la tabla.
	$scope.gridMateriales = {
		paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		scrollable: false,
		enableHorizontalScrollbar: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnasMateriales,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};
	$scope.gridRecursos = {
		paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		scrollable: false,
		enableHorizontalScrollbar: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnasRecursos,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamanoMateriales = function(){
		if( (($scope.gridMateriales.paginationPageSize*36)+100) > (($scope.gridMateriales.data.length*36)+100) ){
			return ($scope.gridMateriales.data.length*36)+100;
		}else{
			return ($scope.gridMateriales.paginationPageSize*36)+100;
		}
	}
	$scope.comprobarTamanoRecursos = function(){
		if( (($scope.gridRecursos.paginationPageSize*36)+100) > (($scope.gridRecursos.data.length*36)+100) ){
			return ($scope.gridRecursos.data.length*36)+100;
		}else{
			return ($scope.gridRecursos.paginationPageSize*36)+100;
		}
	}
			
	//Función que retorna todos los materiales de la instalación.
	var traerMateriales = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.gridMateriales.data = [];
		$scope.totalMateriales = 0;
		$scope.porcentajeMateriales = 0;
	
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				for(var i=0; i<data.registros.length; i++){
					
					if(data.registros[i].configurando == 0 && data.registros[i].salidas == 0 && data.registros[i].entregado == 0 && data.registros[i].devuelto == 0){
						data.registros[i].estado =  'pendiente';
						
					}else if(data.registros[i].entregado >= data.registros[i].solicitado){
						data.registros[i].estado =  'completo';
						
					}else{
						data.registros[i].estado =  'parcial';
					}

					//Realizamos el cálculo del subtotal de cada campo.
					data.registros[i].total = (data.registros[i].coste+(data.registros[i].horas * $scope.costeHoraHombre)) * data.registros[i].solicitado;
					//Redondeamos el resultado a dos decimales (Esto devuelve un string).
					data.registros[i].total = parseFloat(""+data.registros[i].total).toFixed(2);
					//Lo convertimos en float.
					data.registros[i].total = parseFloat(data.registros[i].total);
					//Sumamos al total, cada subtotal.
					$scope.totalMateriales = $scope.totalMateriales + data.registros[i].total;
					//Redondeamos el resultado a dos decimales (Esto devuelve un string).
					$scope.totalMateriales = parseFloat(""+$scope.totalMateriales).toFixed(2);
					//Lo convertimos en float.
					$scope.totalMateriales = parseFloat($scope.totalMateriales);
				}
				
				//Comprobamos que el PVP no sea cero. Este caso podría darse si no se rellena el campo de PVP al insertar la Instalación.
				if($scope.inst.pvp > 0){
					//Calculamos el porcentaje con dos decimales  (Esto devuelve un string).
					$scope.porcentajeMateriales = (($scope.totalMateriales * 100) / $scope.inst.pvp).toFixed(2);
					//Lo convertimos en float.
					$scope.porcentajeMateriales = parseFloat($scope.porcentajeMateriales);
				}
				
				//Una vez realizadas todas las operaciones necesarias con los campos, lo copiamos a la variable de la tabla.
				$scope.gridMateriales.data = angular.copy(data.registros);
				$scope.bufferMateriales = false;
				
				console.log('Materiales de la instalación cargadas (InstalacionesOrdenTrabajoCntrl):', data.registros.length);
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorMaterialesInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
			});
	}
	
	//Función que retorna todos los productos de la BBDD.
	var traerProductos = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.gridMateriales.data = [];
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.productos = angular.copy(data.registros);
				console.log('Productos totales cargados (InstalacionesOrdenTrabajoCntrl):', data.registros.length);
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorMaterialesInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
			});
	}
	traerProductos('productos?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los valores de configuración.
	var traerConfig = function(){
		dataService
			.getAll('configuracion?filtro=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				for( var i=0; i<data.registros.length; i++){
					if(data.registros[i].parametro == 'costeHoraHombre'){
						$scope.costeHoraHombre = parseFloat(data.registros[i].valor);
					}
				}
				console.log('Coste de hora por hombre: '+$scope.costeHoraHombre+' €');
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorMaterialesInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
			});
	}
	traerConfig();
	
	//Función que retorna todos los materiales de la instalación.
	var traerRecursos = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.gridRecursos.data = [];
		$scope.totalRecursos = 0;
		$scope.porcentajeRecursos = 0;
	
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				for(var i=0; i<data.registros.length; i++){
										
					//Realizamos el cálculo del subtotal de cada campo.
					data.registros[i].total = data.registros[i].coste * data.registros[i].unidades;
					//Redondeamos el resultado a dos decimales (Esto devuelve un string).
					data.registros[i].total = parseFloat(""+data.registros[i].total).toFixed(2);
					//Lo convertimos en float.
					data.registros[i].total = parseFloat(data.registros[i].total);
					//Sumamos al total, cada subtotal.
					$scope.totalRecursos = $scope.totalRecursos + data.registros[i].total;
					//Redondeamos el resultado a dos decimales (Esto devuelve un string).
					$scope.totalRecursos = parseFloat(""+$scope.totalRecursos).toFixed(2);
					//Lo convertimos en float.
					$scope.totalRecursos = parseFloat($scope.totalRecursos);
				}
				
				//Comprobamos que el PVP no sea cero. Este caso podría darse si no se rellena el campo de PVP al insertar la Instalación.
				if($scope.inst.pvp > 0){
					//Calculamos el porcentaje con dos decimales  (Esto devuelve un string).
					$scope.porcentajeRecursos = (($scope.totalRecursos * 100) / $scope.inst.pvp).toFixed(2);
					//Lo convertimos en float.
					$scope.porcentajeRecursos = parseFloat($scope.porcentajeRecursos);
				}
				
				//Una vez realizadas todas las operaciones necesarias con los campos, lo copiamos a la variable de la tabla.
				$scope.gridRecursos.data = angular.copy(data.registros);
				$scope.bufferRecursos = false;
				
				console.log('Recursos de la instalación cargados (InstalacionesOrdenTrabajoCntrl):', data.registros.length);
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorRecursosInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorRecursosInst = null;}, 15000);
			});
	}
		
	//Función que trae los valores de configuración.
	var traerPlantillasMaterial = function(){
		dataService
			.getAll('plantillasMaterial?filtro=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				$scope.plantillas = angular.copy(data.registros);
				console.log('Plantillas de materiales recibidas correctamente (InstalacionesOrdenTrabajoCntrl): ', data.registros.length);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorMaterialesInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
			});
	}
	traerPlantillasMaterial();
	
	//Función que ejecuta el mensaje modal.
	var ModalInstanceCtrl = function ($scope, $modalInstance) {
		$scope.ok = function () {
			$modalInstance.close('closed');
		};
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
		$scope.mensaje = $rootScope.mensaje;
	};
	ModalInstanceCtrl.$inject = ["$scope", "$modalInstance"];
	
	//Función que abre el formulario para añadir un nuevo material.
	$scope.mostrarFormMateriales = function(){
		$scope.material = {};
		$scope.material.id_instalacion = $scope.inst.id;
		$scope.material.configurar = false;
		$scope.material.configurando = 0;
		$scope.material.salidas = 0;
		$scope.material.entregado = 0;
		$scope.material.devuelto = 0;
		$scope.material.solicitado = 1;
		$scope.productoSel = 'Seleccione un Producto';
		$scope.mostrarFormMaterialesInst = true;
		$scope.botonMateriales = 'Añadir';
		$scope.tituloMateriales = 'Formulario Añadir Material';
		focus('mainFocusForm');
	}
	
	//Función que abre el formulario para añadir un nuevo recurso.
	$scope.mostrarFormRecursos = function(){
		$scope.recurso = {};
		$scope.recurso.id_instalacion = $scope.inst.id;
		$scope.recurso.coste = 1;
		$scope.recurso.unidades = 1;
		$scope.mostrarFormRecursosInst = true;
		$scope.botonRecursos = 'Añadir';
		$scope.tituloRecursos = 'Formulario Añadir Recurso';
		focus('mainFocusRecursos');
	}
	
	//Funciónq ue abre el formulario pero en forma de edición del material.
	$scope.editarMaterial = function(material){
		$scope.material = angular.copy(material);
		$scope.productoSel = $scope.material.nombre+' - '+$scope.material.descripcion;
		$scope.mostrarFormMaterialesInst = true;
		$scope.botonMateriales = 'Modificar';
		$scope.tituloMateriales = 'Formulario Editar Material';
		focus('mainFocusForm');
	}
	
	//Funciónq ue abre el formulario pero en forma de edición del recurso.
	$scope.editarRecurso = function(recurso){
		$scope.recurso = angular.copy(recurso);
		$scope.mostrarFormRecursosInst = true;
		$scope.botonRecursos = 'Modificar';
		$scope.tituloRecursos = 'Formulario Editar Recurso';
		focus('mainFocusRecursos');
	}
	
	//Función que modifica el listado de materiales según la plantilla que seleccionemos.
	$scope.cambiarPlantilla = function(plantilla){
		var modalInstance = $modal.open({
			templateUrl: '/app/views/partials/ngDialogConfirm.html',
			controller: ModalInstanceCtrl,
			size: 'sm'
		});
		
		var state = $('#modal-state');
		modalInstance.result.then(function () {
			$scope.totalMateriales = 0;
			$scope.porcentajeMateriales = 0;
			for(var i=0; i<$scope.plantillas.length; i++){			
				if($scope.plantillas[i].id == plantilla){
					//Hacemos las llamadas por cada uno de los materiales .
					insertarMateriales(JSON.parse($scope.plantillas[i].materiales));
					break;
				}
			}
		},function () {
			console.log('Cancelo el implantar la plantilla');
		});
	}
	
	//Función recursiva que inserta los materiales en la lista de materiales.
	var insertarMateriales = function(materiales){
		console.log('Materiales: ', materiales);
		var elemento = {};
		for(var i=0; i<$scope.productos.length; i++){
			if($scope.productos[i].referencia == materiales[0].referencia){
				//Inicializamos las variables propias de los materiales e inicializamos los valores del estado y los totales.
				elemento = angular.copy($scope.productos[i]);
				elemento.id = undefined;
				elemento.id_instalacion = $scope.inst.id;
				elemento.configurando = 0;
				elemento.salidas = 0;
				elemento.entregado = 0;
				elemento.devuelto = 0;
				elemento.solicitado = materiales[0].cantidad;
				elemento.estado =  'pendiente';
				
				//Realizamos el cálculo del subtotal de cada campo.
				elemento.total = (elemento.coste+(elemento.horas * $scope.costeHoraHombre)) * elemento.solicitado;
				//Redondeamos el resultado a dos decimales (Esto devuelve un string).
				elemento.total = parseFloat(""+elemento.total).toFixed(2);
				//Lo convertimos en float.
				elemento.total = parseFloat(elemento.total);
				//Sumamos al total, cada subtotal.
				$scope.totalMateriales = $scope.totalMateriales + elemento.total;
				//Redondeamos el resultado a dos decimales (Esto devuelve un string).
				$scope.totalMateriales = parseFloat(""+$scope.totalMateriales).toFixed(2);
				//Lo convertimos en float.
				$scope.totalMateriales = parseFloat($scope.totalMateriales);
		
				if($scope.inst.pvp > 0){
					//Calculamos el porcentaje con dos decimales  (Esto devuelve un string).
					$scope.porcentajeMateriales = (($scope.totalMateriales * 100) / $scope.inst.pvp).toFixed(2);
					//Lo convertimos en float.
					$scope.porcentajeMateriales = parseFloat($scope.porcentajeMateriales);
				}
				
				console.log('Elemento: ', elemento);
				
				dataService
					.postElements('materiales', elemento)
					.then(function(data){
						console.log('Material insertado: ', data.id);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorMaterialesInst = parseError(error.status, error.data);
						$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
					});
				break;
			}
		}
		var aux = materiales.slice(1, materiales.length);		
		if(aux.length >= 1){
			insertarMateriales(aux);
		}else{
			$scope.bufferMateriales = true;
			$timeout(function(){traerMateriales('materiales?filter=id%3E', 0, '%26%26id_instalacion%3D'+$scope.inst.id+'&order=familia', 0, 0, 0, []);}, 250);
		}
	}
	
	//Función que elimina la plantilla que haya seleccionada en el momento.
	$scope.eliminarPlantilla = function(plantilla){
		if(plantilla != undefined){
			dataService
				.deleteElements('plantillasMaterial', '?filter=id%3D'+plantilla)
				.then(function(data){
					console.log('Plantilla eliminada correctamente: ', data.record[0].id);
					$scope.plantillaSel = null;
					traerPlantillasMaterial();
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorMaterialesInst = parseError(error.status, error.data);
					$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
				})
		}else{
			$scope.errorMaterialesInst = 'Debe seleccionar una plantilla para eliminar.';
			$timeout(function(){$scope.errorMaterialesInst = null;}, 10000);
		}
	}
	
	//Función que refresca el select de plantillas.
	$scope.refreshPlantillas = function(){
		$scope.plantillaSel = null;
		traerPlantillasMaterial();
	}
	
	/*------------------------------ Manejador del Select ------------------------------*/
	//Función que despliega el select.
	$scope.desplegar = function(){
		$scope.desplegarUl = !$scope.desplegarUl;
		if($scope.desplegarUl == true){
			focus('focusMe');
			$scope.filtro = '';
		}else{
			focus('mainFocus');
		}
	}
	
	//Función que sustituye el producto actual (convertido en material) por el seleccionado de la tabla.
	$scope.cambiarProducto = function(producto){
		//Comprobamos si el producto contiene mensaje de aviso, en cuyo caso lo mostramos.
		if(producto.aviso == undefined || producto.aviso == null || producto.aviso == ""){
			$scope.productoSel = producto.nombre+' - '+producto.descripcion;
			$scope.material.referencia = producto.referencia;
			$scope.material.nombre = producto.nombre;
			$scope.material.descripcion = producto.descripcion;
			$scope.material.fabricante = producto.fabricante;
			$scope.material.proveedor = producto.proveedor;
			$scope.material.coste = producto.coste;
			$scope.material.horas = producto.horas;
			$scope.material.configurar = producto.configurar;
			$scope.material.familia = producto.familia;		
			$scope.desplegarUl = !$scope.desplegarUl;
			$scope.formMaterialInst.producto.$dirty = false;
			$scope.filtro = '';
			focus('mainFocus');
		}else{
			$rootScope.mensaje = producto.aviso;
			
			var modalInstance = $modal.open({
			templateUrl: '/app/views/partials/ngDialogConfirmProducto.html',
			controller: ModalInstanceCtrl,
			size: 'sm'
			});
			
			var state = $('#modal-state');
			modalInstance.result.then(function () {
				$scope.productoSel = producto.nombre+' - '+producto.descripcion;
				$scope.material.referencia = producto.referencia;
				$scope.material.nombre = producto.nombre;
				$scope.material.descripcion = producto.descripcion;
				$scope.material.fabricante = producto.fabricante;
				$scope.material.proveedor = producto.proveedor;
				$scope.material.coste = producto.coste;
				$scope.material.horas = producto.horas;
				$scope.material.configurar = producto.configurar;
				$scope.material.familia = producto.familia;		
				$scope.desplegarUl = !$scope.desplegarUl;
				$scope.formMaterialInst.producto.$dirty = false;
				$scope.filtro = '';
				focus('mainFocus');
			});
		}
	}	
		
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masProductos = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogProductosMaterial.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (producto) {
			$scope.cambiarProducto(producto);
	    }, function (reason) {
		    $scope.filtro = '';
	    });
	}
	
	//Función que se encarga de filtrar por nombre o descripción del producto.
	$scope.filtrar = function(item){
		if($scope.filtro == undefined){
			return true;
		}else if(item.nombre.toLowerCase().indexOf($scope.filtro.toLowerCase()) != -1 || 
				item.descripcion.toLowerCase().indexOf($scope.filtro.toLowerCase()) != -1){
			return true;
		}else{
			return false;
		}
	}
	/*----------------------------------------------------------------------------------*/
		
	//Función que añade el material de la instalación.
	$scope.addMaterial = function(){
		if($scope.productoSel == 'Seleccione un Producto'){
			$scope.errorFormMaterialesInst = 'Tiene que seleccionar un producto.';
		}else{
			//Comprobamos si es una edición o una inserción.
			if($scope.material.id){
				dataService
					.putElements('materiales', $scope.material)
					.then(function(data){
						console.log('Material modificado: ', data.id);
						$scope.resetFormMaterialesInst();
						traerMateriales('materiales?filter=id%3E', 0, '%26%26id_instalacion='+$scope.inst.id+'&order=familia', 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormMaterialesInst = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormMaterialesInst = null;}, 15000);
					});
			}else{
				dataService
					.postElements('materiales', $scope.material)
					.then(function(data){
						console.log('Material insertado: ', data);
						$scope.resetFormMaterialesInst();
						traerMateriales('materiales?filter=id%3E', 0, '%26%26id_instalacion='+$scope.inst.id+'&order=familia', 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormMaterialesInst = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormMaterialesInst = null;}, 15000);
					});
			}
		}
	}
	
	//Función que añade el recurso a la instalación.
	$scope.addRecurso = function(){
		//Comprobamos si es una edición o una inserción.
		if($scope.recurso.id){
			dataService
				.putElements('recursos', $scope.recurso)
				.then(function(data){
					console.log('Recursos modificados: ', data.id);
					$scope.resetFormRecursosInst();
					traerRecursos('recursos?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormRecursosInst = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormRecursosInst = null;}, 15000);
				});
		}else{
			dataService
				.postElements('recursos', $scope.recurso)
				.then(function(data){
					console.log('Recurso insertado: ', data);
					$scope.resetFormRecursosInst();
					traerRecursos('recursos?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormRecursosInst = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormRecursosInst = null;}, 15000);
				});
		}
	}
	
	//Función que eliminar un material de la instalación.
	$scope.delMaterial = function(material){
		dataService
			.deleteElements('materiales', '?filter=id%3D'+material.id)
			.then(function(data){
				console.log('Material eliminado: ', data);
				$scope.resetFormMaterialesInst();
				traerMateriales('materiales?filter=id%3E', 0, '%26%26id_instalacion='+$scope.inst.id+'&order=familia', 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormMaterialesInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormMaterialesInst = null;}, 15000);
			});
	}
	
	//Función que eliminar un recurso de la instalación.
	$scope.delRecurso = function(){
		dataService
			.deleteElements('recursos', '?filter=id%3D'+$scope.recurso.id)
			.then(function(data){
				console.log('Recurso eliminado: ', data);
				$scope.resetFormRecursosInst();
				traerRecursos('recursos?filter=id%3E', 0, '%26%26id_instalacion%3D'+$scope.inst.id, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormRecursosInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormRecursosInst = null;}, 15000);
			});
	}
		
	//Función que resetea los valores del formulario de Materiales.
	$scope.resetFormMaterialesInst = function(){
		$scope.errorFormMaterialesInst = null;
		$scope.material = {};
		$scope.mostrarFormMaterialesInst = false;
		$scope.formMaterialInst.$setPristine();
	}
	
	//Función que resetea los valores del formulario de Recursos.
	$scope.resetFormRecursosInst = function(){
		$scope.errorFormRecursosInst = null;
		$scope.recursos = {};
		$scope.mostrarFormRecursosInst = false;
		$scope.formRecursos.$setPristine();
	}
	
	//Función que nos indica si está la plantilla ya creada o no.
	var estaPlantilla = function(nombre){
		for(var i=0; i<$scope.plantillas.length; i++){
			//La comprobación la hago pasando todo a minúsculas, puesto que la restricción UNIQUE de la BBDD no distingue entre mayúsculas y minúsculas.
			if($scope.plantillas[i].nombre.toLowerCase() == nombre.toLowerCase()){
				return $scope.plantillas[i].id;
			}
		}
		return 0;
	}
	
	//Función que guarda la plantilla de materiales.
	$scope.crearPlantilla = function(){
		//Si hay alguna plantilla seleccionada, recorremos las plantillas para poner el nombre en el formulario.
		if($scope.plantillaSel != undefined){
			$scope.plantilla = {};
			for(var i=0; i<$scope.plantillas.length; i++){
				if($scope.plantillas[i].id == $scope.plantillaSel){
					$scope.plantilla.nombre = $scope.plantillas[i].nombre;
				}
			}
		}
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogPlantillaMaterial.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (plantilla) {
			
	    }, function (reason) {
		    
	    });
	}
	
	//Función que comprueba si el nombre está ya en la lista de plantillas y en caso afirmativo lanza un mensaje de conformidad de reescribirla.
	$scope.comprobarNombre = function(plantilla){
		
		//Si no hay materiales añadidos mostramos un error.
		if($scope.gridMateriales.data.length == 0){
			$scope.errorMaterialesInst = 'No ha añadido materiales, por lo que no se puede crear la plantilla.';
			$timeout(function(){$scope.errorMaterialesInst = null;}, 10000);
		}else{
			var id = estaPlantilla(plantilla.nombre);
			if(id != 0){
			    
			    var modalInstance = $modal.open({
			    	templateUrl: '/app/views/partials/ngDialogConfirm.html',
					controller: ModalInstanceCtrl,
					size: 'sm'
			    });
			
			    var state = $('#modal-state');
			    modalInstance.result.then(function () {
			    	var materiales = [];
			    	
					for(var i=0; i<$scope.gridMateriales.data.length; i++){
						materiales.push({"referencia": $scope.gridMateriales.data[i].referencia, "cantidad": $scope.gridMateriales.data[i].solicitado});
					}
					
					//Pasamos el array de tipo objeto a texto plano para guardarlo en la tabla.
					plantilla.materiales = JSON.stringify(materiales);
					plantilla.id = id;
					
					dataService
						.putElements('plantillasMaterial', plantilla)
						.then(function(data){
							console.log('Plantilla modificada satisfactoriamente');
							$scope.plantillaSel = null;
							traerPlantillasMaterial();
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.errorMaterialesInst = parseError(error.status, error.data);
							$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
						});
			    },function () {
			    	console.log('Cancelo el hacer la plantilla');
			    });
			    
			}else{
				
				var materiales = [];
				
				for(var i=0; i<$scope.gridMateriales.data.length; i++){
					materiales.push({"referencia": $scope.gridMateriales.data[i].referencia, "cantidad": $scope.gridMateriales.data[i].solicitado});
				}
				
				//Pasamos el array de tipo objeto a texto plano para guardarlo en la tabla.
				plantilla.materiales = JSON.stringify(materiales);
	
				dataService
					.postElements('plantillasMaterial', plantilla)
					.then(function(data){
						console.log('Plantilla guardada satisfactoriamente');
						$scope.plantillaSel = null;
						traerPlantillasMaterial();
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorMaterialesInst = parseError(error.status, error.data);
						$timeout(function(){$scope.errorMaterialesInst = null;}, 15000);
					});
			}
		}
	}
	
	//Función que manda orden de cerrar todo.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarPendientes', false);
	}
		
	//Función que escucha la llamada del padre, inicializa las variables necesarias y recupera los registros de las fases.
	$scope.$on('mostrar', function(e, data){
		$scope.nuevo = true;
	});
	
	//Recibimos llamada de edición de una instalación, inicializamos las variables necesarias y llamamos a las fases de dicha instlación.
	$scope.$on('editar', function(e, data){
		$scope.inst = angular.copy(data);
		$scope.informacion = false;
		$scope.bufferMateriales = true;
		$scope.bufferRecursos = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		$scope.plantillaSel = null;
		traerMateriales('materiales?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id+'&order=familia', 0, 0, 0, []);
		traerRecursos('recursos?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id, 0, 0, 0, []);
		traerPlantillasMaterial();
	});
	
	//Función que indica que es un recall de la instalación
	$scope.$on('recall', function(e, data){
		$scope.inst = angular.copy(data.instalacion);
		$scope.sistemaRecall = angular.copy(data.sistema);
		$scope.bufferMateriales = true;
		$scope.bufferRecursos = true;
		$scope.nuevo = false;		
		$scope.mostrar = true;
		$scope.recall = true;
		traerMateriales('materiales?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id+'&order=familia', 0, 0, 0, []);
		traerRecursos('recursos?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id, 0, 0, 0, []);
		traerPlantillasMaterial();
	});
	
	//Función que recoje la llamada del padre para cerrar la parte de edición.
	$scope.$on('cerrarTodo', function(e,data){
		$scope.material = {};
		$scope.recurso = {};
		
		$scope.errorFormMaterialesInst = null;
		$scope.errorMaterialesInst = null;
		$scope.mostrarFormMaterialesInst = false;
		
		$scope.errorFormRecursosInst = null;
		$scope.errorRecursosInst = null;
		$scope.mostrarFormRecursosInst = false;
	});
	
	//Funciones para cerrar los mensajes de error.
	$scope.clearErrorFormMaterialesInst = function(){
		$scope.errorFormMaterialesInst = null;
	}
	$scope.clearErrorMaterialesInst = function(){
		$scope.errorMaterialesInst = null;
	}
	$scope.clearErrorFormRecursosInst = function(){
		$scope.errorFormRecursosInst = null;
	}
	$scope.clearErrorRecursosInst = function(){
		$scope.errorRecursosInst = null;
	}
	
}