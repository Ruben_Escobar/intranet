/****************************************************************************/
/*		Controlador de la vista de los Pendientes de cada Instalación		*/
/****************************************************************************/
App.controller('InstalacionesPendientesCntrl', InstalacionesPendientesCntrl);
function InstalacionesPendientesCntrl($scope, $rootScope, $timeout, focus, uiGridConstants, dataService){
	
	$scope.errorFormPenInst = null;
	$scope.errorPenInst = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ InstalacionesPendientesCntrl ------------------------------');
		
	//Función que retorna todas las incidencias del sistema.
	var traerPendientes = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.pendientesFinal = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				$scope.pendientesFinal = angular.copy(data.registros);
				console.log('Pendientes de la instalación cargadas (InstalacionesPendientesCntrl):', data.registros.length);
			})	
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPenInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPenInst = null;}, 15000);
			});
	}
	
	//Función que trae los tipos de pendiente de la tabla de configuraciones.
	var traerTipoPendiente = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.tiposPendiente = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				var tipos = data.registros[0].valor.split(",");
				var obj = {};
				for(var i=0; i<tipos.length; i++){
					obj = {};
					obj.nombre = tipos[i];
					obj.id = (i+1);
					$scope.tiposPendiente.push(obj);
				}
				if($scope.penInst.tipo != ''){
					for(var i=0; i<$scope.tiposPendiente.length; i++){
						if($scope.tiposPendiente[i].nombre == $scope.penInst.tipo){
							$scope.penInst.tipo = $scope.tiposPendiente[i].id;
							break;
						}
					}
				}
				console.log('Traigo los tipos de pendientes (PendientesCntrl): ', $scope.tiposPendiente);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que abre el formulario para añadir un nuevo pendiente.
	$scope.newPenInst = function(){
		$scope.penInst = {};
		//Creamos la referencia ejemplo del pendiente.
		$scope.penInst.referencia = "I"+$scope.inst.id+"P"+crearReferencia();
		$scope.penInst.id_instalacion = $scope.inst.id;
		$scope.penInst.instalacion = $scope.inst.descripcion;
		$scope.penInst.estado = 'ABIERTO';
		$scope.mostrarFormPenInst = true;
		$scope.boton = 'Añadir';
		$scope.titulo = 'Formulario Añadir';
		traerTipoPendiente('configuracion?filter=id%3E', 0, "%26%26parametro='tipoPendiente'", 0, 0, 0, []);
	}
	
	//Funciónq ue abre el formulario pero en forma de edición.
	$scope.editPendiente = function(pendiente){
		$scope.penInst = angular.copy(pendiente);
		console.log($scope.penInst);
		$scope.penInst.instalacion = $scope.inst.descripcion;
		$scope.mostrarFormPenInst = true;
		$scope.boton = 'Modificar';
		$scope.titulo = 'Formulario Editar';
		traerTipoPendiente('configuracion?filter=id%3E', 0, "%26%26parametro='tipoPendiente'", 0, 0, 0, []);
	}
	
	//Función que añade la instalación nueva al sistema.
	$scope.add = function(){
		for(var i=0; i<$scope.tiposPendiente.length; i++){
			if($scope.tiposPendiente[i].id == $scope.penInst.tipo){
				$scope.penInst.tipo = $scope.tiposPendiente[i].nombre;
				break;
			}
		}
		//Comprobamos si es una edición o una inserción.
		if($scope.penInst.id){
			dataService
				.putElements('pendientes', $scope.penInst)
				.then(function(data){
					console.log('Pendiente modificada: ', data);
					$scope.resetFormPenInst();
					traerPendientes('pendientes?filter=id%3E', 0, '%26%26id_instalacion='+$scope.inst.id, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormPenInst = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormPenInst = null;}, 15000);
				});
		}else{
			dataService
				.postElements('pendientes', $scope.penInst)
				.then(function(data){
					console.log('Pendiente insertada: ', data);
					$scope.resetFormPenInst();
					traerPendientes('pendientes?filter=id%3E', 0, '%26%26id_instalacion='+$scope.inst.id, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormPenInst = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormPenInst = null;}, 15000);
				});
		}		
	}
	
	//Función que eliminar una llave del sistema.
	$scope.delPendiente = function(){
		dataService
			.deleteElements('pendientes', '?filter=id%3D'+$scope.penInst.id)
			.then(function(data){
				console.log('Pendiente eliminada: ', data);
				$scope.resetFormPenInst();
				traerPendientes('pendientes?filter=id%3E', 0, '%26%26id_instalacion='+$scope.inst.id, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormPenInst = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormPenInst = null;}, 15000);
			});
	}
		
	//Función que resetea los valores del formulario.
	$scope.resetFormPenInst = function(){
		$scope.errorFormPenInst = null;
		$scope.penInst = {};
		$scope.mostrarFormPenInst = false;
		$scope.formPenInst.$setPristine();
	}
	
	//Función qu emanda orden de cerrar todo.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarPendientes', false);
	}
	
	//Función que escucha la llamada del padre, inicializa las variables necesarias y recupera los registros de las fases.
	$scope.$on('mostrar', function(e, data){
		$scope.nuevo = true;
	});
	
	//Recibimos llamada de edición de una instalación, inicializamos las variables necesarias y llamamos a las fases de dicha instlación.
	$scope.$on('editar', function(e, data){
		$scope.inst = angular.copy(data);
		$scope.informacion = false;
		$scope.buffer = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		$scope.fechaHora = null;
		$scope.fasesIns = [];
		$scope.mostrarFormPenInst = false;
		traerPendientes('pendientes?filter=id%3E', 0, '%26%26id_instalacion='+data.id, 0, 0, 0, []);
	});
	
	//Función que indica que es un recall de la instalación
	$scope.$on('recall', function(e, data){
		$scope.inst = angular.copy(data.instalacion);
		$scope.sistemaRecall = angular.copy(data.sistema);
		$scope.informacion = false;
		$scope.buffer = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		$scope.recall = true;
		$scope.fechaHora = null;
		$scope.fasesIns = [];
		$scope.mostrarFormPenInst = false;
		traerPendientes('pendientes?filter=id%3E', 0, '%26%26id_instalacion='+data.instalacion.id, 0, 0, 0, []);
	});
	
	//Función que recoje la llamada del padre para cerrar la parte de edición.
	$scope.$on('cerrarTodo', function(e,data){
		$scope.errorFormPenInst = null;
		$scope.penInst = {};
		$scope.mostrarFormPenInst = false;
		$scope.errorPenInst = null;
	});
	
	//Funciones para cerrar los mensajes de error.
	$scope.clearErrorFormPenInst = function(){
		$scope.errorFormPenInst = null;
	}
	$scope.clearErrorPenInst = function(){
		$scope.errorPenInst = null;
	}
	
}