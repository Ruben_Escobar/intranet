/****************************************************************************/
/*					Controlador del formulario de Instalaciones				*/
/****************************************************************************/
App.controller('InstalacionesFormCntrl', InstalacionesFormCntrl);
function InstalacionesFormCntrl($rootScope, $scope, $timeout, uiGridConstants, focus, ngDialog, dataService, SerInstalaciones){
		
	$scope.buffer3 = true;
	$scope.progressValue3 = 0;
	$scope.sistemaSel = 'Seleccione un Sistema';
	$scope.nuevo = false;
	$scope.open = false;
	$scope.open2 = false;
	$scope.contactos = undefined;
	$scope.errorFormIns = null;
	
	var registrosSis = [];
	var total3 = 0;
	var acumulados3 = 0;
	var fasesAux = [];
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ InstalacionesFormCntrl ------------------------------');
	
	//Llamada get para recoger los sistemas y mostrarlos en la pestaña del formulario.
	var traerSis = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (traerSis InstalacionesFormCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
					}
					$scope.sistemas = angular.copy(data.registros);
					$scope.buffer3 = false;
					$scope.progressValue3 = 0;
				}else{
					$scope.progressValue3 = data.progressValue;
					console.log('Cargando (traerSis InstalacionesFormCntrl): '+$scope.progressValue3+'%.');
					$scope.sistemas = angular.copy(data.registros);
					traerSis(tabla, data.registros[(data.registros.length-1)].id, acumulados, progressValue, total, registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	traerSis('sistema?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los contactos de la instalación (Los mismos que del sistema).
	var traerCont = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				var contactosRecibidos = angular.copy(data.registros);
				console.log('Contactos (InstalacionesFormCntrl): ', contactosRecibidos.length);
				$scope.contactos = [];
				if(contactosRecibidos.length>0){
					for(var i=0; i<contactosRecibidos.length; i++){
						
						var conAux = contactosRecibidos[i].nombre;
						
						if(contactosRecibidos[i].apellidos != null && contactosRecibidos[i].apellidos !=undefined && contactosRecibidos[i].apellidos !=''){
							conAux = conAux+', '+contactosRecibidos[i].apellidos;
						}
						if(contactosRecibidos[i].telefono != null && contactosRecibidos[i].telefono != undefined){
							conAux = conAux+' - '+contactosRecibidos[i].telefono;
						}
						if(contactosRecibidos[i].movil != null && contactosRecibidos[i].movil != undefined){
							conAux = conAux+' - '+contactosRecibidos[i].movil;
						}
						if(contactosRecibidos[i].correo != null && contactosRecibidos[i].correo != undefined){
							conAux = conAux+' - '+contactosRecibidos[i].correo;
						}
						
						$scope.contactos.push({"cuerpo": conAux, "notas": contactosRecibidos[i].notas});
					}
				}else{
					$scope.contactos = undefined;
				}
				//Una vez que tenemos todos los datos del contrato, mostramos el cuadro dentro del formulario de incidencias.
				$scope.mostrarContrato = true;
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Traemos las fases Master para añadirlas a la tabla fase_ins como fases por defecto de la instalación.
	var traerFasesMaster = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				fasesAux = angular.copy(data.registros);
				fasesAux = ordenarFasesMaestras(fasesAux);
				console.log('Fases Master recuperadas: ', fasesAux.length);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormIns = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormIns = null;}, 15000);
			});
	}
	traerFasesMaster('fase?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los presupuestos.
	var traerPresupuestos = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.buffer = true;
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total presupuestos (SistemasContratoCntrl): ', data.registros.length);
					$scope.presupuestos = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 100;
				}else{
					$scope.progressValue = data.progressValue;
					traerClientes(data.tabla, data.registros[data.registros.length-1].id, data.acumulados, data.progressValue, data.total, data.registros)
				}
			})
			.catch(function(Error){
				
			});
	}
	
	//Función que trae los valores de configuración.
	var traerConfig = function(){
		dataService
			.getAll('configuracion?filtro=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				for( var i=0; i<data.registros.length; i++){
					if(data.registros[i].parametro == 'costeHoraHombre'){
						$scope.costeHoraHombre = parseFloat(data.registros[i].valor);
					}
				}
				console.log('Coste de hora por hombre: '+$scope.costeHoraHombre+' €');
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorCatalogo = parseError(error.status, error.data);
				$timeout(function(){$scope.errorCatalogo = null;}, 15000);
			});
	}
	traerConfig();
	
	//Función que ordena las fases maestras.
	var ordenarFasesMaestras = function(fases){
		var fasesOrd = [];
		var indice = 0;
		var fin = 0;
		for(var i=0; i<fases.length;i++){
			if(fases[i].orden == 99999){
				fasesOrd[fases.length-1] = fases[i];
				indice++;
				break;
			}
		}
		while(fasesOrd[0] == undefined){
			fin = fases.length-indice;
			for(var j=0; j<fases.length; j++){
				if(fasesOrd[fin].id == fases[j].orden){
					indice++;
					fin--;
					fasesOrd[fin] = fases[j];
					break;
				}
			}
		}
		return fasesOrd;
	}
	
	//Función que resetea el formulario de incidencias.	
	$scope.resetFormInst = function(){
		$scope.filtro='';
		$scope.formInst.$setPristine();
		$scope.inst = {};
		$scope.currentError = null;
		$scope.informacion = false;
		$scope.recall = false;
		$scope.contactos = undefined;
		$scope.sistemaSel = 'Seleccione un Sistema';
		SerInstalaciones.data.modificado = false;
		$scope.formInst.$dirty = false;
		$rootScope.$broadcast('cerrarFormulario', true);
	}
	
	//Función que testea los cambios del formulario y avisa en caso de haber cambios antes de cerrar.
	$scope.cerrar = function(){
		if($scope.formInst.$dirty){
			if(confirm('Hay cambios en el formulario, si cierra los perderá')){
				$scope.resetFormInst();
			}
		}else{
			$scope.resetFormInst();
		}
	}
	
	//Función que cambia el estado del servicio al actuar sobre algún campo del formualrio.
	$scope.cambiarModificado = function(){
		SerInstalaciones.data.modificado = true;
	}
	
	//Función que comprueba si es una inserción o una modificación de incidencia, y procede.
	$scope.add = function(){
		if($scope.sistemaSel!='Seleccione un Sistema'){
			//Al crear una instalación, esta cogera las fases por defecto de la tabla fase.
			if($scope.nuevo){
				var hoy = new Date();
				$scope.inst.inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
										+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
				
				//insertamos la instalación.
				dataService
					.postElements('instalacion', $scope.inst)
					.then(function(data){
						console.log('Instalación insertada: ', data.id);
						var id = data.id;
						var fasesSel = [];
						var hoy = new Date();
						for(var i=0; i<fasesAux.length; i++){
							//Creamos los objetos de la tabla fase_ins mediante los de la tabla fase.
							fasesSel[i] = {
								"id_instalacion": id,
								"nombre": fasesAux[i].nombre,
								"descripcion": fasesAux[i].descripcion,
								"caducidad": fasesAux[i].caducidad
							};
						}
						//Insertamos las fases por defecto de la instalación.
						dataService
							.postElements('fase_ins', fasesSel)
							.then(function(data){
								console.log('Fases Base añadidas a la instalación '+id);
								//Una vez insertadas, las recuperamos para modificarlas el campo orden.
								dataService
									.getFiltro('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+id, 0, 0, 0, [])
									.then(function(data){
										console.log('Recuperadas las fases de la instalación '+id+': ', data.registros.length);
										var fases = angular.copy(data.registros);
										//Procesamos los datos de la consulta get para poner los campos orden en su valor edacuado.
										//Iniciamos la primera fase poniendole como fecha de inicio la fecha actual
										//y como limite la fecha inicial mas la caducidad.
										var c = 0;
										for(var i=0; i<fases.length; i++){
											if(i==0){
												fases[i].inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
																	+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
												//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
												var tiempo = hoy.getTime();
												var mil = parseInt(fases[i].caducidad*24*60*60*1000);
												var fin = new Date();
												fin.setTime(tiempo+mil);
												fases[i].limite = ""+fin.getFullYear()+'-'+twoDigits(fin.getMonth()+1)+'-'+twoDigits(fin.getDate())+' '
																	+twoDigits(fin.getHours())+':'+twoDigits(fin.getMinutes())+":00";
												fases[i].orden = 0;
											}else{
												fases[i].orden = fases[(i-1)].id;
											}
										}
										//Insertamos el total de las fases.
										dataService
											.putElements('fase_ins', fases)
											.then(function(data){
												console.log('Fases de la instalación '+id+' modificadas.');									
											})
											.catch(function(error){
												if(error.status==401){$rootScope.relogin();}
												$scope.errorFormIns = parseError(error.status, error.data);
												$timeout(function(){$scope.errorFormIns = null;}, 15000);
											});
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormIns = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormIns = null;}, 15000);
									});
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormIns = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormIns = null;}, 15000);
							});
						$scope.resetFormInst();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormIns = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormIns = null;}, 15000);
					});
			//Parte de la edición de la instalación, solo de los datos de la instalación.
			}else{
				if($scope.inst.referencia == null){
					$scope.inst.referencia = "I"+crearReferencia();
				}
				dataService
					.putElements('instalacion', $scope.inst)
					.then(function(data){
						console.log('Instalación modificada:', data.id);
						SerInstalaciones.data.modificado = false;
						$scope.formInst.$dirty = false;
						$scope.formInst.$setPristine();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormIns = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormIns = null;}, 15000);
					});		
			}	
		}else{
			$scope.formInst.sistema.$dirty = true;
		}		
	}
	
	//Función que elimina una instalación
	$scope.del = function(instalacion){
		dataService
			.deleteElements('fase_ins', '?filter=id_instalacion%3D'+instalacion.id)
			.then(function(data){
				console.log('Fases de la Instalacion '+instalacion.referencia+' borradas.');
				dataService
					.deleteElements('instalacion', '?filter=id%3D'+instalacion.id)
					.then(function(data){
						console.log('Instalación eliminada: ', data);
						$scope.resetFormInst();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormIns = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormIns = null;}, 15000);
					});
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormIns = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormIns = null;}, 15000);
			});
	}
		
	//Función para cerrar el panel de error.
	$scope.clearError = function(){
		$scope.errorFormIns = null;
	}
	
	//Función para volver a la vista del sistema.
	$scope.volver = function(sistema){
		$scope.resetFormInst();
		$rootScope.$broadcast('recallSistema', sistema);
	}
	
	//Abrimos un diálogo con el listado de presupuestos para seleccionar el correspondiente
	$scope.abrirPresupuestos = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogPresupuestos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (presupuesto) {
			$scope.inst.presupuesto = presupuesto.referencia;
			$scope.inst.pvp = presupuesto.base;
			$scope.filtro2 = '';
			SerInstalaciones.data.modificado = true;
			$scope.formInst.$dirty = true;
	    }, function (reason) {
	    	console.log('Modal promise rejected. Reason: ', reason);
	    });
	}
	
	//Escuchamos para inicializar el formulario cuando es una inserción.
	$scope.$on('mostrar', function(e, data){
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
		
		$scope.inst = {};
		$scope.inst.referencia = "I"+crearReferencia();
		$scope.inst.costeHoraHombre = $scope.costeHoraHombre;
		$scope.inst.pvp = 0;
		
		$scope.informacion = false;
		$scope.nuevo = true;
		$scope.recall = false;
		$scope.mostrar = data;
		$scope.boton = 'Añadir Instalación';
		$scope.titulo = 'Formulario Añadir';
		focus('mainFocusForm');
	});
	
	//Escuchamos para inicializar el formulario cuando es una edición.
	$scope.$on('editar', function(e, data){
		$scope.inst = angular.copy(data);
		traerCont('contactos?filter=id%3E', 0, '%26%26id_sistema='+$scope.inst.id_sistema,0 ,0 ,0 ,[]);
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
		$scope.sistemaSel = data.sistema;
		$scope.informacion = false;
		$scope.nuevo = false;
		$scope.recall = false;
		$scope.boton = 'Modificar Instalación';
		$scope.titulo = 'Formulario Edición';
		focus('mainFocusForm');
	});
	
	//Función que indica que es un recall de la instalación
	$scope.$on('recall', function(e, data){
		$scope.inst = angular.copy(data.instalacion);
		traerCont('contactos?filter=id%3E', 0, '%26%26id_sistema='+$scope.inst.id_sistema,0 ,0 ,0 ,[]);
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
		$scope.sistemaSel = data.instalacion.sistema;
		$scope.sistemaRecall = angular.copy(data.sistema);
		$scope.mostrar = true;
		$scope.recall = true;
		$scope.informacion = false;
		$scope.nuevo = false;
		$scope.boton = 'Modificar Instalación';
		$scope.titulo = 'Formulario Edición';
		focus('mainFocusForm');
	});
	
	//Escuchamos para inicializar el formulario cuando es una lectura (info).
	$scope.$on('info', function(e, data){
		$scope.inst = angular.copy(data);
		$scope.sistemaSel = data.sistema;
		$scope.informacion = true;	
		$scope.mostrar = true;	
		$scope.titulo = 'Información';
	});
	
	//Escuchamos al hermano que nos indica que guardemos los datos del formulario.
	$rootScope.$on('modificar', function(e, data){
		console.log('Entro en modificar');
		$scope.fasesAdd = data;
		$scope.add();
	});
	
	//Escuchamos al padre que nos indica que hay que cerrar el formulario.
	$rootScope.$on('cerrarTodo', function(e, data){
		$scope.filtro='';
		$scope.inst = {};
		$scope.currentError = null;
		$scope.informacion = false;
		$scope.recall = false;
		$scope.sistemaSel = 'Seleccione un Sistema';
	});
	
	/*------------------------------ Manejador del Select ------------------------------*/
	//Función que despliega el select.
	$scope.desplegar = function(){
		$scope.desplegarUl = !$scope.desplegarUl;
		if($scope.desplegarUl==true){
			focus('focusMe');
			$scope.filtro = '';
		}else{
			focus('mainFocus');
		}
	}
	
	//Función que sustituye el sistema actual por la seleccionada de la tabla.
	$scope.cambiarSistema = function(sistema){
		$scope.sistemaSel = sistema.nombre;
		$scope.inst.id_sistema = sistema.id;
		$scope.desplegarUl = !$scope.desplegarUl;
		$scope.formInst.sistema.$dirty = false;
		$scope.formInst.$dirty = true;
		focus('mainFocus');
	}	
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masSistemas = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogSistemas.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (sistema) {
			$scope.sistemaSel = sistema.nombre;
			$scope.inst.id_sistema = sistema.id;
			$scope.desplegarUl = !$scope.desplegarUl;
			focus('mainFocus');
	    }, function (reason) {
		    $scope.filtro='';
	    });
	}
}