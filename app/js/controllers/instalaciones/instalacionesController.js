/****************************************************************************/
/*						Controlador de Instalaciones						*/
/****************************************************************************/
App.controller('InstalacionesCntrl', InstalacionesCntrl);
function InstalacionesCntrl($scope, $rootScope, $timeout, $state, uiGridConstants, ngDialog, SerInst, dataService, localStorageService, SerInstalaciones){
	
	$scope.buffer = true;
	$scope.buffer2 = false;
	$scope.progressValue = 0;
	$scope.progressValue2 = 0;
	$scope.mostrar = false;
	$scope.sistemaSel = 'Seleccione un Sistema';
	$scope.editar = false;
	$scope.columnasSel = [];
	SerInstalaciones.data.modificado = false;
	if(localStorageService.get('filtroInstalaciones') != null && localStorageService.get('filtroInstalaciones') != ''){
		$scope.filtros = localStorageService.get('filtroInstalaciones');
	}else{
		$scope.filtros = [];
	}
	$scope.filtrado = false;
	$scope.open = {
	    date1: false,
	    date2: false,
	    date3: false
	};
	$scope.currentError = null;
	  
	var registros = [];
	var regFiltro = [];
	var registrosSis = [];
	var fasesOut = [];
	var pendientes = [];
	var acumulados = 0;
	var acumulados2 = 0;
	var total = 0;
	var total2 = 0;
	var url = '';
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ InstalacionesCntrl ------------------------------');
	
	//Columnas de la tabla.
	var referencia = {name: 'referencia', field: 'referencia', enableHiding: false, enablePinning:false, minWidth: 140, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="referencia"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var descripcion = {name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 100, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs', menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="descripcion"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var sistema = {name: 'sistema', field: 'sistema', cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning:false, width: 150, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="sistema"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var inicio = {name: 'inicio', field: 'inicio', enableHiding: false, minWidth: 138, sort:{direction: uiGridConstants.ASC, priority: 2}, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="fin"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var fin = {name: 'fin', field: 'fin', enableHiding: false, minWidth: 138, sort:{direction: uiGridConstants.DESC, priority: 1}, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="fin"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	$scope.columnas = [	  
			//Columnas fijas en la tabla 		    
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'color', width: 35, cellTemplate: '<div class="centrado"><i class="fa fa-circle" data-ng-class="'+"{'text-warning': grid.appScope.comprobarInstalaciones(row.entity)==0, 'text-danger': grid.appScope.comprobarInstalaciones(row.entity)==1, 'text-success': grid.appScope.comprobarInstalaciones(row.entity)==2}"+'"></i></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedLeft: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'out_time', width:0, suppressRemoveSort: true, cellTemplate: '<div class="centrado"></div>', enableSorting: false, enableFiltering: false, enableHiding: false, displayName: '', enableColumnResizing: false, enablePinning: false, cellClass: "hidden", headerCellClass : "hidden", sort:{direction: uiGridConstants.DESC, priority: 0}},
		    			
			//Columnas variables
		    {name: 'referencia', field: 'referencia', enableHiding: false, enablePinning:false, minWidth: 140, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="referencia"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]},
			{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 100, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs', menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="descripcion"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]},
			{name: 'sistema', field: 'sistema', cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning:false, width: 150, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="sistema"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]},
			{name: 'inicio', field: 'inicio', enableHiding: false, minWidth: 138, sort:{direction: uiGridConstants.ASC, priority: 2}, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="fin"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]},
			{name: 'fin', field: 'fin', enableHiding: false, minWidth: 138, sort:{direction: uiGridConstants.DESC, priority: 1}, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="fin"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]}
	];
						
	//Inicializamos la tabla.
	$scope.gridOptions = {
		paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		scrollable: false,
		enableHorizontalScrollbar: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApiInstalaciones) {
		   $scope.gridApiInstalaciones = gridApiInstalaciones;
	    }
	};
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridOptions.paginationPageSize*36)+100) > (($scope.gridOptions.data.length*36)+100) ){
			return ($scope.gridOptions.data.length*36)+100;
		}else{
			return ($scope.gridOptions.paginationPageSize*36)+100;
		}
	}
	
	//Función que añade columnas a la tabla.
	var editCol = function(columna, add){
	    var nCol = putTipoCol(columna);
	    //Si add es true, se añade la columna, si no se elimina.
	    if(add){
		    if(!estaCol(nCol)){
				if(columna==1){
					$scope.columnas.splice(($scope.columnas.length),0,referencia);
				}else if(columna==2){
					$scope.columnas.splice(($scope.columnas.length),0,descripcion);
				}else if(columna==3){
					$scope.columnas.splice(($scope.columnas.length),0,sistema);
				}else if(columna==4){
					$scope.columnas.splice(($scope.columnas.length),0,inicio);
				}else if(columna==5){
					$scope.columnas.splice(($scope.columnas.length),0,fin);
				}
				console.log('Añadida columna ', nCol);
			}else{
				console.log('Intento de añadir columna existente: ', nCol);
			}
		//Si es false, se elimina.
	    }else{
		    if(estaCol(nCol)){
			    //Comprobamos que está dentro del listado de columnas, y si es así lo eliminamos.
			    for(var i=0; i<$scope.columnas.length; i++){
				    if($scope.columnas[i].name == nCol){
					    $scope.columnas.splice(i,1);
				    }
			    }
				console.log('Columna eliminada: ', nCol);
			}
	    }
	    $scope.gridApiInstalaciones.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	}

	//Función que comprueba si una columna está añadida a la tabla.
	var estaCol = function(columna){		
		for(var i=1; i<$scope.columnas.length; i++){			
			if($scope.columnas[i].name == columna){
				return true;
			}
		}
		return false;
	}
	
	//Función que traduce los números del select de las columans a sus nombres correspondientes.
	var putTipoCol = function(columna){
		switch (parseInt(columna)) {
			case 1:
				return 'referencia';
				break;				
			case 2:
				return 'descripcion';
				break;				
			case 3:
				return 'sistema';
				break;				
			case 4:
				return 'inicio';
				break;				
			case 5:
				return 'fin';
				break;
		}
	}
		
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (Instalaciones InstalacionesCntrl): ', data.registros.length);
					//Hacemos la llamada a la lista de fase_ins para que nos mande las fases que están fuera de tiempo.
					fasesFuera(data.registros);
					traerPendientes();
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (Instalaciones InstalacionesCntrl): '+$scope.progressValue+'%.');
					$scope.gridOptions.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
	
	//Llamada get para recoger los sistemas y mostrarlos en el select de la vista de filtros.
	var traerSis = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (traerSis InstalacionesFormCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
					}
					$scope.sistemas = angular.copy(data.registros);
					$scope.buffer3 = false;
					$scope.progressValue3 = 0;
				}else{
					$scope.progressValue3 = data.progressValue;
					console.log('Cargando (traerSis InstalacionesFormCntrl): '+$scope.progressValue3+'%.');
					$scope.sistemas = angular.copy(data.registros);
					traerSis(tabla, data.registros[(data.registros.length-1)].id, acumulados, progressValue, total, registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	traerSis('sistema?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que comprueba las fases fuera de fecha.
	var fasesFuera = function(instalaciones){
		var hoy = new Date();
		hoy = "'"+hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()+" "+hoy.getHours()+"%3A"+hoy.getMinutes()+"%3A00'";
		dataService
			.getFiltro('fase_ins?filter=id%3E', 0, '%26%26limite%3C'+hoy+'%26%26fin%20IS%20NULL', 0, 0, 0, [])
			.then(function(data){
				fasesOut = angular.copy(data.registros);
				console.log('Total de registros (fasesOut InstalacionesCntrl): ', fasesOut.length);
				for(var i=0; i < instalaciones.length; i++){
					for(var j=0; j<fasesOut.length; j++){
						if(instalaciones[i].id == fasesOut[j].id_instalacion){
							instalaciones[i].out_time = 1;
							break;
						}else{
							instalaciones[i].out_time = 0;
						}
					}
				}
				$scope.gridOptions.data = angular.copy(instalaciones);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que retorna todas las pendientes de las instalaciones del sistema.
	var traerPendientes = function(){
		dataService
			.getAll('pendientes?filter=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				console.log('Total de pendientes de las Instalaciones (InstalacionesCntrl): ', data.registros.length);
				pendientes = angular.copy(data.registros);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que abre un ngDialog para indicar el filtro avanzado que queremos y lo muestra en la tabla de incidencias.
	$scope.filtroAvanzado = function(){
		if($scope.filtros[2] && $scope.filtros[2].valor){
			$scope.sistemaSel = $scope.filtros[2].valor;
		}
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogFiltroAvanzadoInstalaciones.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (filtros) {
			console.log('Filtros: ', filtros.length);
			var and = -1;
			//For que recorre el listado de filtros y comprueba que no sean campos vacíos.
			for(var i=0; i<filtros.length; i++){
				if(filtros[i]!=undefined && filtros[i].valor!=undefined){
					and++;
				}
			}
			if(filtros.length>0 && and>-1){
				var pasa = false;
				if(filtros[0]!=undefined && filtros[0].valor!=undefined){
					if(comprobarCrc(filtros[0].valor)){
						pasa = true;
					}
				}else{
					pasa = true;
				}
				if(pasa){
					componerFiltro(filtros);					
				}else{
					$scope.currentError = 'Referencia erronea, introduzca referencia válida';
					$timeout(function(){$scope.currentError = null;}, 15000);
					if($scope.filtrado){
						$scope.filtrado = false;
						$scope.buffer = true;
						$scope.progressValue = 0;
						recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
					}
				}				
			}else{
				console.log('Sin filtros');
				$scope.resetearFiltros();
				if($scope.filtrado){
					$scope.filtrado = false;
					$scope.buffer = true;
					$scope.progressValue = 0;
					recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
				}
			}
	    }, function (reason) {
		    
		    
		});
	}
	
	//Función que llama a la url de filtros que haya fuardad.
	var llamarFiltro = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (filtroInstalaciones InstalacionesCntrl): ', data.registros.length);
					$scope.gridOptions.data = angular.copy(data.registros);
					$scope.buffer = false;
					total = 0;
					regFiltro = [];
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (filtroInstalaciones InstalacionesCntrl): '+$scope.progressValue+'%.');
					$scope.gridOptions.data = angular.copy(data.registros);
					llamarFiltro(data.tabla, data.registros[(data.registros.length-1)].id, data.filtro, data.acumulados, data.progressValue, data.total, data.registros);
				}
				$scope.filtrado = true;
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que compone el filtro.
	var componerFiltro = function(filtros){
		var url = '';
		var and = 0;
		for(var i=0; i<filtros.length; i++){
			if(filtros[i]!=undefined){
				//En caso de que fuera un campo de los de fecha.
				if(i==3 || i==4){	
					filtros[i].valor = new Date(filtros[i].valor);
					var fecha = "'"+filtros[i].valor.getFullYear()+'-'+twoDigits((filtros[i].valor.getMonth()+1))+'-'
								+twoDigits(filtros[i].valor.getDate())+' '+twoDigits(filtros[i].valor.getHours())+':'
								+twoDigits(filtros[i].valor.getMinutes())+":00'";
					url = url+'%26%26'+'('+putTipoCol((i+1));
					//Comprobamos si está seleccionado el checkbox de negar, y procedemos en consonancia.
					if(filtros[i].neg==true){
						url = url+'%20!%3D%20'+fecha+")";
					}else{
						filtros[i].neg = false;
						url = url+'%20%3D%20'+fecha+")";
					}
					if(and>0){
						url = url+'%20%26%26%20';
						and--;
					}
				//El resto de casos, se trataría de texto.
				}else{
					if(filtros[i].valor != ''){
						url = url+'%26%26'+'('+putTipoCol((i+1));
						//Comprobamos si está seleccionado el checkbox de negar, y procedemos en consonancia.
						if(filtros[i].neg==true){
							url = url+"%20!%3D%20'"+filtros[i].valor+"')";
						}else{
							filtros[i].neg = false;
							url = url+"%20%3D%20'"+filtros[i].valor+"')";
						}
						if(and>0){
							url = url+'%20%26%26%20';
							and--;
						}
					}else{
						//En caso de estár en blanco, eliminamos el valor del array.
						filtros[i] = undefined;
					}
				}
			}
		}
		$scope.buffer = true;
		//Mandamos al localstorage el filtro.
		localStorageService.set('filtroInstalaciones', filtros);
		llamarFiltro('instalaciones?filter=id%3E', 0, url, 0, 0, 0, []);
	}
		
	//Función que resetea los datos del formulario de filtrado.
	$scope.resetearFiltros = function(){
		url = "";
		for(var i=0; i<$scope.filtros.length; i++){
			if($scope.filtros[i]!=undefined){
				$scope.filtros[i].valor = undefined;
				$scope.filtros[i].neg = false;
				$scope.filtros[i] = undefined;
			}
		}
		$scope.sistemaSel = 'Seleccione un sistema';
		
		//reseteamos la variable del localstorage del filtro.
		localStorageService.set('filtroInstalaciones', '');
	}
	
	//Función que activa o desactiva los filtros que haya en el formulario de filtro avanzado.
	$scope.togleFiltros = function(){
		if($scope.filtrado){
			$scope.filtrado = false;
			$scope.buffer = true;
			$scope.progressValue = 0;
			recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
		}else{			
			if($scope.filtros != '' && $scope.filtros != null){
				$scope.filtrado = true;
				$scope.buffer = true;
				$scope.progressValue = 0;					
				componerFiltro($scope.filtros);
			}else{
				$scope.currentError = 'No hay filtros.';
				$timeout(function(){$scope.currentError = null;}, 15000);
			}
		}
	}
	
	//Función que llama al ngDialog en el cual seleccionaremos las columnas que queramos que contenga la tabla (La columna inicio siempre estará).
	$scope.cambiarTabla = function(){
		for(var j=0; j<$scope.columnasSel; j++){
			$scope.columnasSel[j]=false;
		}
		//Inicializamos la lista de columnas, marcando cuales forman parte de la tabla en este momento.
		for(var i=0; i<$scope.columnas.length; i++){
			switch($scope.columnas[i].name){
				case "referencia":
					$scope.columnasSel[0]=true;
					break;
					
				case "descripcion":
					$scope.columnasSel[1]=true;
					break;
					
				case "sistema":
					$scope.columnasSel[2]=true;
					break;
					
				case "inicio":
					$scope.columnasSel[3]=true;
					break;
					
				case "fin":
					$scope.columnasSel[4]=true;
					break;
			}
		}				
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogColumnasInstalaciones.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (columnas) {
			if(columnas.length>0){
				for(var i=0; i<columnas.length; i++){
					if(columnas[i]==true){
						editCol((i+1), true);
					}else{
						editCol((i+1), false);
					}
				}
			}
			for(var j=0; j<columnas.length; j++){
				columnas[j]=false;
			}
	    }, function (reason) {
		    console.log('Cerrado filtro sin filtrar.');
	    });
	}    
	
	//Función que comprueba si una instalación está
	$scope.comprobarInstalaciones = function(instalacion){
		if(fasesOut){
			for(var i=0; i<fasesOut.length; i++){
				if(fasesOut[i].id_instalacion == instalacion.id){
					return 1;
				}
			}
		}
		if(pendientes){
			for(var j=0; j<pendientes.length; j++){
				if(pendientes[j].id_instalacion == instalacion.id && pendientes[j].estado!='CERRADO'){
					return 1;
				}
			}
		}
		if(instalacion.fin == undefined || instalacion.fin == null){
			return 0;
		}else{
			return 2;
		}
	}
	
	//Función que le indica al hijo IncidenciasFormCntrl que inicialice el formulario en forma de inserción.
	$scope.mostrarForm = function(){
		$scope.mostrar = 1;
		$rootScope.$broadcast('mostrar', true);
	}
	
	//Función que le indica al hijo IncidenciasFormCntrl que inicialice el formulario en forma de edición.
	$scope.editar = function(instalacion){
		$scope.mostrar = true;
		$rootScope.$broadcast('editar', instalacion);
	}
	
	//Función que abre el datetimepicker para seleccionar la fecha y después la hora.
	$scope.openCalendar = function(e, date) {
	    e.preventDefault();
	    e.stopPropagation();
	
	    $scope.open[date] = true;
	};
	
	//Función para cerrar el panel de error.
	$scope.clearError = function(){
		$scope.currentError = null;
	}
	
	/*------------------------------ Manejador del Select ------------------------------*/
	//Función que despliega el select.
	$scope.desplegar = function(){
		$scope.desplegarUl = !$scope.desplegarUl;
		if($scope.desplegarUl==true){
			focus('focusMe');
			$scope.filtro = '';
		}else{
			focus('mainFocus');
		}
	}
	
	//Función que sustituye el sistema actual por la seleccionada de la tabla.
	$scope.cambiarSistema = function(sistema){
		$scope.filtros[2] = {"valor": sistema.nombre, "neg": false};
		$scope.sistemaSel = sistema.nombre;
		$scope.desplegarUl = !$scope.desplegarUl;
		focus('mainFocus');
	}	
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masSistemas = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogSistemas.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (sistema) {
			$scope.filtros[2] = {"valor": sistema.nombre, "neg": false};
			$scope.sistemaSel = sistema.nombre;
			$scope.desplegarUl = !$scope.desplegarUl;
			focus('mainFocus');
	    }, function (reason) {
		    $scope.filtro='';
	    });
	}
	
	/*---------------------------------------- Escuchamos a los hijos ----------------------------------------*/
	
	//Función que comprueba si el formulario ha sido modificado antes de cambiar de pestaña.
	$scope.pararSiModificado = function (event) {
		//Comprobamos si el servicio está a true, en cuyo caso se habrá modificado algún campo del formulario.
		if (SerInstalaciones.data.modificado){
			var answer = confirm('Hay cambios sin guardar en el formulario. ¿Desea continuar?');
		    !answer && event.preventDefault();
		}
	};
	
    //Recibimos las llamadas de los hijos para recargar los datos de la tabla de incidencias.
    $scope.$on('recargar', function(e, data){
	    $scope.buffer = data;
	    registros = [];
		recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
    });
    
    //Recibimos las llamadas de los hijos para recargar las fases fuera de tiempo
    $scope.$on('recargarFasesOut', function(e, data){
	    fasesFuera($scope.gridOptions.data);
    });
    
    //Recibimos la llamada del hijo para finalizar la instalación
    $scope.$on('finalizar', function(e, data){
	    for(var i=0; i<$scope.gridOptions.data.length; i++){
		    if($scope.gridOptions.data[i].id == data.id){
				$scope.gridOptions.data[i].fin = data.fin;
				dataService
					.putElements('instalacion', $scope.gridOptions.data[i])
					.then(function(data){
						console.log('Instalación modificada:', data.id);
						$scope.buffer = true;
						registros = [];
						recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.currentError = parseError(error.status, error.data);
						$timeout(function(){$scope.currentError = null;}, 15000);
					});
				break; 
		    }
	    }
    });
    
    //Recibimos la llamada del hijo para quitar la fecha de finalizar la instalación
    $scope.$on('noFinalizar', function(e, data){
		for(var i=0; i<$scope.gridOptions.data.length; i++){
		    if($scope.gridOptions.data[i].id == data.id){
			    $scope.gridOptions.data[i].fin = null;
			    dataService
			    	.putElements('instalacion', $scope.gridOptions.data[i])
					.then(function(data){
						console.log('Instalación modificada:', data.id);
						$scope.buffer = true;
						registros = [];
						recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.currentError = parseError(error.status, error.data);
						$timeout(function(){$scope.currentError = null;}, 15000);
					});
				break;
		    }
		}
	});
    
    //Recibimos la señal del hijo para cambiar de vista.
    $scope.$on('recallSistema', function(e, data){
	    SerInst.data.sistema = angular.copy(data);
		$state.go('app.sistemas');
    });
    
    //Funciones que escuchan a los hijos y mandan señal de cerrar todo.
    $scope.$on('cerrarFormulario', function(e, data){
	    $scope.mostrar = false;
	    $rootScope.$broadcast('cerrarTodo', true);
    });
    $scope.$on('cerrarFases', function(e, data){
	    $scope.mostrar = false;
	    $rootScope.$broadcast('cerrarTodo', true);
    });
    $scope.$on('cerrarPendientes', function(e, data){
	    $scope.mostrar = false;
	    $rootScope.$broadcast('cerrarTodo', true);
    });
    
    //Comprobamos si el servicio SerInst tiene datos, en cuyo caso procedemos a mostrra la instalación solicitada.
    if(SerInst.data.sistema != undefined && SerInst.data.instalacion != undefined){
	    $scope.mostrar = true;
	    console.log('Comunicación desde Sistemas con Instalaciones');
	    
		$timeout(function(){$rootScope.$broadcast('recall', SerInst.data);}, 5);
    }
}