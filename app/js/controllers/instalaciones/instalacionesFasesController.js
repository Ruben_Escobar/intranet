/****************************************************************************/
/*				Controlador de las Fases de las Instalaciones				*/
/****************************************************************************/
App.controller('InstalacionesFasesCntrl', InstalacionesFasesCntrl);
function InstalacionesFasesCntrl($scope, $rootScope, $timeout, $modal, uiGridConstants, focus, ngDialog, dataService){
	
	$scope.buffer4 = true;
	$scope.mostrar = false;
	$scope.nuevo = false;
	$scope.fasesIns = [];
	$scope.fasesCambio = [];
	$scope.adds = false;
	$scope.mostrarForm = false;
	$scope.faseSig = 0;
	$scope.errorFormFaseIns = null;
	$scope.errorFasesIns = null;
	
	//Variables del datetimepicker
	$scope.fechaHora = new Date();
	$scope.open = false;
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip2 = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ InstalacionesFasesCntrl ------------------------------');
	
	//Definición de las columnas de la tabla.
	$scope.columnas = [	  
			//Columnas fijas en la tabla 
		    {name: 'eliminar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editarFase(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    			
			//Columnas variables
		    {name: 'nombre', field: 'nombre', enableHiding: false, enablePinning:false, minWidth: 181},
			{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 100, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip2, cellClass: 'cellToolTip hidden-xs'},
			{name: 'inicio', field: 'inicio', enableHiding: false, minWidth: 138, enablePinning:false, headerCellClass : "hidden-sm hidden-xs", cellClass: 'hidden-sm hidden-xs'},
			{name: 'caducidad', field: 'caducidad', enableHiding: false, enablePinning:false, width: 93, headerCellClass : "hidden-sm hidden-xs", cellClass: 'hidden-sm hidden-xs'},
			{name: 'limite', field: 'limite', enableHiding: false, minWidth:138, width: 138, enablePinning:false, headerCellClass : "hidden-xs hidden-md", cellClass: 'hidden-xs hidden-md', displayName: 'Límite'},
			{name: 'fin', field: 'fin', enableHiding: false, minWidth:138, width: 138, enablePinning:false, headerCellClass : "hidden-xs", cellClass: 'hidden-xs'}
	];
	
	//Función que comprueba si se ha pasado el límite de una fase para colorearla de rojo.
	$scope.comprobar = function(limite, fin){
		if(limite!=null){
			var hoy = new Date();
			limite = new Date(limite);
			if(hoy.getTime() >= limite.getTime() && fin == null){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	//Template de la fila para modificar el color de la fila cuando se pase la fecha límite o cuando esté terminada.
	var rowTemplate = '<div ng-class="{\'greenRow\':row.entity.fin!=undefined,  \'redRow\': grid.appScope.comprobar(row.entity.limite, row.entity.fin), \'negrita\':row.entity.inicio!=undefined&&row.entity.fin==undefined}"'
					+' ng-repeat="col in colContainer.renderedColumns track by col.colDef.name"  class="ui-grid-cell" ui-grid-cell></div>';
	
	//Inicializamos la configuración de la tabla.
	$scope.gridFasesInstalacion = {
		paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		scrollable: true,
		enableHorizontalScrollbar: true,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		rowTemplate: rowTemplate,
		onRegisterApi: function(gridApiFasesIns) {
		   $scope.gridApiFasesIns = gridApiFasesIns;
	    }
	};
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridFasesInstalacion.paginationPageSize*36)+100) > (($scope.gridFasesInstalacion.data.length*36)+100) ){
			return ($scope.gridFasesInstalacion.data.length*36)+100;
		}else{
			return ($scope.gridFasesInstalacion.paginationPageSize*36)+100;
		}
	}
	
	//Función que ordena la lista de fases según sus campos.
	var ordenarFases = function(fases){
		var fasesOrd = [];
		var indice = 0;
				
		for(var i=0; i<fases.length;i++){
			if(fases[i].orden == 0){				
				fasesOrd[indice] = fases[i];
				indice++;
				break;
			}
		}
		while(fasesOrd.length!=fases.length){
			for(var j=0; j<fases.length; j++){
				if(fasesOrd[(indice-1)].id == fases[j].orden){
					fasesOrd[indice] = fases[j];
					indice++;
					break;
				}
			}
		}
		return fasesOrd;
	}
	
	//Función que trae los registros de la BDD.
	var traerFases = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.registros.length>0){
					$scope.fasesIns = ordenarFases(data.registros);
				}
				$scope.gridFasesInstalacion.data = $scope.fasesIns;
				console.log('Total de registros (traerFases InstalacionesFasesCntrl): ', $scope.fasesIns.length);
				$scope.buffer4 = false;
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFasesIns = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFasesIns = null;}, 15000);
			});
	}
	
	//Función que trae las tareas de la fase.
	var traerTareas = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.registros.length>0){
					for(var i=0; i<data.registros.length; i++){
						data.registros[i].visible = false;
					}
					$scope.tasks = angular.copy(data.registros);
				}else{
					$scope.tasks = [];
				}
				console.log('Total de registros(traerTareas InstalacionesFasesCntrl): ', $scope.tasks.length);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFasesIns = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFasesIns = null;}, 15000);
			});
	}
	
	//Función que cierra el formulario total de la instalación.
	$scope.resetLista = function(){
		$scope.resetFormFaseIns();
		$rootScope.$broadcast('cerrarFases', false);
	}
	
	//Función que abre el datetimepicker para seleccionar la fecha y después la hora.
	$scope.openCalendar = function(e) {
	    e.preventDefault();
	    e.stopPropagation();
	    $scope.open = true;
	};
  
	//Funciones para cerrar los paneles de error.
	$scope.clearError = function(){
		$scope.currentError = null;
	}
	$scope.clearErrorFormFaseIns = function(){
		$scope.errorFormFaseIns = null;
	}
	$scope.clearErrorFasesIns = function(){
		$scope.errorFasesIns = null;
	}
	
	//Función que comprueba si tiene que estar o no deshabilitado el chackbox del listado de fases.
	$scope.isDisabled = function(fase){
		if($scope.informacion){
			return true;
		}else if(fase.inicio!=undefined){
			return true;
		}else{
			return false;
		}
	}
	
	//Función que muestra el formulario para añadir una nueva fase.
	$scope.newFase = function(){
		$scope.mostrarForm  = true;
		$scope.siguiente = true;
		$scope.boton = 'Añadir';
		$scope.titulo = 'Formulario Añadir';
	}
	
	//Función que muestra el campo de añadir tareas.
	$scope.newTarea = function(){
		$scope.taskEdit = {};
		$scope.taskEdit.id_fase_ins = $scope.faseIns.id;
		$scope.taskEdit.descripcion = '';
		$scope.mostrarTaskEdit = true;
	}
	
	//Función que pone como editable las tareas.
	$scope.editarTarea = function(task){
		for(var i=0; i<$scope.tasks.length; i++){
			if($scope.tasks[i].id == task.id){
				$scope.tasks[i].visible = true;
				$scope.taskEdit = angular.copy($scope.tasks[i]);
				$scope.taskAux = angular.copy($scope.tasks[i]);
				break;
			}
		}
	}
	
	//Función que añade una tarea.
	$scope.addTarea = function(task){
		//Inserción
		if(!task.id){
			if(task.descripcion != undefined && task.descripcion != ''){
				dataService
					.postElements('fase_ins_task', task)
					.then(function(data){
						console.log('Tarea añadida satisfactoriamente: ', data);
						$scope.mostrarTaskEdit = false;
						$scope.cancelTarea(task);
						traerTareas('fase_ins_task?filter=id%3E', 0, '%26%26id_fase_ins%3D'+$scope.faseIns.id, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormFaseIns = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
					});
			}else{
				$scope.errorFormFaseIns = 'La descripción no puede estár vacía.';
			}
			//Edición
		}else{
			if(task.descripcion != undefined && task.descripcion != ''){
				dataService
					.putElements('fase_ins_task', task)
					.then(function(data){
						console.log('Tarea modificada satisfactoriamente: ', data);
						$scope.mostrarTaskEdit = false;
						$scope.cancelTarea(task);
						traerTareas('fase_ins_task?filter=id%3E', 0, '%26%26id_fase_ins%3D'+$scope.faseIns.id, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormFaseIns = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
					});
			}else{
				$scope.errorFormFaseIns = 'La descripción no puede estár vacía.';
			}
		}
	}
	
	//Función que elimina la tarea de la fase.
	$scope.eliminarTarea = function(task){
		dataService
			.deleteElements('fase_ins_task', '?filter=id%3D'+task.id)
			.then(function(data){
				console.log('Tarea Eliminada');
				traerTareas('fase_ins_task?filter=id%3E', 0, '%26%26id_fase_ins%3D'+$scope.faseIns.id, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormFaseIns = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
			});
	}
	
	//Función que cancela el añadir una tarea.
	$scope.cancelTarea = function(task){
		if(task == ''){
			$scope.taskEdit = {};
			$scope.mostrarTaskEdit = false;
		}else{
			for(var i=0; i<$scope.tasks.length; i++){
				if($scope.tasks[i].id == task.id){
					$scope.tasks[i] = angular.copy($scope.taskAux);
					$scope.tasks[i].visible = false;
					break;
				}
			}
		}
	}
	
	//Función que añade una fase a la lista de fases de la instalación. Realizamos las operaciones pertinentes de modificación e inserción.
	$scope.addFaseInst = function(){	
		if($scope.faseIns.id == undefined){	
			for(var i=0; i<$scope.fasesIns.length; i++){
				if($scope.fasesIns[i].id == $scope.faseSig){
					$scope.faseIns.orden = $scope.fasesIns[i].orden;
					$scope.faseIns.id_instalacion = $scope.fasesIns[i].id_instalacion;
					var idInst = $scope.fasesIns[i].id_instalacion;
					break;
				}
			}			
			//Añadimos la nueva fase en la tabla fase_ins.
			dataService
				.postElements('fase_ins', $scope.faseIns)
				.then(function(data){
					console.log('Nueva fase de instalación '+idInst+' añadida: ', data.id);
					//Ahora modificamos la fase siguiente, indicando que el padre es la nueva fase añadida.
					dataService
						.putElements('fase_ins', {"id": $scope.faseSig, "orden": data.id})
						.then(function(data){
							console.log('Fase de instalación '+idInst+' modificada: ', data.id);
							$scope.resetFormFaseIns();
							$scope.buffer4 = true;
							traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+idInst, 0, 0, 0, []);
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.errorFormFaseIns = parseError(error.status, error.data);
							$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
						});
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			var idInst = $scope.faseIns.id_instalacion;
			//Edición
			//Comprobamos si tiene inicio, y en tal caso, reseteamos el campo límite con el nuevo valor.
			if($scope.faseIns.inicio!=undefined){
				var fecha = new Date($scope.faseIns.inicio);
				$scope.faseIns.inicio = ""+fecha.getFullYear()+'-'+twoDigits((fecha.getMonth()+1))+'-'+twoDigits(fecha.getDate())+' '
										+twoDigits(fecha.getHours())+':'+twoDigits(fecha.getMinutes())+":00";
				//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
				var tiempo = fecha.getTime();
				var mil = parseInt($scope.faseIns.caducidad*24*60*60*1000);
				fecha.setTime(tiempo+mil);
				$scope.faseIns.limite = ""+fecha.getFullYear()+'-'+twoDigits((fecha.getMonth()+1))+'-'+twoDigits(fecha.getDate())+' '
										+twoDigits(fecha.getHours())+':'+twoDigits(fecha.getMinutes())+":00";
			}
			//Comprobamos si ha habido modificación en el select (Cambio de posición de la fase).
			if(!$scope.formFaseIns.select.$pristine){
				var fasesAds = [];
				//Buscamos la fase siguiente.
				for(var i=0; i<$scope.gridFasesInstalacion.data.length; i++){
					if($scope.gridFasesInstalacion.data[i].orden == $scope.faseIns.id){
						//Una vez encontramos la fase siguiente, modificamos el campo orden y procedemos a modificarla.
						$scope.gridFasesInstalacion.data[i].orden = $scope.faseIns.orden;
						fasesAds.push($scope.gridFasesInstalacion.data[i]);
						//Una vez modificada la fase siguiente, intercalamos la seleccionada.
						for(var j=0; j<$scope.gridFasesInstalacion.data.length; j++){
							if($scope.gridFasesInstalacion.data[j].id == $scope.faseSig){
								$scope.faseIns.orden = $scope.gridFasesInstalacion.data[j].orden;
								fasesAds.push($scope.faseIns);
								$scope.gridFasesInstalacion.data[j].orden = $scope.faseIns.id;
								fasesAds.push($scope.gridFasesInstalacion.data[j]);
								dataService
									.putElements('fase_ins', fasesAds)
									.then(function(data){
										console.log('Fases modificadas: ', data);
										$scope.resetFormFaseIns();
										$scope.buffer4 = true;
										traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+idInst, 0, 0, 0, []);
										//Le indicamos al padre que refresque la variable que contiene las fases fuera de tiempo.
										$rootScope.$broadcast('recargarFasesOut', true);
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormFaseIns = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
									});
								break;
							}
						}
						break;
					}
				}
				//Si en el for anterior no rellena el array de añadir fases es porque es una fase de fin.
				if(fasesAds.length==0){
					for(var j=0; j<$scope.gridFasesInstalacion.data.length; j++){
						if($scope.gridFasesInstalacion.data[j].id == $scope.faseSig){
							$scope.faseIns.orden = $scope.gridFasesInstalacion.data[j].orden;
							fasesAds.push($scope.faseIns);
							$scope.gridFasesInstalacion.data[j].orden = $scope.faseIns.id;
							fasesAds.push($scope.gridFasesInstalacion.data[j]);
							dataService
								.putElements('fase_ins', fasesAds)
								.then(function(data){
									console.log('Fases modificadas: ', data);
									$scope.resetFormFaseIns();
									$scope.buffer4 = true;
									traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+idInst, 0, 0, 0, []);
									//Le indicamos al padre que refresque la variable que contiene las fases fuera de tiempo.
									$rootScope.$broadcast('recargarFasesOut', true);
								})
								.catch(function(error){
									if(error.status==401){$rootScope.relogin();}
									$scope.errorFormFaseIns = parseError(error.status, error.data);
									$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
								});
							break;
						}
					}
				}
			//En caso de que el select no se modifique (No hay que cambiar de posición la fase),
			//mandamos orden de modificar la fase en la posición que está.					
			}else{				
				dataService
					.putElements('fase_ins', $scope.faseIns)
					.then(function(data){
						console.log('Fase de instalación '+idInst+' modificada: ', data.id);
						$scope.resetFormFaseIns();
						$scope.buffer4 = true;
						traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+idInst, 0, 0, 0, []);
						//Le indicamos al padre que refresque la variable que contiene las fases fuera de tiempo.
						$rootScope.$broadcast('recargarFasesOut', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormFaseIns = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
					});
			}
		}
	}
	
	//Función que edita los datos de una fase de una instalación.
	$scope.editarFase = function(fase){
		$scope.faseSig = undefined;
		$scope.fasesIns = angular.copy($scope.gridFasesInstalacion.data);
		//Buscamos cual es la fase siguiente para asignarla al select. Y quitamos a la editada de la lista.
		for(var i=0; i<$scope.fasesIns.length; i++){
			if($scope.fasesIns[i].orden == fase.id){
				$scope.faseSig = $scope.fasesIns[i].id;
			}
			if($scope.fasesIns[i].id == fase.id){
				var num = i;
			}
		}
		$scope.fasesIns.splice(num,1);
		$scope.mostrarForm  = true;
		$scope.faseIns = angular.copy(fase);
		$scope.siguiente = false;
		$scope.boton = 'Modificar';
		$scope.titulo = 'Formulario Edición';
		traerTareas('fase_ins_task?filter=id%3E', 0, '%26%26id_fase_ins%3D'+$scope.faseIns.id, 0, 0, 0, []);
	}
	
	//Función que ejecuta el mensaje modal.
	var ModalInstanceCtrl = function ($scope, $modalInstance) {
		$scope.ok = function () {
			$modalInstance.close('closed');
		};

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	};
	ModalInstanceCtrl.$inject = ["$scope", "$modalInstance"];
	
	//Función que finaliza la fase en la que se está (Pone la fecha actual en la tupla fin), e inicia la siguiente (pone la fecha actual en la tupla inicio).
	$scope.passFase = function(){

	    var modalInstance = $modal.open({
	    	templateUrl: '/app/views/partials/ngDialogConfirm.html',
			controller: ModalInstanceCtrl,
			size: 'sm'
	    });
	
	    var state = $('#modal-state');
	    modalInstance.result.then(function () {
	    	state.text('Modal dismissed with OK status');
	    	var hoy = new Date();										
			var fasesPass = [];
			//Comprobamos si estamos en la última fase, en caso de estarlo, mostramos un mensaje de error, ya que no podemos pasar a la sigueinte fase.
			if(!($scope.fasesIns[$scope.fasesIns.length-1].inicio != undefined && $scope.fasesIns[$scope.fasesIns.length-1].fin != undefined)){
				//Recoremos el array de fases de la instalación.
				for(var i=0; i<$scope.fasesIns.length; i++){
					//Comprobamos que la fase está iniciada, pero no finalizada.
					if($scope.fasesIns[i].inicio != undefined && $scope.fasesIns[i].fin == undefined){
						//Le asignamos un valor de fecha de finalización
						$scope.fasesIns[i].fin = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
															+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
						//Miramos si es la última fase, y en ese caso, mandamos orden al padre de finalizar la instalación.
						if(i == ($scope.fasesIns.length-1)){
							fasesPass[0] = $scope.fasesIns[i];
							var datos = {
								"id": $scope.fasesIns[i].id_instalacion,
								"fin": $scope.fasesIns[i].fin
							}
							dataService
								.putElements('fase_ins', fasesPass)
								.then(function(data){
									console.log('Última fase finalizada: ', data.record[0].id);
									$scope.buffer4 = true;
									traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+$scope.inst.id, 0, 0, 0, []);
									//Le indicamos al padre que finalice la instalación.
									$rootScope.$broadcast('finalizar', datos);
								})
								.catch(function(error){
									if(error.status==401){$rootScope.relogin();}
									$scope.errorFasesIns = parseError(error.status, error.data);
									$timeout(function(){$scope.errorFasesIns = null;}, 15000);
								});
						}else{
							//En caso de que no sea la última fase, procedemos a los cambios oportunos sobre las fases, sin más.
							for(var j=0; j<$scope.fasesIns.length; j++){
								//Buscamos la siguiente fase
								if($scope.fasesIns[i].id == $scope.fasesIns[j].orden){
									//Hacemos las asignaciones a las varibles según corresponda.
									$scope.fasesIns[j].inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
																+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
									//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
									var tiempo = hoy.getTime();
									var mil = parseInt($scope.fasesIns[j].caducidad*24*60*60*1000);
									var fin = new Date();
									fin.setTime(tiempo+mil);
									$scope.fasesIns[j].limite = ""+fin.getFullYear()+'-'+twoDigits((fin.getMonth()+1))+'-'
																+twoDigits(fin.getDate())+' '+twoDigits(fin.getHours())+':'
																+twoDigits(fin.getMinutes())+":00";
									//Añadimos las fases al array que pasaremos a la sentencia SQL.
									fasesPass[0] = $scope.fasesIns[i];
									fasesPass[1] = $scope.fasesIns[j];
									//Realizamos la llamada SQL para modificar la tabla.
									dataService
										.putElements('fase_ins', fasesPass)
										.then(function(data){
											console.log('Paso de fase '+data.record[0].id+' a fase '+data.record[1].id);
											$scope.buffer4 = true;
											traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+$scope.inst.id, 0, 0, 0, []);
											//Le indicamos al padre que refresque la variable que contiene las fases fuera de tiempo.
											$rootScope.$broadcast('recargarFasesOut', true);
										})
										.catch(function(error){
											if(error.status==401){$rootScope.relogin();}
											$scope.errorFasesIns = parseError(error.status, error.data);
											$timeout(function(){$scope.errorFasesIns = null;}, 15000);
										});
									break;
								}
							}
						}
						break;
					}
				}
			}else{
				$scope.errorFasesIns = 'No se puede pasar la fase, última fase finalizada.';
				$timeout(function(){$scope.errorFasesIns = null;}, 15000);
			}
	    },function () {
	    	state.text('Modal dismissed with Cancel status');
	    });
	}
	
	//Función que vuelve a la fase anterior. (Resetea los campos del tipo fecha de la fase actual, vuelve a la fase anterior y resetea el campo fin).
	$scope.backFase = function(){
		var modalInstance = $modal.open({
	    	templateUrl: '/app/views/partials/ngDialogConfirm.html',
			controller: ModalInstanceCtrl,
			size: 'sm'
	    });
	
	    var state = $('#modal-state');
	    modalInstance.result.then(function () {
	    	state.text('Modal dismissed with OK status');
	    	var hoy = new Date();										
			var fasesBack = [];
			//Compruebo que no estamos en la primera fase, en dicho caso, muestro un mensaje de error.
			if(!($scope.fasesIns[0].inicio != undefined && $scope.fasesIns[0].fin == undefined)){
				//Compruebo que no esté finalizada la última fase.
				if($scope.fasesIns[$scope.fasesIns.length-1].inicio != undefined && $scope.fasesIns[$scope.fasesIns.length-1].fin != undefined){
					var tiempo = hoy.getTime();
					var mil = parseInt($scope.fasesIns[$scope.fasesIns.length-1].caducidad*24*60*60*1000);
					var fin = new Date();
					fin.setTime(tiempo+mil);
					$scope.fasesIns[$scope.fasesIns.length-1].inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
																+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
					$scope.fasesIns[$scope.fasesIns.length-1].limite = ""+fin.getFullYear()+'-'+twoDigits((fin.getMonth()+1))+'-'
																+twoDigits(fin.getDate())+' '+twoDigits(fin.getHours())+':'
																+twoDigits(fin.getMinutes())+":00";
					$scope.fasesIns[$scope.fasesIns.length-1].fin = null;
					fasesBack.push($scope.fasesIns[$scope.fasesIns.length-1]);
					//Mandamos orden al padre de que quite la fecha de finalización de la instalación.
					var datos = {
						"id": $scope.fasesIns[$scope.fasesIns.length-1].id_instalacion
					};
					$rootScope.$broadcast('noFinalizar', datos);
				}else{
					//Una vez descartada la primera y la última fase, recorro el array para buscar que fase es la que tengo que retroceder.
					for(var i=0; i<$scope.fasesIns.length; i++){
						//Para encontrar la fase, compruebo que tenga el inicio con fecha y el fin sin ella.
						if($scope.fasesIns[i].inicio != undefined && $scope.fasesIns[i].fin == undefined){
							//Realizo los cambios de variables correspondientes para retroceder la fase.
							$scope.fasesIns[i].inicio = null;
							$scope.fasesIns[i].limite = null;
							$scope.fasesIns[i].fin = null;
							//Inserto la fase en el array para mandar con la sentencia SQL.
							fasesBack.push($scope.fasesIns[i]);
							//Modificamos la fase anterios.
							$scope.fasesIns[i-1].inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
															+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
							//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
							var tiempo = hoy.getTime();
							var mil = parseInt($scope.fasesIns[i-1].caducidad*24*60*60*1000);
							var fin = new Date();
							fin.setTime(tiempo+mil);
							$scope.fasesIns[i-1].limite = ""+fin.getFullYear()+'-'+twoDigits((fin.getMonth()+1))+'-'+twoDigits(fin.getDate())+' '
															+twoDigits(fin.getHours())+':'+twoDigits(fin.getMinutes())+":00";
							$scope.fasesIns[i-1].fin = null;
							//Inserto la fase en el array para  mandar con la sentencia SQL.
							fasesBack.push($scope.fasesIns[i-1]);
							break;
						}
					}
					console.log('Fases a modificar: ', fasesBack);
				}
				//Realizo la sentencia SQL.
				dataService
					.putElements('fase_ins', fasesBack)
					.then(function(data){
						console.log('Fase retrocedida');
						$scope.buffer4 = true;
						traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+$scope.inst.id, 0, 0, 0, []);
						//Le indicamos al padre que refresque la variable que contiene las fases fuera de tiempo.
						$rootScope.$broadcast('recargarFasesOut', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFasesIns = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFasesIns = null;}, 15000);
					});
			}else{
				$scope.errorFasesIns = 'La fase no se puede retroceder, es la fase inicial.';
				$timeout(function(){$scope.errorFasesIns = null;}, 15000);
			}
		},function () {
	    	state.text('Modal dismissed with Cancel status');
	    });
	}
		
	//Función para eliminar una fase de la lista de fases de la instalación.
	$scope.delFaseIns = function(fase){
		var faseInsAux = {};
		if(fase.inicio && !fase.fin){
			$scope.errorFormFaseIns = 'Fase en curso, imposible eliminar.';
			$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
		}else{
			//Se quiere eliminar la primera fase.
			if(fase.orden == 0){
				for(var i=0; i<$scope.fasesIns.length; i++){
					if($scope.fasesIns[i].orden == fase.id){
						$scope.fasesIns[i].orden = 0;
						faseInsAux = $scope.fasesIns[i];
						//Realizo la llamada al servicio para eliminar la fase.
						dataService
							.putElements('fase_ins', $scope.fasesIns[i])
							.then(function(data){
								console.log('Fase modificada: ',data.id);
								dataService
									.deleteElements('fase_ins', '?filter=id%3D'+fase.id)
									.then(function(data){
										console.log('Fase borrada: ',data.id);
										$scope.buffer4 = true;
										$scope.resetFormFaseIns();
										traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+fase.id_instalacion, 0, 0, 0, []);
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormFaseIns = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
									});													
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormFaseIns = parseError(errror.status, error.data);
								$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
							});
						break;
					}
				}
			//Se quiere eliminar una fase que no es la primera.	
			}else{
				for(var j=0; j<$scope.fasesIns.length; j++){
					if($scope.fasesIns[j].orden == fase.id){
						$scope.fasesIns[j].orden = fase.orden;
						faseInsAux = $scope.fasesIns[j];
						//Una vez que realizamos las modificaciones pertienentes mandamos realizar el update de la fase y después eliminamos la seleccionada.
						dataService
							.putElements('fase_ins', $scope.fasesIns[j])
							.then(function(data){
								console.log('Fase modificada: ',data.id);
								dataService
									.deleteElements('fase_ins', '?filter=id%3D'+fase.id)
									.then(function(data){
										console.log('Fase borrada: ',data.id);
										$scope.buffer4 = true;
										$scope.resetFormFaseIns();
										traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+fase.id_instalacion, 0, 0, 0, []);
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormFaseIns = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
									});													
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormFaseIns = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
							});
						break;
					}
				}
				if(faseInsAux.id==undefined){
					//Se quiere borrar la última fase y esta no está empezada, por lo que se borra sin más.
					console.log('Última fase');
					dataService
						.deleteElements('fase_ins', '?filter=id%3D'+fase.id)
						.then(function(data){
							console.log('Fase borrada: ',data.id);
							$scope.buffer4 = true;
							$scope.resetFormFaseIns();
							traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+fase.id_instalacion, 0, 0, 0, []);
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.errorFormFaseIns = parseError(error.status, error.data);
							$timeout(function(){$scope.errorFormFaseIns = null;}, 15000);
						});
				}
			}
		}
	}
		
	//Función que resetea el formulario de añadir fases.
	$scope.resetFormFaseIns = function(){
		$scope.mostrarForm = false;
		$scope.faseIns = {};
		$scope.faseSig = '';
	}
	
	//Función que escucha la llamada del padre, inicializa las variables necesarias y recupera los registros de las fases.
	$scope.$on('mostrar', function(e, data){
		$scope.nuevo = true;
		$scope.buffer4 = true;
		$scope.informacion = false;
	});
	
	//Recibimos llamada de edición de una instalación, inicializamos las variables necesarias y llamamos a las fases de dicha instlación.
	$scope.$on('editar', function(e, data){
		$scope.inst = angular.copy(data);
		$scope.informacion = false;
		$scope.buffer = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		$scope.fechaHora = null;
		$scope.fasesIns = [];
		traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id, 0, 0, 0, []);
	});
	
	//Función que indica que es un recall de la instalación
	$scope.$on('recall', function(e, data){
		$scope.inst = angular.copy(data.instalacion);
		$scope.sistemaRecall = angular.copy(data.sistema);
		$scope.informacion = false;
		$scope.buffer = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		$scope.recall = true;
		$scope.fechaHora = null;
		$scope.fasesIns = [];
		traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.instalacion.id, 0, 0, 0, []);
	});
	
	//Recibimos llamada de información sobre una instalación, mostramos lo mismo que en edición pero sin posibilidad d emodificar.
	$scope.$on('info', function(e, data){
		$scope.inst = angular.copy(data);
		$scope.informacion = true;
		$scope.buffer = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		traerFases('fase_ins?filter=id%3E', 0, '%26%26id_instalacion%3D'+data.id, 0, 0, 0, []);
	});	
	
	//Escuchamos al padre que nos indica que hay que cerrar el formulario.
	$rootScope.$on('cerrarTodo', function(e, data){
		$scope.resetFormFaseIns();
	});
}