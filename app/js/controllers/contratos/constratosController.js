/****************************************************************************/
/*					Controlador de la vista de Contratos					*/
/****************************************************************************/
App.controller('ContratosCntrl', ContratosCntrl);
function ContratosCntrl($scope, $rootScope, $filter, $timeout, ngDialog, uiGridConstants, dataService, SerContratos){
	$scope.buffer = true;
	$scope.mostrar = false;
	$scope.progressValue = 0;
	$scope.currentError = null;
	$scope.errorFormContrato = null;
	$scope.errorServiciosContrato = null;
	SerContratos.data.servicios = [];
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ContratosCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	//Inicializamos la tabla.
	$scope.gridOptionsComplex = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		//showGridFooter: true,
		//showColumnFooter: true,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableRowSelection: true,
		selectionRowHeaderWidth: 38,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [	    
		    {name: 'referencia', field: 'ref', minWidth: 100, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning: false},
		    {name: 'descripcion', field: 'descripcion', cellClass: "hidden-xs", headerCellClass : "hidden-xs", displayName: 'Descripción', enableHiding: false, enablePinning: false, cellTemplate: templateWithTooltip},
		    {name: 'sistema', field: 'sistema', minWidth: 100, cellClass: "hidden-xs", headerCellClass : "hidden-xs", enableHiding: false, enablePinning: false},
		    {name: 'caduca', field: 'caduca', minWidth: 100, enableHiding: false, enablePinning: false},
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridOptionsComplex.paginationPageSize*36)+100) >(($scope.gridOptionsComplex.data.length*36)+100) ){
			return ($scope.gridOptionsComplex.data.length*36)+100;
		}else{
			return ($scope.gridOptionsComplex.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (contratos ContratosCntrl): ', data.registros.length);
					$scope.gridOptionsComplex.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (contratos ContratosCntrl): '+$scope.progressValue+'%.');
					$scope.gridOptionsComplex.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	recuperarRegistros('contratos?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que elimina uno o varios sistemas.
	$scope.remove = function(){
		data = $scope.gridApi.selection.getSelectedRows();
		//Comprobamos si hay o no objetos seleccionados.
	    var url = '';
	    var url2 = '';
	    for(var i=0; i<data.length; i++){
		    if(i == 0){
			    url = url+data[i].id;
			    url2 = url2+data[i].id;
			}else{
				url = url + '%7C%7Cid_contrato%3D'+data[i].id;
				url2 = url2 + '%7C%7Cid%3D'+data[i].id;
			}
	    }
	    if(data.length!=0){
	        dataService
	        	.deleteElements('servicio_contrato', '?filter=id_contrato%3D'+url)
	        	.then(function(data){
		        	console.log('Servicios de los contratos eliminados.');
		        	dataService
		        		.deleteElements('dontrato', '?filter=id%3D'+url2)
		        		.then(function(data){
			        		console.log('Contratos eliminados.');
			        		recuperarRegistros('contratos?filter=id%3E', 0, 0, 0, 0, []);
			        	})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.currentError = parseError(error.status, error.data);
							$timeout(function(){$scope.currentError = null;}, 15000);
						});
		        })
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			ngDialog.open({
	            template: '<p class="rojo centrado">No has seleccionado ninún elemento para eliminar</p>',
	            plain: true
	        });
		}
	}
	
	/*---------------------------------------- CONTROLES FORMULARIO ----------------------------------------*/
	//Abrir el formulario vacío.
	$scope.mostrarForm = function(){
		$scope.mostrar = true;
		$rootScope.$broadcast('mostrar', true);
	}	
	
	//Abrimos el formulario con los datos de la población a editar.
	$scope.editar = function(contrato){
		$scope.mostrar = true;
		$rootScope.$broadcast('editar', contrato);
	}
	
	//Escuchamos a los hijos, que dan orden de cerrar el formulario.
	$scope.$on('ocultar', function(e,data) {  
        $scope.mostrar = data;   
    });
    
    //Escuchamos a los hijos que dan orden de refrescar los datos de la tabla.
    $scope.$on('recargar', function(e, data){
	    $scope.buffer = data;
		recuperarRegistros('contratos?filter=id%3E', 0, 0, 0, 0, []);
    });
    
    //Funciones para cerrar los paneles de error.
    $scope.clearError = function(){
	    $scope.currentError = null;
    }
    $scope.clearErrorFormContrato = function(){
	    $scope.errorFormContrato = null;
    }
    $scope.clearErrorServiciosContrato = function(){
	    $scope.errorServiciosContrato = null;
    }
}