/****************************************************************************/
/*		Controlador de los servicios del formulario de los Contratos		*/
/****************************************************************************/
App.controller('ContratosServiciosCntrl', ContratosServiciosCntrl);
function ContratosServiciosCntrl($scope, $rootScope, $timeout, ngDialog, dataService, SerContratos){	

	$scope.buffer3 = true;
	$scope.adds = false;
	$scope.errorServiciosContrato = null;
	
	var registrosSer = [];
	var registrosSerCont = [];
	var pasarela = 0;
	var contrato = {};
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ContratosServiciosCntrl ------------------------------');
	
	//Llamada get para recoger los servicios genéricos y mostrarlos en la pestaña del formulario.
	var traerSer = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){				
				if(data.progressValue == 100){
					console.log('Total de registros (traerSer ContratosServiciosCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
					}
					$scope.servicios = angular.copy(data.registros);
					$scope.buffer3 = false;
					$scope.progressValue2 = 0;
					pasarela--;
				}else{
					$scope.progressValue2 = data.progressValue;
					console.log('Cargando (traerSer ContratosServiciosCntrl): '+$scope.progressValue2+'%.');
					$scope.servicios = data.registros;
					traerSer(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				pasarela--;
				if(error.status==401){$rootScope.relogin();}
				$scope.errorServiciosContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorServiciosContrato = null;}, 15000);
			});
	}
	
	//Comprobamos los servicios contratados y los ponemos activos en el listado.
	var comprobarServicios = function(){
		var serIniciales = [];
		for(var i=0; i<$scope.servicios.length; i++){
			for(var j=0; j<registrosSerCont.length; j++){	
				if($scope.servicios[i].id == registrosSerCont[j].id_servicio){
					$scope.servicios[i].asignado = true;
					//Añadimos el servicio que va a ser marcado (ya que forma parte del contrato).
					serIniciales.push($scope.servicios[i]);
				}
			}
		}
		//Mandamos señal al formulario de que el contrato tiene servicios asignados.
		SerContratos.data.servicios = serIniciales;
		registrosSerCont = [];
		serIniciales = [];
	}	
	
	//Llamada get para recoger los servicios enlazados con un contrato en concreto.
	var traerSerCont = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					registrosSerCont = angular.copy(data.registros);
					console.log('Total de registros (serCont ContratosServiciosCntrl): ', registrosSerCont.length);
					comprobarServicios();
					$scope.buffer3 = false;
					$scope.progressValue2 = 0;
				}else{
					registrosSerCont = angular.copy(data.registros);
					$scope.progressValue2 = data.progressValue;
					console.log('Cargando (teaerSerCont ContratosServiciosCntrl): '+$scope.progressValue2+'%.');
					$scope.servicios = registrosSerCont;
					traerSerCont(data.tabla, registrosSerCont[(registrosSerCont.length-1)].id, data.filtro, data.acumulados, data.progressValue, data.total, registrosSerCont);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorServiciosContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorServiciosContrato = null;}, 15000);
			});
	}
	
	//Función que examina si se ha terminado de recibir los servicios para recuperar los propios del contrato y marcarlos.
	var when0 = function(id, interval){
	    if(pasarela>=0){
	    	if (pasarela==0){
				traerSerCont('servicio_contrato?filter=id%3E', 0, '%26%26id_contrato%3D'+id, 0, 0, 0, []);
	    	}else{
	        	setTimeout(function(){ when0(id, interval);}, interval);
	    	}
	    }
	}
	
	//Función para cerrar el panel de error.
	$scope.clearErrorServiciosContrato = function(){
		$scope.errorServiciosContrato = null;
	}
	
	//Escuchamos para cargar los servicios correspondientes a un contrato.
	$scope.$on('editar', function(e, data){
	    contrato=data;
	    pasarela=0;
		pasarela++;
		$scope.servicios = [];
		traerSer('servicio?filter=id%3E', 0, 0, 0, 0, []);
		when0(contrato.id, 500);// llamamos a traerSerCont(id); cuando traerSer haya terminado.
    });
	
	//Escuchamos para cargar los datos del select de servicios.
	$scope.$on('mostrar', function(e, data){
		$scope.servicios = [];
		traerSer('servicio?filter=id%3E', 0, 0, 0, 0, []);
	});
	
	//Función que resetea la lista de servicios.
	$scope.resetLista = function(){
		registrosSer=[];
		$rootScope.$broadcast('ocultar', false);
	}
	
	//Función que Guarda los datos de la lista de servicios (Cuales están seleccionados o no).
	$scope.cambiarServicios = function(){
		var ser = [];
		//Recorremos la lista de servicios, comprobamos los que están seleccionados y sobreescribimos el servicio.
		for(var i=0; i<$scope.servicios.length; i++){
			if($scope.servicios[i].asignado == true){
				ser.push($scope.servicios[i]);
			}
		}
		SerContratos.data.servicios = ser;
		$rootScope.$broadcast('servicioModificado', true);
	}
}