/****************************************************************************/
/*					Controlador del formulario de los Contratos				*/
/****************************************************************************/
App.controller('ContratosFormCntrl', ContratosFormCntrl);
function ContratosFormCntrl($scope, $rootScope, $timeout, ngDialog, focus, dataService, SerContratos){	
	
	$scope.buffer2 = true;
	$scope.buffer3 = true;
	$scope.progressValue = 0;
	$scope.$parent = false;
	$scope.errorFormContrato = null;
	
	var acumulados = 0;
	var total = 0;	
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ContratosFormCntrl ------------------------------');
	
	//Llamada get para recoger los sistemas y mostrarlos en el select del formulario.
	var traerSis = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (traerSis ContratosFormCntrl): ', data.registros.length);
					$scope.sistemas = angular.copy(data.registros);;
					$scope.buffer2 = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (traerSis ContratosFormCntrl): '+$scope.progressValue+'%.');
					$scope.sistemas = angular.copy(data.registros);
					traerSis(data.tabla, data.registrosSis[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);					
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormContrato = null;}, 15000);
			});
	}
	traerSis('sistema?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los presupuestos.
	var traerPresupuestos = function(tabla, id, acumulados, progressValue, total, registros){
		$scope.buffer = true;
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total presupuestos (SistemasContratoCntrl): ', data.registros.length);
					$scope.presupuestos = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 100;
				}else{
					$scope.progressValue = data.progressValue;
					traerClientes(data.tabla, data.registros[data.registros.length-1].id, data.acumulados, data.progressValue, data.total, data.registros)
				}
			})
			.catch(function(Error){
				
			});
	}
	
	//Resetear formulario
	$scope.resetFormCon = function(){
		$scope.formCon.$setPristine();
		$rootScope.$broadcast('ocultar', false);
		$scope.formCon.$dirty = false;
	}
	
	//Función para añadir el registro
	$scope.add = function(){
		console.log('Sistema: ', $scope.con.id_sistema);
		//Comprobamos que se han seleccionado servicios.
		if(SerContratos.data.servicios.length == 0){
			$scope.errorFormContrato = 'Seleccione algún servicio para el contrato.';
			$timeout(function(){$scope.errorFormContrato = null;}, 15000);
		}else if($scope.con.id_sistema == undefined){
			$scope.errorFormContrato = 'Seleccione un sistema para el contrato.';
			$timeout(function(){$scope.errorFormContrato = null;}, 10000);
		}else{
			console.log('FECHA: ', $scope.con.caduca);
			$scope.con.caduca = parseFecha($scope.con.caduca);
			console.log('FECHA: ', $scope.con.inicio);
			$scope.con.inicio = parseFecha($scope.con.inicio);
			//Comprobamos si es una edición o una inserción y procedemos según corresponda.
			if($scope.con.id==undefined){	
				$scope.con.ref = "C"+crearReferencia();	
				//Inserción.
				dataService
					.postElements('contrato', $scope.con)
					.then(function(data){
						console.log('Contrato insertado: ', data);
						eliminarServicios(data.id);
						$scope.resetFormCon();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormContrato = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormContrato = null;}, 15000);
					});
			}else{
				if($scope.con.ref == null){
					$scope.con.ref = "C"+crearReferencia();
				}
				//Edición.
				dataService
					.putElements('contrato', $scope.con)
					.then(function(data){
						console.log('Contrato modificado:', data);
						eliminarServicios(data.id);
						$scope.formCon.$dirty = false;
						$scope.formCon.$setPristine();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormContrato = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormContrato = null;}, 15000);
					});
			}
		}
	}
	
	//Función que elimina el contrato seleccionado.
	$scope.delContrato = function(id){
		dataService
	    	.deleteElements('servicio_contrato', '?filter=id_contrato%3D'+id)
	    	.then(function(data){
		    	console.log('Servicios del contrato '+id+' eliminados.');
		    	dataService
		    		.deleteElements('contrato', '?filter=id%3D'+id)
		    		.then(function(data){
			    		console.log('Contrato '+id+' eliminado.');
			    		$scope.resetFormCon();
						$rootScope.$broadcast('recargar', true);
		        	})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormContrato = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormContrato = null;}, 15000);
					});
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormContrato = null;}, 15000);
			});
	}
	
	//Función que testea los cambios del formulario y avisa en caso de haber cambios antes de cerrar.
	$scope.cerrar = function(){
		if($scope.formCon.$dirty){
			if(confirm('Hay cambios en el formulario, si cierra los perderá')){
				$scope.resetFormCon();
			}
		}else{
			$scope.resetFormCon();
		}
	}
	
	//Función que modifica el servicio para indicar que hay cambios en el formulario.
	$scope. cambiarModificado = function(){
		SerContratos.data.modificado = true;
	}
	
	//Función que elimina las inserciones de la tabla servicio_contrato de un contrato en concreto (id).
	var eliminarServicios = function(id){
		dataService
			.deleteElements('servicio_contrato', '?filter=id_contrato%3D'+id)
			.then(function(data){
				console.log('Servicios de contrato '+id+' eliminados.');
				addServicios(id);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormContrato = null;}, 15000);
			});
	}
	
	//Función que añade los servicios de la lista de seleccionados a la tabla servicio_contrato para crear el enlace.
	var addServicios = function(id){
		var servicioAdd = [];
		//El for empieza en 1 para saltarse la primera posición, que es la que utilizamos para traer el id del contrato.
		for(var i=0; i<SerContratos.data.servicios.length; i++){
			servicioAdd.push({
				'id_servicio': SerContratos.data.servicios[i].id,
				'id_contrato': id
			});
		}
		dataService
			.postElements('servicio_contrato', servicioAdd)
			.then(function(data){
				console.log('Servicio de contrato insertado: ', data);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormContrato = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormContrato = null;}, 15000);
			});
	}
	
	//Abrimos un diálogo con el listado de presupuestos para seleccionar el correspondiente
	$scope.abrirPresupuestos = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogPresupuestos.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (presupuesto) {
			$scope.con.presupuesto = presupuesto.referencia;
			$scope.filtro2 = '';
			$scope.formCon.$dirty = true;
	    }, function (reason) {
	    	console.log('Modal promise rejected. Reason: ', reason);
	    });
	}
	
	//Función para cerrar el panel de error.
	$scope.clearErrorFormContrato = function(){
		$scope.errorFormContrato = null;
	}
	
	/*---------------------------------------- Controlador del Select ----------------------------------------*/
	//Función que sustituye la población actual por la seleccionada de la tabla.
	$scope.cambiarSistema = function(sistema){
		$scope.sistemaSel = sistema.nombre;
		$scope.con.id_sistema = sistema.id;
		$scope.desplegarUl = !$scope.desplegarUl;
		$scope.formCon.$dirty = true;
	}
	
	//Desplegamos el Select.
	$scope.desplegar = function(){
		$scope.desplegarUl = !$scope.desplegarUl;
		$scope.filtro='';
		focus('focusMe');
	}
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masSistemas = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogSistemas.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (sistema) {
			$scope.sistemaSel = sistema.nombre;
			$scope.con.id_sistema = sistema.id;
			$scope.desplegarUl = !$scope.desplegarUl;
	    }, function (reason) {
		    $scope.filtro='';
	    });
	}
	
	//Escuchamos para inicializar el formulario cuando es una inserción.
	$scope.$on('mostrar', function(e, data){
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
		$scope.con = {};
		$scope.con.inicio = new Date();
		$scope.mostrar = data;
		$scope.titulo = 'REGISTRA UN NUEVO CONTRATO';
		$scope.sistemaSel = 'Seleccione un Sistema';
		$scope.boton = 'Añadir Contrato';
		$scope.tituloForm = 'Formulario creación';
		focus('mainFocusForm');
		SerContratos.data.servicios = [];
	});
	
	//Escuchamos para inicializar el formulario cuando es una edición.
	$scope.$on('editar', function(e, data){
		traerPresupuestos('presupuesto?filter=id%3E', 0, 0, 0, 0, []);
		$scope.mostrar = true;
		$scope.titulo = 'EDITAR CONTRATO'+data.ref;
		$scope.sistemaSel = data.sistema;
		$scope.con = angular.copy(data);
		//Convertimos la fecha, puesto que la recogida de la BDD es un campo de texto y la solicitada en el form un tipo Date.
		$scope.con.caduca = new Date($scope.con.caduca);
		$scope.con.inicio = new Date($scope.con.inicio);
		$scope.boton = 'Modificar Contrato';
		$scope.tituloForm = 'Formulario edición';
		focus('mainFocusForm');
		SerContratos.data.servicios = [];
	});
	
	//Escuchamos al hermano para saber que se ha modificado algún servicio.
	$rootScope.$on('servicioModificado', function(e, data){
		$scope.formCon.$dirty = data;
	});
}