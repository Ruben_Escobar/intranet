/****************************************************************************/
/*					Controlador de la vista de Contactos					*/
/****************************************************************************/
 App.controller('ContactosCntrl', ContactosCntrl);
 function ContactosCntrl($scope, $rootScope, $timeout, focus, ngDialog, uiGridConstants, dataService, SerContactos){
	
	$scope.buffer = true;
	$scope.mostrarFormContactos = false;
	$scope.progressValue = 0;
	$scope.currentError = null;
	SerContactos.data.modificado = false;
	
	var total = 0;
	var acumulados = 0;
	var registros = [];
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ContactosCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.notas}} tooltip-placement="top">{{row.entity.notas}}</div>';
	
	//Inicializamos la tabla.
	$scope.gridContactos = {
	    paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
		enableColumnMenus: false,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
	        {name: 'nombre', field: 'nombre', minWidth: 90, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'apellidos', field: 'apellidos', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'tipo', field: 'tipo', minWidth: 120, enableHiding: false, enablePinning:false},
	        {name: 'telefono', field: 'telefono', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Teléfono', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'movil', field: 'movil', cellClass: 'hidden-xs', headerCellClass : 'hidden-xs', displayName: 'Móvil', minWidth: 80, maxWidth: 80, enableHiding: false, enablePinning:false},
	        {name: 'correo', field: 'correo', displayName: 'e-mail', minWidth: 150, enableHiding: false, enablePinning:false, cellClass: 'hidden-xs', headerCellClass : "hidden-xs"},
	        {name: 'notas', field: 'notas', minWidth: 85, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-sm hidden-xs', headerCellClass : 'hidden-sm hidden-xs'},
	        {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi = gridApi;
	    }
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridContactos.paginationPageSize*36)+100) >(($scope.gridContactos.data.length*36)+100) ){
			return ($scope.gridContactos.data.length*36)+100;
		}else{
			return ($scope.gridContactos.paginationPageSize*36)+100;
		}
	}
	
	//Función que retorna todos los contactos de la BBDD.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				//Comprobamos si se ha terminado la carga o no. En caso negativo, volvemos a llamar a la función.
				if(data.progressValue == 100){
					console.log('Total de registros (contactos ContactosCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
						data.registros[i].apellidos = removeAccents(data.registros[i].apellidos);
					}
					$scope.gridContactos.data = data.registros;
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (contactos ContactosCntrl): '+$scope.progressValue+'%.');
					$scope.gridOptionsComplex.data = data.registros;
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].idpoblacion, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
    recuperarRegistros('contacto?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que muestra el formulario.
	$scope.formContactos = function(){
		$scope.mostrarFormContactos = true;
		$rootScope.$broadcast('mostrar', true);
	}
	
	//Función que muestr a el formulario y le indica al hijo que es una edición.
	$scope.editar = function(contacto){
		$scope.mostrarFormContactos = true;
		$rootScope.$broadcast('editar', contacto);
	}
	
	//Función que comprueba si el formulario ha sido modificado antes de cambiar de pestaña.
	$scope.pararSiModificado = function (event) {
		//Comprobamos si el servicio está a true, en cuyo caso se habrá modificado algún campo del formulario.
		if (SerContactos.data.modificado){
			var answer = confirm('Hay cambios sin guardar en el formulario. ¿Desea continuar?');
		    !answer && event.preventDefault();
		}
	};
	
	//Recibimos las llamadas de los hijos para cerrar el panel del formulario.
	$scope.$on('ocultar', function(e,data) {  
        $scope.mostrarFormContactos = data;
    });
    
    //Recibimos las llamadas de los hijos para refrescar los datos de la tabla de contactos.
	$scope.$on('recargar', function(e,data) {  
        $scope.buffer = true;
        $scope.progressValue = 0;
        recuperarRegistros('contacto?filter=id%3E', 0, 0, 0, 0, []);
    });
	
	$scope.clearError = function(){
		$scope.currentError = null;
	}
 }