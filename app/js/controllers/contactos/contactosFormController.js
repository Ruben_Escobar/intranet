/****************************************************************************/
/*			Controlador de la vista del formulario Contactos				*/
/****************************************************************************/
 App.controller('ContactosFormCntrl', ContactosFormCntrl);
 function ContactosFormCntrl($scope, $rootScope, $timeout, focus, ngDialog, uiGridConstants, dataService, SerContactos){
	
	$scope.mostrarFormContactos = false;
	$scope.errorformCon = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ContactosFormCntrl ------------------------------');
	
	//Función que recibe el contacto del formulario y lo añade o lo modifica, según sea necesario.
	$scope.add = function(){
		if($scope.con.tipo == 'TECNICO' || $scope.con.tipo == 'ADMINISTRATIVO'){
			if($scope.con.id==undefined){		
				//Inserción.
				dataService
					.postElements('contacto', $scope.con)
					.then(function(data){
						console.log('Contacto insertado: ', data.id);
						$scope.resetformCon();
						$rootScope.$broadcast('recargar', false);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorformCon = parseError(error.status, error.data);
						$timeout(function(){$scope.errorformCon = null;}, 15000);
					});
			}else{
				//Edición.
				dataService
					.putElements('contacto', $scope.con)
					.then(function(data){
						console.log('Contacto modificado:', data.id);
						SerContactos.data.modificado = false;
						$scope.formCon.$dirty = false;
						$scope.formCon.$setPristine();
						$rootScope.$broadcast('recargar', false);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorformCon = parseError(error.status, error.data);
						$timeout(function(){$scope.errorformCon = null;}, 15000);
					});
			}
		}else{
			$scope.errorformCon = 'Seleccione el tipo de contacto';
			$timeout(function(){$scope.errorformCon = null;}, 15000);
		}
	}
	
	//Función que elimina un contacto.
	$scope.delCon = function(contacto){
		dataService
			.deleteElements('contacto', '?filter=id%3D'+contacto.id)
			.then(function(data){
				console.log('Contacto eliminado: '+contacto.nombre+' '+contacto.apellidos);
				$scope.resetformCon();
				$rootScope.$broadcast('recargar', false);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorformCon = parseError(error.status, error.data);
				$timeout(function(){$scope.errorformCon = null;}, 15000);
			});
	}
	
	//Función que cambia el estado del servicio al actuar sobre algún campo del formualrio.
	$scope.cambiarModificado = function(){
		SerContactos.data.modificado = true;
	}
	
	//Función que resetea el formulario y lo cierra.
	$scope.resetformCon = function(){
		$scope.formCon.$setPristine();
		$scope.con = {};
		$scope.mostrarFormContactos = false;
		$rootScope.$broadcast('ocultar', false);
		SerContactos.data.modificado = false;
		$scope.formCon.$dirty = false;
	}
	
	//Función que testea los cambios del formulario y avisa en caso de haber cambios antes de cerrar.
	$scope.cerrar = function(){
		if($scope.formCon.$dirty){
			if(confirm('Hay cambios en el formulario, si cierra los perderá')){
				$scope.resetformCon();
			}
		}else{
			$scope.resetformCon();
		}
	}
	
	//Función que recoge la llamada del padre y prepara el formulario vacío.
	$scope.$on('mostrar', function(e,data) {
		$scope.mostrarFormContactos = data;
		$scope.con = {};
		$scope.titulo = 'Formulario añadir';
		$scope.boton = 'Añadir';
	});
	
	//Función que recoge la llamada del padre y prepara el formulario con los datos de la edición.
	$scope.$on('editar', function(e,data) {
		$scope.mostrarFormContactos = true;
		$scope.con = angular.copy(data);
		$scope.titulo = 'Formulario modificar';
		$scope.boton = 'Modificar';
	});
	
	//Reseteamos el formulario
	$scope.$on('cerrarSistemasContactos', function(e, data){
		$scope.resetformCon();
	});
	
	//Función que cierra la ventana de error del formulario.
	$scope.clearErrorformCon = function(){
		$scope.errorformCon = null;
	}
 }