/****************************************************************************/
/*			Controlador de la vista de los sistemas del Contacto			*/
/****************************************************************************/
 App.controller('ContactosSistemasCntrl', ContactosSistemasCntrl);
 function ContactosSistemasCntrl($scope, $rootScope, $timeout, focus, ngDialog, uiGridConstants, dataService){
	
	var registros = [];
	var sistemasContacto = [];
	var acumulados = 0;
	var total = 0;
	
	$scope.progressValue2 = 0;
	$scope.buffer2 = true;
	$scope.buffer4 = true;
	$scope.nuevo = false;
	$scope.mostrarFormContactos = false;
	$scope.errorSistemasContactos = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ContactosSistemasCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	//Inicializamos las tablas.
	$scope.gridSisConTotal = {
	    paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
	    enableRowSelection: true,
	    selectionRowHeaderWidth: 38,
		enableColumnMenus: false,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
	        {name: 'nombre', field: 'nombre', minWidth: 130, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'direccion', field: 'direccion', cellClass: "hidden-xs", headerCellClass : "hidden-xs", displayName: 'Dirección', minWidth: 90, enableHiding: false, enablePinning:false},
	        {name: 'poblacion', field: 'poblacion', displayName: 'Población', minWidth: 85, enableHiding: false, enablePinning:false}
	    ],
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi = gridApi;
	    }
	}
	$scope.gridSisCon = {
	    paginationPageSizes: [10, 25, 50, 75],
	    paginationPageSize: 10,
	    enableFiltering: true,
	    enableSorting: true,
	    enableCellEdit: false,
		enableColumnMenus: false,
	    enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
	        {name: 'sistema', field: 'sistema', minWidth: 130, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning:false},
	        {name: 'direccion', field: 'direccion', cellClass: "hidden-xs", headerCellClass : "hidden-xs", displayName: 'Dirección', minWidth: 90, enableHiding: false, enablePinning:false},
	        {name: 'poblacion', field: 'poblacion', displayName: 'Población', minWidth: 85, enableHiding: false, enablePinning:false},
	        {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.delSis(row.entity)" title="Edit" class="btn btn-sm btn-danger"><em class="fa fa-trash"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
	    onRegisterApi: function(gridApi) {
	    	$scope.gridApi2 = gridApi;
	    }
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridSisConTotal.paginationPageSize*36)+100) >(($scope.gridSisConTotal.data.length*36)+100) ){
			return ($scope.gridSisConTotal.data.length*36)+100;
		}else{
			return ($scope.gridSisConTotal.paginationPageSize*36)+100;
		}
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano2 = function(){
		if( (($scope.gridSisCon.paginationPageSize*36)+100) >(($scope.gridSisCon.data.length*36)+100) ){
			return ($scope.gridSisCon.data.length*36)+100;
		}else{
			return ($scope.gridSisCon.paginationPageSize*36)+100;
		}
	}
		
	//Función que trae los contactos del sistema a editar.
	var traerSistemasContacto = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				sistemasContacto = sistemasContacto.concat(data.registros);
				console.log('Total de registros (traerSistemasContacto ContactosSistemasCntrl): ', sistemasContacto.length);
				for(var i=0; i<sistemasContacto.length; i++){
					//Filtramos acentos antes de nada.
					sistemasContacto[i].nombre = removeAccents(sistemasContacto[i].nombre);
					sistemasContacto[i].poblacion = removeAccents(sistemasContacto[i].poblacion);
				}
				$scope.gridSisCon.data = sistemasContacto;
				$scope.buffer4 = false;
				sistemasContacto = [];
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorSistemasContactos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorSistemasContactos = null;}, 15000);
			});
	}
	
	//Función que trae todos los sistemas.
	var traerSistemas = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (traerSistemas ContactosSistemasCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
						if(data.registros[i].poblacion){
							data.registros[i].poblacion = removeAccents(data.registros[i].poblacion);
						}
					}
					$scope.gridSisConTotal.data = data.registros;
					$scope.buffer2 = false;
					$scope.progressValue2 = 0;
				}else{
					$scope.progressValue2 = data.progressValue;
					console.log('Cargando (traerSistemas ContactosSistemasCntrl): '+$scope.progressValue2+'%.');
					$scope.gridSisConTotal.data = data.registros;
					traerSistemas(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorSistemasContactos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorSistemasContactos = null;}, 15000);
			});
	}
	
	//Función que enlaza los contactos que estén seleccionados con el sistema editado.
	$scope.addSistemas = function(){
		sistemasContacto = $scope.gridApi.selection.getSelectedRows();
		if(sistemasContacto.length>0){
			var sistemasInsertar = [];
			//Primero creamos el array completo de elementos a insertar.
			for(var k=0; k<sistemasContacto.length; k++){
				sistemasInsertar.splice(sistemasInsertar.length, 0, {"id_sistema": sistemasContacto[k].id, "id_contacto": $scope.con.id});
			}
			//Recorremos el array que contiene los contactos que el usuario ha seleccionado para añadir.
			for(var i=0; i<sistemasContacto.length; i++){
				//Recorremos el array con los contactos que tiene asignados ahora mismo el sistema.
				for(var j=0; j<$scope.gridSisCon.data.length; j++){
					//Comprobamos si el contacto está ya asociado al sistema, si es así forzamos la salida del bucle y
					//eliminamos el registro del array auxiliar (Que será el que más adelante usaremos para hacer la inserción).
					if(sistemasContacto[i].id == $scope.gridSisCon.data[j].id_sistema){
						//Una vez que sabemos que coinciden los IDs, es decir, que ya está asociado, lo busamos en el array auxiliar y lo quitamos.
						for(var l=0; l<sistemasInsertar.length; l++){
							if($scope.gridSisCon.data[j].id_sistema == sistemasInsertar[l].id_sistema){
								sistemasInsertar.splice(l, 1);
								break;
							}
						}
						break;
					}
				}
			}
			if(sistemasInsertar.length>0){				
				dataService
					.postElements('contacto_sistema', sistemasInsertar)
					.then(function(data){
						console.log('Sistemas enlazados al contacto '+$scope.con.nombre+' '+$scope.con.apellidos+'.');
						$scope.sistemasTotales = false;
						$scope.buffer4 = true;
						sistemasContacto = [];
						sistemasInsertar = [];
						$scope.gridApi.selection.clearSelectedRows();
						traerSistemasContacto('sistemasContacto?filter=id_sistema%3E', 0, '%26%26id_contacto%3D'+$scope.con.id, 0, 0, 0, []);
					})
					.catch(function(error){
						if(status==401){$rootScope.relogin();}
						$scope.errorSistemasContactos = parseError(error.status, error.data);
						$timeout(function(){$scope.errorSistemasContactos = null;}, 15000);
					});
			}else{
				$scope.sistemasTotales = false;
				sistemasContacto = [];
				sistemasInsertar = [];
				$scope.gridApi.selection.clearSelectedRows();
			}
		}else{
			ngDialog.open({
			    template: '<p class="rojo centrado">No has seleccionado ninún sistema para enlazar.</p>',
			    plain: true
			});
		}
	}
	
	//Función que elimina el enlace entre el contacto y el sistema.
	$scope.delSis = function(sistema){
		dataService
			.deleteElements('contacto_sistema', '?filter=id_sistema%3D'+sistema.id_sistema+'%26%26id_contacto%3D'+$scope.con.id)
			.then(function(data){
				console.log('Sistema eliminado correctamente.');
				$scope.buffer4 = true;
				sistemasContacto = [];
				traerSistemasContacto('sistemasContacto?filter=id_sistema%3E', 0, '%26%26id_contacto%3D'+$scope.con.id, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorSistemasContactos = parseError(error.status, error.data);
				$timeout(function(){$scope.errorSistemasContactos = null;}, 15000);
			});
	}
	
	//Función que manda señal de cerrar el formulario.
	$scope.cerrar = function(){
		$scope.sis = {};
		$scope.nuevo = false;
		$scope.mostrarFormContactos = false;
		$scope.sistemasTotales = false;
		$rootScope.$broadcast('cerrarSistemasContactos', false);
	}
	
	//Función que recoge la llamada del padre y prepara el formulario vacío.
	$scope.$on('mostrar', function(e,data) {
		$scope.mostrarFormContactos = data;
		$scope.nuevo = true;
	});
	
	//Función que recoge la llamada del padre y prepara el formulario con los datos de la edición.
	$scope.$on('editar', function(e,data) {
		$scope.mostrarFormContactos = true;
		$scope.con = angular.copy(data);
		$scope.nuevo = false;
		$scope.buffer2 = true;
		$scope.buffer4 = true;
		$scope.sistemasTotales = false;
		traerSistemas('sistemas?filter=id%3E', 0, 0, 0, 0, []);
		traerSistemasContacto('sistemasContacto?filter=id_sistema%3E', 0, '%26%26id_contacto%3D'+$scope.con.id, 0, 0, 0, []);
	});
	
	//Función que cierra el panel de error de la parte de sistemas del formulario.
	$scope.clearErrorSistemasContactos = function(){
		$scope.errorSistemasContactos = null;
	}
 }