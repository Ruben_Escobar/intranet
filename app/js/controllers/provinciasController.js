/****************************************************************************/
/*					Controlador de la vista de las Provincias				*/
/****************************************************************************/
App.controller('ProvinciasCntrl', ProvinciasCntrl);
function ProvinciasCntrl($scope, $rootScope, $filter, $resource, $timeout, $location, ngDialog, uiGridConstants, dataService) {	
	$scope.buffer = true;
	$scope.mostrar = false;
	$scope.progressValue = 0;
	
	var data = [];
	var pasarela = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ProvinciasCntrl ------------------------------');
		
	//Inicializamos la tabla.
	$scope.gridOptionsComplex = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableRowSelection: true,
		selectionRowHeaderWidth: 38,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [
		    {name: 'provincia', field: 'provincia', minWidth: 90, sort:{direction: uiGridConstants.ASC, priority: 0}, enableHiding: false, enablePinning: false},
		    {name: 'provincia3', field: 'provincia3', minWidth: 90, displayName: 'Siglas', cellClass: "hidden-xs", headerCellClass: "hidden-xs", enableHiding: false, enablePinning: false},
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridOptionsComplex.paginationPageSize*36)+100) >(($scope.gridOptionsComplex.data.length*36)+100) ){
			return ($scope.gridOptionsComplex.data.length*36)+100;
		}else{
			return ($scope.gridOptionsComplex.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (provincias ProvinciasCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].provincia = removeAccents(data.registros[i].provincia);
					}
					$scope.gridOptionsComplex.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue= data.progressValue;
					console.log('Cargando (provincias ProvinciasCntrl): '+$scope.progressValue+'%.');
					$scope.gridOptionsComplex.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].idpoblacion, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	recuperarRegistros('provincia?filter=idprovincia%3E', 0, 0, 0, 0, []);
	
	//Eliminar población por id
	$scope.delProv = function(id){
		dataService
			.deleteElements('provincia','?filter=idprovincia%3D'+id)
			.then(function(data){
				console.log('Provincia eliminada: ', data.idprovincia);
				$scope.resetFormProv();
				$scope.buffer = true;
				recuperarRegistros('provincia?filter=idprovincia%3E', 0, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que elimina una o varias provincia.
	$scope.remove = function(){
		data = $scope.gridApi.selection.getSelectedRows();
		var url = '';
		//Comprobamos si hay o no objetos seleccionados.
	    if(data.length!=0){
	        //Recorro los elementos del array de seleccionados y los mando eliminar.
			for(var i=0; i<data.length; i++){
	            if(i == 0){
		        	url = url+data[i].idprovincia;
	            }else{
		            url = url+'%7C%7Cidprovincia%3D'+data[i].idprovincia;
	            }
			}
			dataService
				.deleteElements('provincia', '?filter=idprovincia%3D'+url)
				.then(function(data){
					console.log('Provincias eliminadas: ', data.record);
					$scope.resetFormProv();
					$scope.buffer = true;
					recuperarRegistros('provincia?filter=idprovincia%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			ngDialog.open({
	            template: '<p class="rojo centrado">No has seleccionado ninún elemento para eliminar</p>',
	            plain: true
	        });
		}
	}
	
	/*---------------------------------------- FORMULARIO ----------------------------------------*/
	//Parseo del nombre de la población para crear el campo poblacionseo.
	var parseProv = function(provincia){
		provincia = removeAccents(provincia);		
		provincia = provincia.toLowerCase();		
		provincia = provincia.split(' ').join('-');		
		return provincia;		
	}
	
	//Abrir el formulario vacío.
	$scope.mostrarForm = function(){
		$scope.boton = 'Añadir Provincia';
		$scope.mostrar=true;
		$scope.titulo = 'Formulario Añadir';
	}	
	
	//Abrimos el formulario con los datos de la población a editar.
	$scope.editar = function(provincia){
		$scope.prov = angular.copy(provincia);
		$scope.boton = 'Modificar Provincia';
		$scope.mostrar=true;
		$scope.titulo = 'Formulario Editar';
	}
	
	//Resetear formulario
	$scope.resetFormProv = function(){
		$scope.formProv.$setPristine();
		$scope.prov = undefined;
		$scope.mostrar=false;
	}
	
	//Función para añadir el registro
	$scope.add = function(){
		$scope.prov.provinciaseo = parseProv($scope.prov.provincia);
		//Comprobamos si es una edición o una inserción y procedemos según corresponda.
		if($scope.prov.idprovincia==undefined){		
			//Inserción.
			dataService
				.postElements('provincia', $scope.prov)
				.then(function(data){
					console.log('Provincia insertada: ', data);
					$scope.resetFormProv();
					$scope.buffer = true;
					recuperarRegistros('provincia?filter=idprovincia%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			//Edición.
			dataService
				.putElements('provincia', $scope.prov)
				.then(function(data){
					console.log('Provincia modificada:', data.idprovincia);
					$scope.resetFormProv();
					$scope.buffer = true;
					recuperarRegistros('provincia?filter=idprovincia%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}
	}
	
	//Función para cerrar el panel de error.
	$scope.clearError = function(){
		$scope.currentError = null;
	}
}