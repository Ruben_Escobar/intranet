/****************************************************************************/
/*							Controlador de Financiación						*/
/****************************************************************************/
App.controller('FinanciacionCntrl', FinanciacionCntrl);
function FinanciacionCntrl($scope, $rootScope, $timeout, uiGridConstants, dataService){
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ FinanciacionCntrl ------------------------------');
	
	$scope.columnas = [ 
		{name: 'nombre', field: 'nombre', enableHiding: false, enablePinning:false, minWidth: 80},
		{name: 'capital', field: 'capital', enableHiding: false, enablePinning:false, minWidth: 75},
		{name: 'coeficiente', field: 'coeficiente', enableHiding: false, enablePinning:false, minWidth: 100},
		{name: 'plazo', field: 'plazo', enableHiding: false, enablePinning:false, sort:{direction: uiGridConstants.ASC, priority: 0}, minWidth: 65}
	];
	
	//Inicializamos la tabla.
	$scope.gridFinanciacion = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridFinanciacion.paginationPageSize*36)+100) >(($scope.gridFinanciacion.data.length*36)+100) ){
			return ($scope.gridFinanciacion.data.length*36)+100;
		}else{
			return ($scope.gridFinanciacion.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				console.log('Total de registros (FinanciacionCntrl): ', data.registros.length);
				$scope.gridFinanciacion.data = angular.copy(data.registros);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFinanciacion = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFinanciacion = null;}, 15000);
			});
	}
	recuperarRegistros('financiacion?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función para cerrar el panel de error.
	$scope.clearErrorFinanciacion = function(){
		$scope.errorFinanciacion = null;
	}
}