/****************************************************************************/
/*					Controllador del reseteo de la contraseña				*/
/****************************************************************************/
App.controller('resetPassController', resetPassController);
function resetPassController($scope, $location, $timeout, userService){
	
	$scope.errorRecover = null;
	$scope.errorRecover2 = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ resetPassController ------------------------------');
	
	$scope.recover = {
    	"email": null
	}
	
	$scope.resPass = function(){
		if($scope.recover.email==''){
			$scope.badEmail = 'Hay que rellenar el campo de email.';
		}else{
			userService
				.resetPass('password?reset=true', $scope.recover)
				.then(function(data){
					$location.path("/page/recover2");
				})
				.catch(function(error){
					if(error.status==401){
						$rootScope.relogin();
					}else if(error.status==404){
						$scope.errorRecover = "El email facilitado no consta en la base de datos de usuarios.";
						$timeout(function(){$scope.errorRecover = null;}, 15000);
					}else{
						$scope.errorRecover = parseError(error.status, error.data);
						$timeout(function(){$scope.errorRecover = null;}, 15000);
					}
				});
		}
    }
    
    $scope.reset = {
		"email" : null,
		"code" : null,
		"new_password" : null   
    }   
    
    $scope.resPass2 = function(){
		if($scope.reset.email=='' && $scope.reset.password && $scope.reset.code){
			$scope.badEmail = 'Hay que rellenar el campo de email.';
		}else{
			userService
				.resetPass('password', $scope.reset)
				.then(function(data){
					$location.path("/page/login");
				})
				.catch(function(error){
					if(error.status==401){
						$rootScope.relogin();
					}else{
						$scope.errorRecover2 = parseError(error.status, error.data);
						$timeout(function(){$scope.errorRecover2 = null;}, 15000);
					}
				});
		}
    }
    
    //Función para cerrar el panel de error.
    $scope.clearErrorRecover = function(){
	    $scope.errorRecover = null;
    }
    $scope.clearErrorRecover2 = function(){
	    $scope.errorRecover2 = null;
    }
}