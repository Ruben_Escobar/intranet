/****************************************************************************/
/*							Controlador de Aceptaciones						*/
/****************************************************************************/
App.controller('AceptacionesCntrl', AceptacionesCntrl);
function AceptacionesCntrl($scope, $rootScope, $timeout, $modal, uiGridConstants, ngDialog, focus, printer, dataService){
	
	$scope.buffer = true;
	$scope.progressValue = 0;
	$scope.mostrar = false;
	$scope.uploading = false;
		  
	var registros = [];
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ AceptacionesCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
		
	$scope.columnas = [ 
		{name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info ml-xs"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false},
		{name: 'referencia', field: 'referencia', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 145},
		{name: 'descripcion', field: 'descripcion', displayName: 'Descripción', minWidth: 85, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs', headerCellClass : 'hidden-xs'},
		{name: 'formaPagoInst', field: 'formaPagoInst', displayName: 'Forma de pago', enableHiding: false, enablePinning:false, minWidth: 115, maxWidth: 115},
		{name: 'precioFinal', field: 'precioFinal', displayName: 'Precio final', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 100, maxWidth: 100, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.precioFinal}} €</div>'}
	];
	
	//Declaramos las características de la tabla.
	$scope.gridAceptaciones = {
		paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApiProductos = gridApi;
	    }
	};

	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridAceptaciones.paginationPageSize*36)+100) > (($scope.gridAceptaciones.data.length*36)+100) ){
			return ($scope.gridAceptaciones.data.length*36)+100;
		}else{
			return ($scope.gridAceptaciones.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (AceptacionesCntrl): ', data.registros.length);
					$scope.gridAceptaciones.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (aceptaciones AceptacionesCntrl): '+$scope.progressValue+'%.');
					$scope.gridAceptaciones.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorAceptaciones = parseError(error.status, error.data);
				$timeout(function(){$scope.errorAceptaciones = null;}, 15000);
			});
	}
	recuperarRegistros('aceptaciones?filter=id%3E',0, 0, 0, 0, []);
	
	//Función que abre el formulario en forma de edición.
	$scope.editar = function(aceptacion){
		$scope.tituloAceptacion = 'Aceptación '+aceptacion.referencia+' del presupuesto '+aceptacion.presupuesto
		$scope.mostrar = true;
		$rootScope.$broadcast('editarAceptacion', aceptacion);
	}
	
	//Función que escucha a los hijos para cerrar el apartado del formulario de la aceptación.
	$scope.$on('cerrarAceptacionTotal', function(e, data){
		$scope.mostrar = data;
		$rootScope.$broadcast('cerrarFormAceptacion', false);
		recuperarRegistros('aceptaciones?filter=id%3E',0, 0, 0, 0, []);
	});
	
	//Función que recoge la llamada del hijo de que está subiendo archivos.
	$scope.$on('uploading', function(e, data){
		if(data.progressUpload == 100){
			$scope.progressUpload = data.progressUpload;
			$timeout(function(){$scope.uploading = data.uploading;}, 1000);
		}else{
			$scope.uploading = data.uploading;
			$scope.progressUpload = data.progressUpload;	
		}
	});
	
	//Función para cerrar el panel de error.
	$scope.clearErrorAceptaciones = function(){
		$scope.errorAceptaciones = null;
	}
}