/****************************************************************************/
/*			Controlador de la pestaña de los servicios de la Aceptación		*/
/****************************************************************************/
App.controller('AceptacionServiciosCntrl', AceptacionServiciosCntrl);
function AceptacionServiciosCntrl($scope, $rootScope, $timeout, ngDialog, focus){
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PresupuestosAceptacionServiciosCntrl ------------------------------');
	
	//Función que escucha al padre e inicializa la pestaña del renting.
	$scope.$on('editarAceptacion', function(e, data){
    	//Inicializamos las variables que cogen datos del presupuesto para el formulario y mostramos el mismo.
		$scope.aceptacionServicios = {};
		$scope.aceptacionServicios.servicios = data.servicios;
		$scope.aceptacionServicios.costeServicio = data.costeServicio;
		$scope.aceptacionServicios.formaPagoSer = data.formaPagoSer;
		$scope.aceptacionServicios.tipoPagoSer = data.tipoPagoSer;
		$scope.aceptacionServicios.observacionesSer = data.observacionesSer;
		$scope.titulo = 'Servicios';
		focus('mainFocus');
	});
	
	//Función para mandar los datos introducitos al hermano.
	$scope.aceptar = function(){
		$rootScope.$broadcast('datosServiciosAceptacion', $scope.aceptacionServicios);
		$scope.enviado = 'Datos enviados correctamente al formulario principal.';
		$timeout(function(){$scope.enviado = null;}, 5000);
	}
	
	//Función para cerrar el formulario de la aceptación del presupuesto.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarAceptacionTotal', false);
	}
}