/****************************************************************************/
/*					Controlador del formulario de la Aceptación				*/
/****************************************************************************/
App.controller('AceptacionFormCntrl', AceptacionFormCntrl);
function AceptacionFormCntrl($scope, $rootScope, $timeout, ngDialog, focus, dataService, uploadService){
	
	$scope.files = [];
	$scope.filesUploaded = [];
	
	var contactosFinales = [];
	var datosContactos = {};
	var servicios = false;
	var contactos = true;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PresupuestosAceptacionFormCntrl ------------------------------');
	
	//Función que trae los archivos de la aceptación indicada.
	var traerFicheros = function(id){
		dataService
			.getFiltro('ficheros_aceptacion?filter=id%3E', 0, '%26%26id_aceptacion%3D'+id, 0, 0, 0, [])
			.then(function(data){
				for(var i=0; i<data.registros.length; i++){
					$scope.files.push({
						"id": data.registros[i].id,
						"file": {"name": data.registros[i].nombre},
						"descripcion": data.registros[i].descripcion
					});
				}
				$scope.progressUpload = 100;
				console.log('Ficheros de la aceptación '+id+' recuperados correctamente: ', data.registros.length);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorAceptaciones = parseError(error.status, error.data);
				$timeout(function(){$scope.errorAceptaciones = null;}, 15000);
			});
	}
		
	//Función que modifica la aceptación.
	$scope.editAceptacion = function(){
		if(contactos){
			//Una vez verificados los datos de las demás pestañas, verificamos que se han añadido ficheros.
			if(($scope.files.length != 0) && ($scope.progressUpload == 100)){
				//Una vez comprobado que todo está correctamente completado, insertamos la aceptación en su tabla correspondiente.						
				//Insertamos la aceptación (aceptacion)
				dataService
					.putElements('aceptacion', $scope.aceptacion)
					.then(function(data){
						$scope.aceptacion.id = data.id;
						console.log('Aceptación insertada correctamante: ', $scope.aceptacion.id);
						//Creamos los contactos correspondientes para la inserción en la tabla de unión (contactos_aceptacion).
						contactosFinales = [];
						for(var i=0; i<datosContactos.length; i++){
							contactosFinales.push({
								"id_contacto": datosContactos[i].id,
								"id_aceptacion": $scope.aceptacion.id										
							});
						}
						console.log('Contactos: ', contactosFinales);
						//Primero eliminamos los contactos antigüos de la aceptación
						dataService
							.deleteElements('contacto_aceptacion', '?filter=id_aceptacion%3D'+$scope.aceptacion.id)
							.then(function(data){
								console.log('Eliminación de contactos intermedia correcta');
								//Insertamos la relación de contactos en su tabla correspondiente (contacto_aceptacion).
								dataService
									.postElements('contacto_aceptacion', contactosFinales)
									.then(function(data){
										console.log('Contactos añadidos correctamente: ', data.record.length);
										var contactosElim = data.record;
										$scope.filesUploaded = [];
										//Creamos la estructura para hacer la inserción en la tabla ficheros_aceptacion.
										for(var i=0; i<$scope.files.length; i++){
											$scope.filesUploaded.push({
												"id": $scope.files[i].id,
												"id_aceptacion": $scope.aceptacion.id,
												"nombre": $scope.files[i].file.name,
												"descripcion": $scope.files[i].descripcion
											});
										}
										dataService
											.deleteElements('ficheros_aceptacion', '?filter=id_aceptacion%3D'+$scope.aceptacion.id)
											.then(function(data){
												console.log('Eliminación de ficheros intermedia correcta');
												//Insertamos la relación de ficheros en su tabla correspondiente (ficheros_aceptacion).
												dataService
													.postElements('ficheros_aceptacion', $scope.filesUploaded)
													.then(function(data){
														console.log('Ficheros añadidos correctamente: ', data.record.length);
														$scope.cerrar();
													})
													.catch(function(error){
														if(error.status==401){$rootScope.relogin();}
														$scope.errorFormAceptacion = parseError(error.status, error.data);
														$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
													});
											})
											.catch(function(error){
												if(error.status==401){$rootScope.relogin();}
												$scope.errorFormAceptacion = parseError(error.status, error.data);
												$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
											});
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormAceptacion = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
									});
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormAceptacion = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
							});	
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormAceptacion = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
					});
			}else{
				$scope.errorFormAceptacion = 'No ha adjuntado ningún fichero a la aceptación, o está en progreso de subida.';
				$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
			}
		}else{
			$scope.errorFormAceptacion = 'Se han realizado cambios en la lista de contactos sin pulsar el botón aceptar.';
			$timeout(function(){$scope.errorFormAceptacion = null;}, 15000);
		}
	}
	
	//Función para cerrar el formulario de la aceptación del presupuesto.
	$scope.cerrar = function(){
		$rootScope.$broadcast('cerrarAceptacionTotal', false);
	}
	
	//Función que escucha al padre e inicializa la pestaña del formulario.
	$scope.$on('editarAceptacion', function(e, data){
    	//Inicializamos las variables que cogen datos del presupuesto para el formulario y mostramos el mismo.
		$scope.aceptacion = {};
		$scope.aceptacion = angular.copy(data);
		traerFicheros($scope.aceptacion.id);
		$scope.titulo = 'General';
		$scope.mostrarFormAceptacion = true;
		focus('mainFocus');
		$scope.files = [];
	});
	
	//Función que identifica un cambio en la tabla de contactos.
	$scope.$on('cambioContactos', function(e, data){
		contactos = data;
	});
	
	//Función que escucha al padre y resetea el formulario.
	$scope.$on('cerrarFormAceptacion', function(e, data){
		$scope.mostrarFormAceptacion = false;
		$scope.aceptacion = {};
		$scope.formAceptacion.$setPristine();
		$scope.files = [];
		$scope.errorFormAceptacion = null;
		$scope.presupuesto = {};
		$scope.progressUpload = null;
	});
	
	//Funciones que reciben datos de los hermanos.
	$scope.$on('datosRentingAceptacion', function(e, data){
		console.log('datosRentingAceptacion recibidos: ', data);
		$scope.aceptacion.plazoRenting = data.plazoRenting;
		$scope.aceptacion.cuotaRenting = data.cuotaRenting;
		$scope.aceptacion.observacionesRenting = data.observacionesRenting;
	});
	$scope.$on('datosServiciosAceptacion', function(e, data){
		console.log('datosServiciosAceptacion recibidos: ', data);
		$scope.aceptacion.servicios = data.servicios;
		$scope.aceptacion.costeServicio = data.costeServicio;
		$scope.aceptacion.formaPagoSer = data.formaPagoSer;
		$scope.aceptacion.tipoPagoSer = data.tipoPagoSer;
		$scope.aceptacion.observacionesSer = data.observacionesSer;
		servicios = true;
	});
	$scope.$on('datosContactosAceptacion', function(e, data){
		console.log('datosContactosAceptacion recibidos: ', data);
		datosContactos = angular.copy(data);
		contactos = true;
	});
	
	//Función que cierra el panel de error del formulario.
	$scope.clearErrorFormAceptacion = function(){
		$scope.errorFormAceptacion = null;
	}
	
	/*--------------------------------------------- Upload Files ---------------------------------------------*/
	
	$scope.uploadFiles = function (files) {
        $rootScope.$broadcast('uploading', {"uploading": true, "progressUpload": 0});
        for(var i=0; i<files.length; i++){
	        $scope.files.push({
		        "file": files[i],
				"descripcion": ''
			});
        }
        var contador = 0;
        for(var i=0; i<files.length; i++){
	        //Primero hacemos la inserción del archivo en 
	        uploadService
	            .subirArchivos($scope.aceptacion.referencia+'/'+files[i].name+'?app_name=intranet', files[i])
	            .then(function(data){
		            console.log('Archivo subido:', data.file[0].path);
		            contador = contador + 1;
	                $scope.progressUpload = ((((contador)*100)/files.length).toFixed());
	                
	                if($scope.progressUpload == 100){
		                $rootScope.$broadcast('uploading', {"uploading": false, "progressUpload": $scope.progressUpload});
	                }else{
		                $rootScope.$broadcast('uploading', {"uploading": true, "progressUpload": $scope.progressUpload});
	                }
	            })	
	            .catch(function(error){
	                if(error.status==401){$rootScope.relogin();}
					$scope.errorMsg = parseError(error.status, error.data);
					$timeout(function(){$scope.errorMsg = null;}, 15000);
	                $scope.progressUpload = null;
	            });
        }
    }
    
    $scope.eliminarArchivo = function(archivo){
	    for(var i=0; i<$scope.files.length; i++){
		    if($scope.files[i] == archivo){
			    $scope.files.splice(i, 1);
		    }
	    }
	    if($scope.files.length == 0){
		    $scope.progressUpload = null;
	    }
	    uploadService
	        .eliminarArchivos($scope.aceptacion.referencia+'/'+archivo.file.name+'?app_name=intranet')
	        .then(function(data){
	            console.log('Archivo eliminado:', data.file[0].path);
	        })	
	        .catch(function(data, status){
	        	if(error.status==401){$rootScope.relogin();}
				$scope.errorMsg = parseError(error.status, error.data);
				$timeout(function(){$scope.errorMsg = null;}, 15000);
	        });
    }
}