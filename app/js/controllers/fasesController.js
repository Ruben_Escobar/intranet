/****************************************************************************/
/*							Controlador de Fases							*/
/****************************************************************************/
App.controller('FasesCntrl', FasesCntrl);
function FasesCntrl($scope, $rootScope, $timeout, uiGridConstants, ngDialog, focus, dataService){
	
	$scope.buffer = true;
	$scope.progressValue = 0;
	$scope.mostrar = false;
	$scope.editar = false;
	$scope.currentError = null;
	$scope.errorFormFase = null;
	
	var registros = [];
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ FasesCntrl ------------------------------');
	
	//Columnas de la tabla.	
	var nombre = {name: 'nombre', field: 'nombre', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 181, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="nombre"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var descripcion = {name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 90, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs', menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="descripcion"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var caducidad = {name: 'caducidad', field: 'caducidad', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="caducidad"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var master = {name: 'master', field: 'master', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="master"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var orden = {name: 'orden', field: 'orden', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="orden"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	$scope.columnas = [	  
			//Columnas fijas en la tabla  
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'nombre', field: 'nombre', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 200, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="nombre"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]},
			{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 90, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'hidden-xs cellToolTip', menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="descripcion"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]},
			{name: 'caducidad', field: 'caducidad', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="caducidad"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]}
	];
	
	//Inicializamos la tabla.
	$scope.gridFases = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};

	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridFases.paginationPageSize*36)+100) > (($scope.gridFases.data.length*36)+100) ){
			return ($scope.gridFases.data.length*36)+100;
		}else{
			return ($scope.gridFases.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (fases FasesCntrl): ', data.registros.length);
					$scope.gridFases.data = ordenarFases(data.registros);
					$scope.fases = $scope.gridFases.data;
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (fases FasesCntrl): '+$scope.progressValue+'%.');
					$scope.gridFases.data = angular.copy(data.registros);;
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
					//Cuando recuperamos los registros, creamos la variable que utilizaremos en el formulario.
					$scope.gridFases.data = ordenarFases(data.registros);
					$scope.fases = angular.copy($scope.gridFases.data);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	recuperarRegistros('fase?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que ordena la lista de fases según sus campos.
	var ordenarFases = function(fases){
		var fasesOrd = [];
		var indice = 0;
		var fin = 0;
		for(var i=0; i<fases.length;i++){
			if(fases[i].orden == 99999){
				fasesOrd[fases.length-1] = fases[i];
				indice++;
				break;
			}
		}
		while(fasesOrd[0] == undefined){
			fin = fases.length-indice;
			for(var j=0; j<fases.length; j++){
				if(fasesOrd[fin].id == fases[j].orden){
					indice++;
					fin--;
					fasesOrd[fin] = fases[j];
					break;
				}
			}
		}
		return fasesOrd;
	}
	
	//Función que inicializa el formulario.
	$scope.mostrarForm = function(){
		$scope.mostrar = true;
		$scope.informacion = false;
		$scope.anterior = true;
		$scope.titulo = 'Formulario Añadir';
		$scope.boton = 'Añadir';
		focus('mainFocus');
	}
	
	//Función que inicializa el formulario en forma de edición.
	$scope.editar = function(fase){
		$scope.fases = angular.copy($scope.gridFases.data);
		$scope.faseOrder = undefined;
		$scope.fase = angular.copy(fase);
		//Buscamos en la lista de fases la fase anterior a la que vamos a editar para indicarla en el select.
		for(var i=0; i<$scope.fases.length; i++){
			if($scope.fases[i].orden==fase.id){
				$scope.faseOrder = $scope.fases[i].id;
			}
			if($scope.fases[i].id == fase.id){
				var num = i;
			}
		}
		$scope.fases.splice(num,1);
		$scope.mostrar = true;
		$scope.informacion = false;
		$scope.anterior = false;
		$scope.titulo = 'Formulario Editar';
		$scope.boton = 'Modificar';
		focus('mainFocus');
	}
	
	//Función que resetea el formulario.
	$scope.resetformFase = function(){
		$scope.fase = {};
		$scope.faseOrder = '';
		$scope.mostrar = false;
		$scope.informacion = false;
		$scope.formFase.$setPristine();
	}
	
	//Función que intercala la fase en el orden que se le indique.
	var intercalar = function(id){
		for(var i=0; i<$scope.gridFases.data.length; i++){
			if($scope.gridFases.data[i].id == id){
				var orden = $scope.gridFases.data[i].orden;
				var fase = i;
				//Primero adjudico el orden a la fase nueva, y la inserto.
				$scope.fase.orden = orden;						
				dataService
					.postElements('fase', $scope.fase)
					.then(function(data){
						console.log('Fase insertada: ', data.id);
						
						//Una vez insertada la fase nueva, le adjudico el id al campo orden de la fase anterior, y la modifico.
						$scope.gridFases.data[fase].orden = data.id;
						dataService
							.putElements('fase', $scope.gridFases.data[fase])
							.then(function(data){
								console.log('Campo orden fase anterior('+data.id+') modificado.');
								$scope.resetformFase();
								$scope.buffer = true;
								recuperarRegistros('fase?filter=id%3E', 0, 0, 0, 0, []);
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormFase = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormFase = null;}, 15000);
							});
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormFase = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormFase = null;}, 15000);
					});
				break;
			}
		}
	}
	
	//Función para insertar fases.
	$scope.add = function(){
		//Comprobamos si es una inserción o una edición.
		if($scope.fase.id == undefined){
			//Insertar
			intercalar($scope.faseOrder);
		}else{
			//Modificar
			//Comprobamos si ha habido modificación en el select (Cambio de posición de la fase).
			if(!$scope.formFase.select.$pristine){
				var fasesAds = [];
				//Buscamos la fase anterior.
				for(var i=0; i<$scope.gridFases.data.length; i++){
					//Comprobamos que el id de la fase está dentro de uno de los ordenes de la tabla.
					if($scope.gridFases.data[i].orden == $scope.fase.id){
						console.log('Voy a modificar una fase que no es la primera');
						fasesAds = [];
						//Una vez encontramos la fase anterior, modificamos el campo orden (Sacamos de la lista la fase a editar).
						$scope.gridFases.data[i].orden = $scope.fase.orden;
						fasesAds.push($scope.gridFases.data[i]);

						//Una vez modificada la fase anterior (sacada la fase del listado), procedemos a insertarla en el orden especificado.
						for(var j=0; j<$scope.gridFases.data.length; j++){
							//Buscamos la fase que queremos sea la anterior.
							if($scope.gridFases.data[j].id == $scope.faseOrder){
								//indicamos que su orden, será el de nuestra fase a editar.
								$scope.fase.orden = $scope.gridFases.data[j].orden;
								fasesAds.push($scope.fase);
								//Y el orden de la anterior, será ahora el id de nuestra fase a editar.
								$scope.gridFases.data[j].orden = $scope.fase.id;
								fasesAds.push($scope.gridFases.data[j]);
								//Una vez añadidas las 3 fases a la lista de fases a modificar, realizamos la edición de las tres.
								console.log('Fases a Modificar: ', fasesAds);								
								dataService
									.putElements('fase', fasesAds)
									.then(function(data){
										console.log('Fases modificadas: ', data.record);
										$scope.resetformFase();
										$scope.buffer = true;
										recuperarRegistros('fase?filter=id%3E', 0, 0, 0, 0, []);
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormFase = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormFase = null;}, 15000);
									});
								break;
							}
						}
						break;
					}
				}
				//Si en el for anterior no rellena el array de añadir fases es porque es una fase de inicio.
				if(fasesAds.length == 0){
					console.log('Voy a modificar la primera fase');
					//Si es la primera (No tiene fase anterior), la insertamos en la posición que se desee.
						for(var j=0; j<$scope.gridFases.data.length; j++){
							if($scope.gridFases.data[j].id == $scope.faseOrder){
								$scope.fase.orden = $scope.gridFases.data[j].orden;
								fasesAds.push($scope.fase);
								$scope.gridFases.data[j].orden = $scope.fase.id;
								fasesAds.push($scope.gridFases.data[j]);
								console.log('Fases a Modificar: ', fasesAds);
								dataService
									.putElements('fase', fasesAds)
									.then(function(data){
										console.log('Fases modificada: ', data.record);
										$scope.resetformFase();
										$scope.buffer = true;
										recuperarRegistros('fase?filter=id%3E', 0, 0, 0, 0, []);
									})
									.catch(function(error){
										if(error.status==401){$rootScope.relogin();}
										$scope.errorFormFase = parseError(error.status, error.data);
										$timeout(function(){$scope.errorFormFase = null;}, 15000);
									});
								break;
							}
						}
				}
			//En caso de que el select no se modifique (No hay que cambiar de posición la fase),
			//mandamos orden de modificar la fase en la posición que está.					
			}else{
				dataService
					.putElements('fase', $scope.fase)
					.then(function(data){
						console.log('Fase modificada: ', data.id);
						$scope.resetformFase();
						$scope.buffer = true;
						recuperarRegistros('fase?filter=id%3E', 0, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormFase = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormFase = null;}, 15000);
					});
			}
		}
	}
	
	//Función que elimina una fase
	$scope.delFase = function(fase){
		var eliminada = true;
		//Buscamos la fase anterior.
		for(var i=0; i<$scope.gridFases.data.length; i++){
			if($scope.gridFases.data[i].orden == fase.id){
				//Una vez encontramos la fase anterior, modificamos el campo orden y procedemos a modificarla.
				$scope.gridFases.data[i].orden = fase.orden;
				eliminada = false;
				dataService
					.putElements('fase', $scope.gridFases.data[i])
					.then(function(data){
						console.log('Fase modificada: ', data.id);
						//Una vez modificada la fase anterior, eliminamos la seleccionada.
						dataService
							.deleteElements('fase', '?filter=id%3D'+fase.id)
							.then(function(data){
								console.log('Fase eliminada: ', data.record[0].id);
								$scope.resetformFase();
								$scope.buffer = true;
								recuperarRegistros('fase?filter=id%3E', 0, 0, 0, 0, []);
							})
							.catch(function(error){
								if(error.status==401){$rootScope.relogin();}
								$scope.errorFormFase = parseError(error.status, error.data);
								$timeout(function(){$scope.errorFormFase = null;}, 15000);
							});
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormFase = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormFase = null;}, 15000);
					});
				break;
			}
		}		
		//En caso de no encontrar fase anterior, es por que es la primera.
		if(eliminada){
			dataService
				.deleteElements('fase', '?filter=id%3D'+fase.id)
				.then(function(data){
					console.log('Fase eliminada: ', data.record[0].id);
					$scope.resetformFase();
					$scope.buffer = true;
					recuperarRegistros('fase?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormFase = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormFase = null;}, 15000);
				});
		}
	}
		
	//Función para cerrar los paneles de error.
	$scope.clearError = function(){
		$scope.currentError = null;
	}
	$scope.clearErrorFormFase = function(){
		$scope.errorFormFase = null;
	}
}