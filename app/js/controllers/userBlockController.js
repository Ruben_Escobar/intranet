/****************************************************************************/
/*							Controller del Sidebar							*/
/****************************************************************************/
App.controller('UserBlockController', ['$scope', function($scope) {

	$scope.userBlockVisible = true;
	
	//Aquí habrá que cargar las fotos por cada uno de los usuarios, o cargar una por defecto.
	$scope.user.picture = 'app/img/user/02.jpg'

	$scope.$on('toggleUserBlock', function(event, args) {
    	$scope.userBlockVisible = ! $scope.userBlockVisible;
  	});
}]);