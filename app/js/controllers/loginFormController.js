/****************************************************************************/
/*							Controlador del Login							*/
/****************************************************************************/
App.controller('LoginFormController', LoginFormController);
function LoginFormController($scope, $rootScope, $cookieStore, $state, $location, $timeout, UserEventsService, dataService) {
	
	$scope.currentError = null;
	    
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ LoginFormController ------------------------------');
	
	//Objeto base de la cuenta.
	$scope.account = {
    	email: null,
		password: null,
		duration:0
	}

	//Miramos si es un reload y si es así, inicializamos las variables.
	if($rootScope.user.email){
		$scope.account.email = $rootScope.user.email;
		$scope.account.password = $rootScope.user.password
	}
    
    //Función que realiza el logueo en DreamFactory con los datos del usuario facilitados.
	$scope.login = function() {
    	$rootScope.loggedInUser = true;
		if($scope.loginForm.$valid){
	    	//Realizamos el broadcast de la llamada de login
			$scope.$broadcast(UserEventsService.login.loginRequest, $scope.account);
    	}else{
        	$scope.loginForm.account_email.$dirty = true;
			$scope.loginForm.account_password.$dirty = true;
    	}
	}
  
	//Escuchamos el evento de vuelta del logueo y recogemos los datos para procesarlos.
	$scope.$on(UserEventsService.login.loginSuccess, function(e, userDataObj) {
    	//Si se selecciona recordar, se crean dos cookies que contienen los valores de login del usuario.
		if($scope.account.remember==true){
	    	var now = new Date();
	    	//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
			var tiempo = now.getTime();
			var mil = parseInt(3*24*60*60*1000);
			now.setTime(tiempo+mil);
			var time = new Date(now.getFullYear(), now.getMonth(), now.getDate());
			$cookieStore.put('SessionEmail',$scope.account.email,{expires: time});
			$cookieStore.put('SessionPassword',$scope.account.password,{expires: time});
		}else{
			$cookieStore.put('SessionEmail',$scope.account.email);
			$cookieStore.put('SessionPassword',$scope.account.password);
		}
		console.log('Recibo los datos del login correcto: ', userDataObj);
		
		//Función que realiza la carga inicial de poblaciones de la aplicación.
		var traerPob = function(tabla, id, acumulados, progressValue, total, registros){
			$rootScope.$broadcast('cargando', true);
			dataService
				.getAll(tabla, id, acumulados, progressValue, total, registros)
				.then(function(data){
					//Comprobamos si se ha terminado la carga o no. En caso negativo, volvemos a llamar a la función.
					if(data.progressValue == 100){
						console.log('Carga inicial de Poblaciones Finalizada (Login): '+data.registros.length+' registros.');
						$rootScope.poblaciones = angular.copy(data.registros);
						$rootScope.$broadcast('cargando', false);	
					}else{
						console.log('Carga inicial de Poblaciones (Login): '+data.progressValue+'%.');
						traerPob(data.tabla, data.registros[(data.registros.length-1)].idpoblacion, data.acumulados, data.progressValue, data.total, data.registros);
					}
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}
	    traerPob('poblaciones?filter=idpoblacion>', 0, 0, 0, 0, []);
	    
		$rootScope.user = userDataObj;
		$scope.$parent.currentUser = userDataObj;
		
		$scope.currentError = null;
		
		if($rootScope.user.is_sys_admin){
			$location.path("/app/dashboard");
		}else{
			switch($rootScope.user.role){
				case 'accesoBDD':
					$location.path("/app/dashboard");
					break;
				
				case 'Tecnico':
					$location.path("/app/dashboard-tecnico");
					break;
				
				case 'Incidencias':
					$location.path("/app/dashboard-tecnico");
					break;
			}
		}
	});
}