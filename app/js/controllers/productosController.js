/****************************************************************************/
/*							Controlador de Productos						*/
/****************************************************************************/
App.controller('ProductosCntrl', ProductosCntrl);
function ProductosCntrl($scope, $rootScope, $timeout, uiGridConstants, dataService){
	
	$scope.buffer = true;
	$scope.progressValue = 0;
	$scope.mostrar = false;
	$scope.editar = false;
	$scope.errorProd = null;
	$scope.errorFormProd = null;
	  
	var registros = [];
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ProductosCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	$scope.columnas = [	  
			//Columnas fijas en la tabla  
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},

		    {name: 'nombre', field: 'nombre', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 150},
			{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 150, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'hidden-xs cellToolTip'},
			{name: 'fabricante', field: 'fabricante', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 95},
			{name: 'proveedor', field: 'proveedor', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 95, headerCellClass : "hidden-xs", cellClass : "hidden-xs"},
			{name: 'coste', field: 'coste', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 65, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.coste}} €</div>', headerCellClass : "hidden-xs", cellClass : "hidden-xs"},
			{name: 'horas', field: 'horas', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 65, headerCellClass : "hidden-xs", cellClass : "hidden-xs"},
	];
	
	//Inicializamos la tabla.
	$scope.gridProductos = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};

	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridProductos.paginationPageSize*36)+100) > (($scope.gridProductos.data.length*36)+100) ){
			return ($scope.gridProductos.data.length*36)+100;
		}else{
			return ($scope.gridProductos.paginationPageSize*36)+100;
		}
	}
	
	//Función que trae los valores de configuración.
	var traerFamilias = function(){
		dataService
			.getAll('familias?filtro=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				for(var i=0; i<data.registros.length; i++){
					data.registros[i].nombre2 = data.registros[i].nombre+' - '+data.registros[i].descripcion;
				}
				$scope.familias = angular.copy(data.registros);
				console.log('Familias recuperadas correctamente (traerFamilias ProductosCntrl): ', data.registros.length);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorProd = parseError(error.status, error.data);
				$timeout(function(){$scope.errorProd = null;}, 15000);
			});
	}
	traerFamilias();
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de productos (recuperarRegistros ProductosCntrl): ', data.registros.length);
					
					$scope.gridProductos.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
					
					/*eliminar después***********
					for(var i=0; i<data.registros.length; i++){

						
							
						console.log('Referencia: ', data.registros[i].referencia);
						console.log('Tipo Referencia: ', data.registros[i].referencia.length);
						console.log('Iteración '+i+' del bucle.');
						console.log('Familia Destino: ', data.registros[i].familia2);
						
						for(var j=0; j<$scope.familias.length; j++){
							
							data.registros[i].familia2 = data.registros[i].familia2.replace(/(\r\n|\n|\r)/gm,"");
							
							data.registros[i].coste = data.registros[i].costeT.replace(",",".");
							console.log('Coste: ', data.registros[i].coste);
							data.registros[i].horas = data.registros[i].horasT.replace(",",".");
							console.log('Horas: ', data.registros[i].horas);
							
							if(data.registros[i].familia2 == $scope.familias[j].nombre){
								console.log('Familia Origen '+j+': '+$scope.familias[j].nombre);
								console.log('ID: ', $scope.familias[j].id);
								data.registros[i].familia = $scope.familias[j].id;
								break;
							}
						}
							
						if(data.registros[i].referencia.length < 17){
							
							var cad = '20151228'+twoDigits(Math.floor((Math.random() * 24)))+twoDigits(Math.floor((Math.random() * 60)))+twoDigits(Math.floor((Math.random() * 60)));
							
							var nums = cad.split("");
							var crc = 0;
							for(var k=0; k<nums.length; k++){
								crc += parseInt(nums[k]);
							}
							cad += twoDigits(crc);
							
							data.registros[i].referencia = 'P'+cad;
							console.log('Referencia nueva: ', data.registros[i].referencia);
						}
						
					}
					//console.log('Productos: ', data.registros);
					
					dataService
						.putElements('productos', data.registros)
						.then(function(data){
							console.log('PRODUCTOS ACTUALIZADOS!!!');
							$scope.gridProductos.data = angular.copy(data.registros);
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.errorProd = parseError(error.status, error.data);
							$timeout(function(){$scope.errorProd = null;}, 15000);
						});
					
					***************************/
					
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (productos ProductosCntrl): '+$scope.progressValue+'%.');
					$scope.gridProductos.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorProd = parseError(error.status, error.data);
				$timeout(function(){$scope.errorProd = null;}, 15000);
			});
	}
	recuperarRegistros('productos?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que inicializa el formulario.
	$scope.mostrarForm = function(){
		$scope.producto = {};
		$scope.producto.referencia = 'P'+crearReferencia();
		$scope.mostrar = true;
		$scope.titulo = 'Formulario Añadir';
		$scope.boton = 'Añadir';
		focus('mainFocus');
	}
	
	//Función que inicializa el formulario en forma de edición.
	$scope.editar = function(producto){
		$scope.producto = {};
		$scope.producto = angular.copy(producto);
		$scope.mostrar = true;
		$scope.titulo = 'Formulario Editar';
		$scope.boton = 'Modificar';
		focus('mainFocus');
	}
	
	//Función que resetea el formulario.
	$scope.resetFormProd = function(){
		$scope.producto = {};
		$scope.mostrar = false;
		$scope.formProd.$setPristine();
	}
	
	//Función para insertar o modificar productos.
	$scope.add = function(){		
		//Comprobamos si es una inserción o una edición.
		if($scope.producto.id == undefined){
			//Insertar
			dataService
				.postElements('productos', $scope.producto)
				.then(function(data){
					console.log('Producto añadido (productos ProductosCntrl): ', data.id);
					$scope.resetFormProd();
					$scope.buffer = true;
					recuperarRegistros('productos?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormProd = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormProd = null;}, 15000);
				});
		}else{
			//Modificar
			dataService
				.putElements('productos', $scope.producto)
				.then(function(data){
					console.log('Producto modificado (productos ProductosCntrl): ', data.record);
					$scope.resetFormProd();
					$scope.buffer = true;
					recuperarRegistros('productos?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormProd = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormProd = null;}, 15000);
				});
		}
	}
	
	//Función que elimina un producto
	$scope.delProducto = function(producto){
		dataService
			.deleteElements('productos', '?filter=id%3D'+producto.id)
			.then(function(data){
				console.log('Producto eliminado: ', data.id);
				$scope.resetFormProd();
				$scope.buffer = true;
				recuperarRegistros('productos?filter=id%3E', 0, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormProd = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormProd = null;}, 15000);
			});
	}
		
	//Función para cerrar los paneles de error.
	$scope.clearErrorProd = function(){
		$scope.errorProd = null;
	}
	$scope.clearErrorFormProd = function(){
		$scope.errorFormProd = null;
	}
}