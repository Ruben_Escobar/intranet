/****************************************************************************/
/*							Controlador de los Clientes						*/
/****************************************************************************/
App.controller('ClientesCntrl', ClientesCntrl);
function ClientesCntrl($scope, $rootScope, $timeout, uiGridConstants, ngDialog, focus, dataService){
	
	$scope.buffer = true;
	$scope.progressValue = 0;
	$scope.mostrar = false;
	$scope.editar = false;
	$scope.errorCliente = null;
	$scope.errorFormCliente = null;
	  
	var registros = [];
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ClientesCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.direccion}} tooltip-placement="top">{{row.entity.direccion}}</div>';
	
	$scope.columnas = [	  
			//Columnas fijas en la tabla  
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    {name: 'nombre', field: 'nombre', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 150},
			{name: 'direccion', field: 'direccion', headerCellClass : "hidden-xs", displayName: 'Dirección', enableSorting: false, minWidth: 150, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'hidden-xs cellToolTip'},
			{name: 'telefono', field: 'telefono', displayName: 'Teléfono', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 60},
			{name: 'email', field: 'email', displayName: 'e-mail', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 120}
	];
	
	//Inicializamos la tabla.
	$scope.gridCliente = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};

	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridCliente.paginationPageSize*36)+100) > (($scope.gridCliente.data.length*36)+100) ){
			return ($scope.gridCliente.data.length*36)+100;
		}else{
			return ($scope.gridCliente.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (clientes ClientesCntrl): ', data.registros.length);
					$scope.gridCliente.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (clientes ClientesCntrl): '+$scope.progressValue+'%.');
					$scope.gridCliente.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorClientes = parseError(error.status, error.data);
				$timeout(function(){$scope.errorClientes = null;}, 15000);
			});
	}
	recuperarRegistros('cliente?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que inicializa el formulario.
	$scope.mostrarForm = function(){
		$scope.mostrar = true;
		$scope.titulo = 'Formulario Añadir';
		$scope.boton = 'Añadir';
		focus('mainFocus');
	}
	
	//Función que inicializa el formulario en forma de edición.
	$scope.editar = function(cliente){
		$scope.cliente = {};
		$scope.cliente = angular.copy(cliente);
		$scope.mostrar = true;
		$scope.titulo = 'Formulario Editar';
		$scope.boton = 'Modificar';
		focus('mainFocus');
	}
	
	//Función que resetea el formulario.
	$scope.resetFormCliente = function(){
		$scope.cliente = {};
		$scope.mostrar = false;
		$scope.formCliente.$setPristine();
	}
	
	//Función para insertar o modificar clientes.
	$scope.add = function(){
		//Comprobamos si es una inserción o una edición.
		if($scope.cliente.id==undefined){
			//Insertar
			dataService
				.postElements('cliente', $scope.cliente)
				.then(function(data){
					console.log('Cliente añadido: ', data.id);
					$scope.resetFormCliente();
					$scope.buffer = true;
					registros = [];
					recuperarRegistros('cliente?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormCliente = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormCliente = null;}, 15000);
				});
		}else{
			//Modificar
			dataService
				.putElements('cliente', $scope.cliente)
				.then(function(data){
					console.log('Cliente modificado: ', data.record);
					$scope.resetFormCliente();
					$scope.buffer = true;
					registros = [];
					recuperarRegistros('cliente?filter=id%3E', 0, 0, 0, 0, []);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormCliente = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormCliente = null;}, 15000);
				});
		}
	}
	
	//Función que elimina un cliente
	$scope.delCliente = function(cliente){
		dataService
			.deleteElements('cliente', '?filter=id%3D'+cliente.id)
			.then(function(data){
				console.log('Cliente eliminado: ', data);
				$scope.resetFormCliente();
				$scope.buffer = true;
				registros = [];
				recuperarRegistros('cliente?filter=id%3E', 0, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormCliente = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormCliente = null;}, 15000);
			});
	}
		
	//Función para cerrar los paneles de error.
	$scope.clearErrorCliente = function(){
		$scope.errorCliente = null;
	}
	$scope.clearErrorFormCliente = function(){
		$scope.errorFormCliente = null;
	}
}