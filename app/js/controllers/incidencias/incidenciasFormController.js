/****************************************************************************/
/*					Controlador del formulario de Incidencias				*/
/****************************************************************************/
App.controller('IncidenciasFormCntrl', IncidenciasFormCntrl);
function IncidenciasFormCntrl($rootScope, $scope, $timeout, uiGridConstants, focus, ngDialog, dataService, SerIncidencias){
	
	$scope.buffer2=true;
	$scope.progressValue=0;
	$scope.open = false;
	$scope.open2 = false;
	$scope.open3 = false;
	$scope.inci = {};
	$scope.mostrarContrato = false;
	$scope.errorFormInci = null;
	SerIncidencias.data.modificado = false;
	
	var registrosSis = [];
	var total = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ IncidenciasFormCntrl ------------------------------');
	
	//Llamada get para recoger los sistemas y mostrarlos en la pestaña del formulario.
	var traerSis = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros(traerSis IncidenciasFormCntrl): ', data.registros.length);
					for(var i=0; i<data.registros.length; i++){
						//Filtramos acentos antes de nada.
						data.registros[i].nombre = removeAccents(data.registros[i].nombre);
					}
					$scope.sistemas = angular.copy(data.registros);
					$scope.buffer2 = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (traerSis IncidenciasFormCntrl): '+$scope.progressValue+'%.');
					$scope.sistemas = angular.copy(data.registros);
					traerSis(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	traerSis('sistema?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los tipos de incidencia de la tabla de configuraciones.
	var traerTipoIncidencia = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.tiposIncidencia = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				var tipos = data.registros[0].valor.split(",");
				var obj = {};
				for(var i=0; i<tipos.length; i++){
					obj = {};
					obj.nombre = tipos[i];
					obj.id = (i+1);
					$scope.tiposIncidencia.push(obj);
				}
				if($scope.inci.tipo != ''){
					for(var i=0; i<$scope.tiposIncidencia.length; i++){
						if($scope.tiposIncidencia[i].nombre == $scope.inci.tipo){
							$scope.inci.tipo = $scope.tiposIncidencia[i].id;
							break;
						}
					}
				}
				console.log('Traigo los tipos de incidencias (SistemasInciCntrl): ', $scope.tiposIncidencia);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que resetea el formulario de incidencias.	
	$scope.resetFormInci = function(){
		$scope.filtro='';
		$scope.inci = {};
		$scope.contrato = {};
		$scope.descripcion = '';
		$scope.status = '';
		$scope.tipo = '';
		$scope.currentError = null
		$scope.informacion = false;
		$scope.mostrarContrato = false;
		$scope.sistemaSel = 'Seleccione un sistema';
		$scope.formInci.$setPristine();
		$rootScope.$broadcast('ocultar', false);
		SerIncidencias.data.modificado = false;
		$scope.formInci.$dirty = false;
	}
	
	//Función que testea los cambios del formulario y avisa en caso de haber cambios antes de cerrar.
	$scope.cerrar = function(){
		if($scope.formInci.$dirty){
			if(confirm('Hay cambios en el formulario, si cierra los perderá')){
				$scope.resetFormInci();
			}
		}else{
			$scope.resetFormInci();
		}
	}
	
	//Función que comprueba si es una inserción o una modificación de incidencia, y procede.
	$scope.add = function(){
		if($scope.sistemaSel!='Seleccione un Sistema'){
			$scope.inci.descripcion = $scope.descripcion;
			for(var i=0; i<$scope.tiposIncidencia.length; i++){
				if($scope.tiposIncidencia[i].id == $scope.inci.tipo){
					$scope.inci.tipo = $scope.tiposIncidencia[i].nombre;
					break;
				}
			}
			$scope.inci.status = putStatus($scope.status);
			var hoy = new Date();
						
			//Comprobamos si es una inserción o una edición.
			if($scope.inci.id==undefined){		
				//Inserción.
				$scope.inci.inicio = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
									+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
				//Sumamos los días en milisegundos para evitar problemas de desbordamiento de la fecha.
				var tiempo = hoy.getTime();
				var mil = parseInt(2*24*60*60*1000);
				var fin = new Date();
				fin.setTime(tiempo+mil);
				$scope.inci.cad = ""+fin.getFullYear()+'-'+twoDigits((fin.getMonth()+1))+'-'+twoDigits(fin.getDate())+''
									+twoDigits(fin.getHours())+':'+twoDigits(fin.getMinutes())+":00";
				$scope.inci.referencia = "IC"+crearReferencia();
						
				dataService
					.postElements('incidencia', $scope.inci)
					.then(function(data){
						console.log('Incidencia insertada: ', data);
						$scope.resetFormInci();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormInci = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormInci = null;}, 15000);
					});
			}else{
				//Edición.
				if($scope.inci.status=='CERRADA'){
					$scope.inci.fin = ""+hoy.getFullYear()+'-'+twoDigits((hoy.getMonth()+1))+'-'+twoDigits(hoy.getDate())+' '
										+twoDigits(hoy.getHours())+':'+twoDigits(hoy.getMinutes())+":00";
				}else{
					$scope.inci.fin = null;
				}
				
				if($scope.inci.referencia == null){
					$scope.inci.referencia = "IC"+crearReferencia();
				}
				dataService
					.putElements('incidencia', $scope.inci)
					.then(function(data){
						console.log('Incidencia modificada:', data.id);
						SerIncidencias.data.modificado = false;
						$scope.formInci.$dirty = false;
						$scope.formInci.$setPristine();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormInci = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormInci = null;}, 15000);
					});
			}			
		}else{
			$scope.formInci.sistema.$dirty = true;
		}		
	}
	
	//Función que elimina una incidencia
	$scope.delInci = function(id){
		dataService
			.deleteElements('incid_task', '?filter=id_incidencia%3D'+id)
			.then(function(data){
				console.log('Tareas de la incidencia '+id+' eliminadas.');
				dataService
					.deleteElements('incidencia', '?filter=id%3D'+id)
					.then(function(data){
						console.log('Incidencia eliminada: ', data.id);
						$scope.resetFormInci();
						$rootScope.$broadcast('recargar', true);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormInci = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormInci = null;}, 15000);
					});
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormInci = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormInci = null;}, 15000);
			});
	}
	
	//Función que traduce de texto a número los status.
	var getStatus = function(status){
		switch(status){
			case "ABIERTA":
				return 1;
				break;
			case "PENDIENTE":
				return 2;
				break;
			case "CERRADA":
				return 3;
				break;
		}
	}
	
	//Función que traduce los números del select a texto de los tipos.
	var putStatus = function(status){
		switch(parseInt(status)){
			case 1:
				return 'ABIERTA';
				break;
				
			case 2:
				return 'PENDIENTE';
				break;
				
			case 3:
				return 'CERRADA';
				break;
		}
	}
	
	//Función que comprueba el tipo que se pone y modifica el campo registro policial.
	$scope.registro = function(tipo){
		if(tipo == 1 || tipo == 2){
			$scope.inci.registro_policia = true;
		}else{
			$scope.inci.registro_policia = false;
		}
		SerIncidencias.data.modificado = true;
		$scope.formInci.$dirty = true;
	}
	
	//Función que cambia el estado del servicio al actuar sobre algún campo del formualrio.
	$scope.cambiarModificado = function(){
		SerIncidencias.data.modificado = true;
	}
	
	//Función que abre el datetimepicker para seleccionar la fecha y después la hora.
	$scope.openCalendar = function(e, num) {
	    e.preventDefault();
	    e.stopPropagation();
	
		if(num == 1){
		    $scope.open = true;
		}
		if(num == 2){
		    $scope.open2 = true;
		}
		if(num == 3){
		    $scope.open3 = true;
		}
	};
	
	//Función para cerrar el panel de error.
	$scope.clearErrorFormInci = function(){
		$scope.errorFormInci = null;
	}
	
	//Función que trae los datos del contrato del sistema de la incidencia.
	var traerContrato = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.contrato = null;
		$scope.llavesContrato = undefined;
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.registros.length>0){
					$scope.contrato = angular.copy(data.registros[0]);
					$scope.contrato.servicios='';
					//Hacemos la petición de los servicios de dicho contrato.
					dataService
						.getFiltro('servicios?filter=id_servicio%3E', 0, '%26%26id_contrato%3D'+$scope.contrato.id, 0, 0, 0, [])
						.then(function(data){
							var servicios = angular.copy(data.registros);
							var hoy = new Date();
							$scope.contrato.caduca = new Date($scope.contrato.caduca);
							
							//Recorremos la lista de servicios quedándonos con los nombres
							for(var i=0; i<servicios.length; i++){
								if($scope.contrato.servicios == ''){
									$scope.contrato.servicios = servicios[i].nombre;
								}else{
									$scope.contrato.servicios = $scope.contrato.servicios +', '+servicios[i].nombre;
								}
							}
							//Comprobamos si está caducado o no el contrato.
							if(hoy.getTime() >= $scope.contrato.caduca.getTime()){
								$scope.contrato.caducado = true;
							}else{
								$scope.contrato.caducado = false;
							}
							console.log('Contrato del sistema: ', $scope.contrato.ref);
							
							//Realizamos la petición de los contactos del contrato (Según el sistema).
							dataService
								.getFiltro('contactos?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.contrato.id_sistema, 0, 0, 0, [])
								.then(function(data){
									var contactosRecibidos = angular.copy(data.registros);
									console.log('Contactos: ', contactosRecibidos.length);
									$scope.contactos = [];
									if(contactosRecibidos.length>0){
										if(contactosRecibidos.length>1){
											for(var i=0; i<contactosRecibidos.length; i++){
												$scope.contactos.push({"cuerpo": contactosRecibidos[i].nombre+', '+contactosRecibidos[i].apellidos+' - '
													+contactosRecibidos[i].telefono+' - '+contactosRecibidos[i].movil+' - '+contactosRecibidos[i].correo,
													"notas": contactosRecibidos[i].notas});
											}
										}else{
											$scope.contactos.push({"cuerpo": contactosRecibidos[0].nombre+', '+contactosRecibidos[0].apellidos+' - '
													+contactosRecibidos[0].telefono+' - '+contactosRecibidos[0].movil+' - '+contactosRecibidos[0].correo,
													"notas": contactosRecibidos[0].notas});
										}
									}else{
										$scope.contactos = undefined;
									}
									//Una vez que tenemos todos los datos del contrato, mostramos el cuadro dentro del formulario de incidencias.
									$scope.mostrarContrato = true;
								})
								.catch(function(error){
									if(error.status==401){$rootScope.relogin();}
									$scope.currentError = parseError(error.status, error.data);
									$timeout(function(){$scope.currentError = null;}, 15000);
								});
						})
						.catch(function(error){
							if(error.status==401){$rootScope.relogin();}
							$scope.currentError = parseError(error.status, error.data);
							$timeout(function(){$scope.currentError = null;}, 15000);
						});
				}else{
					$scope.mostrarContrato = true;
				}
			})
			.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
			});
		
		//Hacemos la llamada para traer las llaves del sistema.
		dataService
			.getFiltro('llaves?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.inci.id_sistema, 0, 0, 0, [])
			.then(function(data){
				if(data.registros.length != 0){
					$scope.llavesContrato = angular.copy(data.registros);
					console.log('Llaves del sistema (IncidenciasFormCntrl): ', $scope.llavesContrato.length);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}	
	
	//Función que muestra en un ngDialog el contenido total del contrato.
	$scope.contratoCompleto = function(id){
		dataService
			.getAll('contrato?filter=id%3D', id, 0, 0, 0, [])
			.then(function(data){
				$scope.contratoCompleto = angular.copy(data.record[0]);
				ngDialog.openConfirm({
					template: '/app/views/partials/ngDialogContrato.html',
					className: 'ngdialog-theme-default',
					scope: $scope
				}).then(function (sistema) {
					
			    }, function (reason) {
				    
			    });
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función para volver a la vista del sistema.
	$scope.volver = function(sistema){
		$scope.resetFormInci();
		$rootScope.$broadcast('recallSistema', sistema);
	}
	
	//Escuchamos para inicializar el formulario cuando es una inserción.
	$scope.$on('mostrar', function(e, data){
		$scope.mostrar = data;
		$scope.boton='Añadir Incidencia';
		focus('mainFocus');
		$scope.inci = {};
		$scope.status = 1;
		$scope.titulo = 'REGISTRAR NUEVA INCIDENCIA';
		$scope.recall = false;
		traerTipoIncidencia('configuracion?filter=id%3E', 0, "%26%26parametro='tipoIncidencia'", 0, 0, 0, []);
	});
	
	//Escuchamos para inicializar el formulario cuando es una edición.
	$scope.$on('editar', function(e, data){
		$scope.inci = angular.copy(data);
		$scope.status = getStatus(data.status);
		$scope.descripcion = data.descripcion;
		$scope.sistemaSel = data.sistema;
		$scope.mostrar = true;
		$scope.mostrarContrato = false;
		$scope.boton='Modificar Incidencia';
		focus('mainFocus');
		$scope.titulo = 'EDITAR UNA INCIDENCIA';
		$scope.informacion = false;
		traerTipoIncidencia('configuracion?filter=id%3E', 0, "%26%26parametro='tipoIncidencia'", 0, 0, 0, []);
		traerContrato('contrato?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.inci.id_sistema, 0, 0, 0, []);
		$scope.recall = false;
	});
	
	//Función que indica que es un recall de la incidencia
	$scope.$on('recall', function(e, data){
		$scope.inci = angular.copy(data.incidencia);
		$scope.tipo = getTipo(data.incidencia.tipo);
		$scope.status = getStatus(data.incidencia.status);
		$scope.descripcion = data.incidencia.descripcion;
		$scope.sistemaSel = data.incidencia.sistema;
		$scope.mostrar = true;
		$scope.mostrarContrato = false;
		$scope.boton='Modificar Incidencia';
		focus('mainFocus');
		$scope.titulo = 'EDITAR UNA INCIDENCIA';
		$scope.informacion = false;
		traerContrato('contrato?filter=id%3E', 0, '%26%26id_sistema%3D'+$scope.inci.id_sistema, 0, 0, 0, []);
		$scope.sistemaRecall = angular.copy(data.sistema);
		$scope.recall = true;
	});
	
	//Escuchamos al hermano que nos indica que hay que cerrar el formulario.
	$rootScope.$on('cerrarInci', function(e, data){
		$scope.resetFormInci();
	});
	
	/*------------------------------ Manejador del Select ------------------------------*/
	//Función que despliega el select.
	$scope.desplegar = function(){
		$scope.desplegarUl = !$scope.desplegarUl;
		if($scope.desplegarUl==true){
			focus('focusMe');
			$scope.filtro = '';
		}else{
			focus('mainFocus');
		}
	}
	
	//Función que sustituye el sistema actual por la seleccionada de la tabla.
	$scope.cambiarSistema = function(sistema){
		$scope.mostrarContrato = false;
		$scope.contrato = null;
		$scope.sistemaSel = sistema.nombre;
		$scope.inci.id_sistema = sistema.id;
		traerContrato('contrato?filter=id%3E', 0, '%26%26id_sistema%3D'+sistema.id, 0, 0, 0, []);
		$scope.desplegarUl = !$scope.desplegarUl;
		$scope.formInci.sistema.$dirty = false;
		focus('mainFocus');
		SerIncidencias.data.modificado = true;
		$scope.formInci.$dirty = true;
	}
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masSistemas = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogSistemas.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (sistema) {
			$scope.mostrarContrato = false;
			$scope.contrato = null;
			$scope.sistemaSel = sistema.nombre;
			$scope.inci.id_sistema = sistema.id;
			traerContrato('contrato?filter=id%3E', 0, '%26%26id_sistema%3D'+sistema.id, 0, 0, 0, []);
			$scope.desplegarUl = !$scope.desplegarUl;
			$scope.formInci.sistema.$dirty = false;
			focus('mainFocus');
	    }, function (reason) {
		    $scope.filtro='';
	    });
	}
}