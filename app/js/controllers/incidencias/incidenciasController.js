/****************************************************************************/
/*							Controlador de Incidencias						*/
/****************************************************************************/
App.controller('IncidenciasCntrl', IncidenciasCntrl);
function IncidenciasCntrl($scope, $rootScope, $timeout, $state, uiGridConstants, ngDialog, SerInci, dataService, localStorageService, SerIncidencias){
	
	$scope.buffer=true;
	$scope.progressValue=0;
	$scope.mostrar = false;
	$scope.sistemaSel = 'Seleccione un Sistema';
	$scope.editar = false;
	$scope.informacion = false;
	$scope.columnasSel = [];
	if(localStorageService.get('filtroIncidencias') != null && localStorageService.get('filtroIncidencias') != ''){
		$scope.filtros = localStorageService.get('filtroIncidencias');
	}else{
		$scope.filtros = [];
	}
	$scope.filtrado = false;
	$scope.open = {
	    date1: false,
	    date2: false,
	    date3: false
	};
	$scope.currentError = null;
	SerIncidencias.data.modificado = false;

	var url = '';
	var incidenciaAux = {};
	
	//Columnas de la tabla.
	var avisa = {name: 'avisa', field: 'avisa', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="avisa"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var tecnico = {name: 'tecnico', field: 'tecnico', displayName: 'Técnico', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="tecnico"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var concepto = {name: 'concepto', field: 'concepto', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="concepto"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var tipo = {name: 'tipo', field: 'tipo', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="tipo"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var fin = {name: 'fin', field: 'fin', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="fin"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var caducidad = {name: 'caducidad', field: 'cad', enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="caducidad"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var factura = {name: 'factura', field: 'factura', enableHiding: false, enableSorting: false, displayName: 'Concepto factura', enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="factura"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var registro_policia = {name: 'registro_policia', field: 'registro_policia', enableHiding: false, enableSorting: false, displayName: '¿Registro policial?', enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="registro_policia"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var facturar = {name: 'facturar', field: 'facturar', enableHiding: false, enableSorting: false, displayName: '¿Facturar?', enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="facturar"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var descripcion = {name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 90, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs', menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="descripcion"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var inicio = {name: 'inicio', field: 'inicio', suppressRemoveSort: true, minWidth: 135, enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="inicio"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	var status = {name: 'status', field: 'status', minWidth: 80, enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="status"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
		        }
			]};
	var sistema = {name: 'sistema', field: 'sistema', minWidth: 80, enableHiding: false, enableSorting: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="sistema"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]};
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ IncidenciasCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	$scope.columnas = [	  
			//Columnas fijas en la tabla
			{name: 'iconos', width: 50, cellTemplate: '<div class="centrado"><i class="fa fa-circle mr-sm" data-ng-class="'+"{'text-warning': grid.appScope.comprobarIncidencias(row.entity)==0, 'text-danger': grid.appScope.comprobarIncidencias(row.entity)==1, 'text-success': grid.appScope.comprobarIncidencias(row.entity)==2}"+'"></i><i class="{{row.entity.img}} ml-sm"></i></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedLeft: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'out_time', width:0, suppressRemoveSort: true, enableSorting: false, enableFiltering: false, enableHiding: false, displayName: '', enableColumnResizing: false, enablePinning: false, cellClass: "hidden", headerCellClass : "hidden", sort:{direction: uiGridConstants.DESC, priority: 1}},
		    
		    //Esta columna no aparecerá nunca en la tabla. Son para ayudar a ordenar la tabla.
		    //Las incidencias terminadas se mostrarán al final de la tabla, en el medio iran las que están en curso y superiores serán las caducadas.
		    {name: 'terminada', width:0, enableHiding: false, enableSorting: false, enablePinning:false, cellClass: "hidden", headerCellClass: "hidden", sort:{direction: uiGridConstants.ASC, priority: 0}},
		    
		    {name: 'inicio', field: 'inicio', cellClass: "hidden-xs", headerCellClass : "hidden-xs", minWidth: 135, sort:{direction: uiGridConstants.DESC, priority: 2}, enableHiding: false, enablePinning:false},
			
			//Columnas que pueden quitarse de la tabla.
			{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 90, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs', menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="descripcion"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]},				   
		    {name: 'status', field: 'status', minWidth: 80, enableHiding: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="status"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
		        }
			]},			
		    {name: 'sistema', field: 'sistema', minWidth: 80, enableHiding: false, enablePinning:false, menuItems: [
				{
		            title: 'Ocultar',
		            action: function() {
		              for(var i=0; i<$scope.columnas.length; i++){
			              if($scope.columnas[i].name=="sistema"){
				              $scope.columnas.splice(i, 1);
			              }
		              }
		            }
	            }
			]}
	];		
					
	//Inicializamos la tabla.
	$scope.gridOptions = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridOptions.paginationPageSize*36)+100) >(($scope.gridOptions.data.length*36)+100) ){
			return ($scope.gridOptions.data.length*36)+100;
		}else{
			return ($scope.gridOptions.paginationPageSize*36)+100;
		}
	}

	//Función que abre un ngDialog para indicar el filtro avanzado que queremos y lo muestra en la tabla de incidencias.
	$scope.filtroAvanzado = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogFiltroAvanzado.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (filtros) {
			console.log('Filtros: ', filtros.length);
			var and = -1;
			//For que recorre el listado de filtros y comprueba que no sean campos vacíos.
			for(var i=0; i<filtros.length; i++){
				if(filtros[i]!=undefined && filtros[i].valor!=undefined){
					and++;
				}
			}
			if(filtros.length>0 && and>-1){
				componerFiltro(filtros);
			}else{
				console.log('Sin filtros');
				$scope.resetearFiltros();
				if($scope.filtrado){
					$scope.filtrado = false;
					$scope.buffer = true;
					$scope.progressValue = 0;
					recuperarRegistros('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
				}
			}
	    }, function (reason) {
		    
		    
		});
	}
	
	//Función que compone el filtro.
	var componerFiltro = function(filtros){
		var url = '';
		var and = 0;
		for(var i=0; i<filtros.length; i++){
			if(filtros[i]!=undefined){
				//En caso de que fuera un campo de los de fecha.
				if(i==4 || i==5 || i==10){	
					filtros[i].valor = new Date(filtros[i].valor);
					var fecha = "'"+filtros[i].valor.getFullYear()+'-'+twoDigits((filtros[i].valor.getMonth()+1))+'-'
								+twoDigits(filtros[i].valor.getDate())+' '+twoDigits(filtros[i].valor.getHours())+':'
								+twoDigits(filtros[i].valor.getMinutes())+":00'";
					url = url+'%26%26'+'('+putTipoCol((i+1));
					//Comprobamos si está seleccionado el checkbox de negar, y procedemos en consonancia.
					if(filtros[i].neg==true){
						url = url+'%20!%3D%20'+fecha+")";
					}else{
						filtros[i].neg = false;
						url = url+'%20%3D%20'+fecha+")";
					}
					if(and>0){
						url = url+'%20%26%26%20';
						and--;
					}
				//En caso de que sea un campo numérico.
				}else if(i==7 || i==8){
					if(filtros[i].valor==true){
						url = url+'%26%26'+'('+putTipoCol((i+1));
							//Comprobamos si está seleccionado el checkbox de negar, y procedemos en consonancia.
							if(filtros[i].neg==true){
								url = url+'%20!%3D%20'+filtros[i].valor+")";
							}else{
								filtros[i].neg = false;
								url = url+'%20%3D%20'+filtros[i].valor+")";
							}
							if(and>0){
								url = url+'%20%26%26%20';
								and--;
							}
						}
				//El resto de casos, se trataría de texto.
				}else{
					if(filtros[i].valor != ''){
						url = url+'%26%26'+'('+putTipoCol((i+1));
						//Comprobamos si está seleccionado el checkbox de negar, y procedemos en consonancia.
						if(filtros[i].neg==true){
							if(i == 3){
								for(var j=0; j<$scope.tiposIncidencia.length; j++){
									if($scope.tiposIncidencia[j].id == filtros[i].valor){
										url = url+"%20!%3D%20%27"+$scope.tiposIncidencia[j].nombre.replace(" ", "%20")+"%27)";
										break;
									}
								}
							}else{
								url = url+"%20!%3D%20%27"+filtros[i].valor.replace(" ", "%20")+"%27)";
							}
						}else{
							if(i == 3){
								for(var j=0; j<$scope.tiposIncidencia.length; j++){
									if($scope.tiposIncidencia[j].id == filtros[i].valor){
										url = url+"%20%3D%20%27"+$scope.tiposIncidencia[j].nombre.replace(" ", "%20")+"%27)";
										break;
									}
								}
							}else{
								filtros[i].neg = false;
								url = url+"%20%3D%20%27"+filtros[i].valor.replace(" ", "%20")+"%27)";
							}
						}
						if(and>0){
							url = url+'%20%26%26%20';
							and--;
						}
					}else{
						//En caso de que el valor esté vacío, eliminamos el elemento del array.
						filtros[i] = undefined;
					}
				}
			}
		}
		$scope.buffer = true;
		//Mandamos al localstorage el filtro.
		localStorageService.set('filtroIncidencias', filtros);
		llamarFiltro('incidencias?filter=id%3E', 0, url, 0, 0, 0, []);
	}
	
	//Función que llama a la url de filtros que haya fuardad.
	var llamarFiltro = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (llamarFiltro IncidenciasCntrl): ', data.registros.length);
					for(var i=0; i < data.registros.length; i++){
						data.registros[i].img = getTipo(data.registros[i].tipo);
					}
					$scope.gridOptions.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (llamarFiltro IncidenciasCntrl): '+$scope.progressValue+'%.');
					for(var i=0; i < data.registros.length; i++){
						data.registros[i].img = getTipo(data.registros[i].tipo);
					}
					$scope.gridOptions.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.filtro, data.acumulados, data.progressValue, data.total, data.registros);
				}
				$scope.filtrado = true;
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que resetea los datos del formulario de filtrado.
	$scope.resetearFiltros = function(){
		url = "";
		for(var i=0; i<$scope.filtros.length; i++){
			if($scope.filtros[i]!=undefined){
				$scope.filtros[i].valor = undefined;
				$scope.filtros[i].neg = false;
				$scope.filtros[i] = undefined;
			}
		}
	}
	
	//Función que resetea los filtros avanzados, es decir, recupera todos los registros de nuevo de la BDD.
	$scope.togleFiltros = function(){
		if($scope.filtrado){
			$scope.filtrado = false;
			$scope.buffer = true;
			$scope.progressValue = 0;
			if(localStorageService.get('filtroIncidencias') != null && localStorageService.get('filtroIncidencias') != ''){
				$scope.filtros = localStorageService.get('filtroIncidencias');
			}else{
				$scope.filtros = [];
			}
			recuperarRegistros('incidencias?filter=id%3E', 0, 0, 0, 0, []);
		}else{
			if($scope.filtros != '' && $scope.filtros != null){
				$scope.filtrado = true;
				$scope.buffer = true;
				$scope.progressValue = 0;					
				componerFiltro($scope.filtros);
			}else{
				$scope.currentError = 'No hay filtros.';
				$timeout(function(){$scope.currentError = null;}, 15000);
			}
			
		}
	}
	
	//Función que llama al ngDialog en el cual seleccionaremos las columnas que queramos que contenga la tabla (La columna inicio siempre estará).
	$scope.cambiarTabla = function(){
		for(var j=0; j<$scope.columnasSel; j++){
			$scope.columnasSel[j]=false;
		}
		//Inicializamos la lista de columnas, marcando cuales forman parte de la tabla en este momento.
		for(var i=0; i<$scope.columnas.length; i++){
			switch($scope.columnas[i].name){
				case "avisa":
					$scope.columnasSel[0]=true;
					break;
					
				case "tecnico":
					$scope.columnasSel[1]=true;
					break;
					
				case "concepto":
					$scope.columnasSel[2]=true;
					break;
					
				case "tipo":
					$scope.columnasSel[3]=true;
					break;
					
				case "fin":
					$scope.columnasSel[4]=true;
					break;
					
				case "caducidad":
					$scope.columnasSel[5]=true;
					break;
					
				case "factura":
					$scope.columnasSel[6]=true;
					break;
					
				case "registro_policia":
					$scope.columnasSel[7]=true;
					break;
					
				case "facturar":
					$scope.columnasSel[8]=true;
					break;
					
				case "descripcion":
					$scope.columnasSel[9]=true;
					break;
				
				case "inicio":
					$scope.columnasSel[10]=true;
					break;
					
				case "status":
					$scope.columnasSel[11]=true;
					break;
					
				case "sistema":
					$scope.columnasSel[12]=true;
					break;
				
			}
		}				
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogColumnas.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (columnas) {
			if(columnas.length>0){
				for(var i=0; i<columnas.length; i++){
					if(columnas[i]==true){
						editCol((i+1), true);
					}else{
						editCol((i+1), false);
					}
				}
			}
			for(var j=0; j<columnas.length; j++){
				columnas[j]=false;
			}			
	    }, function (reason) {
		    console.log('Cerrado filtro sin filtrar.');
	    });
	}    
	
	//Función que añade columnas a la tabla.
	var editCol = function(columna, add){
	    var nCol = putTipoCol(columna);
	    //Si add es true, se añade la columna, si no se elimina.
	    if(add){
		    if(!estaCol(nCol)){
				if(columna==1){
					$scope.columnas.splice(4,0,avisa);
				}else if(columna==2){
					$scope.columnas.splice(4,0,tecnico);
				}else if(columna==3){
					$scope.columnas.splice(4,0,concepto);
				}else if(columna==4){
					$scope.columnas.splice(4,0,tipo);
				}else if(columna==5){
					$scope.columnas.splice(4,0,fin);
				}else if(columna==6){
					$scope.columnas.splice(4,0,caducidad);
				}else if(columna==7){
					$scope.columnas.splice(4,0,factura);
				}else if(columna==8){
					$scope.columnas.splice(4,0,registro_policia);
				}else if(columna==9){
					$scope.columnas.splice(4,0,facturar);
				}else if(columna==10){
					$scope.columnas.splice(4,0,descripcion);
				}else if(columna==11){
					$scope.columnas.splice(4,0,inicio);
				}else if(columna==12){
					$scope.columnas.splice(4,0,status);
				}else if(columna==13){
					$scope.columnas.splice(4,0,sistema);
				}
				console.log('Añadida columna ', nCol);
			}else{
				console.log('Intento de añadir columna existente: ', nCol);
			}
		//Si es false, se elimina.
	    }else{
		    if(estaCol(nCol)){
			    //Comprobamos que está dentro del listado de columnas, y si es así lo eliminamos.
			    for(var i=0; i<$scope.columnas.length; i++){
				    if($scope.columnas[i].name == nCol){
					    $scope.columnas.splice(i,1);
				    }
			    }
				console.log('Columna eliminada: ', nCol);
			}
	    }
	    $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	}
	
	//Función que traduce de texto a la clase correspondiente (imagen asignada a cada tipo).
	var getTipo = function(tipo){
		switch(tipo){
			case "TECNICA":
				return 'fa fa-wrench';
				break;
			case "IMAGENES":
				return 'fa fa-eye';
				break;
			case "SOPORTE REMOTO":
				return 'icon-earphones-alt';
				break;
			case "SOPORTE IN SITU":
				return 'fa fa-car';
				break;
			case "SIN CONEXION":
				return 'fa fa-chain-broken';
				break;
			case "DOCUMENTACION":
				return 'icon-docs';
				break;
		}
	}
	
	//Función que comprueba si una columna está añadida a la tabla.
	var estaCol = function(columna){		
		for(var i=1; i<$scope.columnas.length; i++){			
			if($scope.columnas[i].name==columna){
				return true;
			}
		}
		return false;
	}
	
	//Función que traduce los números del select de las columans a sus nombres correspondientes.
	var putTipoCol = function(columna){
		switch (parseInt(columna)) {
			case 1:
				return 'avisa';
				break;				
			case 2:
				return 'tecnico';
				break;				
			case 3:
				return 'concepto';
				break;				
			case 4:
				return 'tipo';
				break;				
			case 5:
				return 'fin';
				break;				
			case 6:
				return 'cad';
				break;			
			case 7:
				return 'factura';
				break;			
			case 8:
				return 'registro_policia';
				break;			
			case 9:
				return 'facturar';
				break;			
			case 10:
				return 'descripcion';
				break;			
			case 11:
				return 'inicio';
				break;			
			case 12:
				return 'status';
				break;			
			case 13:
				return 'sistema';
				break;
		}
	}
		
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros(incidencias IncidenciasCntrl): ', data.registros.length);
					var hoy = new Date();
					for(var i=0; i < data.registros.length; i++){
						data.registros[i].img = getTipo(data.registros[i].tipo);
						//La parte del replace hay que hacerla para que acepte el parseo a fecha, puesto que en la BBDD viene con
						//el caracter de separación - y es necesario que sea /. Creamos una variable auxiliar para la comprobación.
						var cadAux = new Date(data.registros[i].cad.replace(/-/g, "/"));
								
						if(hoy.getTime() > cadAux.getTime() && data.registros[i].fin == null){
							data.registros[i].out_time = 1;
						}else{
							data.registros[i].out_time = 0;
						}
						if(data.registros[i].fin != undefined){
							data.registros[i].terminada = true;
						}else{
							data.registros[i].terminada = false;
						}
					}
					$scope.gridOptions.data = angular.copy(data.registros);
					
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue= data.progressValue;
					console.log('Cargando (incidencias IncidenciasCntrl): '+$scope.progressValue+'%.');
					for(var i=0; i < data.registros.length; i++){
						data.registros[i].img = getTipo(data.registros[i].tipo);
					}
					$scope.gridOptions.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	recuperarRegistros('incidencias?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los tipos de incidencia de la tabla de configuraciones.
	var traerTipoIncidencia = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.tiposIncidencia = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				var tipos = data.registros[0].valor.split(",");
				var obj = {};
				for(var i=0; i<tipos.length; i++){
					obj = {};
					obj.nombre = tipos[i];
					obj.id = (i+1);
					$scope.tiposIncidencia.push(obj);
				}
				//Una vez que traemos los tipos de incidencia, comprobamos si hay filtros, y en caso de estar guardado el filtro de tipo, le parseamos.
				if($scope.filtros[3] && $scope.filtros[3].valor != ''){
					for(var i=0; i<$scope.tiposIncidencia.length; i++){
						if($scope.tiposIncidencia[i].nombre == $scope.filtros[3].valor){
							$scope.filtros[3].valor = $scope.tiposIncidencia[i].id;
							break;
						}
					}
				}
				console.log('Traigo los tipos de incidencias (SistemasInciCntrl): ', $scope.tiposIncidencia);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	traerTipoIncidencia('configuracion?filter=id%3E', 0, "%26%26parametro='tipoIncidencia'", 0, 0, 0, []);
	
	//Función que comprueba si una instalación está
	$scope.comprobarIncidencias = function(incidencia){
		if(incidencia.status=='ABIERTA'){
			if(incidencia.out_time==1){
				return 1;
			}else{
				return 0;
			}
		}else if(incidencia.status=='PENDIENTE'){
			//Si está en pendiente, no hace comprobaciones, directamente se muestra como que está en proceso. (Color amarillo).
			return 0;
		}else if(incidencia.status=='CERRADA'){
			return 2;
		}
	}
	
	//Función que le indica al hijo IncidenciasFormCntrl que inicialice el formulario en forma de inserción.
	$scope.mostrarForm = function(){
		$scope.mostrar = true;
		$timeout(function(){$rootScope.$broadcast('mostrar', true);}, 5);
	}
	
	//Función que le indica al hijo IncidenciasFormCntrl que inicialice el formulario en forma de edición.
	$scope.editar = function(incidencia){
		$scope.mostrar = true;
		incidenciaAux = angular.copy(incidencia);
		//La parte del replace hay que hacerla para que acepte el parseo a fecha, puesto que en la BBDD viene con
		//el caracter de separación - y es necesario que sea /. Este cambio lo acemos para que lo acepte el campo del formulario.
		incidenciaAux.cad = new Date(incidencia.cad.replace(/-/g, "/"));
		incidenciaAux.inicio = new Date(incidencia.inicio.replace(/-/g, "/"));
		$timeout(function(){$rootScope.$broadcast('editar', incidencia);}, 5);
	}
	
	//Función que abre el datetimepicker para seleccionar la fecha y después la hora.
	$scope.openCalendar = function(e, date) {
	    e.preventDefault();
	    e.stopPropagation();
	    $scope.open[date] = true;
	};
	
	//Función que comprueba si el formulario ha sido modificado antes de cambiar de pestaña.
	$scope.pararSiModificado = function (event) {
		//Comprobamos si el servicio está a true, en cuyo caso se habrá modificado algún campo del formulario.
		if (SerIncidencias.data.modificado){
			var answer = confirm('Hay cambios sin guardar en el formulario. ¿Desea continuar?');
		    !answer && event.preventDefault();
		}
	};
	
	//Función para cerrar el panel de error.
	$scope.clearError = function(){
		$scope.currentError = null;
	}
	
	//Recibimos las llamadas de los hijos para cerrar el panel del formulario.
	$scope.$on('ocultar', function(e,data) {  
        $scope.mostrar = data;
    });
    
    //Recibimos las llamadas de los hijos para recargar los datos de la tabla de incidencias.
    $scope.$on('recargar', function(e, data){
	    $scope.buffer = data;
		recuperarRegistros('incidencias?filter=id%3E', 0, 0, 0, 0, []);
    });
    
    //Recibimos la señal del hijo para cambiar de vista.
    $scope.$on('recallSistema', function(e, data){
	    SerInci.data.sistema = angular.copy(data);
		$state.go('app.sistemas');
    });
    
    //Comprobamos si el servicio SerInci tiene datos, en cuyo caso procedemos a mostrra la incidencia solicitada.
    if(SerInci.data.sistema != undefined && SerInci.data.incidencia != undefined){
	    $scope.mostrar = true;
	    console.log('Comunicación desde Sistemas con Incidencias');
	    
		$timeout(function(){$rootScope.$broadcast('recall', SerInci.data);}, 5);
    }
}