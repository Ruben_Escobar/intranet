/****************************************************************************/
/*				Controlador de las Tareas de las Incidencias				*/
/****************************************************************************/
App.controller('IncidenciasTreasCntrl', IncidenciasTreasCntrl);
function IncidenciasTreasCntrl($scope, $rootScope, $timeout, uiGridConstants, focus, ngDialog, dataService){
	
	$scope.buffer = true;
	$scope.progressValue = 0;
	$scope.mostrarFormTareas = false;
	$scope.nuevo = false;
	$scope.open = false;
	//Variables del datetimepicker
	$scope.fechaHora = new Date();
	$scope.open = false;
	$scope.errorFormTareas = null;
	$scope.errorTareas = null;
	
	var registros = [];
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ IncidenciasTareasCntrl ------------------------------');
	
	//Función que trae los registros de la BDD.
	var traerTasks = function(id, idInci){
		dataService
			.getFiltro('incid_task?filter=id%3E', id, '%26%26id_incidencia%3D'+idInci, 0, 0, 0, [])
			.then(function(data){
				console.log('Total de registros (traerTasks IncidenciasTreasCntrl): ', data.registros.length);
				$scope.tasks = angular.copy(data.registros);
				$scope.buffer = false;
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función para insertar tareas.
	$scope.add = function(){
		$scope.task.id_incidencia = $scope.inci.id;
		
		if($scope.fechaHora != null){
			$scope.task.fecha_hora = parseFechaHora($scope.fechaHora);
		}else{
			$scope.task.fecha_hora = new Date();
			$scope.task.fecha_hora.setHours($scope.task.fecha_hora.getHours()+1);
		}
		//Hacemos el tratamiento del campo.
		$scope.task.tiempo = ""+$scope.mytime.getHours()+':'+$scope.mytime.getMinutes()+':00';
		//Comprobamos si es una inserción o una edición.
		if($scope.task.id==undefined){
			dataService
				.postElements('incid_task', $scope.task)
				.then(function(data){
					console.log('Tarea de incidencia insertada: ', data);
					$scope.resetFormInciTask();
					$scope.buffer = true;
					registros = [];
					traerTasks(0, $scope.inci.id);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormTareas = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormTareas = null;}, 15000);
				});
		}else{
			dataService
				.putElements('incid_task', $scope.task)
				.then(function(data){
					console.log('Tarea de incidencia modificada: ', data.id);
					$scope.resetFormInciTask();
					$scope.buffer = true;
					registros = [];
					traerTasks(0, $scope.inci.id);
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormTareas = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormTareas = null;}, 15000);
				});
		}
	}
	
	//Función que elimina la tarea.
	$scope.delTask = function(id){
		dataService
			.deleteElements('incid_task', '?filter=id%3D'+id)
			.then(function(data){
				console.log('Tarea de incidencia eliminada: ', data);
				$scope.resetFormInciTask();
				$scope.buffer = true;
				registros = [];
				traerTasks(0, $scope.inci.id);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormTareas = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormTareas = null;}, 15000);
			});
	}
	
	//Función que muestra el formulario de las tareas.
	$scope.mostrarFormTasks = function(){
		$scope.mytime = new Date();
		$scope.mytime.setHours(0,0,0,0);
		$scope.mostrarFormTareas = true;
		$scope.boton = 'Añadir';
		$scope.titulo = 'AÑADIR UNA NUEVA TAREA A LA INCIDENCIA';
	}
	
	//Función que resetea el formulario de tareas.
	$scope.resetFormInciTask = function(){
		$scope.mostrarFormTareas = false;
		$scope.fechaHora = null;
		$scope.formInciTask.$setPristine();
		$scope.task = {};
		$scope.currentError = null;
	}
	
	//Función que manda señal al controlador IncidenciasFormCntrl de que resetee el formulario y resetea el formulario de las tareas.
	$scope.resetFormInci = function(){
		$scope.resetFormInciTask();
		$scope.tasks = [];
		$rootScope.$broadcast('cerrarInci', true);
	}
	
	//Función que manda los datos de las tareas al formulario.
	$scope.editarTarea = function(task){
		$scope.mostrarFormTareas = true;
		$scope.task = angular.copy(task);
		//Hacemos la transformación necesaria de los datos de hora de la BBDD para mostrarlos en el formulario.
		var hora = $scope.task.tiempo.split(':');
		var time = new Date();
		time.setHours(hora[0]);
		time.setMinutes(hora[1]);
		//Asignamos el valor de hora al timepicker.
		$scope.mytime = time;
		if($scope.task.fecha_hora != null){						
			$scope.fechaHora = getFechaHora(new Date($scope.task.fecha_hora));
		}
		$scope.boton = 'Modificar';
		$scope.titulo = 'MODIFICAR TAREA DE LA INCIDENCIA';
	}
	
	//Función que abre el datetimepicker para seleccionar la fecha y después la hora.
	$scope.openCalendar = function(e) {
	    e.preventDefault();
	    e.stopPropagation();
	    $scope.open = true;
	};
  
	//Funciones para cerrar los paneles de error.
	$scope.clearErrorFormTareas = function(){
		$scope.errorFormTareas = null;
	}
	$scope.clearErrorTareas = function(){
		$scope.errorTareas = null;
	}
	
	//Abrimos una lista vacía de tareas, puesto que la incidencia se va a crear.
	$scope.$on('mostrar', function(e, data){
		$scope.nuevo = true;
		$scope.buffer = false;
	});
	
	//Recibimos llamada de edición de una incidencia. Mostramos una lista de tareas y un botón para añadir una nueva.
	$scope.$on('editar', function(e, data){
		$scope.inci = angular.copy(data);
		$scope.buffer = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		$scope.mostrarFormTareas = false;
		$scope.fechaHora = null;
		traerTasks(0, data.id);
	});
	
	//Función que indica que es un recall de la incidencia
	$scope.$on('recall', function(e, data){
		$scope.inci = angular.copy(data.incidencia);
		$scope.buffer = true;
		$scope.nuevo = false;
		$scope.mostrar = true;
		$scope.mostrarFormTareas = false;
		$scope.fechaHora = null;
		traerTasks(0, data.incidencia.id);
		$scope.sistemaRecall = angular.copy(data.sistema);
		$scope.recall = true;
	});
}