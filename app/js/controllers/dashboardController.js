/****************************************************************************/
/*					Controlador de la vista de dashboard					*/
/****************************************************************************/
App.controller('DashboardCntrl', DashboardCntrl);
function DashboardCntrl($scope, $rootScope, $timeout, uiGridConstants, dataService){

	var instalaciones = [];
	var incidencias = [];
	var fasesOut = [];
	var pendientes = [];
	
	$scope.buffer = true;	
	$scope.buffer2 = true;
	$scope.errorInsDash = null;
	
	//Variables para mostrar los tooltip de las columnas descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	var templateWithTooltip2 = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ DashboardCntrl ------------------------------');
	
	//Definición de las columnas de cada una de las tablas.
	$scope.columnasInci = [  
		    {name: 'iconos', width: 50, cellTemplate: '<div class="centrado"><i class="fa fa-circle mr-sm" data-ng-class="'+"{'text-warning': grid.appScope.comprobarIncidencias(row.entity)==0, 'text-danger': grid.appScope.comprobarIncidencias(row.entity)==1, 'text-success': grid.appScope.comprobarIncidencias(row.entity)==2}"+'"></i><i class="{{row.entity.img}} ml-sm"></i></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedLeft: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'out_time', width:0, suppressRemoveSort: true, enableSorting: false, enableFiltering: false, enableHiding: false, enableColumnResizing: false, enablePinning: false, cellClass: "hidden", headerCellClass : "hidden", sort:{direction: uiGridConstants.DESC, priority: 0}},
		    
		    {name: 'inicio', width:0, cellClass: "hidden", headerCellClass : "hidden", enableFiltering: false, sort:{direction: uiGridConstants.DESC, priority: 1}, enableHiding: false, enablePinning:false},
					
		    {name: 'sistema', field: 'sistema', minWidth: 135, width: '50%', enableHiding: false, enablePinning:false},
		    {name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 150, width: '50%', enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'cellToolTip hidden-xs'}
	];	

	$scope.columnasInst = [
			//Columnas variables
		    {name: 'out_time', width:0, suppressRemoveSort: true, cellTemplate: '<div class="centrado"></div>', enableSorting: false, enableFiltering: false, enableHiding: false, displayName: '', enableColumnResizing: false, enablePinning: false, cellClass: "hidden", headerCellClass : "hidden", sort:{direction: uiGridConstants.DESC, priority: 0}},
		    
		    {name: 'color', width: 35, cellTemplate: '<div class="centrado"><i class="fa fa-circle" data-ng-class="'+"{'text-warning': grid.appScope.comprobarInstalaciones(row.entity)==0, 'text-danger': grid.appScope.comprobarInstalaciones(row.entity)==1, 'text-success': grid.appScope.comprobarInstalaciones(row.entity)==2}"+'"></i></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedLeft: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'referencia', field: 'referencia', enableHiding: false, enablePinning:false, minWidth: 135, width: '50%'},
			{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 150, width: '50%', enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip2, cellClass: 'cellToolTip hidden-xs'},
			{name: 'inicio', enableHiding: false, width: 0, sort:{direction: uiGridConstants.ASC, priority: 1}, enablePinning:false, cellClass: "hidden", headerCellClass : "hidden"},
			{name: 'fin', field: 'fin', width:0, enableHiding: false, minWidth: 135, enablePinning:false, cellClass: "hidden", headerCellClass : "hidden"}
	];
	
	//Inicializamos las tablas.
	$scope.gridIncidencias = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,		
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnasInci,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};
	$scope.gridInstalaciones = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnasInst,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};
	
	//Funciones que cambian los tamaños de las tablas.
	$scope.comprobarTamano = function(){
		if( (($scope.gridIncidencias.paginationPageSize*36)+90) > (($scope.gridIncidencias.data.length*36)+90) ){
			return ($scope.gridIncidencias.data.length*36)+90;
		}else{
			return ($scope.gridIncidencias.paginationPageSize*36)+90;
		}
	}
	$scope.comprobarTamano2 = function(){
		if( (($scope.gridInstalaciones.paginationPageSize*36)+90) > (($scope.gridInstalaciones.data.length*36)+90) ){
			return ($scope.gridInstalaciones.data.length*36)+90;
		}else{
			return ($scope.gridInstalaciones.paginationPageSize*36)+90;
		}
	}
	
	//Función que recupera todos las Incidencias de la BDD de manera recursiva.
	var recuperarIncidencias = function(tabla, id, filtro, acumulados, progressValue, total, registros){
				
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				//Comprobamos si se ha terminado la carga o no. En caso negativo, volvemos a llamar a la función.
				if(data.progressValue == 100){
					console.log('Total de registros (Incidencias Dashboard): '+data.registros.length);
					incidencias = angular.copy(data.registros);
					var hoy = new Date();
					for(var i=0; i < incidencias.length; i++){
						incidencias[i].img = getTipo(incidencias[i].tipo);
						//La parte del replace hay que hacerla para que acepte el parseo a fecha, puesto que en la BDD viene con
						//el caracter de separación - y es necesario que sea /.
						incidencias[i].cad = new Date(incidencias[i].cad.replace(/-/g, "/"));
							
						if(hoy.getTime() >= incidencias[i].cad.getTime() && incidencias[i].fin == null){
							incidencias[i].out_time = true;
						}else{
							incidencias[i].out_time = false;
						}
					}
					$scope.gridIncidencias.data = angular.copy(incidencias);
					$scope.buffer = false;
					incidencias = [];						
				}else{
					console.log('Carga (Incidencias Dashboard): '+data.progressValue+'%.');
					recuperarIncidencias(data.tabla, data.registros[(data.registros.length-1)].id, '&&fin%20IS%20NULL', data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorInciDash = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInciDash = null;}, 5000);
			});
	}
    recuperarIncidencias('incidencias?filter=id%3E', 0, '%26%26fin%20IS%20NULL', 0, 0, 0, []);
	
	//Función que recupera todos las Instalaciones de la BDD de manera recursiva.
	var recuperarInstalaciones = function(tabla, id, acumulados, progressValue, total, registros){
		//Primero traemos las pendientes.
		dataService
			.getAll('pendientes?filter=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				console.log('Total de pendientes de las Instalaciones (DashboardCntrl): ', data.registros.length);
				pendientes = angular.copy(data.registros);
				//Una vez tenemos las pendientes, recuperamos las instalaciones.
				dataService
					.getAll(tabla, id, acumulados, progressValue, total, registros)
					.then(function(data){
						//Comprobamos si se ha terminado la carga o no. En caso negativo, volvemos a llamar a la función.
						if(data.progressValue == 100){
							console.log('Total de registros (Instalaciones Dashboard): '+data.registros.length);
							//Recorremos las instalaciones y las pendientes viendo las instalaciones que hay sin finalizar o con pendientes.
							for(var i=0; i<data.registros.length; i++){
								for(var j=0; j<pendientes.length; j++){
									if(data.registros[i].fin == null || data.registros[i].fin == undefined || data.registros[i].id == pendientes[j].id_instalacion){
										instalaciones.push(data.registros[i]);
										break;
									}
								}
							}
							//instalaciones = angular.copy(data.registros);
							var hoy = new Date();
							hoy = "'"+hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()+" "+hoy.getHours()+"%3A"+hoy.getMinutes()+"%3A00'";
							//Hacemos la llamada a la lista de fase_ins para que nos mande las fases que están fuera de tiempo.
							traerFasesFueraTiempo('fase_ins?filter=id%3E', 0, '%26%26limite%3C'+hoy+'%26%26fin%20IS%20NULL', 0, 0, 0, []);						
						}else{
							console.log('Carga (Instalaciones Dashboard): '+data.progressValue+'%.');
							recuperarInstalaciones(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
						}
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorInsDash = parseError(error.status, error.data);
						$timeout(function(){$scope.errorInsDash = null;}, 5000);
					});
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorInsDash = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInsDash = null;}, 5000);
			});
	}
	recuperarInstalaciones('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae las fases fuera de tiempo.
	var traerFasesFueraTiempo = function(tabla, id, filtro, acumulados, progressValue, total, registros){				
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					fasesOut = angular.copy(data.registros);
					console.log('Total de registros(traerFasesFueraTiempo Dashboard): ', fasesOut.length);
					for(var i=0; i < instalaciones.length; i++){
						for(var j=0; j<fasesOut.length; j++){
							if(instalaciones[i].id == fasesOut[j].id_instalacion){
								instalaciones[i].out_time = true;
								break;
							}else{
								instalaciones[i].out_time = false;
							}
						}
					}
					$scope.gridInstalaciones.data = angular.copy(instalaciones);
					$scope.buffer2 = false;
					instalaciones = [];
				}else{
					console.log('Carga (traerFasesFueraTiempo): '+data.progressValue+'%.');
					traerFasesFueraTiempo(data.tabla, data.registros[(data.registros.length-1)].id, data.filtro, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorInsDash = parseError(error.status, error.data);
				$timeout(function(){$scope.errorInsDash = null;}, 5000);
			});
	}
	
	//Función que traduce de texto a la clase correspondiente (imagen asignada a cada tipo).
	var getTipo = function(tipo){
		switch(tipo){
			case "TECNICA":
				return 'fa fa-wrench';
				break;
			case "IMAGENES":
				return 'fa fa-eye';
				break;
			case "SOPORTE REMOTO":
				return 'icon-earphones-alt';
				break;
			case "SOPORTE IN SITU":
				return 'fa fa-car';
				break;
			case "SIN CONEXION":
				return 'fa fa-chain-broken';
				break;
			case "DOCUMENTACION":
				return 'icon-docs';
				break;
		}
	}
		
	//Función que comprueba si una instalación está
	$scope.comprobarInstalaciones = function(instalacion){
		if(fasesOut){
			for(var i=0; i<fasesOut.length; i++){
				if(fasesOut[i].id_instalacion == instalacion.id){
					return 1;
				}
			}
		}
		if(pendientes){
			for(var j=0; j<pendientes.length; j++){
				if(pendientes[j].id_instalacion == instalacion.id && pendientes[j].estado!='CERRADO'){
					return 1;
				}
			}
		}
		if(instalacion.fin == undefined || instalacion.fin == null){
			return 0;
		}else{
			return 2;
		}
	}
	
	//Función que comprueba si una instalación está
	$scope.comprobarIncidencias = function(incidencia){
		if(incidencia.status=='ABIERTA'){
			if(incidencia.out_time==1){
				return 1;
			}else{
				return 0;
			}
		}else if(incidencia.status=='PENDIENTE'){
			return 0;
		}else if(incidencia.status=='CERRADA'){
			return 2;
		}
	}
}