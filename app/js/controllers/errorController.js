/****************************************************************************/
/*						Controller de los errores de la BDD					*/
/****************************************************************************/
App.controller('ErrorCntrl', ErrorCntrl);
function ErrorCntrl($scope, $timeout) {
    //Inicializamos a null la variable que muestra el error.
    $scope.currentError = null;
    
    //Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ ErrorCntrl ------------------------------');  
       
    //Función que está escuchando para recibir los errores de DreamFactory.
    $scope.$on('error:dreamfactory', function(e, errorMessageObj) {
        $scope.currentError = $scope.parseDreamFactoryError(errorMessageObj);
        $timeout(function(){$scope.currentError = null;}, 15000);
    });
        
    //Función que parsea el mensaje de error.
    $scope.parseDreamFactoryError = function (errorDataObj) {
        console.log('Objeto del error: ', errorDataObj);
        //Variable para almacenar el error.
        var error = null;
        //Si el error es un String, lo pasamos directamente a la variable.
        if (typeof errorDataObj.exception === 'string'){
	        console.log('Error simple');
	        error = errorDataObj.exception;
            //Si el error no es un String, es porque viene del servidor.
            //Y por lo tanto, hay que procesarlo antes de mandarlo a la variable.
        }else{
            //Comprobamos si hay más de un error almacenado dentro del objeto,
            //aunque no sería lo normal.
            if(errorDataObj.exception.data.error.length > 1) {
                angular.forEach(errorDataObj.exception, function(obj) {
                    if(obj.status==400){
	                    error = 'Datos de inicio de sesión incorrectos.';
	                }else{
                    	//Añadimos los mensajes de error a la variable.
						error += obj.data.error.message + '\n';
					}
                });
            }else{
                if(errorDataObj.exception.status==400){
	                error = 'Datos de inicio de sesión incorrectos.';
	            }else{
                    //Si solo tenemos un mensaje de error, lo almacenamos directamente.
					error = errorDataObj.exception.data.error[0].message;
				}
            }
        }
        //Devolvemos el mensaje para mostrarselo al usuario.
        return error;
    };
    //Reseteamos la variable de errores.
    $scope.clearError = function() {
        $scope.currentError = null;
    };
}