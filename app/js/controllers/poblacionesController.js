/****************************************************************************/
/*					Controlador de la vista de las Poblaciones				*/
/****************************************************************************/
App.controller('PoblacionesCntrl', PoblacionesCntrl);
function PoblacionesCntrl($scope, $rootScope, $filter, $resource, $timeout, $location, ngDialog, focus, uiGridConstants, dataService) {	
	$scope.buffer = true;
	$scope.mostrar = false;
	
	var data = [];
	var pasarela = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PoblacionesCntrl ------------------------------');
	
	//Inicializamos la tabla.
	$scope.gridOptionsComplex = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableRowSelection: true,
		selectionRowHeaderWidth: 38,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: [		    
		    {name: 'poblacion', field: 'poblacion', minWidth: 100, sort:{direction: uiGridConstants.ASC, priority: 0}, displayName: 'Población', enableHiding: false, enablePinning: false},
		    {name: 'postal', field: 'postal', cellClass: "hidden-xs", headerCellClass : "hidden-xs", displayName: 'CP', enableHiding: false, enablePinning: false},
		    {name: 'provincia', field: 'provincia', minWidth: 100, enableHiding: false, enablePinning: false},
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false}
	    ],
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridOptionsComplex.paginationPageSize*36)+100) >(($scope.gridOptionsComplex.data.length*36)+100) ){
			return ($scope.gridOptionsComplex.data.length*36)+100;
		}else{
			return ($scope.gridOptionsComplex.paginationPageSize*36)+100;
		}
	}
	
	//Función que copia los registros y les elimina acentos a los campos necesarios.
	var recuperarRegistros = function(){
		if(!$scope.cargando && $scope.cargando != undefined || $rootScope.poblaciones.length!=0){
			for(var i=0; i<$rootScope.poblaciones.length; i++){
				//Filtramos acentos antes de nada.
				$rootScope.poblaciones[i].poblacion = removeAccents($rootScope.poblaciones[i].poblacion);
				$rootScope.poblaciones[i].provincia = removeAccents($rootScope.poblaciones[i].provincia);
				$scope.progressValue = (i*100)/$rootScope.poblaciones.length;
			}
			$scope.gridOptionsComplex.data = angular.copy($rootScope.poblaciones);
			$scope.buffer = false;
			$scope.progressValue = 0;
			$scope.cargado = true;
		}
	}
	recuperarRegistros();
	
	//Función que elimina registros (poblaciones) mediante el id.
	$scope.delPob = function(id){
		dataService
			.deleteElements('poblacion', '?filter=idpoblacion%3D'+id)
			.then(function(data){
				console.log('Población eliminada: ', data.idpoblacion);
				$scope.resetFormPob();
				$scope.buffer = true;
				recuperarRegistros();
			})
			.catch(function(error){
				pasarela--;
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que elimina uno o varios sistemas.
	$scope.remove = function(){
		data = $scope.gridApi.selection.getSelectedRows();
		var url5 = '';
		//Comprobamos si hay o no objetos seleccionados.
	    if(data.length!=0){
	        //Recorro los elementos del array de seleccionados y los mando eliminar.
			for(var i=0; i<data.length; i++){
	            if(i == 0){
		        	url5 = url5+data[i].idpoblacion;
	            }else{
		            url5 = url5+'%7C%7Cidpoblacion%3D'+data[i].idpoblacion;
	            }
			}
			dataService
				.deleteElements('poblacion', '?filter=idpoblacion%3D'+url5)
				.then(function(data){
					console.log('Poblaciones eliminadas: ', data.record);
					$scope.resetFormPob();
					$scope.buffer = true;
					recuperarRegistros();
				})
				.catch(function(error){
					pasarela--;
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			ngDialog.open({
	            template: '<p class="rojo centrado">No has seleccionado ninún elemento para eliminar</p>',
	            plain: true
	        });
		}
	}
	
	/*---------------------------------------- FORMULARIO ----------------------------------------*/
	//Llamada get para recoger las provincias y mostrarlas en el select del formulario.
	var traerProv = function(){
		dataService
			.getFiltro('provincia?idprovincia%3E', 0, '&order=provincia', 0, 0, 0, [])
			.then(function(data){
				$scope.provincias = angular.copy(data.registros);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Parseo del nombre de la población para crear el campo poblacionseo.
	var parsePob = function(poblacion){
		poblacion = removeAccents(poblacion);		
		poblacion = poblacion.toLowerCase();		
		poblacion = poblacion.split(' ').join('-');		
		return poblacion;		
	}
	
	//Abrir el formulario vacío.
	$scope.mostrarForm = function(){
		traerProv();
		$scope.pobla = {};
		$scope.ciudadSel = 'Seleccione una Provincia';
		$scope.boton = 'Añadir Población';
		$scope.mostrar=true;
		$scope.titulo = 'Formulario Añadir';
	}	
	
	//Abrimos el formulario con los datos de la población a editar.
	$scope.editar = function(poblacion){
		traerProv();
		$scope.pobla = angular.copy(poblacion);		
		$scope.ciudadSel = $scope.pobla.provincia;
		$scope.boton = 'Modificar Población';
		$scope.mostrar=true;
		$scope.titulo = 'Formulario Editar';
	}
	
	//Resetear formulario
	$scope.resetFormPob = function(){
		$scope.formPob.$setPristine();
		$scope.pobla = undefined;
		$scope.mostrar=false;
		$scope.select="";
	}
	
	//Función para añadir el registro
	$scope.add = function(){
		//Creamos el campo poblacionseo.
		$scope.pobla.poblacionseo = parsePob($scope.pobla.poblacion);
		
		//Comprobamos si es una edición o una inserción y procedemos según corresponda.
		if($scope.pobla.idpoblacion==undefined){		
			console.log('Población: ', $scope.pobla);
			//Inserción.
			dataService
				.postElements('poblacion', $scope.pobla)
				.then(function(data){
					console.log('Población insertada: ', data);
					$scope.resetFormPob();
					$scope.buffer = true;
					recuperarRegistros();
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}else{
			//Edición.
			dataService
				.putElements('poblacion', $scope.pobla)
				.then(function(data){
					console.log('Población modificada:', data.idpoblacion);
					$scope.resetFormPob();
					$scope.buffer = true;
					recuperarRegistros();
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.currentError = parseError(error.status, error.data);
					$timeout(function(){$scope.currentError = null;}, 15000);
				});
		}
	}
	
	//Funciones para cerrar los paneles de error.
	$scope.clearError = function(){
		$scope.currentError = null;
	}
	$scope.clearErrorFormPob = function(){
		$scope.errorFormPob = null;
	}
	
	
	/*---------------------------------------- Controlador del Select ----------------------------------------*/
	//Función que sustituye la población actual por la seleccionada de la tabla.
	$scope.cambiarProvincia = function(provincia){
		$scope.ciudadSel = provincia.provincia;
		$scope.pobla.idprovincia = provincia.idprovincia;
		$scope.pobla.provincia = provincia.provincia;
		$scope.desplegarUl = !$scope.desplegarUl;
	}
	
	//Desplegamos el Select.
	$scope.desplegar = function(){
		$scope.desplegarUl = !$scope.desplegarUl;
		$scope.filtro='';
		focus('focusMe');
	}
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masProvincias = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogProvincias.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (provincia) {
			$scope.ciudadSel = provincia.provincia;
			$scope.pobla.idprovincia = provincia.idprovincia;
			$scope.pobla.provincia = provincia.provincia;
			$scope.desplegarUl = !$scope.desplegarUl;
	    }, function (reason) {
	    	console.log('Modal promise rejected. Reason: ', reason);
	    });
	}
	
	
	//Función que recoge las llamadas de precarga general.
	$scope.$on('cargando', function(e, data){
		$scope.cargando = data;
		//Cuando terminamos la carga, hacemos la llamada de la función que se encarga de preparar los datos de la tabla.
		recuperarRegistros();
	});
}