/****************************************************************************/
/*							Controlador del Logout							*/
/****************************************************************************/
App.controller('LogoutController', LogoutController);
function LogoutController($scope, $rootScope, $location, UserEventsService, $cookieStore) {
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ LogoutController ------------------------------');
	
	$scope.$on(UserEventsService.logout.logoutSuccess, function(e, userDataObj){
		console.log('LogOut '+$rootScope.user);
		$rootScope.user = userDataObj;
		$scope.$parent.currentUser = userDataObj;
	    $rootScope.loggedInUser = false;
	    $cookieStore.remove('SessionEmail');
		$cookieStore.remove('SessionPassword');
	    $location.path('/page/login');
	});
}