/****************************************************************************/
/*							Controlador de Catálogo							*/
/****************************************************************************/
App.controller('CatalogoCntrl', CatalogoCntrl);
function CatalogoCntrl($scope, $rootScope, $timeout, uiGridConstants, ngDialog, focus, dataService){
	
	$scope.buffer = true;
	$scope.progressValue = 0;
	$scope.mostrar = false;
	$scope.editar = false;
	$scope.errorCatalogo = null;
	$scope.errorFormCatalogo = null;
	  
	var registros = [];
	var total = 0;
	var acumulados = 0;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ CatalogoCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	
	$scope.columnas = [	  
			//Columnas fijas en la tabla  
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'referencia', field: 'referencia', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 140},
		    {name: 'nombre', field: 'nombre', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 150},
			{name: 'descripcion', field: 'descripcion', headerCellClass : "hidden-xs", displayName: 'Descripción', enableSorting: false, minWidth: 150, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, cellClass: 'hidden-xs cellToolTip'},
			{name: 'pvp', field: 'pvp', displayName: 'PVP', enableHiding: false, enableSorting: false, enablePinning:false, minWidth: 60, cellTemplate:'<div class="ui-grid-cell-contents">{{row.entity.pvp}} €</div>'}
	];
	
	//Inicializamos la tabla.
	$scope.gridCatalogo = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};

	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridCatalogo.paginationPageSize*36)+100) > (($scope.gridCatalogo.data.length*36)+100) ){
			return ($scope.gridCatalogo.data.length*36)+100;
		}else{
			return ($scope.gridCatalogo.paginationPageSize*36)+100;
		}
	}
	
	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros(catalogo CatalogoCntrl): ', data.registros.length);
					$scope.gridCatalogo.data = angular.copy(data.registros);
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (catalogo CatalogoCntrl): '+$scope.progressValue+'%.');
					$scope.gridCatalogo.data = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorCatalogo = parseError(error.status, error.data);
				$timeout(function(){$scope.errorCatalogo = null;}, 15000);
			});
	}
	recuperarRegistros('catalogo?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los valores de configuración.
	var traerConfig = function(){
		dataService
			.getAll('configuracion?filtro=id%3E', 0, 0, 0, 0, [])
			.then(function(data){
				for( var i=0; i<data.registros.length; i++){
					if(data.registros[i].parametro == 'costeHoraHombre'){
						$scope.costeHoraHombre = parseFloat(data.registros[i].valor);
					}
					if(data.registros[i].parametro == 'margenVenta'){
						$scope.margenVenta = parseFloat(data.registros[i].valor);
					}
				}
				console.log('Margen de venta (%): '+$scope.margenVenta);
				console.log('Coste de hora por hombre: '+$scope.costeHoraHombre+' €');
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorCatalogo = parseError(error.status, error.data);
				$timeout(function(){$scope.errorCatalogo = null;}, 15000);
			});
	}
	traerConfig();
	
	//Función que inicializa el formulario.
	$scope.mostrarForm = function(){
		$scope.producto = {};
		$scope.producto.referencia = 'P'+crearReferencia();
		$scope.producto.margen = $scope.margenVenta;
		$scope.mostrar = true;
		$scope.titulo = 'Formulario Añadir';
		$scope.boton = 'Añadir';
		focus('mainFocus');
	}
	
	//Función que inicializa el formulario en forma de edición.
	$scope.editar = function(producto){
		$scope.producto = {};
		$scope.producto.margen = producto.margen * 100;
		$scope.producto = angular.copy(producto);
		$scope.mostrar = true;
		$scope.titulo = 'Formulario Editar';
		$scope.boton = 'Modificar';
		focus('mainFocus');
	}
	
	//Función que resetea el formulario.
	$scope.resetformCatalogo = function(){
		$scope.producto = {};
		$scope.mostrar = false;
		$scope.formCatalogo.$setPristine();
	}
	
	//Función para insertar o modificar productos.
	$scope.add = function(){
		//Calculamos el valor del pvp del producto en base a la formula acordada.
		var margen = $scope.producto.margen / 100;
		var dividendo = ($scope.producto.coste + ($scope.producto.horas_hombre * $scope.costeHoraHombre));
		var divisor = 1 - margen;
		$scope.producto.pvp = dividendo / divisor;
		//Redondeamos a 2 cifras
		$scope.producto.pvp = Math.round(parseFloat($scope.producto.pvp) *100)/100;
		
		//Comprobamos si es una inserción o una edición.
		if($scope.producto.id==undefined){
			//Insertar
			dataService
				.postElements('catalogo', $scope.producto)
				.then(function(data){
					console.log('Producto añadido: ', data.id);
					$scope.resetformCatalogo();
					$scope.buffer = true;
					registros = [];
					recuperarRegistros('catalogo?filter=id%3E', 0, 0, 0, 0, []);
					traerConfig();
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormCatalogo = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormCatalogo = null;}, 15000);
				});
		}else{
			//Modificar
			dataService
				.putElements('catalogo', $scope.producto)
				.then(function(data){
					console.log('Producto modificado: ', data.record);
					$scope.resetformCatalogo();
					$scope.buffer = true;
					registros = [];
					recuperarRegistros('catalogo?filter=id%3E', 0, 0, 0, 0, []);
					traerConfig();
				})
				.catch(function(error){
					if(error.status==401){$rootScope.relogin();}
					$scope.errorFormCatalogo = parseError(error.status, error.data);
					$timeout(function(){$scope.errorFormCatalogo = null;}, 15000);
				});
		}
	}
	
	//Función que elimina un producto
	$scope.delProducto = function(producto){
		dataService
			.deleteElements('catalogo', '?filter=id%3D'+producto.id)
			.then(function(data){
				console.log('Producto eliminado: ', data.id);
				$scope.resetformCatalogo();
				$scope.buffer = true;
				registros = [];
				recuperarRegistros('catalogo?filter=id%3E', 0, 0, 0, 0, []);
				traerConfig();
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormCatalogo = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormCatalogo = null;}, 15000);
			});
	}
		
	//Función para cerrar los paneles de error.
	$scope.clearErrorCatalogo = function(){
		$scope.errorCatalogo = null;
	}
	$scope.clearErrorFormCatalogo = function(){
		$scope.errorFormCatalogo = null;
	}
}