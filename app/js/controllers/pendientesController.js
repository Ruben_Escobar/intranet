/****************************************************************************/
/*							Controlador de Pendientes						*/
/****************************************************************************/
App.controller('PendientesCntrl', PendientesCntrl);
function PendientesCntrl($scope, $rootScope, $timeout, uiGridConstants, ngDialog, focus, dataService){
	
	$scope.buffer = true;
	$scope.buffer2 = true;
	$scope.progressValue = 0;
	$scope.mostrar = false;
	$scope.errorPendientes = null;
	$scope.errorFormPend = null;
	
	//Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ PendientesCntrl ------------------------------');
	
	//Variable para mostrar el tooltip de la columna descripción y notas.
	var templateWithTooltip = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.descripcion}} tooltip-placement="top">{{row.entity.descripcion}}</div>';
	var templateWithTooltip2 = '<div class="celdaToolTip ui-grid-cell-contents" tooltip={{row.entity.notas}} tooltip-placement="top">{{row.entity.notas}}</div>';
	
	$scope.columnas = [	  
			//Columnas fijas en la tabla  
		    {name: 'editar', width: 35, cellTemplate:'<div class="buttons"><button type="button" data-ng-click="grid.appScope.editar(row.entity)" title="Edit" class="btn btn-sm btn-info"><em class="fa fa-pencil"></em></button></div>', enableSorting: false, enableFiltering: false, enableHiding: false, pinnedRight: true, enablePinning: false, displayName: '', enableColumnResizing: false, enableColumnMenu: false},
		    
		    {name: 'referencia', field: 'referencia', enableHiding: false, enablePinning:false, minWidth: 150},
			{name: 'descripcion', field: 'descripcion', displayName: 'Descripción', enableSorting: false, minWidth: 90, enableHiding: false, enablePinning:false, cellTemplate: templateWithTooltip, headerCellClass : "hidden-sm hidden-xs", cellClass: 'hidden-sm hidden-xs cellToolTip'},
			{name: 'tipo', field: 'tipo', enableHiding: false, enablePinning:false, minWidth: 135},
			{name: 'instalacion', field: 'instalacion', displayName: 'Instalación', enableHiding: false, enablePinning:false, minWidth: 125},
			{name: 'estado', field: 'estado', enableHiding: false, enablePinning:false, minWidth: 75},
			{name: 'notas', field: 'notas', enableHiding: false, enableSorting: false, enablePinning:false, cellClass: 'hidden-xs cellToolTip', cellTemplate: templateWithTooltip2, minWidth: 90}
	];
	
	//Inicializamos la tabla.
	$scope.gridPendientes = {
	    paginationPageSizes: [10, 25, 50, 75],
		paginationPageSize: 10,
		enableFiltering: true,
		enableSorting: true,
		enableCellEdit: false,
		enableColumnMenus: false,
		enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
		columnDefs: $scope.columnas,
		onRegisterApi: function(gridApi) {
		   $scope.gridApi = gridApi;
	    }
	};

	//Función que recupera todos los registros de la BDD de manera recursiva.
	var recuperarRegistros = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de registros (pendientes PendientesCntrl): ', data.registros.length);
					$scope.gridPendientes.data = angular.copy(data.registros);
					$scope.pendientesFinal = $scope.gridPendientes.data;
					$scope.buffer = false;
					$scope.progressValue = 0;
				}else{
					$scope.progressValue = data.progressValue;
					console.log('Cargando (pendientes PendientesCntrl): '+$scope.progressValue+'%.');
					//Cuando recuperamos los registros, creamos la variable que utilizaremos en el formulario.
					$scope.gridPendientes.data = angular.copy(data.registros);
					$scope.pendientesFinal = $scope.gridPendientes.data;
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPendientes = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPendientes = null;}, 15000);
			});
	}
	recuperarRegistros('pendientesInstalacion?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae el listado de instalaciones para mostrarlas en el formulario.
	var traerInstalaciones = function(tabla, id, acumulados, progressValue, total, registros){
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				if(data.progressValue == 100){
					console.log('Total de Instalaciones (PendientesCntrl): ', data.registros.length);
					$scope.instalacionesFinal = angular.copy(data.registros);
					$scope.buffer2 = false;
				}else{
					//Cuando recuperamos los registros, creamos la variable que utilizaremos en el formulario.
					$scope.instalacionesFinal = angular.copy(data.registros);
					recuperarRegistros(data.tabla, data.registros[(data.registros.length-1)].id, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorPendientes = parseError(error.status, error.data);
				$timeout(function(){$scope.errorPendientes = null;}, 15000);
			});
	}
	traerInstalaciones('instalaciones?filter=id%3E', 0, 0, 0, 0, []);
	
	//Función que trae los tipos de pendiente de la tabla de configuraciones.
	var traerTipoPendiente = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		$scope.tiposPendiente = [];
		dataService
			.getFiltro(tabla, id, filtro, acumulados, progressValue, total, registros)
			.then(function(data){
				var tipos = data.registros[0].valor.split(",");
				var obj = {};
				for(var i=0; i<tipos.length; i++){
					obj = {};
					obj.nombre = tipos[i];
					obj.id = (i+1);
					$scope.tiposPendiente.push(obj);
				}
				if($scope.pend.tipo != ''){
					for(var i=0; i<$scope.tiposPendiente.length; i++){
						if($scope.tiposPendiente[i].nombre == $scope.pend.tipo){
							$scope.pend.tipo = $scope.tiposPendiente[i].id;
							break;
						}
					}
				}
				console.log('Traigo los tipos de pendientes (PendientesCntrl): ', $scope.tiposPendiente);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
	
	//Función que cambia el tamaño de la tabla.
	$scope.comprobarTamano = function(){
		if( (($scope.gridPendientes.paginationPageSize*36)+100) > (($scope.gridPendientes.data.length*36)+100) ){
			return ($scope.gridPendientes.data.length*36)+100;
		}else{
			return ($scope.gridPendientes.paginationPageSize*36)+100;
		}
	}
	
	/*------------------------------ Manejador del Select ------------------------------*/
	//Función que despliega el select.
	$scope.desplegar = function(){
		$scope.desplegarUl = !$scope.desplegarUl;
		if($scope.desplegarUl==true){
			focus('focusMe');
			$scope.filtro = '';
		}else{
			focus('mainFocus');
		}
	}
	
	//Función que sustituye el sistema actual por la seleccionada de la tabla.
	$scope.cambiarInstalacion = function(instalacion){
		$scope.instalacionSel = instalacion.descripcion;
		$scope.pend.id_instalacion = instalacion.id;
		$scope.desplegarUl = !$scope.desplegarUl;
		$scope.formPendientes.instalacion.$dirty = false;
		focus('mainFocus');
	}	
	
	//Abrimos el diálogo con la tabla completa de poblaciones, dando la posibilidad de filtrar también.
	$scope.masInstalaciones = function(){
		ngDialog.openConfirm({
			template: '/app/views/partials/ngDialogInstalaciones.html',
			className: 'ngdialog-theme-default',
			scope: $scope
		}).then(function (instalacion) {
			$scope.instalacionSel = instalacion.descripcion;
			$scope.pend.id_instalacion = instalacion.id;
			$scope.desplegarUl = !$scope.desplegarUl;
			focus('mainFocus');
	    }, function (reason) {
		    $scope.filtro='';
	    });
	}
	/*------------------------------ Fin del Select ------------------------------*/
	
	//Función que inicializa el formulario.
	$scope.newPendiente = function(){
		$scope.pend = {};
		$scope.pend.estado = 'ABIERTO';
		$scope.instalacionSel = 'Seleccione una Instalación';
		$scope.mostrar = true;
		$scope.boton = 'Añadir';
		$scope.titulo = 'Formulario Añadir';
		focus('mainFocus');
		traerTipoPendiente('configuracion?filter=id%3E', 0, "%26%26parametro='tipoPendiente'", 0, 0, 0, []);
	}
	
	//Función que resetea el formulario.
	$scope.resetformPend = function(){
		$scope.pend = {};
		$scope.instalacionSel = 'Seleccione una Instalación';
		$scope.mostrar = false;
		$scope.formPendientes.$setPristine();
		$scope.desplegarUl = false;
	}
	
	//Función que inicializa el formulario en forma de edición.
	$scope.editar = function(pendiente){
		$scope.pend = angular.copy(pendiente);
		$scope.instalacionSel = $scope.pend.instalacion;
		$scope.mostrar = true;
		$scope.boton = 'Modificar';
		$scope.titulo = 'Formulario Editar';
		focus('mainFocus');
		traerTipoPendiente('configuracion?filter=id%3E', 0, "%26%26parametro='tipoPendiente'", 0, 0, 0, []);
	}
		
	//Función para insertar o midificar un pendiente.
	$scope.add = function(){
		for(var i=0; i<$scope.tiposPendiente.length; i++){
			if($scope.tiposPendiente[i].id == $scope.pend.tipo){
				$scope.pend.tipo = $scope.tiposPendiente[i].nombre;
				break;
			}
		}
		//Comprobamos que se ha seleccionado una instalación.
		if($scope.pend.id_instalacion == undefined){
			$scope.formPendientes.instalacion.$dirty = true;
		}else{
			//Comprobamos si es una inserción o una edición.
			if($scope.pend.id==undefined){
				$scope.pend.referencia = 'I'+$scope.pend.id_instalacion+'P'+crearReferencia();
				//Insertar
				dataService
					.postElements('pendientes', $scope.pend)
					.then(function(data){
						console.log('Pendiente insertada: ', data.id);
						$scope.resetformPend();
						$scope.buffer = true;
						recuperarRegistros('pendientesInstalacion?filter=id%3E', 0, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormPend = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormPend = null;}, 15000);
					});
			}else{
				//Modificar
				dataService
					.putElements('pendientes', $scope.pend)
					.then(function(data){
						console.log('Pendiente modificada: ', data.id);
						$scope.resetformPend();
						$scope.buffer = true;
						recuperarRegistros('pendientesInstalacion?filter=id%3E', 0, 0, 0, 0, []);
					})
					.catch(function(error){
						if(error.status==401){$rootScope.relogin();}
						$scope.errorFormPend = parseError(error.status, error.data);
						$timeout(function(){$scope.errorFormPend = null;}, 15000);
					});
			}
		}
	}
	
	//Función que elimina un pendiente.
	$scope.delPend = function(pendiente){
		dataService
			.deleteElements('pendientes', '?filter=id%3D'+pendiente.id)
			.then(function(data){
				console.log('Pendiente eliminada: ', data.record[0].id);
				$scope.resetformPend();
				$scope.buffer = true;
				recuperarRegistros('pendientesInstalacion?filter=id%3E', 0, 0, 0, 0, []);
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.errorFormPend = parseError(error.status, error.data);
				$timeout(function(){$scope.errorFormPend = null;}, 15000);
			});
	}
		
	//Función para cerrar los paneles de error.
	$scope.clearErrorPendientes = function(){
		$scope.errorPendientes = null;
	}
	$scope.clearErrorFormPend = function(){
		$scope.errorFormPend = null;
	}
}