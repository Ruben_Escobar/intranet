/****************************************************************************/
/*							Controlador del index							*/
/****************************************************************************/
App.controller('IndexController', IndexController);
function IndexController($scope, $rootScope, $cookieStore, $location, $timeout, UserDataService, dataService){
	
    $rootScope.user = UserDataService.getCurrentUser();
    $rootScope.poblaciones = [];
	
    var registrosPob = [];
    var total = 0;
    var progressValue = 0;
    var acumulados = 0;
    
    //Imprimimos en consola una linea con el nombre del controlador al que accedemos, de esta manera será más legible el log.
	console.log('------------------------------ IndexController ------------------------------');
	
    //Función que hace la carga inicial de poblaciones de la aplicación.
	var traerPob = function(tabla, id, acumulados, progressValue, total, registros){
		$rootScope.$broadcast('cargando', true);
		dataService
			.getAll(tabla, id, acumulados, progressValue, total, registros)
			.then(function(data){
				//Comprobamos si se ha terminado la carga o no. En caso negativo, volvemos a llamar a la función.
				if(data.progressValue == 100){
					console.log('Carga inicial de Poblaciones Finalizada: '+data.registros.length+' registros.');
					$rootScope.poblaciones = data.registros;
					$rootScope.$broadcast('cargando', false);	
				}else{
					console.log('Carga inicial de Poblaciones: '+data.progressValue+'%.');
					traerPob(data.tabla, data.registros[(data.registros.length-1)].idpoblacion, data.acumulados, data.progressValue, data.total, data.registros);
				}
			})
			.catch(function(error){
				if(error.status==401){$rootScope.relogin();}
				$scope.currentError = parseError(error.status, error.data);
				$timeout(function(){$scope.currentError = null;}, 15000);
			});
	}
    traerPob('poblaciones?filter=idpoblacion>', 0, 0, 0, 0, []);
    
    //Funcion en el rootScope para reloguear en caso de fallo de sesion (Siempre que las cookies sigan activas).		    
	$rootScope.relogin = function(){		
		//Si hay cookies, realizamos el relogueo
		if($cookieStore.get('SessionPassword')){
			$scope.cargando = false;
			
			$rootScope.user = {
				"email": $cookieStore.get('SessionEmail'),
				"password": $cookieStore.get('SessionPassword')
			};
			
			$location.path("/page/relogin");
		}else{
			$location.path("/page/login");
		}
    }
}