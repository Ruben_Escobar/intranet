/********************************************************************************************************************************************************/
/*													Servicios y directivas propias de la aplicación														*/
/********************************************************************************************************************************************************/

//Servicio para hacer el va y ven entre Sistemas e instalaciones
App.factory("SerInst", function() {
	return {
		data: {}
	};
});

//Servicio para hacer el va y ven entre Sistemas e incidencias
App.factory("SerInci", function() {
	return {
		data: {}
	};
});

//Servicios para comprobar los cambios en los formularios.
App.factory("SerInstalaciones", function(){
	return {
		data: {}
	};
})
App.factory("SerSistemas", function(){
	return {
		data: {}
	};
})
App.factory("SerContratos", function(){
	return {
		data: {}
	};
})
App.factory("SerContactos", function(){
	return {
		data: {}
	};
})
App.factory("SerIncidencias", function(){
	return {
		data: {}
	};
})

//Servicio para imprimir los presupuestos.
App.factory('printer', ['$rootScope', '$compile', '$http', '$timeout','$q', function ($rootScope, $compile, $http, $timeout, $q) {
        
    var doc;
    
    //Función para imprimir la vista de los presupuestos
    var printHtml = function (html, presupuesto) {
            
        var deferred = $q.defer();
        var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
            
        hiddenFrame.contentWindow.printAndRemove = function() {
            hiddenFrame.contentWindow.print();
            $(hiddenFrame).remove();
        };
            
        //Hacemos que se carguen los datos cuando está completamente cargado el documento.
        $(hiddenFrame).load(function(){
			if (!hiddenFrame.contentDocument.execCommand('print', false, null)){
				hiddenFrame.contentWindow.focus();
				hiddenFrame.contentWindow.print();
			} 
			$(hiddenFrame).remove();
		});
            
        var htmlContent="<html>"+
                        	'<body>'+
                            html+
							'</body>'+
                        "</html>";
                        
        doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
        doc.write(htmlContent);
            
        /*Para importar todos los css que ya tiene la aplicación (En este caso no sirve).
	        $('link[rel="stylesheet"]').each(function (i, ele) {
			var cssLink = doc.createElement('link');
			cssLink.href = $(this).attr('href');
        */
		var cssLink = doc.createElement('link');
		cssLink.href = 'app/css/print.css';
		cssLink.rel = 'stylesheet';
		cssLink.type = 'text/css';
		doc.getElementsByTagName("head")[0].appendChild(cssLink);
				
        /*-------------------- Controlamos los tamaños de la vista --------------------*/
        var presupuesto = angular.copy($rootScope.presupuesto);
	        
	    //Cambio:    "context.childNodes[1].childNodes[2].childNodes[39].contentDocument.all".
	    //Creamos la variable que recoge el tamaño de la tabla.
		var el = angular.element($("#totalBody")).context.childNodes[1].childNodes[3].childNodes[57].contentDocument.all;
			
		for(var i=0; i<el.length; i++){
			
			if(el[i].id == "divFooter"){
				var footer = el[i].offsetTop;
				console.log('Footer - H: ', el[i].offsetTop);
			}
				
			if(el[i].id == "divTabla"){
				var tamano = el[i].offsetHeight;
				console.log('Tamaño - H: ', el[i].offsetHeight);
            }
        }
			
		var numero = 0;
		var resto = 0;
		var tamFinal = 0;
		
		if(footer < 0){
			//Comprobamos si hay descuento o no para variar los tamaños de los patrones de la vista.
			if(presupuesto.mostrarObservaciones){
				if(tamano > 405){
	                tamFinal = tamFinal +100;
                    numero = parseInt(tamano / 1465) ;	//Parte entera de la división.
                    resto = tamano % 405;				//Resto de la división.
                    //Primero calculamos el número de partes enteras que hay y sumamos al tamaño.
                    for(var i=0; i<numero; i++){
                        tamFinal = tamFinal +100;
                    }
                    //En caso de que haya resto significa que la tabla ocupa algo más que una parte entera, por lo tanto hay que sumar.
                    if(resto != 0){
		                tamFinal = tamFinal +100;
                    }
                    doc.body.style.height = tamFinal+"%";	
                }
			}else{
                if(tamano > 545){
	                tamFinal = tamFinal +100;
                    numero = parseInt(tamano / 1550) ;	//Parte entera de la división.
                    resto = tamano % 545;				//Resto de la división.
                    //Primero calculamos el número de partes enteras que hay y sumamos al tamaño.
                    for(var i=0; i<numero; i++){
                        tamFinal = tamFinal +100;
                    }
                    //En caso de que haya resto significa que la tabla ocupa algo más que una parte entera, por lo tanto hay que sumar.
                    if(resto != 0){
		                tamFinal = tamFinal +100;
                    }
                    doc.body.style.height = tamFinal+"%";	
                }
            }
        }else{
            if(presupuesto.mostrarObservaciones){
	            if(tamano > 655){
	                tamFinal = tamFinal +100;
                    numero = parseInt(tamano / 2165) ;	//Parte entera de la división.
                    resto = tamano % 655;				//Resto de la división.
                    //Primero calculamos el número de partes enteras que hay y sumamos al tamaño.
                    for(var i=0; i<numero; i++){
                        tamFinal = tamFinal +100;
                    }
                    //En caso de que haya resto significa que la tabla ocupa algo más que una parte entera, por lo tanto hay que sumar.
                    if(resto != 0){
                        tamFinal = tamFinal +100;
                    }
                    doc.body.style.height = tamFinal+"%";	
                }
	        }else{
                if(tamano > 820){
	                tamFinal = tamFinal +100;
                    numero = parseInt(tamano / 2260) ;	//Parte entera de la división.
                    resto = tamano % 820;				//Resto de la división.
                    //Primero calculamos el número de partes enteras que hay y sumamos al tamaño.
                    for(var i=0; i<numero; i++){
                        tamFinal = tamFinal +100;
                    }
                    //En caso de que haya resto significa que la tabla ocupa algo más que una parte entera, por lo tanto hay que sumar.
                    if(resto != 0){
                        tamFinal = tamFinal +100;
                    }
                    doc.body.style.height = tamFinal+"%";	
                }
            }
        }
		console.log('Tamaño del cuerpo a corde con el tamaño de la tabla: ', doc.body.style.height);
		$rootScope.presupuesto = null;			
        deferred.resolve();
        doc.close();
        return deferred.promise;
    };
    
    //Función para impresión estandar.
    var printHtml2 = function (html) {
        var deferred = $q.defer();
        var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
        hiddenFrame.contentWindow.printAndRemove = function() {
            hiddenFrame.contentWindow.print();
            $(hiddenFrame).remove();
        };
        var htmlContent = "<!doctype html>"+
						  "<html>"+
            	          '<body onload="printAndRemove();">' +
                	      html +
						  '</body>'+
						  "</html>";
        var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
        doc.write(htmlContent);
        deferred.resolve();
        doc.close();
        return deferred.promise;
    };
    
    var openNewWindow = function (html) {
        var newWindow = window.open("impresionPresupuesto.html");
        newWindow.addEventListener('load', function(){ 
            $(newWindow.document.body).html(html);
        }, false);
    };

    var print = function (templateUrl, data) {         
        $http.get(templateUrl).success(function(template){
            var printScope = $rootScope.$new()
            angular.extend(printScope, data);
            var element = $compile($('<div>' + template + '</div>'))(printScope);
            var waitForRenderAndPrint = function() {
                if(printScope.$$phase || $http.pendingRequests.length) {
                    $timeout(waitForRenderAndPrint);
                }else{
                    printHtml(element.html(), data.presupuesto);
					//openNewWindow(element.html()); Para abrirlo en una página nueva en ved de en el gestor de impresiones.
                    printScope.$destroy();
                }
            };
            waitForRenderAndPrint();
        });
    };

    var printFromScope = function (templateUrl, scope) {
        $rootScope.isBeingPrinted = true;
        $http.get(templateUrl).success(function(template){
            var printScope = scope;
            var element = $compile($('<div>' + template + '</div>'))(printScope);
            var waitForRenderAndPrint = function() {
                if (printScope.$$phase || $http.pendingRequests.length) {
                    $timeout(waitForRenderAndPrint);
                }else{
                    printHtml2(element.html()).then(function() {
                		$rootScope.isBeingPrinted = false;
                    });

                }
            };
			waitForRenderAndPrint();
        });
    };
        
    return {
    	print: print,
        printFromScope: printFromScope
    }
}]);

//Servicio para mandar e-mails.
App.factory('emailSender', ['$rootScope', '$compile', '$http', '$timeout','$q', function ($rootScope, $compile, $http, $timeout, $q) {
	    
	var send = function (from, to, asunto, templateUrl, scope) {
        var deferred = $q.defer();
		var promise = deferred.promise;
        
        $http.get(templateUrl).success(function(template){
            var emailScope = scope;
            var element = $compile($('<div>' + template + '</div>'))(emailScope);
            var waitForRenderAndEmail = function() {
                if (emailScope.$$phase || $http.pendingRequests.length) {
                    $timeout(waitForRenderAndEmail);
                }else{
					var obj = {
						"to": [{
							"name": to.name,
							"email": to.email
						}],
						"subject": asunto,
						"body_html": element.html(),
						"from_name": from.name,
						"from_email": from.email
					}					
					$http
						.post('http://dreamfactory.df-altatec.bitnamiapp.com/rest/email', obj)
						.success(function(response){
							deferred.resolve(true);
						})
						.error(function(data, status){
							deferred.reject({
								"status": status,
								"data": data
							});
						});
				}
            };
            waitForRenderAndEmail();
        });
    };
        
    return {
    	send: send
    }
}]);

//Servicio de comunicación con la BBDD.
App.factory('dataService', ['$http', '$q', function($http, $q){
	
	var url = 'http://dreamfactory.df-altatec.bitnamiapp.com/rest/absql/';
		
	//Función que retorna el total de los elementos de la tabla.
	var getAll = function(tabla, id, acumulados, progressValue, total, registros){
		
		var deferred = $q.defer();
        var promise = deferred.promise;
        
		$http
			.get(url+tabla+id)						//Para probar que funciona el cargar más de 1000 registros -> +'&limit=100&include_count=true'
			.success(function(response){
				registros = registros.concat(response.record);
				
				if(response.meta){
					if(total==0){total=response.meta.count;}
					acumulados = registros.length;
					progressValue = (((acumulados*100)/total).toFixed());
					
					//En cada respuesta devolvemos el progreso y los registros acumulados. De manera que si 'progressValue' no es 100, estaremos en
					//periodo de carga. Si es así, realizaremos una llamada recursiva sobre la función.
					deferred.resolve({
						"tabla": tabla,
						"acumulados": acumulados,
						"progressValue": progressValue,
						"total": total,
						"registros": registros
					});
				}else{
					progressValue = 100;
					deferred.resolve({
						"tabla": tabla,
						"acumulados": acumulados,
						"progressValue": progressValue,
						"total": total,
						"registros": registros
					});
				}
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});		
		return promise;
	}
	
	//Función que retorna unos registros concretos mediante un filtro.
	var getFiltro = function(tabla, id, filtro, acumulados, progressValue, total, registros){
		
		var deferred = $q.defer();
        var promise = deferred.promise;
        
		$http
			.get(url+tabla+id+filtro)
			.success(function(response){
				registros = registros.concat(response.record);
				
				if(response.meta){
					if(total==0){total=response.meta.count;}
					acumulados = registros.length;
					progressValue = (((acumulados*100)/total).toFixed());
					
					//En cada respuesta devolvemos el progreso y los registros acumulados. De manera que si 'progressValue' no es 100, estaremos en
					//periodo de carga. Si es así, realizaremos una llamada recursiva sobre la función.
					deferred.resolve({
						"tabla": tabla,
						"filtro": filtro,
						"acumulados": acumulados,
						"progressValue": progressValue,
						"total": total,
						"registros": registros
					});
				}else{
					progressValue = 100;
					deferred.resolve({
						"tabla": tabla,
						"filtro": filtro,
						"acumulados": acumulados,
						"progressValue": progressValue,
						"total": total,
						"registros": registros
					});
				}
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});		
		return promise;
	}
	
	//Función que inserta uno o varios elementos en la BBDD.
	var postElements = function(tabla, elementos){
		var deferred = $q.defer();
        var promise = deferred.promise;
        
		$http
			.post(url+tabla, elementos)
			.success(function(response){
				deferred.resolve(response);
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});		
		return promise;
	}
	
	//Funciónq que modifica uno o varios elementos de la BBDD.
	var putElements = function(tabla, elementos){
		var deferred = $q.defer();
        var promise = deferred.promise;
        
		$http
			.put(url+tabla, elementos)
			.success(function(response){
				deferred.resolve(response);
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});		
		return promise;
	}
	
	//Función que elimina uno o varios elementos de la BBDD.
	var deleteElements = function(tabla, filtro){
		var deferred = $q.defer();
        var promise = deferred.promise;
        
		$http
			.delete(url+tabla+filtro)
			.success(function(response){
				deferred.resolve(response);
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});		
		return promise;
	}
	
	return{
		getAll: getAll,
		getFiltro: getFiltro,
		postElements: postElements,
		putElements: putElements,
		deleteElements: deleteElements
	}
}]);

//Servicio de manejo de usuarios de la aplicación.
App.factory('userService', ['$http', '$q', function($http, $q){
	
	var url = 'http://dreamfactory.df-altatec.bitnamiapp.com/rest/user/';
	
	var registerUser = function(tabla, user){
		var deferred = $q.defer();
        var promise = deferred.promise;
        
        $http
		    .post(url+tabla, user)
		    .success(function(response) {
		    	deferred.resolve(response);
		    })
		    .error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});		
		return promise;
	}
	
	var resetPass = function(tabla, datos){
		var deferred = $q.defer();
        var promise = deferred.promise;
        
        $http
        	.post(url, $scope.recover)
			.success(function(response){
				deferred.resolve(response);
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});			
        return promise;
	}
	
	return{
		registerUser: registerUser,
		resetPass: resetPass
	}
	
}]);

//Servicio de comunicación con la BBDD.
App.factory('uploadService', ['$http', '$q', 'Upload', function($http, $q, Upload){
	
	var urlUpload = 'http://dreamfactory.df-altatec.bitnamiapp.com/rest/files/intranet/';
		
	//Función que sube archivos al servidor de DreamFactory.
	var subirArchivos = function(destino, ficheros){
		
		var deferred = $q.defer();
        var promise = deferred.promise;
        
		$http            
			.post(urlUpload+destino, ficheros)
			.success(function(response){
				deferred.resolve(response);
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});		
		/*Esta parte sirve para subir los archivos al servidor que digamos.
        if (files && files.length) {
            Upload.upload({
                url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                data: {
                    files: files
                }
            }).then(function (response) {
                $timeout(function () {
                    $scope.result = response.data;
                });
            }, function (response) {
                if (response.status > 0) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }
            }, function (evt) {
                $scope.progress = 
                    Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });*/		
		return promise;
	}
	
	//Función que elimina archivos del servidor de DreamFactory.
	var eliminarArchivos = function(direccion){
		
		var deferred = $q.defer();
        var promise = deferred.promise;
        
		$http            
			.delete(urlUpload+direccion)
			.success(function(response){
				deferred.resolve(response);
			})
			.error(function(data, status){
				deferred.reject({
					"status": status,
					"data": data
				});
			});				
		return promise;
	}
	
	return{
		subirArchivos: subirArchivos,
		eliminarArchivos: eliminarArchivos
	}
}]);