/********************************************************************************************************************************************************/
/*					  										Funciones propias de la aplicación															*/
/********************************************************************************************************************************************************/
  
//Filtro para eliminar los acentos.
var removeAccents = function(source) {
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, 			// N, n
        /[\307]/g, /[\347]/g 			// C, c
    ],
    noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
		
    for (var i = 0; i < accent.length; i++){
        source = source.replace(accent[i], noaccent[i]);
    }
    return source;
}

//Función que se encarga de capturar el error y "traducirlo".
var parseError = function(status, data){
	switch(status) {
	    case 400:
	        return 'Error Dreamfactory: Parámetros incorrectos para realizar la petición (faltan parámetros o son erroneos).';
	        break;
	        
	    case 401:
	        return 'Error Dreamfactory: Autorización denegada, no hay una sesión válida (Loguee de nuevo).';
	        break;
	        
	    case 403:
		    return 'EL USUARIO NO TIENE PERMISOS PARA ESTA OPERACIÓN: '+data.error[0].message+'.';
		    break;
	        
	    case 404:
	        return 'Error Dreamfactory: El recurso solicitado no se encuentra.';
	        break;
	        
	    case 500:
	        return 'Error Dreamfactory: Error interno del sistema: '+data.error[0].message;
	        break;
	    default:
	        return 'Error Dreamfactory inesperado: '+data.error[0].message+'.';
	        break;
	}
}

//Función que pasa una cadena a formato sin acentos, ni mayústulas ni espacios.
var parser = function(cadena){
	cadena = removeAccents(cadena);		
	cadena = cadena.toLowerCase();		
	cadena = cadena.split(' ').join('-');		
	return cadena;		
}

//Función que recibe una fecha por referencia y devuelve una fecha con el formato YYYY-MM-dd.
var parseFecha = function(fecha){
	var aux = new Date(fecha.getFullYear()+'-'+twoDigits(fecha.getMonth()+1)+'-'+twoDigits(fecha.getDate()));
	return aux;
}

//Función que recibe una fecha por referencia y devuelve una fecha con el formato YYYY-MM-dd HH+2:mm:ss.
var parseFechaHora = function(fecha){
	var aux = ""+fecha.getFullYear()+'-'+twoDigits(fecha.getMonth()+1)+'-'+twoDigits(fecha.getDate())+' '
						+twoDigits(fecha.getHours())+':'+twoDigits(fecha.getMinutes())+":00";
	return aux;
}

//Función que recibe una fecha por referencia y devuelve una fecha con el formato YYYY-MM-dd HH:mm:ss.
var getFechaHora = function(fecha){
	var aux = new Date(""+fecha.getFullYear()+'-'+twoDigits(fecha.getMonth()+1)+'-'+twoDigits(fecha.getDate())+' '
						+twoDigits(fecha.getHours())+':'+twoDigits(fecha.getMinutes())+':'+twoDigits(fecha.getSeconds())+"");
	return aux;
}

//Función que recibe un número y lo devuelve pasándolo a dos cifras, ya sea menor o mayor que 10.
var twoDigits = function(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

//Función para generar referencias.
var crearReferencia = function(){
	var hoy = new Date();
	
	var srt = ""+hoy.getFullYear()+twoDigits((hoy.getMonth()+1))+twoDigits(hoy.getDate())+''
				+twoDigits(hoy.getHours())+twoDigits(hoy.getMinutes())+twoDigits(hoy.getSeconds());
	var nums = srt.split("");
	var crc = 0;
	for(var i=0; i<nums.length; i++){
		crc += parseInt(nums[i]);
	}
	return srt+twoDigits(crc);
}

//Función que comprueba que la referencia es correcta.
var comprobarCrc = function(ref){
	var car = ref.split("");
	var suma = 0;
	//Cogemos los dos últimos valores del array y los concatenamos para sacar el valor del CRC.
	var crc = parseInt(car[(car.length)-2].concat(car[car.length-1]));
	//Recorremos el array de valores para ir sumándolos. Saltamos el primero (La inicial carácterística) y los dos últimos, valor del crc
	for(var i=(car.length-16);i<(car.length-2);i++){
		suma += parseInt(car[i]);
	}
	if(suma == crc){
		console.log('CRC correcto: ', ref);
		return true;
	}else{
		console.log('CRC incorrecto: ', ref);
		return false;
	}
}